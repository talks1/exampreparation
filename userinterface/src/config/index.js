
let location = window.location.origin

//let port = process.env.REACT_APP_BACKEND_PORT || 3000

export function getAPIDomain() {
    if(location.indexOf('localhost')>=0)
        return "http://localhost:7022"
    else
        return "https://fnap-scenario-funapp-dev.azurewebsites.net"
}

// export function getSocketUrl() {
//     return getAPIDomain().replace(':3000',`:${port}`)
// }

export function getAPI() {
    let domain = getAPIDomain()
    return `${domain}/api`
}

export function getMSALConfiguration() {    
    if(location.indexOf('localhost')>=0)
        return {
            //Here is the app registration https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/Overview/appId/ab8c7f24-a30e-4ef4-bf82-713d248f96f9/isMSAApp/
            clientId: "ab8c7f24-a30e-4ef4-bf82-713d248f96f9",
            tenantId:  '951b1ed2-d31c-4c2a-9dd6-8ea6137ceb9d',
            scopes:["api://ab8c7f24-a30e-4ef4-bf82-713d248f96f9/simulation"],
            redirectUri: "https://localhost:3002"
        }
        else if(location.indexOf('scenario-dev.azureedge.net')>=0){
            return {
                clientId: "ab8c7f24-a30e-4ef4-bf82-713d248f96f9",
                tenantId:  '951b1ed2-d31c-4c2a-9dd6-8ea6137ceb9d',
                scopes:["api://ab8c7f24-a30e-4ef4-bf82-713d248f96f9/simulation"],
                redirectUri: "https://scenario-dev.azureedge.net"            
            }
        }
}
