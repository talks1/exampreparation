import React from 'react'
import {TokenComponent} from '../components/TokenComponent'
import style from '../styles/style'

export const Menu = (props: any) => {
  return <div style={style.menu}>Menu|
    <a href="#/">Home</a>|
    <a href="#/exam">Exam</a>|
    <a href="#/statistics">Statistics</a>|
  </div>
}

export const Layout = (props: any) => {
  return <div style={style.layoutContainer}>
    <TokenComponent/>  
      <div style={style.header}>
        Exam Questions        
      </div>
      <Menu/>   
      {props.children}
      <div style={style.footer}>

      </div>
    </div>
}
