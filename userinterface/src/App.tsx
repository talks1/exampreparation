import React, { Suspense } from 'react';
import { HashRouter, Switch, Route } from "react-router-dom"
import { Layout } from './layout'
import Home from './pages/Home.page'
import Exam from './pages/Exam.page'
import Statistics from './pages/Statistics.page'
import { Loader } from './components'
import style from './styles/Page.scss';


function App() {
  return <Suspense fallback={<Loader />}>
          <HashRouter hashType='slash'>
            <Layout>
              <Switch>
                <Route path="/statistics" render={() => <Statistics  />} />
                <Route path="/question/:id" render={() => <Exam  />} />
                <Route path="/exam" render={() => <Exam  />} />
                <Route path="/" exact render={() => <Home  />} />
              </Switch>
              </Layout>
            </HashRouter>
          </Suspense>
}

export default App;