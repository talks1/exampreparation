import React, { FC, useEffect, useState } from 'react';
import { queryStatistics, queryQuestions  } from '../contract/api';
import {IQuestion} from '../../../application/src/types'
import {ExamQuestions} from '../components/ExamQuestions.component'
import {useParams} from "react-router-dom";
import style from '../styles/style'

interface IPageProps {    
}

const Exam: FC<IPageProps> = (props: IPageProps) => {
    
    const [questions,setQuestions] = useState<IQuestion[]>([])

    const params:any = useParams()

    useEffect(()=>{        
        async function getData(){
            let s = await queryStatistics()
            let qs = await queryQuestions()
            let enrichedQs = await qs.map((q:IQuestion)=>{
                const qStat = s.questionStats[q.id]
                const questionDifficulty = qStat ? (qStat.answeredInCorrectly/qStat.answered) : 1            
                const answeredInCorrectly = (qStat ? qStat.answeredInCorrectly : 0)
                const answeredCorrectly = (qStat ? qStat.answeredCorrectly :0)
                return {
                    ...q,
                    questionDifficulty,
                    answeredInCorrectly,
                    answeredCorrectly
                }
            })
            .sort((q1:any,q2:any)=>{
                return (-q1.questionDifficulty) - (-q2.questionDifficulty)
            })
            
            for(var i=0;i<enrichedQs.length;i++){
                if(i%2===0){
                    const another = Math.floor(Math.random()*enrichedQs.length/2)*2
                    if(i!==another){                        
                        const temp = enrichedQs[i]
                        enrichedQs[i] = enrichedQs[another]
                        enrichedQs[another]=temp
                    }
                }
            }
            setQuestions(enrichedQs)
        }
        getData()
    },[])
    
    let filteredQuestions = questions
    if(params.id){
        filteredQuestions = questions.filter((q:IQuestion)=>{
            
            return q.id===params.id
        })
    }
    

    return <div style={style.pageContainer}>
    <div style={style.pageTitle}>Exam</div>
    <div style={style.pageContent}>         
            {/* Filtered Questions: {filteredQuestions.length} */}
            {filteredQuestions.length>0 && <ExamQuestions questions={filteredQuestions} />   }
            
        </div>
    </div>
}

export default Exam