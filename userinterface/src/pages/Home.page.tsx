import React, { FC, useEffect, useState } from 'react';
import { queryQuestions  } from '../contract/api';
import {IQuestion} from '../../../application/src/types'
import style from '../styles/style'
import Button from '@mui/material/Button'
import StartIcon from '@mui/icons-material/StarTwoTone'
import Grid from '@mui/material/Grid'
import {TOKEN,DOMAIN} from '../../../application/src/domain'

interface IPageProps {    
}

const Home: FC<IPageProps> = (props: IPageProps) => {

    const domainStored = window.localStorage.getItem(DOMAIN)
    const [domain,domainSet] = useState<string>(domainStored || 'CCSP')

    const [questions,setQuestions] = useState<IQuestion[]>([])


    const handleDomain = (newdomain:string) => () =>{
        window.localStorage.setItem(DOMAIN,newdomain)
        domainSet(newdomain)
    }

    useEffect(()=>{
        async function getData(){
            let qs = await queryQuestions()
            qs = qs.sort(()=>Math.random()>0.5 ? 1 : -1)
            setQuestions(qs)
        }
        getData()
    },[domain])

    return <div style={style.pageContainer}>
            <div style={style.pageTitle}>Home</div>
            <div style={style.pageContent}>

            <Grid container spacing={3}>
                <Grid item xs={12}>
                    Questions: {questions.length}    
                </Grid>
                <Grid item xs={12}>
                <Button
                    variant={domain==='CCSP' ? 'contained' : 'outlined'}
                    color='primary'
                    
                    endIcon={<StartIcon />}            
                    onClick={handleDomain('CCSP')}
                >CCSP</Button>
                <Button
                        variant={domain==='CISSP' ? 'contained' : 'outlined'}
                        color='primary'
                        
                        endIcon={<StartIcon />}            
                        onClick={handleDomain('CISSP')}
                    >CIISP</Button>
                </Grid>
            </Grid>
            </div>
    </div>
}

export default Home