import React, { FC, useEffect, useState , useMemo, Fragment} from 'react';
import { queryStatistics, queryQuestions  } from '../contract/api';
import {IQuestion} from '../../../application/src/types'
import { Chart } from 'react-charts'
import style from '../styles/style'

interface IPageProps {
    
}

const Statistics: FC<IPageProps> = (props: IPageProps) => {
    
    const [statistics,setStatistics] = useState<any>({})
    const [questions,setQuestions] = useState<IQuestion[]>([])

    const series = React.useMemo(
    () => ({
        type: 'bar'
    }),
    []
    )

    const axes = React.useMemo(
    () => [
        { primary: true, type: 'ordinal', position: 'left' },
        { position: 'bottom', type: 'linear', stacked: true ,showTicks: false, tickSizeOuter: 3 }
    ],
    []
    )

    const tooltip = React.useMemo(
    () => ({
        render: (props:any) => {
            const { datum } = props
        return datum ? <div>{datum.originalSeries.label} {datum.originalDatum.secondary-1}</div> : null
        }
    }),
    []
    )

    useEffect(()=>{        
        async function getData(){
            let s = await queryStatistics()
            setStatistics(s)

            let qs = await queryQuestions()
            let enrichedQs = await qs.map((q:IQuestion)=>{
                const qStat = s.questionStats[q.id]
                const questionDifficulty = qStat ? (Math.floor(100*(qStat.answeredInCorrectly/qStat.answered))) : 100            
                const answeredInCorrectly = (qStat ? qStat.answeredInCorrectly : 0)+1
                const answeredCorrectly = (qStat ? qStat.answeredCorrectly :0)+1
                return {
                    ...q,
                    questionDifficulty,
                    answeredInCorrectly,
                    answeredCorrectly
                }
            })
            .sort((q1:any,q2:any)=>{
                return (-q1.questionDifficulty) - (-q2.questionDifficulty)
            })
            //console.log(enrichedQs)
            setQuestions(enrichedQs)
        }
        getData()
    },[])

    const data = React.useMemo(() => {        
        if(questions.length==0 || Object.keys(statistics).length==0) return []

        let subset = questions        
            .filter((q:any,index)=>index < 50)
        return [
                {
                label: 'Answered Correctly',
                data: subset.map((q:any)=>{
                    return {primary: q.id, secondary: q.answeredCorrectly }
                })
                },
                {
                label: 'Answered In Correctly',
                data: subset.map((q:any)=>{                    
                    return {primary: q.id, secondary: q.answeredInCorrectly }
                })
                },
            ]
        },
        [questions,statistics]
    )

    if(questions.length==0 || Object.keys(statistics).length==0)
        return null        
    
    return <div style={style.pageContainer}>
        <div style={style.pageTitle}>Questions</div>
        <div style={style.pageContent}>                       
            <div style={style.pageSection}>            
                <div>
                Questions Answered: {statistics.totalQuestionsAnswered}
                </div>
                <div>
                Questions Answered Correctly: {statistics.totalQuestionsAnsweredCorrectly}
                </div>
                <div>
                Questions Answered In Correctly: {statistics.totalQuestionsAnsweredInCorrectly}            
                </div>
                <div>
                Questions Not Answered: {questions.length - statistics.totalUniqueQuestionsAnswered}            
                </div>
                <div>
                Percentage: {Math.floor(100*statistics.totalQuestionsAnsweredCorrectly/statistics.totalQuestionsAnswered)}
                </div>
                
            </div>
            <div style={style.pageSection}>
                <div style={{width: '400px',height: '500px'}}>
                    Most Difficult Questions

                 
                    <div style={style.questionList}>
                        <div style={style.questionItemQuestion}>
                            Question
                        </div>
                        <div style={style.questionItemInfo}>
                            Difficulty
                        </div>
                        <div style={style.questionItemInfo}>
                            Correct
                        </div>
                        <div style={style.questionItemInfo}>
                            Incorrect
                        </div>
                        { questions.map((q:any)=> {
                            return <Fragment> <div key={q.id} style={style.questionItemQuestion}>
                                    <a href={`#/question/${q.id}`}>{q.question.substring(0,60)}...</a>
                                </div>
                                <div style={style.questionItemInfo}>
                                    {q.questionDifficulty}
                                </div>
                                <div style={style.questionItemInfo}>
                                    {q.answeredCorrectly-1}
                                </div>
                                <div style={style.questionItemInfo}>
                                    {q.answeredInCorrectly-1}
                                </div>
                                </Fragment>
                        })}
                    </div>

                    Most Difficult Questions Chart
                    <Chart data={data} axes={axes} series={series} tooltip={tooltip}/>
                </div>                                
            </div>            
        </div>
    </div>
}

export default Statistics