import { getAPI, getAPIDomain } from '../config';
import {TOKEN,DOMAIN} from '../../../application/src/domain'
import {IMarkings} from '../../../application/src/types'

const handleResponse = (res:Response) => {
  if (res.status === 401) {
    window.location.href = `/`
  } else if (res.status === 500) {
      return null
  } else {
    return (res!== null) ? res.json() : []
  }
}

const service = {
  get: (url:string,options:object) => fetch(getAPI() + url, { ...options,/*credentials: 'include'*/ }).then(handleResponse),
  post: (url:string, body:object,options:object={}) => fetch(getAPI() + url, { ...options,/*credentials: 'include',*/ method: 'POST', body: JSON.stringify(body) }).then(handleResponse),
  put: (url:string, body:object,options:object={}) => fetch(getAPI() + url, { ...options,/*credentials: 'include',*/ method: 'PUT', body: JSON.stringify(body) }).then(handleResponse),
  delete: (url:string,options:object={}) => fetch(getAPI() + url, {...options, /*credentials: 'include',*/ method: 'DELETE' }).then(handleResponse)
}

export async function queryQuestions() {
  const localToken = window.localStorage.getItem(TOKEN)
  const domain = window.localStorage.getItem(DOMAIN)
  return service.get(`/ModelService/questions?domain=${domain}`,{
    headers: { 
      topic: 'CISSP',
      Authorization: `Bearer ${localToken}`
    }
  })
}

export async function queryStatistics() {
  const localToken = window.localStorage.getItem(TOKEN)
  const domain = window.localStorage.getItem(DOMAIN)
  return service.get(`/ModelService/statistics?domain=${domain}`,{
    headers: { 
      topic: 'CISSP',
      Authorization: `Bearer ${localToken}`
    }
  })
}

export async function recordMarkings(markings: IMarkings) {  
  const localToken = window.localStorage.getItem(TOKEN)
  const payload = {
    ...markings,
    token: localToken
  }
  return service.post(`/ModelService/markings`,
    payload, {
      headers: { 
        topic: 'CISSP',
        Authorization: `Bearer ${localToken}`
      },
      
    })
}