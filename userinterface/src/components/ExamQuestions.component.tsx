import React, { FC, useState } from 'react';
import {CORRECT,INCORRECT,NEUTRAL} from '../../../application/src/domain'
import {IQuestion,IAnswer} from '../../../application/src/types'
import {recordMarkings} from '../contract/api'
import style from '../styles/style'

interface IExamQuestionProps {    
    questions: IQuestion[]
}

export const ExamQuestions: FC<IExamQuestionProps> = (props:IExamQuestionProps) => {

    const {questions} =props

    const [questionIndex,setQuestionIndex] = useState(0)

    const [showAnswer, setShowAnswer] = useState(false)

    const [markings,setMarkings]= useState<any>({})
    const [answers,setAnswers]= useState<any>({})

    const question = questions[questionIndex]
    
    const [showDetail,setShowDetail] = useState(false)

    
    const answer = ()=>{        
        let m = question.Answers?.reduce((h:any,answer:IAnswer) => {

            if(question.answer.indexOf(answer.choice)>=0){
                if(answers[answer.choice]){
                    h[answer.choice]=CORRECT
                }else {
                    h[answer.choice]=INCORRECT
                }
            }else {
                if(answers[answer.choice]){
                    h[answer.choice]=INCORRECT
                }else {
                    h[answer.choice]=NEUTRAL
                }
            }
            return h
        },{})

        
        setMarkings(m)
        setShowAnswer(true)


        let grade = Object.values(m).reduce((h:number,cnt:any)=>h+cnt,0)
        
        recordMarkings({
            question,
            markings: m,
            grade,
            date: Date.now()
        })
    }

    const recordAnswer =  (event:any)=>{
        if(answers[event.target.value]){
            let clone = {...answers}
            delete clone[event.target.value]
            setAnswers(clone)
        }else {
            let clone = {...answers}
            clone[event.target.value]=true
            setAnswers(clone)
        }
    }
   
    return <div>
        {questionIndex < questions.length-1 &&  <button onClick={()=>{setQuestionIndex(questionIndex+1);setShowAnswer(false);setAnswers({})}}>Next</button>}
        {questionIndex >0 && <button onClick={()=>{setQuestionIndex(questionIndex-1);setShowAnswer(false);setAnswers({});}}>Previous</button>}

        <div style={style.question}>
        {question.question}
        </div>
                
        <div style={style.answer}>
            {showAnswer && <div>Correct Answer:{question.answer} <br/> {question.explanation}</div>}
            {!showAnswer && <button onClick={answer}>Answer</button>}
        </div>

        {question.Answers?.map((answer:IAnswer) => {
            let answerStyle :any = style.answerChoice
            if(showAnswer){                
                answerStyle = markings[answer.choice]===CORRECT ? style.answerChoiceCorrect :
                    (markings[answer.choice]===INCORRECT ? style.answerChoiceInCorrect : style.answerChoice) 
            }

            return <div key={answer.id} style={style.answer}>
                    <div style={answerStyle}> 
                        <input type="checkbox" id={answer.choice} value={answer.choice} onClick={recordAnswer}/>{answer.choice}
                    </div>
                    <div style={style.answerAnswer}>
                    {answer.answer}
                    </div>
                </div>
            })
        }
        {!showDetail ?  <button onClick={()=>setShowDetail(true)}>Show Detail</button> :
            <div>
                <button onClick={()=>setShowDetail(false)}>Hide Detail</button> :
                <div>
                Difficulty: {question?.questionDifficulty ? question.questionDifficulty *100 : ''}
                </div>
                <div>
                Answers: Correct:{question.answeredCorrectly}: Incorrect:{question.answeredInCorrectly}
                </div>
                <div>
                Question Id: {question.id}
                </div>
            </div>
        }   
        
    </div>

}