import React, { FC, useEffect } from 'react';
import {v4 as uuid} from 'uuid'
import {TOKEN} from '../../../application/src/domain'

export const TokenComponent: FC = () => {
    let token = window.localStorage.getItem(TOKEN)
    if(!token){
        token = uuid()
        window.localStorage.setItem(TOKEN,token)
    }
    return <></>
}