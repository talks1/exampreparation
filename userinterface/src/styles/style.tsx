
 const pageContainer = {
    padding: "10px",
    background: "white"
};
  
 const pageContent = {
    padding: "10px",
    background: "#DDDDDD"
  
};

 const pageTitle = {
      
};

 const pageSection = {
      
};

 const question :any = {
  padding: "10px",
  margin:  "10px"
}

 const answer :any = {
  padding: "10px",
  margin:  "10px",
  display: "flex"
}

 const answerChoice = {
    padding: "10px",
    margin:  "0px  ",
    flex: "1",
    background: "white"
    
}
  
 const answerAnswer = {
    padding: "10px",
    margin:  "0px",
    background: "white ",
    flex: "20"
  
}
  
 const answerChoiceCorrect = {
  background: "green"
}

 const answerChoiceInCorrect = {
    background: "red"
}
  



 const questionList = {
  display: "flex",
  width: "800px",
  "flex-wrap": "wrap",
  background: "white ",
}

 const questionItemQuestion  ={
  margin:  "auto",
  width: "500px"
}

 const questionItemInfo = {
  margin:  "auto",
  width: "100px"
}


const layoutContainer = {
  background: "#EEFFEE"
}

const header = {
  padding: "20px",
  background: "#CCFFCC"
}

const menu = {
  padding: "20px",
  background: "#EEFFEE"
}

const footer = {
  padding: "20px",
  background: "#CCFFCC"
}

 export default {
   header, menu,footer,layoutContainer,
    pageContainer,
    pageTitle,
    pageContent,
    pageSection,
    question,
    answer,
    answerChoice,
    answerChoiceCorrect,
    answerChoiceInCorrect,
    answerAnswer,
    questionList,
    questionItemQuestion,
    questionItemInfo
}