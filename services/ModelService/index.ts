import { createAzureFunctionHandler } from "azure-function-express"
import bodyparser from 'body-parser'
import Debug from 'debug'
import express from "express"
import ChannelFactory from 'real-value-channel-hypercore'
import { getModel } from '../../application/src/model/exam'
import { Log as CCSP } from '../../application/src/persistentlog/ccsp'
import { Log as CISSP } from '../../application/src/persistentlog/cissp'
import { ExamService } from '../../application/src/service'
import { IMarkings } from '../../application/src/types'
import { config } from '../src/config'

const debug = Debug('ModelService')

const app = express();    
app.use(bodyparser.json())

const stats:any = {
    totalQuestionsAnswered:0,
    totalQuestionsAnsweredCorrectly:0,
    totalQuestionsAnsweredInCorrectly:0,
    questionStats : {}
}


const domains = {
    'CISSP': {...stats},
    'CCSP': {...stats}
}


const answeredQuestionsMap = {}

const statsmanager = (event:any) =>{
    try {
        //console.log(event)

        const statistics = domains[event.value.question.domain]

        const markings:IMarkings = event.value    
        statistics.totalQuestionsAnswered += 1
        statistics.totalQuestionsAnsweredCorrectly += markings.grade>0 ? 1 : 0
        statistics.totalQuestionsAnsweredInCorrectly += markings.grade<=0 ? 1 : 0
        answeredQuestionsMap[markings.question.id]=true
        statistics.totalUniqueQuestionsAnswered = Object.keys(answeredQuestionsMap).length

        let qStats = statistics.questionStats[markings.question.id] || {answered:0,answeredCorrectly:0,answeredInCorrectly: 0}
        statistics.questionStats[markings.question.id]=qStats
        qStats.answered += 1
        qStats.answeredCorrectly += markings.grade>0 ? 1 : 0
        qStats.answeredInCorrectly += markings.grade<=0 ? 1 : 0
    }catch(ex){
        console.log(ex)
    }
} 

const channel = ChannelFactory('plog',statsmanager)()

let examModel = null
let examService = null
async function initialize(){
    if(!examService){
        examModel = await getModel(config.examDatabase)
        examService = await ExamService(examModel)    
        await examService.seed('CCSP',CCSP)
        await examService.seed('CISSP',CISSP)
    }
}
initialize()

app.get("/api/ModelService/questions", 
    //passport.authenticate('oauth-bearer', { session: false }), 
    async (req, res) => {
    
    const topic=req.headers.topic    

    const domain = req.query.domain

    await initialize()
    
    const id = req.params.id    
    let components = await examService.queryQuestions(examModel,domain)
    res.status(200).send(components)
});

app.get("/api/ModelService/statistics", 
    //passport.authenticate('oauth-bearer', { session: false }), 
    async (req, res) => {        

        const domain = req.query.domain
        const statistics = domains[domain]

        res.status(200).send(statistics)
});

app.post("/api/ModelService/markings", 
    //passport.authenticate('oauth-bearer', { session: false }), 
    async (req, res) => {
        const topic=req.headers.topic
        //console.log(req.body)
        channel.enqueue(req.body)
        res.status(200).send({})
    }
)

export default createAzureFunctionHandler(app);
