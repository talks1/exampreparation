const DOTENV = require('dotenv')
const DOTENV_CONFIG = process.env.ENVIRONMENT ? `.env.${process.env.ENVIRONMENT}` : '.env'
console.log(`Sourcing configuration from ${DOTENV_CONFIG}`)
DOTENV.config({ path: DOTENV_CONFIG })

console.log(`Debugging with ${process.env.DEBUG}`)

const LOCAL = process.env.NODE_ENV !== 'production'
const SERVER_PORT = process.env.PORT || 3001

const examDatabase = {
  DBDIALECT: 'sqlite',
  force: true,
  RESYNC: 'true',
  logging: false,
}

// const examDatabase = {    
//   USER: "sa",
//   PASSWORD: 'PA22word',
//   DBHOST: 'localhost',
//   DB: 'exam',
//   DBDIALECT: 'mssql',
//   schema: 'exam',
//   force: true,
//   RESYNC: 'true',
//   logging: true,
// }


const APP_URL = process.env.APP_URL

//Here is the app registration https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/Overview/appId/ab8c7f24-a30e-4ef4-bf82-713d248f96f9/isMSAApp/
const TENANT_CLIENT_ID=process.env.TENANT_CLIENT_ID || "ab8c7f24-a30e-4ef4-bf82-713d248f96f9"
const TENANT_ID= process.env.TENANT_ID || '951b1ed2-d31c-4c2a-9dd6-8ea6137ceb9d'

const STORAGE_DIRECTORY = process.env.STORAGE_DIRECTORY || './tmp'

export const config =  {
  LOCAL,
  SERVER_PORT,
  APP_URL,
  examDatabase,  
  seed: true, //Seed the database
  testMode: false, // use this to turn off authentication  
  storage: {
    directory: STORAGE_DIRECTORY
  },
  auth: {
    tenantId: TENANT_ID,
    clientId: TENANT_CLIENT_ID
  }
}
