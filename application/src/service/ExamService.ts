import {IExamService,IExamModel} from '../types'
import Debug from 'debug'
const debug = Debug('examService')
import {ANSWER_TYPE,QUESTION_TYPE} from '../domain'

let seeded : any[] = []

export const ExamService = async (model : IExamModel) : Promise<IExamService> => {
    
    const queryQuestions = async (topic:string,domain:string)=>{    
        const relationalModel = await model.Question.query({domain})
        return relationalModel
    }

    const create = (Exam:any)=>{
        debug(Exam)
        if(Exam.type===ANSWER_TYPE){
            return model.Answer.create(Exam)        
        }else if(Exam.type===QUESTION_TYPE){
            return model.Question.create(Exam)        
        }
    }

    const seed = async (domain:string,log:any) => {
        
        if(!seeded.find((i:any)=>i.domain === domain)){
            debug(`Seeding ${log.length}`)
            for(var i=0;i<log.length;i++){
                try {
                await create({...log[i],domain: domain})
                }catch(ex){
                console.error("Unable to create")
                console.log(ex)
                console.error(log[i])
                }
            }
            seeded.push(domain)
        }
    }
    

    return {
        model,
        seed,

        create,
                
        queryQuestions
    } 
}

