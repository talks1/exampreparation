
export const ANSWER_TYPE='ANSWER'
export const QUESTION_TYPE='QUESTION'

export const TOKEN='performance-token'
export const DOMAIN='domain'

export const CORRECT=1
export const NEUTRAL=0
export const INCORRECT=-1