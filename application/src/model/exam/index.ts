import {ModelFactory} from './ModelFactory'
import {IExamModel} from '../../types'
import Debug from 'debug'

const debug = Debug('model')

let model :IExamModel|null = null

export const getModel = async (config:any) : Promise<IExamModel> => {
    if(!model){        
        debug('Building ModelFactory')
        model = await ModelFactory(config)
    }
    return model
}

export * from './ModelFactory'