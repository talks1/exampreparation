import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const AnswerFactory = async (sequelize:any, config:any) => {
  const Answer = sequelize.define('Answers', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },    
    answer: {
      type: DataTypes.STRING(512),
      allowNull: false,
    },
    choice: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    QuestionId: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  },
  {
    schema: config.schema,
    tableName: 'Answers',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await Answer.sync({ force: config.force }) }

  async function create (data:any) {
      return await Answer.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return Answer.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await Answer.count({ where })
    return result
  }

  async function query (where = {}) {    
    return Answer.findAll(where)
  }

  async function destroy (id = '') {
    return Answer.destroy({ where: { id } })
  }


  return {
    Answer,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}
