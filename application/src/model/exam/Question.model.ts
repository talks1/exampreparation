import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const QuestionFactory = async (sequelize:any, config:any) => {
  const Question = sequelize.define('Questions', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },   
    domain: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    question: {
      type: DataTypes.STRING(512),
      allowNull: false,
    },
    // description: {
    //   type: DataTypes.STRING,
    //   allowNull: false,
    // },
    answer: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    explanation: {
      type: DataTypes.STRING,
      allowNull: true,
    }
  },
  {
    schema: config.schema,
    tableName: 'Questions',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await Question.sync({ force: config.force }) }

  async function create (data:any) {
    return Question.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return Question.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await Question.count({ where })
    return result
  }

  async function query (where = {}) {    
    return Question.findAll({where,include: [{ all: true, nested: true }]})
  }

  async function destroy (id = '') {
    return Question.destroy({ where: { id } })
  }


  return {
    // CONSTANT_TYPE,
    // RANGE_TYPE,
    // SET_TYPE,
    // DISTRIBUTION_TYPE,
    // DISTRIBUTIONSET_TYPE,

    Question,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}
