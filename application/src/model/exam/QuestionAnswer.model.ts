import Debug from 'debug'
import { Sequelize,DataTypes  } from 'sequelize'

const debug = Debug('ModelService')

export const QuestionAnswerFactory = async (sequelize:any, config:any) => {
  const QuestionAnswer = sequelize.define('QuestionAnswers', {
    questionId: {
      type: DataTypes.STRING,      
    },    
    answerId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    schema: config.schema,
    tableName: 'QuestionAnswers',
    timestamps: false,
  })

  if (config.RESYNC === 'true') { await QuestionAnswer.sync({ force: config.force }) }

  async function create (data:any) {
    return QuestionAnswer.create(data)
  }

  async function update (data:any) {
    return data.save()
  }

  async function get (id = '') {
    const where = { id }
    return QuestionAnswer.findOne({ where })
  }

  async function queryCount (where = {}) {
    const result = await QuestionAnswer.count({ where })
    return result
  }

  async function query (where = {}) {    
    return QuestionAnswer.findAll(where)
  }

  async function destroy (id = '') {
    return QuestionAnswer.destroy({ where: { id } })
  }

  return {
    QuestionAnswer,
    create,
    update,
    get,
    query,
    queryCount,
    destroy,
  }
}
