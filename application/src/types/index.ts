

export interface IExamService {    
    model: any,
    seed: any,
    create: any,
    queryQuestions: any
}

export interface IExamModel {
    Question: any,
    Answer: any,    
    close: any
}

export interface IQuestion {
    id: string,
    topic: string,
    question: string,
    description: string,
    answer: string,
    explanation: string,
    Answers?: IAnswer[]
    questionDifficulty?: number,
    answeredCorrectly?: number,
    answeredInCorrectly?: number
}

export interface IAnswer {
    id: string,    
    answer: string 
    choice: string
}

export interface IMarkings {
    question: any,    
    markings: any,
    grade: number,
    date: number,
}