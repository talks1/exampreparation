export const Log = [
 {
  "type": "QUESTION",
  "id": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b",
  "question": "Incident handling requires prioritization in order to address incidents in an appropriate manner and order. Which of the following is NOT an factor to determine prioritization",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.04309bb7-a825-4774-9921-4f833c294a44",
  "question": "Which component of SCAP serves to provide a standardized reference for publicly known security vulnerabilities and exposures",
  "answer": "A",
  "explanation": "Common Vulnerabilities and Exposures (CVE)\nCommon Configuration Enumeration (CCE) (prior web-site at MITRE)\nCommon Platform Enumeration (CPE)\nCommon Vulnerability Scoring System (CVSS)\nExtensible Configuration Checklist Description Format (XCCDF)\nOpen Vulnerability and Assessment Language (OVAL)"
 },
 {
  "type": "QUESTION",
  "id": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328",
  "question": "Which of the following is not one of the rules of evidence",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70",
  "question": "Examine this IP package and indicated what type of attack it is. SourceIP: 200.16.44.200, DestIP: 200.16.44.200, SourcePort: 88, DestPort: 88, Syn: 1",
  "answer": "C",
  "explanation": [
   {
    "value": "A land attack is a remote denial-of-service (DOS) attack caused by sending a packet to a machine with the source host/port the same as the destination host/port."
   },
   {
    "value": "A teardrop attack is a denial-of-service (DoS) attack that involves sending fragmented packets to a target machine. Since the machine receiving such packets cannot reassemble them due to a bug in TCP/IP fragmentation reassembly, the packets overlap one another, crashing the target network device."
   },
   {
    "value": "MitM attach - Man in the middle attach"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f",
  "question": "Your system is configured to generate an audit log event whenever an attempt to log in with a disabled user account is made. What is this an example of?",
  "answer": "D",
  "explanation": "A clipping level is a threshold for normal mistakes a user may commit before investigation or notification begins. An understanding of the term clipping level is essential for mastery of the CISSP exam. A clipping level establishes a baseline violation count to ignore normal user errors."
 },
 {
  "type": "QUESTION",
  "id": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58",
  "question": "Your company is cloud migrating many of its existing apps and services. Which of the following will allow moving of the apps and services to the cloud while still providing control of the underlying operation system on which the app/services run?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a",
  "question": "You are exploring deployment options for network based intrusion detection systems. One of your performance objectives is to minimize false positives as much as possible. Based on this requirement, which type of IDS should you considers?",
  "answer": "B",
  "explanation": "Anomoly or Behaviour based systems are prone to false positives whereas Knowledge Based Systems are not because those are about match to known patterns where than just violations of 'normal' behavior."
 },
 {
  "type": "QUESTION",
  "id": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225",
  "question": "Which of the following are most likely to decrease the effectiveness of an incident reporting system (Choose 2)",
  "answer": "B,D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d",
  "question": "Which of the following is most important in order to maintain the integrity and admissibilty of digital evidence?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2",
  "question": "You have been tasked with reducing the likelihood that nodes in your network can forward packets with spoofed source IP addresses. Which of the following is the best way to accomplish this.",
  "answer": "D",
  "explanation": "In Reverse Path Forward a router looks at the Source IP address of a packet and asks itself would it route a packet destined for the Source IP address via this interface. if they answer is no then the packet is dropeed."
 },
 {
  "type": "QUESTION",
  "id": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6",
  "question": "You have a building which has a higher likelihood of electrical fire than other types. Given this what class of fire suppression system should you have.",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3",
  "question": "Which of the following best describes an electrical blackout?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6",
  "question": "A new incident investigator is preparing her incident 'jump kit'. Which of the following is least likely to be in the kit.",
  "answer": "D",
  "explanation": "There is probably no reason to have copies of malware"
 },
 {
  "type": "QUESTION",
  "id": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125",
  "question": "Which one of the following is NOT one of the Incident Response  Life Cycle",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3",
  "question": "Audit logs are generated by most computer systems. Which of the following is the most important reason to implement logging?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d",
  "question": "In addition to IP addresses, DHCP servers provide network nodes with additional useful information such as DNS servers address, default gateway and netbios node type. Which of the following is not a legitimate attack that may be executed against your DHCP deployment? (Choose 3)",
  "answer": "C,D,F",
  "explanation": "As DHCP is not TCP based (rather UDP) option C is not a concern.\nDHCP doesnt relate to SMTP\nIt would be necessary to be on the network link to spoof an acknowlegement."
 },
 {
  "type": "QUESTION",
  "id": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67",
  "question": "A new admin in your enterprise  has asked you about an entry she discovered in an /etc/hosts file on a users ubuntu workstation.  The entry read: 'evilapp.evilsite.com: 127.0.0.1' Which of the following is the BEST response to the admin?",
  "answer": "B",
  "explanation": "The entry prevents ask to that site."
 },
 {
  "type": "QUESTION",
  "id": "SOP.636823fd-5712-4388-a771-363eafd8e4ac",
  "question": "You have determined that a user current unknown has been modifying and in some cases deleting files without authorization. Which of the following will not help mitigate the problem. Choose two.",
  "answer": "A,C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574",
  "question": "Packet Filtering Firewalls have several limitations that make them less appropriate than more modern solutions when protected internal resources from the internet threats. Which of the following are short comings of packet filtering? (Choose Two)",
  "answer": "A,C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "question": "Which of the following are UDP based? (Choose 6)",
  "answer": "B,E,F,G,H,J",
  "explanation": "RADIUS ports 1812 and 1813\nIMAP4 port 143 or 993(SSL)\nSNMP port 162\nNTP port 123\nDNS port 53\nTFTP port 69 (trival file transfer protocol)"
 },
 {
  "type": "QUESTION",
  "id": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9",
  "question": "Which of the following is not a component of a configuration management system?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6",
  "question": "You are evaluating the merits of differential and incremental backup strategies.  Which of the following is true?",
  "answer": "D",
  "explanation": "Differentials back up files since last full or last incremental backup.\n Incremental backups require full back and all incremental backups"
 },
 {
  "type": "QUESTION",
  "id": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c",
  "question": "Which of the following best defines a Recover Point Objective?",
  "answer": "D",
  "explanation": "MTD - (Maximum Tolerable Downtime) - Maximum amount of time a business process can be unavailable\nRTO - Maximum amount of time to recover a business process\nWTR - (Work Receovery Time) - The amount of time needed to verify a system/data after recovery"
 },
 {
  "type": "QUESTION",
  "id": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1",
  "question": "What type of investigation would typically be launched in response to a report of high network latency?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.96644989-2185-4805-a905-2245e27ebe5c",
  "question": "Server logs are an example of ",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85",
  "question": "Which evidence source should be collected first when considering the order of volatility?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624",
  "question": "What type of technology prevents a forensic examiner from accidentally corrupting evidence while creating an image of a disk?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.42671402-184d-4df4-a367-c268184a5df9",
  "question": "Three of these choices are data elements found in NetFlow data. Which is not?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.2993d07c-eb9a-4ffc-af14-8aa0b055d57c",
  "question": "Federal law requires U.S. businesses to report verified security incidents to US-CERT.",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe",
  "question": "During what phase of ediscovery does an organization share information with the other side?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7",
  "question": "During what phase of continuous security monitoring does the organization define metrics?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.f0a0275e-3b63-4ecb-9912-181365697090",
  "question": "What DLP technique tags sensitive content and then watches for those tags in data leaving the organization?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b",
  "question": "What type of hypervisor runs directly on top of bare hardware?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6",
  "question": "What additional patching requirements apply only in a virtualized environment?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc",
  "question": "Which public cloud computing tier places the most security responsibility on the vendor?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7",
  "question": "What security principle most directly applies to limiting information access?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f",
  "question": "What security principle requires two individuals to perform a sensitive action?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a",
  "question": "Which one of the following individuals would not normally be found on the incident response team?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a",
  "question": "During an incident response, what is the highest priority of first responders?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc",
  "question": "Which one of the following tools is used primarily to perform network discovery scans?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069",
  "question": "Adam recently ran a network port scan of a web server running in his organization. He ran the scan from an external network to get an attacker’s perspective on the scan. Which one of the following results is the greatest cause for alarm?",
  "answer": "D",
  "explanation": "Only open ports represent potentially significant security risks. Ports 80 and 443 are expected to be open on a web server. Port 1433 is a database port and should never be exposed to an external network."
 },
 {
  "type": "QUESTION",
  "id": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab",
  "question": "Which one of the following factors should not be taken into consideration when planning a security testing schedule for a particular system?",
  "answer": "C",
  "explanation": "The sensitivity of information stored on the system, difficulty of performing the test, and likelihood of an attacker targeting the system are all valid considerations when planning a security testing schedule. The desire to experiment with new testing tools should not influence the production testing schedule."
 },
 {
  "type": "QUESTION",
  "id": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f",
  "question": "Which one of the following is not normally included in a security assessment?",
  "answer": "C",
  "explanation": "C. Security assessments include many types of tests designed to identify vulnerabilities, and the assessment report normally includes recommendations for mitigation. The assessment does not, however, include actual mitigation of those vulnerabilities."
 },
 {
  "type": "QUESTION",
  "id": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d",
  "question": "Who is the intended audience for a security assessment report?",
  "answer": "A",
  "explanation": "Security assessment reports should be addressed to the organization’s management. For this reason, they should be written in plain English and avoid technical jargon."
 },
 {
  "type": "QUESTION",
  "id": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d",
  "question": "Beth would like to run an nmap scan against all of the systems on her organization’s private network. These include systems in the 10.0.0.0 private address space. She would like to scan this entire private address space because she is not certain what subnets are used. What network address should Beth specify as the target of her scan?",
  "answer": "B",
  "explanation": "The use of an 8-bit subnet mask means that the first octet of the IP address represents the network address. In this case, that means 10.0.0.0/8 will scan any IP address beginning with 10."
 },
 {
  "type": "QUESTION",
  "id": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea",
  "question": "Alan ran an nmap scan against a server and determined that port 80 is open on the server. What tool would likely provide him the best additional information about the server’s purpose and the identity of the server’s operator?",
  "answer": "B",
  "explanation": "The server is likely running a website on port 80. Using a web browser to access the site may provide important information about the site’s purpose."
 },
 {
  "type": "QUESTION",
  "id": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c",
  "question": "What port is typically used to accept administrative connections using the SSH utility?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69",
  "question": "Which one of the following tests provides the most accurate and detailed information about the security state of a server?",
  "answer": "D",
  "explanation": "Authenticated scans can read configuration information from the target system and reduce the instances of false positive and false negative reports."
 },
 {
  "type": "QUESTION",
  "id": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b",
  "question": "What type of network discovery scan only follows the first two steps of the TCP handshake?",
  "answer": "C",
  "explanation": "The TCP SYN scan sends a SYN packet and receives a SYN ACK packet in response, but it does not send the final ACK required to complete the three-way handshake.\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 974). Wiley. Kindle Edition."
 },
 {
  "type": "QUESTION",
  "id": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7",
  "question": "Matthew would like to test systems on his network for SQL injection vulnerabilities. Which one of the following tools would be best suited to this task?",
  "answer": "D",
  "explanation": "SQL injection attacks are web vulnerabilities, and Matthew would be best served by a web vulnerability scanner. A network vulnerability scanner might also pick up this vulnerability, but the web vulnerability scanner is specifically designed for the task and more likely to be successful."
 },
 {
  "type": "QUESTION",
  "id": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7",
  "question": "Badin Industries runs a web application that processes e-commerce orders and handles credit card transactions. As such, it is subject to the Payment Card Industry Data Security Standard (PCI DSS). The company recently performed a web vulnerability scan of the application and it had no unsatisfactory findings. How often must Badin rescan the application?",
  "answer": "C",
  "explanation": "PCI DSS requires that Badin rescan the application at least annually and after any change in the application."
 },
 {
  "type": "QUESTION",
  "id": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e",
  "question": "Grace is performing a penetration test against a client’s network and would like to use a tool to assist in automatically executing common exploits. Which one of the following security tools will best meet her needs?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709",
  "question": "Paul would like to test his application against slightly modified versions of previously used input. What type of test does Paul intend to perform? Code review",
  "answer": "C",
  "explanation": "Mutation fuzzing uses bit flipping and other techniques to slightly modify previous inputs to a program in an attempt to detect software flaws."
 },
 {
  "type": "QUESTION",
  "id": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b",
  "question": "Users of a banking application may try to withdraw funds that don’t exist from their account. Developers are aware of this threat and implemented code to protect against it. What type of software testing would most likely catch this type of vulnerability if the developers have not already remediated it?",
  "answer": "A",
  "explanation": "Misuse case testing identifies known ways that an attacker might exploit a system and tests explicitly to see if those attacks are possible in the proposed code."
 },
 {
  "type": "QUESTION",
  "id": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831",
  "question": "What type of interface testing would identify flaws in a program’s command-line interface?",
  "answer": "B",
  "explanation": "User interface testing includes assessments of both graphical user interfaces (GUIs) and command-line interfaces (CLIs) for a software program."
 },
 {
  "type": "QUESTION",
  "id": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3",
  "question": "During what type of penetration test does the tester always have access to system configuration information?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46",
  "question": "Which one of the following is the final step of the Fagan inspection process?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6",
  "question": "What information security management task ensures that the organization’s data protection requirements are met effectively?",
  "answer": "B",
  "explanation": "The backup verification process ensures that backups are running properly and thus meeting the organization’s data protection objectives."
 },
 {
  "type": "QUESTION",
  "id": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463",
  "question": "What is the most commonly used technique to protect against virus attacks?",
  "answer": "A",
  "explanation": "Signature detection mechanisms use known descriptions of viruses to identify malicious code resident on a system."
 },
 {
  "type": "QUESTION",
  "id": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444",
  "question": "You are the security administrator for an e-commerce company and are placing a new web server into production. What network zone should you use?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.3fcff942-b99a-409e-950b-b72be6942100",
  "question": "Which one of the following types of attacks relies on the difference between the timing of two events?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d",
  "question": "Which one of the following techniques is most closely associated with APT attacks?",
  "answer": "A",
  "explanation": "While an advanced persistent threat (APT) may leverage any of these attacks, they are most closely associated with zero-day attacks."
 },
 {
  "type": "QUESTION",
  "id": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f",
  "question": "What advanced virus technique modifies the malicious code of a virus on each system it infects?",
  "answer": "A",
  "explanation": "In an attempt to avoid detection by signature-based antivirus software packages, polymorphic viruses modify their own code each time they infect a system."
 },
 {
  "type": "QUESTION",
  "id": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95",
  "question": "Which one of the following tools provides a solution to the problem of users forgetting complex passwords?",
  "answer": "A",
  "explanation": "LastPass is a tool that allows users to create unique, strong passwords for each service they use without the burden of memorizing them all."
 },
 {
  "type": "QUESTION",
  "id": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a",
  "question": "What type of application vulnerability most directly allows an attacker to modify the contents of a system’s memory?",
  "answer": "D",
  "explanation": "Buffer overflow attacks allow an attacker to modify the contents of a system’s memory by writing beyond the space allocated for a variable."
 },
 {
  "type": "QUESTION",
  "id": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab",
  "question": "What technique may be used to limit the effectiveness of rainbow table attacks?",
  "answer": "B",
  "explanation": "Salting passwords adds a random value to the password prior to hashing, making it impractical to construct a rainbow table of all possible values."
 },
 {
  "type": "QUESTION",
  "id": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658",
  "question": "What character should always be treated carefully when encountered as user input on a web form?",
  "answer": "D",
  "explanation": "The single quote character (') is used in SQL queries and must be handled carefully on web forms to protect against SQL injection attacks."
 },
 {
  "type": "QUESTION",
  "id": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04",
  "question": "What database technology, if implemented for web forms, can limit the potential for SQL injection attacks?",
  "answer": "B",
  "explanation": "Developers of web applications should leverage database stored procedures to limit the application’s ability to execute arbitrary code. With stored procedures, the SQL statement resides on the database server and may only be modified by database administrators."
 },
 {
  "type": "QUESTION",
  "id": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1",
  "question": "What condition is necessary on a web page for it to be used in a cross-site scripting attack?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e",
  "question": "What type of virus utilizes more than one propagation technique to maximize the number of penetrated systems?",
  "answer": "D",
  "explanation": "Multipartite viruses use two or more propagation techniques (for example, file infection and boot sector infection) to maximize their reach."
 },
 {
  "type": "QUESTION",
  "id": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75",
  "question": "What worm was the first to cause major physical damage to a facility?",
  "answer": "A",
  "explanation": "Stuxnet was a highly sophisticated worm designed to destroy nuclear enrichment centrifuges attached to Siemens controllers."
 },
 {
  "type": "QUESTION",
  "id": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0",
  "question": "Ben’s system was infected by malicious code that modified the operating system to allow the malicious code author to gain access to his files. What type of exploit did this attacker engage in?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37",
  "question": "When designing firewall rules to prevent IP spoofing, which of the following principles should you follow?",
  "answer": "A",
  "explanation": "Packets with internal source IP addresses should not be allowed to enter the network from the outside because they are likely spoofed."
 },
 {
  "type": "ANSWER",
  "id": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b-A",
  "QuestionId": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b",
  "choice": "A",
  "answer": "Functional Impact of the Incident"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b-B",
  "QuestionId": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b",
  "choice": "B",
  "answer": "Information impact of the Incident"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b-C",
  "QuestionId": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b",
  "choice": "C",
  "answer": "Detectability of the Incident"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b-D",
  "QuestionId": "SOP.9c313238-d225-4876-a5d3-bf7da9f26d8b",
  "choice": "D",
  "answer": "Recoverability from the Incident"
 },
 {
  "type": "ANSWER",
  "id": "SOP.04309bb7-a825-4774-9921-4f833c294a44-A",
  "QuestionId": "SOP.04309bb7-a825-4774-9921-4f833c294a44",
  "choice": "A",
  "answer": "CVE"
 },
 {
  "type": "ANSWER",
  "id": "SOP.04309bb7-a825-4774-9921-4f833c294a44-B",
  "QuestionId": "SOP.04309bb7-a825-4774-9921-4f833c294a44",
  "choice": "B",
  "answer": "CCE"
 },
 {
  "type": "ANSWER",
  "id": "SOP.04309bb7-a825-4774-9921-4f833c294a44-C",
  "QuestionId": "SOP.04309bb7-a825-4774-9921-4f833c294a44",
  "choice": "C",
  "answer": "CPE"
 },
 {
  "type": "ANSWER",
  "id": "SOP.04309bb7-a825-4774-9921-4f833c294a44-D",
  "QuestionId": "SOP.04309bb7-a825-4774-9921-4f833c294a44",
  "choice": "D",
  "answer": "CVSSS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328-A",
  "QuestionId": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328",
  "choice": "A",
  "answer": "Admissable"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328-B",
  "QuestionId": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328",
  "choice": "B",
  "answer": "Authentic"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328-C",
  "QuestionId": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328",
  "choice": "C",
  "answer": "Complete"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328-D",
  "QuestionId": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328",
  "choice": "D",
  "answer": "Auditable"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328-E",
  "QuestionId": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328",
  "choice": "E",
  "answer": "Reliable"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328-F",
  "QuestionId": "SOP.a5ef2a40-7588-45fb-8a0f-7434cad5e328",
  "choice": "F",
  "answer": "Believable"
 },
 {
  "type": "ANSWER",
  "id": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70-A",
  "QuestionId": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70",
  "choice": "A",
  "answer": "TCP Syn Flood"
 },
 {
  "type": "ANSWER",
  "id": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70-B",
  "QuestionId": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70",
  "choice": "B",
  "answer": "Fraggle"
 },
 {
  "type": "ANSWER",
  "id": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70-C",
  "QuestionId": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70",
  "choice": "C",
  "answer": "LAND"
 },
 {
  "type": "ANSWER",
  "id": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70-D",
  "QuestionId": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70",
  "choice": "D",
  "answer": "Teardrop"
 },
 {
  "type": "ANSWER",
  "id": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70-E",
  "QuestionId": "SOP.84875bc9-012a-4cba-bbde-b6ccdfb21c70",
  "choice": "E",
  "answer": "MitM"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f-A",
  "QuestionId": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f",
  "choice": "A",
  "answer": "A performance baseline"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f-B",
  "QuestionId": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f",
  "choice": "B",
  "answer": "A security breach"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f-C",
  "QuestionId": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f",
  "choice": "C",
  "answer": "Defense in depth"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f-D",
  "QuestionId": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f",
  "choice": "D",
  "answer": "A clipping level"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f-E",
  "QuestionId": "SOP.4f8f5ffc-34f1-4045-b61f-322f93a2510f",
  "choice": "E",
  "answer": "Manadatory access control"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58-A",
  "QuestionId": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58",
  "choice": "A",
  "answer": "Platform as a Service"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58-B",
  "QuestionId": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58",
  "choice": "B",
  "answer": "Infrastructure as a Service"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58-C",
  "QuestionId": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58",
  "choice": "C",
  "answer": "Software as a Service"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58-D",
  "QuestionId": "SOP.ba6969f0-7697-4643-a976-e57fc4975c58",
  "choice": "D",
  "answer": "Storage as a Service"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a-A",
  "QuestionId": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a",
  "choice": "A",
  "answer": "Behaviour Based"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a-B",
  "QuestionId": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a",
  "choice": "B",
  "answer": "Knowledge Based"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a-C",
  "QuestionId": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a",
  "choice": "C",
  "answer": "Host Based"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a-D",
  "QuestionId": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a",
  "choice": "D",
  "answer": "Anonomoly Based"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a-E",
  "QuestionId": "SOP.0452b5db-fea5-4351-adf0-d7a526d2e71a",
  "choice": "E",
  "answer": "Application based"
 },
 {
  "type": "ANSWER",
  "id": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225-A",
  "QuestionId": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225",
  "choice": "A",
  "answer": "Communicate the effectives of the incident reporting system"
 },
 {
  "type": "ANSWER",
  "id": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225-B",
  "QuestionId": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225",
  "choice": "B",
  "answer": "Eliminate anonymity in reporting"
 },
 {
  "type": "ANSWER",
  "id": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225-C",
  "QuestionId": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225",
  "choice": "C",
  "answer": "Simplified process for report submission"
 },
 {
  "type": "ANSWER",
  "id": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225-D",
  "QuestionId": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225",
  "choice": "D",
  "answer": "Lack of clear communication on what needs to be reported"
 },
 {
  "type": "ANSWER",
  "id": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225-E",
  "QuestionId": "SOP.44255857-25aa-4a6a-9eb4-71ad49baa225",
  "choice": "E",
  "answer": "Feedback on previously reported results"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d-A",
  "QuestionId": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d",
  "choice": "A",
  "answer": "Use on NIST approved algorithms for hashing files"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d-B",
  "QuestionId": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d",
  "choice": "B",
  "answer": "Maintain chain of custody"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d-C",
  "QuestionId": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d",
  "choice": "C",
  "answer": "Never interact with live system"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d-D",
  "QuestionId": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d",
  "choice": "D",
  "answer": "Hash all the files before examing the original disk"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d-E",
  "QuestionId": "SOP.c75e5b90-ae79-4d90-b48b-ee47e3285e2d",
  "choice": "E",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2-A",
  "QuestionId": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2",
  "choice": "A",
  "answer": "Use SNMP to generate a list of allowed MACs for each VLAN"
 },
 {
  "type": "ANSWER",
  "id": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2-B",
  "QuestionId": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2",
  "choice": "B",
  "answer": "Implement ACLs on each router interface allowing only traffic sourced from the local segment."
 },
 {
  "type": "ANSWER",
  "id": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2-C",
  "QuestionId": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2",
  "choice": "C",
  "answer": "Enable MLD snooping on Layer2 switches"
 },
 {
  "type": "ANSWER",
  "id": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2-D",
  "QuestionId": "SOP.e630e3da-e229-4619-93a7-28ea550c98b2",
  "choice": "D",
  "answer": "Configure Reverse Path forwarding on the routers"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6-A",
  "QuestionId": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6",
  "choice": "A",
  "answer": "Class A"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6-B",
  "QuestionId": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6",
  "choice": "B",
  "answer": "Class B"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6-C",
  "QuestionId": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6",
  "choice": "C",
  "answer": "Class C"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6-D",
  "QuestionId": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6",
  "choice": "D",
  "answer": "Class D"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6-E",
  "QuestionId": "SOP.1fbf161f-7900-4be1-b11d-fb98662ba6f6",
  "choice": "E",
  "answer": "Class K"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3-A",
  "QuestionId": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3",
  "choice": "A",
  "answer": "A reduction in voltage that lasts minutes or hours"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3-B",
  "QuestionId": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3",
  "choice": "B",
  "answer": "A prolonged loss of electrical power"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3-C",
  "QuestionId": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3",
  "choice": "C",
  "answer": "A momentary low-voltage condition lasting only a few seconds"
 },
 {
  "type": "ANSWER",
  "id": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3-D",
  "QuestionId": "SOP.ee088061-84f4-48b5-91cc-babc1867cde3",
  "choice": "D",
  "answer": "A momentary loss of electrical power"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6-A",
  "QuestionId": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6",
  "choice": "A",
  "answer": "Network Diagrams and List of critical equipment"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6-B",
  "QuestionId": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6",
  "choice": "B",
  "answer": "Hashes of critical files"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6-C",
  "QuestionId": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6",
  "choice": "C",
  "answer": "Baseline documentation for systems, applications and network equipment"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6-D",
  "QuestionId": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6",
  "choice": "D",
  "answer": "Copies of well-known and common malware for comparison"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6-E",
  "QuestionId": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6",
  "choice": "E",
  "answer": "A laptop loaded with packet sniffers and forsenics software"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6-F",
  "QuestionId": "SAT.6cf35d92-9a2a-4e6c-bd59-792e655716d6",
  "choice": "F",
  "answer": "Drive Imaging Tools"
 },
 {
  "type": "ANSWER",
  "id": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125-A",
  "QuestionId": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125",
  "choice": "A",
  "answer": "Containment, Eradication and Recovery"
 },
 {
  "type": "ANSWER",
  "id": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125-B",
  "QuestionId": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125",
  "choice": "B",
  "answer": "Preparation"
 },
 {
  "type": "ANSWER",
  "id": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125-C",
  "QuestionId": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125",
  "choice": "C",
  "answer": "Detection and Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125-D",
  "QuestionId": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125",
  "choice": "D",
  "answer": "Prevention"
 },
 {
  "type": "ANSWER",
  "id": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125-E",
  "QuestionId": "SOP.afc3ac91-1a99-4fde-aa4d-08ad8e008125",
  "choice": "E",
  "answer": "Post-Incident Assessment"
 },
 {
  "type": "ANSWER",
  "id": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3-A",
  "QuestionId": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3",
  "choice": "A",
  "answer": "Mutual Authentication"
 },
 {
  "type": "ANSWER",
  "id": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3-B",
  "QuestionId": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3",
  "choice": "B",
  "answer": "Individual Accountability"
 },
 {
  "type": "ANSWER",
  "id": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3-C",
  "QuestionId": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3",
  "choice": "C",
  "answer": "Origin Authentication"
 },
 {
  "type": "ANSWER",
  "id": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3-D",
  "QuestionId": "SOP.146cdc14-e877-4a49-af2c-7d278b96dec3",
  "choice": "D",
  "answer": "Preserve Data Integrity"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d-A",
  "QuestionId": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d",
  "choice": "A",
  "answer": "A rogue DHCP server on your network can offer IP addresses to legitmate users, thereby creating DoS of MitM situation"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d-B",
  "QuestionId": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d",
  "choice": "B",
  "answer": "An attacker may gain control of your DHCP server and reconfigure the options assigned to clients."
 },
 {
  "type": "ANSWER",
  "id": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d-C",
  "QuestionId": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d",
  "choice": "C",
  "answer": "Using TCP redirects, an attacker can send client DHCP packets to a remote DHCP server"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d-D",
  "QuestionId": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d",
  "choice": "D",
  "answer": "A rogue DHCP server can be used to reconfigure SMTP connection settings for internal email systems."
 },
 {
  "type": "ANSWER",
  "id": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d-E",
  "QuestionId": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d",
  "choice": "E",
  "answer": "An attacker can request multiple IP addresses from the legitimate DHCP server thereby exhausing the IP address pool"
 },
 {
  "type": "ANSWER",
  "id": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d-F",
  "QuestionId": "SOP.1e47f560-7d52-42ee-95fb-bb5784399a9d",
  "choice": "F",
  "answer": "An attacker can send negative acknowledgements whenever a client attempts to renew an IP address"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67-A",
  "QuestionId": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67",
  "choice": "A",
  "answer": "The entry is null route to the hostname specified"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67-B",
  "QuestionId": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67",
  "choice": "B",
  "answer": "it is a sinkhole entry to protect the the Linux system"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67-C",
  "QuestionId": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67",
  "choice": "C",
  "answer": "The system has been comprised by an attacker"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67-D",
  "QuestionId": "SOP.9e0e361c-c974-4e01-a5ce-1279da996a67",
  "choice": "D",
  "answer": "It forces traffic to that site to be route out the interface 127.0.0.1"
 },
 {
  "type": "ANSWER",
  "id": "SOP.636823fd-5712-4388-a771-363eafd8e4ac-A",
  "QuestionId": "SOP.636823fd-5712-4388-a771-363eafd8e4ac",
  "choice": "A",
  "answer": "Encrypt the files to prevent unauthorized deletion."
 },
 {
  "type": "ANSWER",
  "id": "SOP.636823fd-5712-4388-a771-363eafd8e4ac-B",
  "QuestionId": "SOP.636823fd-5712-4388-a771-363eafd8e4ac",
  "choice": "B",
  "answer": "Configure user level auditing for files/folders"
 },
 {
  "type": "ANSWER",
  "id": "SOP.636823fd-5712-4388-a771-363eafd8e4ac-C",
  "QuestionId": "SOP.636823fd-5712-4388-a771-363eafd8e4ac",
  "choice": "C",
  "answer": "Digitally sign the files to prevent unauthorized modification."
 },
 {
  "type": "ANSWER",
  "id": "SOP.636823fd-5712-4388-a771-363eafd8e4ac-D",
  "QuestionId": "SOP.636823fd-5712-4388-a771-363eafd8e4ac",
  "choice": "D",
  "answer": "Set appropriate file/folder permissions in the file system."
 },
 {
  "type": "ANSWER",
  "id": "SOP.636823fd-5712-4388-a771-363eafd8e4ac-E",
  "QuestionId": "SOP.636823fd-5712-4388-a771-363eafd8e4ac",
  "choice": "E",
  "answer": "Maintain backsup of the data."
 },
 {
  "type": "ANSWER",
  "id": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574-A",
  "QuestionId": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574",
  "choice": "A",
  "answer": "They control access based on source IP address and cannot verify if the address is being spoofed."
 },
 {
  "type": "ANSWER",
  "id": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574-B",
  "QuestionId": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574",
  "choice": "B",
  "answer": "They use reverse path forwarding lookups."
 },
 {
  "type": "ANSWER",
  "id": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574-C",
  "QuestionId": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574",
  "choice": "C",
  "answer": "They are stateless"
 },
 {
  "type": "ANSWER",
  "id": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574-D",
  "QuestionId": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574",
  "choice": "D",
  "answer": "They do not support logging packets that match firewall rules."
 },
 {
  "type": "ANSWER",
  "id": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574-E",
  "QuestionId": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574",
  "choice": "E",
  "answer": "They are stateful."
 },
 {
  "type": "ANSWER",
  "id": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574-F",
  "QuestionId": "SOP.40b0d7db-49a3-4540-af57-353c98f8a574",
  "choice": "F",
  "answer": "They defend against TCP syn flood attacks which reduces there effective throughtput."
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-A",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "A",
  "answer": "BGP"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-B",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "B",
  "answer": "RADIUS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-C",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "C",
  "answer": "SMB"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-D",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "D",
  "answer": "IMAP4"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-E",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "E",
  "answer": "SNMP,"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-F",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "F",
  "answer": "NTP"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-G",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "G",
  "answer": "TFTP"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-H",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "H",
  "answer": "DNS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-I",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "I",
  "answer": "SMTP"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184-J",
  "QuestionId": "SOP.d2d2d072-d5a0-440a-834e-78756b6c9184",
  "choice": "J",
  "answer": "DHCP"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9-A",
  "QuestionId": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9",
  "choice": "A",
  "answer": "Version Control Mechanism"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9-B",
  "QuestionId": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9",
  "choice": "B",
  "answer": "System Threat Modeling"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9-C",
  "QuestionId": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9",
  "choice": "C",
  "answer": "Automated Deployment Scripts"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9-D",
  "QuestionId": "SOP.3a546dca-ae00-4f0a-aa87-3fc474ac64f9",
  "choice": "D",
  "answer": "Change history logging"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6-A",
  "QuestionId": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6",
  "choice": "A",
  "answer": "Differential begin with a full back and incrementals do not"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6-B",
  "QuestionId": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6",
  "choice": "B",
  "answer": "Incremental backups do not evalate the archive bit when determining if a file should be backed up"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6-C",
  "QuestionId": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6",
  "choice": "C",
  "answer": "Differentials backups only backup files modified since the previous differenetial of full backup"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6-D",
  "QuestionId": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6",
  "choice": "D",
  "answer": "Compared to differentials, a complete restore will take longer if using an incremental strategy"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6-E",
  "QuestionId": "SOP.3e2ec968-351f-4fbd-a258-a9e6f949c4a6",
  "choice": "E",
  "answer": "A 'copy' backup cannot be used if using a differential backup strategy"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c-A",
  "QuestionId": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c",
  "choice": "A",
  "answer": "Maximum amount of time a business process can be unavailable"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c-B",
  "QuestionId": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c",
  "choice": "B",
  "answer": "Maximum amount of time to recover a business process"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c-C",
  "QuestionId": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c",
  "choice": "C",
  "answer": "The amount of time needed to verify a system/data after recovery"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c-D",
  "QuestionId": "SOP.8b1dc255-f09c-42cb-91f9-3681bf4bdd7c",
  "choice": "D",
  "answer": "Measure of time indicating the maximum amount of data that can be lost"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1-A",
  "QuestionId": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1",
  "choice": "A",
  "answer": "Civil"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1-B",
  "QuestionId": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1",
  "choice": "B",
  "answer": "Operational"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1-C",
  "QuestionId": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1",
  "choice": "C",
  "answer": "Regulatory"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1-D",
  "QuestionId": "SOP.2fd0039e-5158-46ba-be95-802cb7cc70f1",
  "choice": "D",
  "answer": "Criminal"
 },
 {
  "type": "ANSWER",
  "id": "SOP.96644989-2185-4805-a905-2245e27ebe5c-A",
  "QuestionId": "SOP.96644989-2185-4805-a905-2245e27ebe5c",
  "choice": "A",
  "answer": "Testimonial"
 },
 {
  "type": "ANSWER",
  "id": "SOP.96644989-2185-4805-a905-2245e27ebe5c-B",
  "QuestionId": "SOP.96644989-2185-4805-a905-2245e27ebe5c",
  "choice": "B",
  "answer": "Documentary"
 },
 {
  "type": "ANSWER",
  "id": "SOP.96644989-2185-4805-a905-2245e27ebe5c-C",
  "QuestionId": "SOP.96644989-2185-4805-a905-2245e27ebe5c",
  "choice": "C",
  "answer": "Expert Opinion"
 },
 {
  "type": "ANSWER",
  "id": "SOP.96644989-2185-4805-a905-2245e27ebe5c-D",
  "QuestionId": "SOP.96644989-2185-4805-a905-2245e27ebe5c",
  "choice": "D",
  "answer": "Real"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85-A",
  "QuestionId": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85",
  "choice": "A",
  "answer": "Process Information"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85-B",
  "QuestionId": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85",
  "choice": "B",
  "answer": "Logs"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85-C",
  "QuestionId": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85",
  "choice": "C",
  "answer": "Memory Contents"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85-D",
  "QuestionId": "SOP.5dad8d56-1dec-48cc-b8e2-eb6d83296b85",
  "choice": "D",
  "answer": "Temporary Files"
 },
 {
  "type": "ANSWER",
  "id": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624-A",
  "QuestionId": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624",
  "choice": "A",
  "answer": "hashing"
 },
 {
  "type": "ANSWER",
  "id": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624-B",
  "QuestionId": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624",
  "choice": "B",
  "answer": "evidence log"
 },
 {
  "type": "ANSWER",
  "id": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624-C",
  "QuestionId": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624",
  "choice": "C",
  "answer": "sealed container"
 },
 {
  "type": "ANSWER",
  "id": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624-D",
  "QuestionId": "SOP.af4f8e91-3de9-4cec-aa0c-cbcabea7e624",
  "choice": "D",
  "answer": "write blocker"
 },
 {
  "type": "ANSWER",
  "id": "SOP.42671402-184d-4df4-a367-c268184a5df9-A",
  "QuestionId": "SOP.42671402-184d-4df4-a367-c268184a5df9",
  "choice": "A",
  "answer": "Packet Contents"
 },
 {
  "type": "ANSWER",
  "id": "SOP.42671402-184d-4df4-a367-c268184a5df9-B",
  "QuestionId": "SOP.42671402-184d-4df4-a367-c268184a5df9",
  "choice": "B",
  "answer": "Destination Address"
 },
 {
  "type": "ANSWER",
  "id": "SOP.42671402-184d-4df4-a367-c268184a5df9-C",
  "QuestionId": "SOP.42671402-184d-4df4-a367-c268184a5df9",
  "choice": "C",
  "answer": "Source Address"
 },
 {
  "type": "ANSWER",
  "id": "SOP.42671402-184d-4df4-a367-c268184a5df9-D",
  "QuestionId": "SOP.42671402-184d-4df4-a367-c268184a5df9",
  "choice": "D",
  "answer": "Amount of Data Transfered"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2993d07c-eb9a-4ffc-af14-8aa0b055d57c-A",
  "QuestionId": "SOP.2993d07c-eb9a-4ffc-af14-8aa0b055d57c",
  "choice": "A",
  "answer": "True"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2993d07c-eb9a-4ffc-af14-8aa0b055d57c-B",
  "QuestionId": "SOP.2993d07c-eb9a-4ffc-af14-8aa0b055d57c",
  "choice": "B",
  "answer": "False"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe-A",
  "QuestionId": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe",
  "choice": "A",
  "answer": "Collection"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe-B",
  "QuestionId": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe",
  "choice": "B",
  "answer": "Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe-C",
  "QuestionId": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe",
  "choice": "C",
  "answer": "Production"
 },
 {
  "type": "ANSWER",
  "id": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe-D",
  "QuestionId": "SOP.8877bf13-3d52-43b6-abd1-aca09eaf87fe",
  "choice": "D",
  "answer": "Preservation"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7-A",
  "QuestionId": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7",
  "choice": "A",
  "answer": "Analyze/Report"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7-B",
  "QuestionId": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7",
  "choice": "B",
  "answer": "Establish"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7-C",
  "QuestionId": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7",
  "choice": "C",
  "answer": "Implement"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7-D",
  "QuestionId": "SOP.b0b02852-24ed-4cd7-8942-69c169f48cb7",
  "choice": "D",
  "answer": "Define"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f0a0275e-3b63-4ecb-9912-181365697090-A",
  "QuestionId": "SOP.f0a0275e-3b63-4ecb-9912-181365697090",
  "choice": "A",
  "answer": "Host Base DLP"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f0a0275e-3b63-4ecb-9912-181365697090-B",
  "QuestionId": "SOP.f0a0275e-3b63-4ecb-9912-181365697090",
  "choice": "B",
  "answer": "watermarking"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f0a0275e-3b63-4ecb-9912-181365697090-C",
  "QuestionId": "SOP.f0a0275e-3b63-4ecb-9912-181365697090",
  "choice": "C",
  "answer": "intrusion detection"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f0a0275e-3b63-4ecb-9912-181365697090-D",
  "QuestionId": "SOP.f0a0275e-3b63-4ecb-9912-181365697090",
  "choice": "D",
  "answer": "Pattern recogniation"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b-A",
  "QuestionId": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b",
  "choice": "A",
  "answer": "Type 4"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b-B",
  "QuestionId": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b",
  "choice": "B",
  "answer": "Type 1"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b-C",
  "QuestionId": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b",
  "choice": "C",
  "answer": "Type 2"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b-D",
  "QuestionId": "SOP.c6465ae8-e1bf-482d-ad2d-d23aaeadcc4b",
  "choice": "D",
  "answer": "Type 3"
 },
 {
  "type": "ANSWER",
  "id": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6-A",
  "QuestionId": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6",
  "choice": "A",
  "answer": "Patching the hypervisor"
 },
 {
  "type": "ANSWER",
  "id": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6-B",
  "QuestionId": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6",
  "choice": "B",
  "answer": "Patching Firewalls"
 },
 {
  "type": "ANSWER",
  "id": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6-C",
  "QuestionId": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6",
  "choice": "C",
  "answer": "Patching Apps"
 },
 {
  "type": "ANSWER",
  "id": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6-D",
  "QuestionId": "SOP.6bffd6bc-ccf6-4d3b-9844-7c6afd052ca6",
  "choice": "D",
  "answer": "Patching the OS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc-A",
  "QuestionId": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc",
  "choice": "A",
  "answer": "IaaS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc-B",
  "QuestionId": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc",
  "choice": "B",
  "answer": "IDaaS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc-C",
  "QuestionId": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc",
  "choice": "C",
  "answer": "SaaS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc-D",
  "QuestionId": "SOP.385523fb-1054-43b9-8a87-c0cfa21d1bfc",
  "choice": "D",
  "answer": "PaaS"
 },
 {
  "type": "ANSWER",
  "id": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7-A",
  "QuestionId": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7",
  "choice": "A",
  "answer": "Two person control"
 },
 {
  "type": "ANSWER",
  "id": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7-B",
  "QuestionId": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7",
  "choice": "B",
  "answer": "separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7-C",
  "QuestionId": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7",
  "choice": "C",
  "answer": "least privilege"
 },
 {
  "type": "ANSWER",
  "id": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7-D",
  "QuestionId": "SOP.364679fc-a38a-458e-88bf-59b2d361b8d7",
  "choice": "D",
  "answer": "need to known"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f-A",
  "QuestionId": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f",
  "choice": "A",
  "answer": "2 person control"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f-B",
  "QuestionId": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f",
  "choice": "B",
  "answer": "separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f-C",
  "QuestionId": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f",
  "choice": "C",
  "answer": "least privilege"
 },
 {
  "type": "ANSWER",
  "id": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f-D",
  "QuestionId": "SOP.0ca091d6-bcf5-4cfa-9a3b-36d171b6495f",
  "choice": "D",
  "answer": "need to know"
 },
 {
  "type": "ANSWER",
  "id": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a-A",
  "QuestionId": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a",
  "choice": "A",
  "answer": "Legal Counsel"
 },
 {
  "type": "ANSWER",
  "id": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a-B",
  "QuestionId": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a",
  "choice": "B",
  "answer": "Human Resource Staff"
 },
 {
  "type": "ANSWER",
  "id": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a-C",
  "QuestionId": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a",
  "choice": "C",
  "answer": "Information security professional"
 },
 {
  "type": "ANSWER",
  "id": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a-D",
  "QuestionId": "SOP.e0d10f59-044d-4d65-a36e-120ca32df26a",
  "choice": "D",
  "answer": "CEO"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a-A",
  "QuestionId": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a",
  "choice": "A",
  "answer": "Identifying Root Cause"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a-B",
  "QuestionId": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a",
  "choice": "B",
  "answer": "Containing the damage"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a-C",
  "QuestionId": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a",
  "choice": "C",
  "answer": "Collecting Evidence"
 },
 {
  "type": "ANSWER",
  "id": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a-D",
  "QuestionId": "SOP.2f0bac6f-a8d6-4a2a-9014-8a85f7f56f6a",
  "choice": "D",
  "answer": "Restoring operations"
 },
 {
  "type": "ANSWER",
  "id": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc-A",
  "QuestionId": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc",
  "choice": "A",
  "answer": "Nmap"
 },
 {
  "type": "ANSWER",
  "id": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc-B",
  "QuestionId": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc",
  "choice": "B",
  "answer": "Nessus"
 },
 {
  "type": "ANSWER",
  "id": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc-C",
  "QuestionId": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc",
  "choice": "C",
  "answer": "Metasploit"
 },
 {
  "type": "ANSWER",
  "id": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc-D",
  "QuestionId": "SAT.84238ca0-19f9-49fb-adc2-da62d00c76fc",
  "choice": "D",
  "answer": "lsof"
 },
 {
  "type": "ANSWER",
  "id": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069-A",
  "QuestionId": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069",
  "choice": "A",
  "answer": "80/filtered"
 },
 {
  "type": "ANSWER",
  "id": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069-B",
  "QuestionId": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069",
  "choice": "B",
  "answer": "22/filtered"
 },
 {
  "type": "ANSWER",
  "id": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069-C",
  "QuestionId": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069",
  "choice": "C",
  "answer": "433/open"
 },
 {
  "type": "ANSWER",
  "id": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069-D",
  "QuestionId": "SAT.7ea1883d-05a4-4d7e-a6d7-d37eadd8b069",
  "choice": "D",
  "answer": "1433/open"
 },
 {
  "type": "ANSWER",
  "id": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab-A",
  "QuestionId": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab",
  "choice": "A",
  "answer": "Sensitivity of the information stored on the system"
 },
 {
  "type": "ANSWER",
  "id": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab-B",
  "QuestionId": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab",
  "choice": "B",
  "answer": "Difficulty of performing the test"
 },
 {
  "type": "ANSWER",
  "id": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab-C",
  "QuestionId": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab",
  "choice": "C",
  "answer": "Desire to experiment with new testing tools"
 },
 {
  "type": "ANSWER",
  "id": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab-D",
  "QuestionId": "SAT.114ed533-cc87-491d-aa36-000d5c3f4dab",
  "choice": "D",
  "answer": "Desirability of the system to attackers"
 },
 {
  "type": "ANSWER",
  "id": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f-A",
  "QuestionId": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f",
  "choice": "A",
  "answer": "Vulnerability Scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f-B",
  "QuestionId": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f",
  "choice": "B",
  "answer": "Risk assessment"
 },
 {
  "type": "ANSWER",
  "id": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f-C",
  "QuestionId": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f",
  "choice": "C",
  "answer": "Mitigation Vulnerabilities"
 },
 {
  "type": "ANSWER",
  "id": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f-D",
  "QuestionId": "SAT.3471e7d9-8b8e-48cd-8ab1-aed94e2b003f",
  "choice": "D",
  "answer": "Threat assessment"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d-A",
  "QuestionId": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d",
  "choice": "A",
  "answer": "Management"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d-B",
  "QuestionId": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d",
  "choice": "B",
  "answer": "Security Auditor"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d-C",
  "QuestionId": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d",
  "choice": "C",
  "answer": "Security Professional"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d-D",
  "QuestionId": "SAT.e7317cc1-68b2-4199-a532-64bbff150c0d",
  "choice": "D",
  "answer": "Customers"
 },
 {
  "type": "ANSWER",
  "id": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d-A",
  "QuestionId": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d",
  "choice": "A",
  "answer": "10.0.0.0/0"
 },
 {
  "type": "ANSWER",
  "id": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d-B",
  "QuestionId": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d",
  "choice": "B",
  "answer": "10.0.0.0/8"
 },
 {
  "type": "ANSWER",
  "id": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d-C",
  "QuestionId": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d",
  "choice": "C",
  "answer": "10.0.0.0/16"
 },
 {
  "type": "ANSWER",
  "id": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d-D",
  "QuestionId": "SAT.aa0ddee5-f507-4c98-be38-5fef27e7e05d",
  "choice": "D",
  "answer": "10.0.0.0/24"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea-A",
  "QuestionId": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea",
  "choice": "A",
  "answer": "SSH"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea-B",
  "QuestionId": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea",
  "choice": "B",
  "answer": "Web Browser"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea-C",
  "QuestionId": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea",
  "choice": "C",
  "answer": "telnet"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea-D",
  "QuestionId": "SAT.ccbcfaea-b581-4729-b151-e072fa8064ea",
  "choice": "D",
  "answer": "ping"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c-A",
  "QuestionId": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c",
  "choice": "A",
  "answer": "20"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c-B",
  "QuestionId": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c",
  "choice": "B",
  "answer": "22"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c-C",
  "QuestionId": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c",
  "choice": "C",
  "answer": "25"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c-D",
  "QuestionId": "SAT.f73b8168-e059-40fb-9c90-21dbe7982e1c",
  "choice": "D",
  "answer": "80"
 },
 {
  "type": "ANSWER",
  "id": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69-A",
  "QuestionId": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69",
  "choice": "A",
  "answer": "Unauthenticated scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69-B",
  "QuestionId": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69",
  "choice": "B",
  "answer": "Port scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69-C",
  "QuestionId": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69",
  "choice": "C",
  "answer": "Half-open scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69-D",
  "QuestionId": "SAT.61a46f1d-286d-4dba-8d71-398e9fafcd69",
  "choice": "D",
  "answer": "Authenticated scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b-A",
  "QuestionId": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b",
  "choice": "A",
  "answer": "TCP connection scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b-B",
  "QuestionId": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b",
  "choice": "B",
  "answer": "Xmas scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b-C",
  "QuestionId": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b",
  "choice": "C",
  "answer": "TCP Syn Scan"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b-D",
  "QuestionId": "SAT.6a97e3f9-f4a9-4b0f-9f4f-5ba6cc71d95b",
  "choice": "D",
  "answer": "TCP Ack San"
 },
 {
  "type": "ANSWER",
  "id": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7-A",
  "QuestionId": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7",
  "choice": "A",
  "answer": "Port Scanner"
 },
 {
  "type": "ANSWER",
  "id": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7-B",
  "QuestionId": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7",
  "choice": "B",
  "answer": "Network Vulnerablity Scanner"
 },
 {
  "type": "ANSWER",
  "id": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7-C",
  "QuestionId": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7",
  "choice": "C",
  "answer": "Network discovery scanner"
 },
 {
  "type": "ANSWER",
  "id": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7-D",
  "QuestionId": "SAT.c16f7cdf-a681-48ca-9648-4fbf0cc068a7",
  "choice": "D",
  "answer": "Web vulnerablity scanner"
 },
 {
  "type": "ANSWER",
  "id": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7-A",
  "QuestionId": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7",
  "choice": "A",
  "answer": "Only if the appication changes"
 },
 {
  "type": "ANSWER",
  "id": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7-B",
  "QuestionId": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7",
  "choice": "B",
  "answer": "At least monthly"
 },
 {
  "type": "ANSWER",
  "id": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7-C",
  "QuestionId": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7",
  "choice": "C",
  "answer": "At least annually"
 },
 {
  "type": "ANSWER",
  "id": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7-D",
  "QuestionId": "SAT.21ab0d2d-b99b-4b5e-9d57-2f8e7e556ab7",
  "choice": "D",
  "answer": "There is no rescanning requirement"
 },
 {
  "type": "ANSWER",
  "id": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e-A",
  "QuestionId": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e",
  "choice": "A",
  "answer": "nnap"
 },
 {
  "type": "ANSWER",
  "id": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e-B",
  "QuestionId": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e",
  "choice": "B",
  "answer": "metasploit"
 },
 {
  "type": "ANSWER",
  "id": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e-C",
  "QuestionId": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e",
  "choice": "C",
  "answer": "nessus"
 },
 {
  "type": "ANSWER",
  "id": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e-D",
  "QuestionId": "SAT.03364d32-b755-42f9-a74c-5db90d774e5e",
  "choice": "D",
  "answer": "snort"
 },
 {
  "type": "ANSWER",
  "id": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709-A",
  "QuestionId": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709",
  "choice": "A",
  "answer": "Code Review"
 },
 {
  "type": "ANSWER",
  "id": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709-B",
  "QuestionId": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709",
  "choice": "B",
  "answer": "Application Vulnerability Review"
 },
 {
  "type": "ANSWER",
  "id": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709-C",
  "QuestionId": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709",
  "choice": "C",
  "answer": "Mutation Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709-D",
  "QuestionId": "SAT.01b1e58a-4bd5-401b-aa4e-f3c13310d709",
  "choice": "D",
  "answer": "Generational Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b-A",
  "QuestionId": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b",
  "choice": "A",
  "answer": "Misuse case testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b-B",
  "QuestionId": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b",
  "choice": "B",
  "answer": "user interface testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b-C",
  "QuestionId": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b",
  "choice": "C",
  "answer": "physical interface testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b-D",
  "QuestionId": "SAT.e43305b3-c5f3-4bbd-9a28-4430bd31496b",
  "choice": "D",
  "answer": "security inteface testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831-A",
  "QuestionId": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831",
  "choice": "A",
  "answer": "Application Programming Interface Testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831-B",
  "QuestionId": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831",
  "choice": "B",
  "answer": "User interface testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831-C",
  "QuestionId": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831",
  "choice": "C",
  "answer": "Physical Interface Testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831-D",
  "QuestionId": "SAT.30bb57df-122c-4c2d-b9da-2050bd254831",
  "choice": "D",
  "answer": "Security Interface Testing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3-A",
  "QuestionId": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3",
  "choice": "A",
  "answer": "Black Box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3-B",
  "QuestionId": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3",
  "choice": "B",
  "answer": "White Box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3-C",
  "QuestionId": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3",
  "choice": "C",
  "answer": "Gray Box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3-D",
  "QuestionId": "SAT.6095e0f2-9330-465b-8784-87654cdfcdb3",
  "choice": "D",
  "answer": "Red Box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46-A",
  "QuestionId": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46",
  "choice": "A",
  "answer": "Inspection"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46-B",
  "QuestionId": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46",
  "choice": "B",
  "answer": "Rework"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46-C",
  "QuestionId": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46",
  "choice": "C",
  "answer": "Follow-Up"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46-D",
  "QuestionId": "SAT.ce73bd95-5fd9-4064-97c7-e622a5f5cd46",
  "choice": "D",
  "answer": "None of the above"
 },
 {
  "type": "ANSWER",
  "id": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6-A",
  "QuestionId": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6",
  "choice": "A",
  "answer": "Account Management"
 },
 {
  "type": "ANSWER",
  "id": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6-B",
  "QuestionId": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6",
  "choice": "B",
  "answer": "Backup Verification"
 },
 {
  "type": "ANSWER",
  "id": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6-C",
  "QuestionId": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6",
  "choice": "C",
  "answer": "Log Review"
 },
 {
  "type": "ANSWER",
  "id": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6-D",
  "QuestionId": "SAT.1751890d-9647-4ee7-9706-576d6f0669f6",
  "choice": "D",
  "answer": "Key Performance Indictors"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463-A",
  "QuestionId": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463",
  "choice": "A",
  "answer": "Signature Detection"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463-B",
  "QuestionId": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463",
  "choice": "B",
  "answer": "Heuristic Detection"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463-C",
  "QuestionId": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463",
  "choice": "C",
  "answer": "Data integrity assurance"
 },
 {
  "type": "ANSWER",
  "id": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463-D",
  "QuestionId": "SOP.4f2c5d0f-ebfc-44a0-a264-ffcad16bc463",
  "choice": "D",
  "answer": "Automated Reconstruction"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444-A",
  "QuestionId": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444",
  "choice": "A",
  "answer": "Internet"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444-B",
  "QuestionId": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444",
  "choice": "B",
  "answer": "DMZ"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444-C",
  "QuestionId": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444",
  "choice": "C",
  "answer": "Intranet"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444-D",
  "QuestionId": "SOP.c243ab0b-1533-44e1-97bb-36d7467c0444",
  "choice": "D",
  "answer": "Sandbox"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fcff942-b99a-409e-950b-b72be6942100-A",
  "QuestionId": "SOP.3fcff942-b99a-409e-950b-b72be6942100",
  "choice": "A",
  "answer": "Smurf"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fcff942-b99a-409e-950b-b72be6942100-B",
  "QuestionId": "SOP.3fcff942-b99a-409e-950b-b72be6942100",
  "choice": "B",
  "answer": "TOCTTOU"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fcff942-b99a-409e-950b-b72be6942100-C",
  "QuestionId": "SOP.3fcff942-b99a-409e-950b-b72be6942100",
  "choice": "C",
  "answer": "Land"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fcff942-b99a-409e-950b-b72be6942100-D",
  "QuestionId": "SOP.3fcff942-b99a-409e-950b-b72be6942100",
  "choice": "D",
  "answer": "Fraggle"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d-A",
  "QuestionId": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d",
  "choice": "A",
  "answer": "Zero-day exploit"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d-B",
  "QuestionId": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d",
  "choice": "B",
  "answer": "Social Engineering"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d-C",
  "QuestionId": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d",
  "choice": "C",
  "answer": "Trojan Horse"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d-D",
  "QuestionId": "SOP.c0769ce8-16e4-4ffd-9bdc-03796b72e30d",
  "choice": "D",
  "answer": "SQL Injection"
 },
 {
  "type": "ANSWER",
  "id": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f-A",
  "QuestionId": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f",
  "choice": "A",
  "answer": "Polymorphism"
 },
 {
  "type": "ANSWER",
  "id": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f-B",
  "QuestionId": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f",
  "choice": "B",
  "answer": "Stealth"
 },
 {
  "type": "ANSWER",
  "id": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f-C",
  "QuestionId": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f",
  "choice": "C",
  "answer": "Encryption"
 },
 {
  "type": "ANSWER",
  "id": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f-D",
  "QuestionId": "SOP.19d1f13f-d5f2-40a5-a9e6-af3e49667c5f",
  "choice": "D",
  "answer": "Multipartitism"
 },
 {
  "type": "ANSWER",
  "id": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95-A",
  "QuestionId": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95",
  "choice": "A",
  "answer": "LastPass"
 },
 {
  "type": "ANSWER",
  "id": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95-B",
  "QuestionId": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95",
  "choice": "B",
  "answer": "Crack"
 },
 {
  "type": "ANSWER",
  "id": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95-C",
  "QuestionId": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95",
  "choice": "C",
  "answer": "Shadow password files"
 },
 {
  "type": "ANSWER",
  "id": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95-D",
  "QuestionId": "SOP.158ee2fe-1418-4682-ad69-01141fa18d95",
  "choice": "D",
  "answer": "Tripwire"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a-A",
  "QuestionId": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a",
  "choice": "A",
  "answer": "Rootkit"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a-B",
  "QuestionId": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a",
  "choice": "B",
  "answer": "Back door"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a-C",
  "QuestionId": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a",
  "choice": "C",
  "answer": "TOCTOU"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a-D",
  "QuestionId": "SOP.3aebeb50-1594-44b7-8432-c44833a4d67a",
  "choice": "D",
  "answer": "Buffer Overflow"
 },
 {
  "type": "ANSWER",
  "id": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab-A",
  "QuestionId": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab",
  "choice": "A",
  "answer": "Hashing"
 },
 {
  "type": "ANSWER",
  "id": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab-B",
  "QuestionId": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab",
  "choice": "B",
  "answer": "Salting"
 },
 {
  "type": "ANSWER",
  "id": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab-C",
  "QuestionId": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab",
  "choice": "C",
  "answer": "Digital Signature"
 },
 {
  "type": "ANSWER",
  "id": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab-D",
  "QuestionId": "SOP.11b4470a-bbd9-4143-ae1c-f3386b956cab",
  "choice": "D",
  "answer": "Transport Encryption"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658-A",
  "QuestionId": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658",
  "choice": "A",
  "answer": "Exclamation"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658-B",
  "QuestionId": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658",
  "choice": "B",
  "answer": "Ampersand"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658-C",
  "QuestionId": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658",
  "choice": "C",
  "answer": "Quote"
 },
 {
  "type": "ANSWER",
  "id": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658-D",
  "QuestionId": "SOP.9fe60b3a-6f02-4b31-983a-3b7a1d639658",
  "choice": "D",
  "answer": "Apostrophy"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04-A",
  "QuestionId": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04",
  "choice": "A",
  "answer": "Triggers"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04-B",
  "QuestionId": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04",
  "choice": "B",
  "answer": "Stored Procedure"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04-C",
  "QuestionId": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04",
  "choice": "C",
  "answer": "Column Encryption"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04-D",
  "QuestionId": "SOP.f16fc34a-034b-4c2b-a55f-0021aa908c04",
  "choice": "D",
  "answer": "Concurrency Control"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1-A",
  "QuestionId": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1",
  "choice": "A",
  "answer": "Reflected Input"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1-B",
  "QuestionId": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1",
  "choice": "B",
  "answer": "Database driven content"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1-C",
  "QuestionId": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1",
  "choice": "C",
  "answer": ".Net Technology"
 },
 {
  "type": "ANSWER",
  "id": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1-D",
  "QuestionId": "SOP.5241b832-13e2-417b-ae1e-f9adef03c0e1",
  "choice": "D",
  "answer": "CGI Scripts"
 },
 {
  "type": "ANSWER",
  "id": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e-A",
  "QuestionId": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e",
  "choice": "A",
  "answer": "Stealth Virus"
 },
 {
  "type": "ANSWER",
  "id": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e-B",
  "QuestionId": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e",
  "choice": "B",
  "answer": "Companion Virus"
 },
 {
  "type": "ANSWER",
  "id": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e-C",
  "QuestionId": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e",
  "choice": "C",
  "answer": "Polymorhic Virus"
 },
 {
  "type": "ANSWER",
  "id": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e-D",
  "QuestionId": "SOP.73d05727-bc7c-4609-a87c-1b7a3ec9b29e",
  "choice": "D",
  "answer": "Multipartite virus"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75-A",
  "QuestionId": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75",
  "choice": "A",
  "answer": "Stuxnet"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75-B",
  "QuestionId": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75",
  "choice": "B",
  "answer": "Code Red"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75-C",
  "QuestionId": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75",
  "choice": "C",
  "answer": "Melissa"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75-D",
  "QuestionId": "SOP.d680d9d2-403b-495c-8656-4db8b8bdbe75",
  "choice": "D",
  "answer": "RTM"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0-A",
  "QuestionId": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0",
  "choice": "A",
  "answer": "Escalation of Privilege"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0-B",
  "QuestionId": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0",
  "choice": "B",
  "answer": "Back door"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0-C",
  "QuestionId": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0",
  "choice": "C",
  "answer": "Root kit"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0-D",
  "QuestionId": "SOP.f7042693-a796-4881-aba4-32b72fad9dd0",
  "choice": "D",
  "answer": "Buffer overflow"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37-A",
  "QuestionId": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37",
  "choice": "A",
  "answer": "Packets with internal source IP addresses don’t enter the network from the outside."
 },
 {
  "type": "ANSWER",
  "id": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37-B",
  "QuestionId": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37",
  "choice": "B",
  "answer": "Packets with internal source IP addresses don’t exit the network from the inside."
 },
 {
  "type": "ANSWER",
  "id": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37-C",
  "QuestionId": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37",
  "choice": "C",
  "answer": "Packets with public IP addresses don’t pass through the router in either direction."
 },
 {
  "type": "ANSWER",
  "id": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37-D",
  "QuestionId": "SOP.3225644f-3cf8-47a4-b102-e2186eea5b37",
  "choice": "D",
  "answer": "Packets with external source IP addresses don’t enter the network from the outside."
 }
]