export const Log = [
 {
  "type": "QUESTION",
  "id": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0",
  "question": "Which of the of the following is a VALID technique for attacking smartcard",
  "answer": "C",
  "explanation": "Apparently you can measure the power consumption of a device which reads a smart card. If you have the right keys the power usage goes up."
 },
 {
  "type": "QUESTION",
  "id": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5",
  "question": "Which 2 should you consider before moving your companies accounting system to a cloud saas?",
  "answer": "A, C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a",
  "question": "Which of the following is a key concern for ephemeral diffie-helman?",
  "answer": "B",
  "explanation": "There is no authentication involved in diffie helman so it needs to be augemented to support this.\n Diffier Helman is about 2 parties being able to establish a secret key over unsecure channel. It uses Public Private Key Crypto to acheive this."
 },
 {
  "type": "QUESTION",
  "id": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039",
  "question": "What is the best way to control data when you only want users in certain location to access?",
  "answer": "D",
  "explanation": "Location attribute can be provided"
 },
 {
  "type": "QUESTION",
  "id": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c",
  "question": "Which of the following is NOT a property of the bell-lapula security model",
  "answer": "D",
  "explanation": "Bell-Lapadula relates to confidentiality and not integrity."
 },
 {
  "type": "QUESTION",
  "id": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1",
  "question": "Which one of the following is a major part of Trusted Computer Base?",
  "answer": "D",
  "explanation": "The trusted computing base (TCB) of a computer system is the set of all hardware, firmware, and/or software components that are critical to its security\nA reference monitor is a system component that enforces access controls on an object."
 },
 {
  "type": "QUESTION",
  "id": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd",
  "question": "Which of the following are symmetric algorithms?",
  "answer": "A,D,E",
  "explanation": "Serpent, Blowfish are symmetric block ciphers\nRC5 is a symmetric stream cipher"
 },
 {
  "type": "QUESTION",
  "id": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a",
  "question": "You are sending an email ecrypted with a symmetric key. The symmetric key is encrypted using the recipients public key. What is the commmon term used to describe the encrypte message structure",
  "answer": "D",
  "explanation": "HMAC is taking a message, adding a shared secret and hashing it and sending"
 },
 {
  "type": "QUESTION",
  "id": "SAE.9249acaa-d979-4ace-be76-58d172682f38",
  "question": "You are using an encryption schema which generate seemingly random bits which are then XOR'd with the plaintext data into order to produce ciphertext. Which type of algorithm is this?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940",
  "question": "Confidentiality is a critical component of modern distributed computing systems. WHich of the following presents the greatest challenge to providing confidentiality for such an environment?",
  "answer": "F",
  "explanation": "other answers are problematic but doesnt directly related to confidentiality."
 },
 {
  "type": "QUESTION",
  "id": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a",
  "question": "Which of the following is an example of tunneling network traffic",
  "answer": "A",
  "explanation": "NAT-T is a technique to wrap an encrypted IPSEC packet in UDP (as an example). UDP will be supported by NAT and thus be routable whereas the IPSEC by itself is not.\nMasquerading is another name for NAT"
 },
 {
  "type": "QUESTION",
  "id": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12",
  "question": "Which of the following represents the best reason to upgrade your web application servers to TLS 1.3",
  "answer": "B",
  "explanation": "With earlier versions of TLS the server certificate is not encrypted which means that the server you are access is visible in plain text. This may not be desired.\n SNI lets a client specify a hostname for the IP address it is targeting which allow one IP address to support many names.\nTLS 1.3 actually drops support for a number of legacy algorthms."
 },
 {
  "type": "QUESTION",
  "id": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2",
  "question": "In an effort to increase the security of SSH on your server you implement, a technique that requires users to connect to 3 seemingly random ports before connecting to port 22. Which of the following best describes this techqniue?",
  "answer": "C",
  "explanation": "Port knocking is security by obsecurity."
 },
 {
  "type": "QUESTION",
  "id": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c",
  "question": "What is the term for hiding a message inside a large body of text",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.e319e162-8501-40cb-9acf-70599a7946f5",
  "question": "Which of the followingis not an example of a side-channel attack?",
  "answer": "C",
  "explanation": "ome examples of side-channel attacks that have been carried out on smart cards are differential power analysis (examining the power emissions released during processing), electromagnetic analysis (examining the frequencies emitted), and timing (how long a specific process takes to complete). These types of attacks are used to uncover sensitive information about how a component works without trying to compromise any type of flaw or weakness. They are commonly used for data collection."
 },
 {
  "type": "QUESTION",
  "id": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1",
  "question": "\"Subjects can access resources in domains of equal or lower trust levels.\" This is an easy sentence, but a difficult concept for many people to really understand.Which of the following is not an example of this concept?",
  "answer": "D",
  "explanation": "A subject can be a user, application, process or service – any active entity that isattempting to access a passive entity (object). Each of the answers providedexamples of how the more trusted subjects were given access to resources (a domain)that corresponded with its trust level. A Guest account does not have the trustlevel to access all administrator accounts."
 },
 {
  "type": "QUESTION",
  "id": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac",
  "question": "In the past 2 years your company has grown from locally maintained servers to a large number of cloud based instances. Managing system integrity has become more complex and error-prone process. As it relates to the this issue, which of the following broad approaches should you be implementing?",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9",
  "question": "Which of the following does a hot site have which a warm site does not?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "question": "Which of the following are characteristics of elliptic curve cryptopgraphy (choose 4)?",
  "answer": "A,C,D,G",
  "explanation": "ECC uses smaller keys which required less computation power."
 },
 {
  "type": "QUESTION",
  "id": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8",
  "question": "Which of the following is TRUE of  TLS, IPSEC and SSH?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "question": "Which of these key lengths is available in the Rjindael encryption algorithm?",
  "answer": "C,E,F",
  "explanation": "Rjindeal supports key size in increments of 32 but these 3 are the only ones implemented."
 },
 {
  "type": "QUESTION",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "question": "Mobile devices place an emphasis on battery consumption and frequently have limited processing power. What type of encryption is best suited to this",
  "answer": "E",
  "explanation": "ECC keys 12 times shorter than RSA keys are just as strong, so there is a efficiency savings"
 },
 {
  "type": "QUESTION",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "question": "Blowfish is an encryption algorthm originally designed as an alternative to DES. Which of the following are characteristics of Blowfish (Choose 4)?",
  "answer": "B,C,F,G",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.7f008538-eefc-445d-b284-e62c82f2b477",
  "question": "Which of the following algorithms are asymmetric",
  "answer": "D,E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "question": "Cryptography provides many different types of protections for information. When utilized correctly, which of the following represent things that cryptography can do (Choose 6)",
  "answer": "A,B,D,F,G,J",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c",
  "question": "A use hashes a file using SHA1. The user makes a small change, renames it and emails to a friend.  Hashing the new file would give a particular value.  6 months later an investigator wants to determine if the image file before change is located on the original users computer. Which technique might help determine this. Choose the best answer",
  "answer": "D",
  "explanation": "Fuzzy hashing - hashes part of the file and then can be used to calculate a percentage of similarity to another file"
 },
 {
  "type": "QUESTION",
  "id": "SRM.01ac8071-2c60-4081-b84b-185322c86e55",
  "question": "You have just received a digitally signed email message.What do you need in order to validate the integrity and authenticity of the message?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573",
  "question": "Which of the following best describes a zero day vulnerability?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31",
  "question": "A public facing web server has been infected with a kernel-level rootkit. Which of the following is the BEST recommendation on how the incident should be handled?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c",
  "question": "What is the length of a hash of a message produced with MD5 Hash?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08",
  "question": "Which of the following is not a certification",
  "answer": "E",
  "explanation": "The others are authorizations/accrediations\nAuthorization To Operate (ATO)\nInterim Authorization to Operate (IATO)\nInterim Authorization to Test (IATT)\nDenial of AUthorization to Operate (DATO)"
 },
 {
  "type": "QUESTION",
  "id": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b",
  "question": "What security rule says that no subject should be able to read data at a higher level than the subject's security clearance?",
  "answer": "C",
  "explanation": "Integrity - No Read down, No write up\nSecurity - No read up, no write down"
 },
 {
  "type": "QUESTION",
  "id": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98",
  "question": "What layers are the reponsibility of the customer in a 'platform as a service' cloud model?",
  "answer": "A,C",
  "explanation": "IAAS - Vendors manages hardware - Customer manages OS, application and data\nPAAS - Vendor manages hardware and OS - Customer manages application and data\nSAAS - Vendor manages hardware, OS, Application - Customer manages data"
 },
 {
  "type": "QUESTION",
  "id": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58",
  "question": "Which of the following are not approaches to prevent sql injection attacks?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672",
  "question": "What is the best defense against cross-site scripting attacks?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9",
  "question": "Which one of the following is not an effective defense against XSRF attacks?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda",
  "question": "What type of fuzz testing captures real software input and modifies it?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930",
  "question": "Your web application is suspectible to XSS but cannot be re-written. Which of the following is the best option for you?",
  "answer": "D",
  "explanation": "C is not the right answer because we dont just want detection. A WAF working as a web proxy can reject requests when it detects WAF."
 },
 {
  "type": "QUESTION",
  "id": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36",
  "question": "The core issues around BYOD relate to ",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9",
  "question": "What technology can you use as a compensating control when it's not possible to patch an embedded system?",
  "answer": "B",
  "explanation": "Security Wrappers monitor requests to IOT devices only letting valid requests through."
 },
 {
  "type": "QUESTION",
  "id": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045",
  "question": "What is the most important control to apply to smart devices?",
  "answer": "C",
  "explanation": "Same principle as Web DMZ. Isolate system."
 },
 {
  "type": "QUESTION",
  "id": "SAE.f7536883-e971-420b-89f0-6a2094cd5209",
  "question": "When can non-repudiation not be acheived?",
  "answer": "C",
  "explanation": "With symetric keys you can prove who generated anything."
 },
 {
  "type": "QUESTION",
  "id": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b",
  "question": "Bob is planning to use a cryptographic cipher that rearranges the characters in a message. What type of cipher is Bob planning to use?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6",
  "question": "Which of these is Kerckchoff principle?",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9",
  "question": "Using symmetric encryption when must the keys be regenerated?",
  "answer": "C,D",
  "explanation": "You must regenerate new keys for any key that the participant knew and distribute to the group."
 },
 {
  "type": "QUESTION",
  "id": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591",
  "question": "How many possible keys exist in a 4-bit key space?",
  "answer": "C",
  "explanation": "2 ^ 4"
 },
 {
  "type": "QUESTION",
  "id": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0",
  "question": "What is the length of the cryptographic key used in the Data Encryption Standard (DES) cryptosystem?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.d1f67934-7d75-411a-ba62-806323be13a0",
  "question": "Which one of the following cannot be achieved by a secret key cryptosystem?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20",
  "question": "What is the output value of the mathematical function 16 mod 3?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4",
  "question": "What block size is used by the 3DES encryption algorithm?",
  "answer": "B",
  "explanation": "3DES simply repeats the use of the DES algorithm three times. Therefore, it has the same block length as DES: 64 bits.\nChapple, Mike; Stewart, James Michael; Gibson, Darril. (ISC)2 CISSP Certified Information Systems Security Professional Official Study Guide (p. 959). Wiley. Kindle Edition."
 },
 {
  "type": "QUESTION",
  "id": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8",
  "question": "What is the minimum number of cryptographic keys required for secure two-way communications in symmetric key cryptography?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3",
  "question": "Dave is developing a key escrow system that requires multiple people to retrieve a key but does not depend on every participant being present. What type of technique is he using?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2",
  "question": "Which one of the following Data Encryption Standard (DES) operating modes can be used for large messages with the assurance that an error early in the encryption/decryption process won’t spoil results throughout the communication?",
  "answer": "D",
  "explanation": "Output feedback (OFB) mode prevents early errors from interfering with future encryption/decryption. Cipher Block Chaining and Cipher Feedback modes will carry errors throughout the entire encryption/decryption process. Electronic Code Book (ECB) operation is not suitable for large amounts of data."
 },
 {
  "type": "QUESTION",
  "id": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d",
  "question": "Many cryptographic algorithms rely on the difficulty of factoring the product of large prime numbers. What characteristic of this problem are they relying on?",
  "answer": "C",
  "explanation": "A one-way function is a mathematical operation that easily produces output values for each possible combination of inputs but makes it impossible to retrieve the input values."
 },
 {
  "type": "QUESTION",
  "id": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af",
  "question": "How many keys are required to fully implement a symmetric algorithm with 10 participants?",
  "answer": "C",
  "explanation": "n(n-1)/2"
 },
 {
  "type": "QUESTION",
  "id": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d",
  "question": "What type of cryptosystem commonly makes use of a passage from a well-known book for the encryption key?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4",
  "question": "Which AES finalist makes use of prewhitening and postwhitening techniques?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2",
  "question": "How many encryption keys are required to fully implement an asymmetric algorithm with 10 participants?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad",
  "question": "What key is actually used to encrypt the contents of a message when using PGP?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e",
  "question": "Which one of the following encryption approaches is most susceptible to a quantum computing attack?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776",
  "question": "The difficulty of solving what mathematical problem provides the security underlying the Diffie-Hellman algorithm?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135",
  "question": "In the early 1990s, the National Security Agency attempted to introduce key escrow using what failed technology?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80",
  "question": "What algorithm uses the Blowfish cipher along with a salt to strengthen cryptographic keys?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998",
  "question": "The BIBA integrity model endeavours to ensure data integrity by only allowing authorized modifications. To do so subject and objects are grouped into authorization levels. Which of the following is one of Biba rules?",
  "answer": "A",
  "explanation": [
   {
    "value": "Dont trust stuff read at a lower level in case its modified"
   },
   {
    "value": "Consequently dont write stuff for higher auth levels as they wont trust it"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf",
  "question": "In a computer system the ability for subject to interact with objects is controlled. Which of the following is responsible for exerting that control. Choose the best answer?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.379b6147-5315-4520-95b8-715299ab6497",
  "question": "When a user receives a digitally signed message which of these wont be true?",
  "answer": "B,E",
  "explanation": "While the others are true, the message content cant confirm which device sent a message and if the certificate does not include the senders email address you can be sure of the senders email address"
 },
 {
  "type": "QUESTION",
  "id": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23",
  "question": "Which one of the following is not a barrier to using the web of trust (WoT) approach?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239",
  "question": "Which one of the following is not a possible hash length from the SHA-2 function?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.82391306-4098-4b35-9da1-ada8728f2785",
  "question": "What standard governs the structure and content of digital certificates?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5",
  "question": "Harold works for a certificate authority and wants to ensure that his organization is able to revoke digital certificates that it creates. What is the most effective method of revoking digital certificates?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07",
  "question": "An Hashed Message Authentication Code (HMAC) does not allow which of the following",
  "answer": "B",
  "explanation": "Because it encrypts the hash with a secret key it doesnt support non-repdiation"
 },
 {
  "type": "QUESTION",
  "id": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e",
  "question": "Which of the following are digital signature algorithms",
  "answer": "B,C,D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf",
  "question": "As part of the rollout of smartcard system you have been tasked with providing a summary of features to senior management. Which of the following is true and could be included in the report",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd",
  "question": "How are the keys on the smart card used to authenticate the user? WHich is the best answer?",
  "answer": "D",
  "explanation": "A computer has separate access to your public certificate (from AD for instance).\nWhen you plug in your smart card the computer issues challenge (some text) which the smart card encryptes and returns back to computer.\nThe computer checks it can use your public key to decrypt and get back to the challenge"
 },
 {
  "type": "QUESTION",
  "id": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4",
  "question": "What of the following are not one the 5 fundamentals of a hash algorthm?",
  "answer": "D",
  "explanation": "Hashes provide variable length ouput"
 },
 {
  "type": "QUESTION",
  "id": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa",
  "question": "What is the length of SHA-1 vs SHA-2 and SHA-3 hash lengths?",
  "answer": "C",
  "explanation": "SHA-1 produces a 160-bit message digest whereas SHA-2 supports variable lengths, ranging up to 512 bits. SHA-3 improves upon the security of SHA-2 and supports the same hash lengths."
 },
 {
  "type": "QUESTION",
  "id": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c",
  "question": "What are the components of the DSS standard",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.f77ca056-76a1-4604-991f-43857ad69348",
  "question": "What approach does PGP take to encrypt emails",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560",
  "question": "What are the 2 modes of IPSEC",
  "answer": "B,D",
  "explanation": "In IPsec transport mode, packet contents are encrypted for peer-to-peer communication. In tunnel mode, the entire packet, including header information, is encrypted for gateway-to-gateway communications."
 },
 {
  "type": "QUESTION",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "question": "What is known type of encryption attack",
  "answer": "E",
  "explanation": "Brute-force attacks are attempts to randomly find the correct cryptographic key. Known plaintext, chosen ciphertext, and chosen plaintext attacks require the attacker to have some extra information in addition to the ciphertext. The meet-in-the-middle attack exploits protocols that use two rounds of encryption. The man-in-the-middle attack fools both parties into communicating with the attacker instead of directly with each other. The birthday attack is an attempt to find collisions in hash functions. The replay attack is an attempt to reuse authentication requests."
 },
 {
  "type": "QUESTION",
  "id": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608",
  "question": "What is an approach to data center design?",
  "answer": "C",
  "explanation": "HVAC units pull warm air from aisle which expel heat from compute devices"
 },
 {
  "type": "QUESTION",
  "id": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b",
  "question": "What is the maximum acceptable temperature for a data center?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8",
  "question": "Fires are typically classified by fuels that start them. Which of the following is associated with fires caused by flammable , liquids  like gasoline, petroleum oil or propone?",
  "answer": "B",
  "explanation": [
   {
    "value": "Class A- common cumbustible material - wood, trash"
   },
   {
    "value": "Class B - cumbustible liquids"
   },
   {
    "value": "Class C - Electrical"
   },
   {
    "value": "Class D - Combustible metals"
   },
   {
    "value": "Class K - Kitchen - fats"
   },
   {
    "value": "https://en.wikipedia.org/wiki/Fire_class"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "SAE.a8411004-c988-444c-ac3a-a4abf9161342",
  "question": "What class of fire extinguisher is designed to work on electrical fires?",
  "answer": "C",
  "explanation": [
   {
    "value": "Class A- common cumbustible material - wood, trash"
   },
   {
    "value": "Class B - cumbustible liquids"
   },
   {
    "value": "Class C - Electrical"
   },
   {
    "value": "Class D - Combustible metals"
   },
   {
    "value": "Class K - Kitchen - fats"
   },
   {
    "value": "https://en.wikipedia.org/wiki/Fire_class"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd",
  "question": "Which of the following hashing algorithms produces output less than 200",
  "answer": "C,F",
  "explanation": "SHA1 - 160 bit hash\nMD5 - 128 bit hash\n Whirlpool - 512\n AES-CCMP - not a hash\nRC5 - not a hash"
 },
 {
  "type": "QUESTION",
  "id": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e",
  "question": "Brian computes the digest of a single sentence of text using a SHA-2 hash function. He then changes a single character of the sentence and computes the hash value again. Which one of the following statements is true about the new hash value?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0",
  "question": "Which cryptographic algorithm forms the basis of the El Gamal cryptosystem?",
  "answer": "B",
  "explanation": "The El Gamal cryptosystem extends the functionality of the Diffie-Hellman key exchange protocol to support the encryption and decryption of messages."
 },
 {
  "type": "QUESTION",
  "id": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd",
  "question": "If Richard wants to send an encrypted message to Sue using a public key cryptosystem, which key does he use to encrypt the message?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.c8ca9146-cf0c-4716-b496-73f408237891",
  "question": "If a 2,048-bit plaintext message were encrypted with the El Gamal public key cryptosystem, how long would the resulting ciphertext message be?",
  "answer": "C",
  "explanation": "The major disadvantage of the El Gamal cryptosystem is that it doubles the length of any message it encrypts. Therefore, a 2,048-bit plain-text message would yield a 4,096-bit ciphertext message when El Gamal is used for the encryption process."
 },
 {
  "type": "QUESTION",
  "id": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c",
  "question": "Acme Widgets currently uses a 1,024-bit RSA encryption standard companywide. The company plans to convert from RSA to an elliptic curve cryptosystem. If it wants to maintain the same cryptographic strength, what ECC key length should it use?",
  "answer": "A",
  "explanation": "The elliptic curve cryptosystem requires significantly shorter keys to achieve encryption that would be the same strength as encryption achieved with the RSA"
 },
 {
  "type": "QUESTION",
  "id": "SAE.4982434d-73e5-49be-838d-cd06267900dc",
  "question": "John wants to produce a message digest of a 2,048-byte message he plans to send to Mary. If he uses the SHA-1 hashing algorithm, what size will the message digest for this particular message be?",
  "answer": "A",
  "explanation": "The SHA-1 hashing algorithm always produces a 160-bit message digest, regardless of the size of the input message. In fact, this fixed-length output is a requirement of any secure hashing algorithm."
 },
 {
  "type": "QUESTION",
  "id": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe",
  "question": "Which one of the following technologies is considered flawed and should no longer be used?",
  "answer": "C",
  "explanation": "The WEP algorithm has documented flaws that make it trivial to break. It should never be used to protect wireless networks."
 },
 {
  "type": "QUESTION",
  "id": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed",
  "question": "Richard received an encrypted message sent to him from Sue. Which key should he use to decrypt the message?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6",
  "question": "Richard wants to digitally sign a message he’s sending to Sue so that Sue can be sure the message came from him without modification while in transit. Which key should he use to encrypt the message digest?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d",
  "question": "Which one of the following algorithms is not supported by the Digital Signature Standard?",
  "answer": "C",
  "explanation": "The Digital Signature Standard allows federal government use of the Digital Signature Algorithm, RSA, or the Elliptic Curve DSA in conjunction with the SHA-1 hashing function to produce secure digital signatures."
 },
 {
  "type": "QUESTION",
  "id": "COM.c927df0d-8486-4788-990d-47b5c11686f9",
  "question": "Which International Telecommunications Union (ITU) standard governs the creation and endorsement of digital certificates for secure electronic communication?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a",
  "question": "What cryptosystem provides the encryption/decryption technology for the commercial version of Phil Zimmerman’s Pretty Good Privacy secure email system?",
  "answer": "B",
  "explanation": "Pretty Good Privacy uses a “web of trust” system of digital signature verification. The encryption technology is based on the IDEA private key cryptosystem."
 },
 {
  "type": "QUESTION",
  "id": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043",
  "question": "What type of cryptographic attack rendered Double DES (2DES) no more effective than standard DES encryption?",
  "answer": "C",
  "explanation": "In the meet-in-the-middle attack, the attacker uses a known plaintext message. The plain text is then encrypted using every possible key (k1), and the equivalent ciphertext is decrypted using all possible keys (k2). When a match is found, the corresponding pair (k1, k2) represents both portions of the double encryption. This type of attack generally takes only double the time necessary to break a single round of encryption (or 2n rather than the anticipated 2n * 2n), offering minimal added protection."
 },
 {
  "type": "QUESTION",
  "id": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20",
  "question": "Which of the following tools can be used to improve the effectiveness of a brute-force password cracking attack?",
  "answer": "A",
  "explanation": "Rainbow tables contain precomputed hash values for commonly used passwords and may be used to increase the efficiency of password cracking attacks."
 },
 {
  "type": "QUESTION",
  "id": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb",
  "question": "Which of the following links would be protected by WPA encryption?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.4e501025-9160-4346-85b2-1018121edc4c",
  "question": "What is the major disadvantage of using certificate revocation lists?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757",
  "question": "Which one of the following encryption algorithms is now considered insecure?",
  "answer": "D",
  "explanation": "The Merkle-Hellman Knapsack algorithm, which relies on the difficulty of factoring super-increasing sets, has been broken by cryptanalysts."
 },
 {
  "type": "QUESTION",
  "id": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4",
  "question": "What does IPsec define?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "ANSWER",
  "id": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0-A",
  "QuestionId": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0",
  "choice": "A",
  "answer": "Brute force attacks against symetric keys"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0-B",
  "QuestionId": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0",
  "choice": "B",
  "answer": "Factor the product of RSA primes"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0-C",
  "QuestionId": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0",
  "choice": "C",
  "answer": "Side channel attacks using differential power analysis"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0-D",
  "QuestionId": "SAE.4a1cc150-1207-4b17-bce2-7490cfaa00c0",
  "choice": "D",
  "answer": "Malware on users compute that will extract the private key when user logs in."
 },
 {
  "type": "ANSWER",
  "id": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5-A",
  "QuestionId": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5",
  "choice": "A",
  "answer": "SLA (Service Level Agreement)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5-B",
  "QuestionId": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5",
  "choice": "B",
  "answer": "MOU (Memorandum of Understand)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5-C",
  "QuestionId": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5",
  "choice": "C",
  "answer": "NDA (Non Disclosure Agreement)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5-D",
  "QuestionId": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5",
  "choice": "D",
  "answer": "Software License"
 },
 {
  "type": "ANSWER",
  "id": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5-E",
  "QuestionId": "SAE.06b2c0ab-8470-471e-8b3f-0f461fd179c5",
  "choice": "E",
  "answer": "ISA (Interconnection Service Agreement)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a-A",
  "QuestionId": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a",
  "choice": "A",
  "answer": "Forward Secrecy is not available"
 },
 {
  "type": "ANSWER",
  "id": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a-B",
  "QuestionId": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a",
  "choice": "B",
  "answer": "No authentication"
 },
 {
  "type": "ANSWER",
  "id": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a-C",
  "QuestionId": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a",
  "choice": "C",
  "answer": "Weak encryption"
 },
 {
  "type": "ANSWER",
  "id": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a-D",
  "QuestionId": "SAE.79fb5946-3cde-428d-9e3a-f69c8a102d9a",
  "choice": "D",
  "answer": "Long term private key compromise"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039-A",
  "QuestionId": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039",
  "choice": "A",
  "answer": "MAC"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039-B",
  "QuestionId": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039",
  "choice": "B",
  "answer": "DAC"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039-C",
  "QuestionId": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039",
  "choice": "C",
  "answer": "RBAC"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039-D",
  "QuestionId": "SAE.4c57f512-23d5-43bb-99a8-26a9142c5039",
  "choice": "D",
  "answer": "ABAC"
 },
 {
  "type": "ANSWER",
  "id": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c-A",
  "QuestionId": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c",
  "choice": "A",
  "answer": "Simple Security Property"
 },
 {
  "type": "ANSWER",
  "id": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c-B",
  "QuestionId": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c",
  "choice": "B",
  "answer": "Property Rule"
 },
 {
  "type": "ANSWER",
  "id": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c-C",
  "QuestionId": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c",
  "choice": "C",
  "answer": "Tranquility Principle"
 },
 {
  "type": "ANSWER",
  "id": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c-D",
  "QuestionId": "SAE.babad34d-b39d-4eb7-b3e1-9225a2057e7c",
  "choice": "D",
  "answer": "Strong Star Property"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1-A",
  "QuestionId": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1",
  "choice": "A",
  "answer": "The software"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1-B",
  "QuestionId": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1",
  "choice": "B",
  "answer": "The security subsystem"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1-C",
  "QuestionId": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1",
  "choice": "C",
  "answer": "The operating system software"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1-D",
  "QuestionId": "SAE.6a01fced-175c-4544-ad0a-dda6b5d200a1",
  "choice": "D",
  "answer": "The reference monitor"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd-A",
  "QuestionId": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd",
  "choice": "A",
  "answer": "Serpent"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd-B",
  "QuestionId": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd",
  "choice": "B",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd-C",
  "QuestionId": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd",
  "choice": "C",
  "answer": "MQV"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd-D",
  "QuestionId": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd",
  "choice": "D",
  "answer": "Blowfisth"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd-E",
  "QuestionId": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd",
  "choice": "E",
  "answer": "RC5"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd-F",
  "QuestionId": "SAE.0a11af5b-f328-44e5-a7cf-ddbea82796cd",
  "choice": "F",
  "answer": "Diffie Helman"
 },
 {
  "type": "ANSWER",
  "id": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a-A",
  "QuestionId": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a",
  "choice": "A",
  "answer": "Digital Signature"
 },
 {
  "type": "ANSWER",
  "id": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a-B",
  "QuestionId": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a",
  "choice": "B",
  "answer": "Hashed Message Authentication Code (HMAC)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a-C",
  "QuestionId": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a",
  "choice": "C",
  "answer": "Message Integrity Check (MIC)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a-D",
  "QuestionId": "SAE.be55d654-e5e3-499d-9a88-b695b53e152a",
  "choice": "D",
  "answer": "Digital Envelope"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9249acaa-d979-4ace-be76-58d172682f38-A",
  "QuestionId": "SAE.9249acaa-d979-4ace-be76-58d172682f38",
  "choice": "A",
  "answer": "Stream Cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9249acaa-d979-4ace-be76-58d172682f38-B",
  "QuestionId": "SAE.9249acaa-d979-4ace-be76-58d172682f38",
  "choice": "B",
  "answer": "ECC"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9249acaa-d979-4ace-be76-58d172682f38-C",
  "QuestionId": "SAE.9249acaa-d979-4ace-be76-58d172682f38",
  "choice": "C",
  "answer": "Diffie-Helman"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9249acaa-d979-4ace-be76-58d172682f38-D",
  "QuestionId": "SAE.9249acaa-d979-4ace-be76-58d172682f38",
  "choice": "D",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9249acaa-d979-4ace-be76-58d172682f38-E",
  "QuestionId": "SAE.9249acaa-d979-4ace-be76-58d172682f38",
  "choice": "E",
  "answer": "Block Cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940-A",
  "QuestionId": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940",
  "choice": "A",
  "answer": "Heterogenity of systems"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940-B",
  "QuestionId": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940",
  "choice": "B",
  "answer": "Lack of protocol standardization"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940-C",
  "QuestionId": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940",
  "choice": "C",
  "answer": "Network scalability"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940-D",
  "QuestionId": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940",
  "choice": "D",
  "answer": "Inadequate system transparency"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940-E",
  "QuestionId": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940",
  "choice": "E",
  "answer": "Missing digital signatures"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940-F",
  "QuestionId": "SAE.a1345cb3-480a-4f90-9b50-4893c8219940",
  "choice": "F",
  "answer": "Transmitting unencrypted data"
 },
 {
  "type": "ANSWER",
  "id": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a-A",
  "QuestionId": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a",
  "choice": "A",
  "answer": "NAT-T"
 },
 {
  "type": "ANSWER",
  "id": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a-B",
  "QuestionId": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a",
  "choice": "B",
  "answer": "Masquerading"
 },
 {
  "type": "ANSWER",
  "id": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a-C",
  "QuestionId": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a",
  "choice": "C",
  "answer": "PAT (Port Address Translation)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a-D",
  "QuestionId": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a",
  "choice": "D",
  "answer": "SOCKS PROXY"
 },
 {
  "type": "ANSWER",
  "id": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a-E",
  "QuestionId": "SAE.80ba46b3-b6f6-4993-8943-8b330c2cc61a",
  "choice": "E",
  "answer": "Stateless NAT64"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12-A",
  "QuestionId": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12",
  "choice": "A",
  "answer": "The SNI is encrypted by default in 1.3"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12-B",
  "QuestionId": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12",
  "choice": "B",
  "answer": "The server certificate is encrypted when sent to the client"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12-C",
  "QuestionId": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12",
  "choice": "C",
  "answer": "TLS 1.3 supports a larger number of legacy algorthms"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12-D",
  "QuestionId": "SAE.6b623304-1239-48e4-ae30-79fbfd8d6e12",
  "choice": "D",
  "answer": "The server uses certificate pinning to speed up connection times."
 },
 {
  "type": "ANSWER",
  "id": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2-A",
  "QuestionId": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2",
  "choice": "A",
  "answer": "Firewalking"
 },
 {
  "type": "ANSWER",
  "id": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2-B",
  "QuestionId": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2",
  "choice": "B",
  "answer": "Port mapping"
 },
 {
  "type": "ANSWER",
  "id": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2-C",
  "QuestionId": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2",
  "choice": "C",
  "answer": "Port knocking"
 },
 {
  "type": "ANSWER",
  "id": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2-D",
  "QuestionId": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2",
  "choice": "D",
  "answer": "Secret handshake"
 },
 {
  "type": "ANSWER",
  "id": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2-E",
  "QuestionId": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2",
  "choice": "E",
  "answer": "Session triggering"
 },
 {
  "type": "ANSWER",
  "id": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2-F",
  "QuestionId": "COM.37b5862f-996c-414b-86a1-23e7b8d968e2",
  "choice": "F",
  "answer": "kernel handshaking"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c-A",
  "QuestionId": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c",
  "choice": "A",
  "answer": "One Time Pad"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c-B",
  "QuestionId": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c",
  "choice": "B",
  "answer": "Digest"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c-C",
  "QuestionId": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c",
  "choice": "C",
  "answer": "Null Cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c-D",
  "QuestionId": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c",
  "choice": "D",
  "answer": "Steganography"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c-E",
  "QuestionId": "SAE.bfe26575-8dc5-45a3-8113-2fd842bd2b5c",
  "choice": "E",
  "answer": "Block Cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e319e162-8501-40cb-9acf-70599a7946f5-A",
  "QuestionId": "SAE.e319e162-8501-40cb-9acf-70599a7946f5",
  "choice": "A",
  "answer": "Differential power analysis"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e319e162-8501-40cb-9acf-70599a7946f5-B",
  "QuestionId": "SAE.e319e162-8501-40cb-9acf-70599a7946f5",
  "choice": "B",
  "answer": "Electromagnetic emission"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e319e162-8501-40cb-9acf-70599a7946f5-C",
  "QuestionId": "SAE.e319e162-8501-40cb-9acf-70599a7946f5",
  "choice": "C",
  "answer": "Corruptive"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e319e162-8501-40cb-9acf-70599a7946f5-D",
  "QuestionId": "SAE.e319e162-8501-40cb-9acf-70599a7946f5",
  "choice": "D",
  "answer": "Timing"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1-A",
  "QuestionId": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1",
  "choice": "A",
  "answer": "The security officer can access over 80% of the files within a company."
 },
 {
  "type": "ANSWER",
  "id": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1-B",
  "QuestionId": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1",
  "choice": "B",
  "answer": "A contractor is only given access to three files on one file server."
 },
 {
  "type": "ANSWER",
  "id": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1-C",
  "QuestionId": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1",
  "choice": "C",
  "answer": "A security kernel process can access all processes within an operating system."
 },
 {
  "type": "ANSWER",
  "id": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1-D",
  "QuestionId": "SAE.1d71adc6-dfad-4bb2-bea4-b76cd51329b1",
  "choice": "D",
  "answer": "A Guest account has access to all administrator accounts in the domain."
 },
 {
  "type": "ANSWER",
  "id": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac-A",
  "QuestionId": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac",
  "choice": "A",
  "answer": "Information Security Continuous Monitoring"
 },
 {
  "type": "ANSWER",
  "id": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac-B",
  "QuestionId": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac",
  "choice": "B",
  "answer": "Risk Managment"
 },
 {
  "type": "ANSWER",
  "id": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac-C",
  "QuestionId": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac",
  "choice": "C",
  "answer": "Application Threat Modeling"
 },
 {
  "type": "ANSWER",
  "id": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac-D",
  "QuestionId": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac",
  "choice": "D",
  "answer": "Public Key Infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac-E",
  "QuestionId": "SAE.48e331a5-6b22-43ba-9d41-0a49c52072ac",
  "choice": "E",
  "answer": "Configuration Management"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9-A",
  "QuestionId": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9",
  "choice": "A",
  "answer": "Near Real Time Data"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9-B",
  "QuestionId": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9",
  "choice": "B",
  "answer": "Climate Control"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9-C",
  "QuestionId": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9",
  "choice": "C",
  "answer": "Physical Servers"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9-D",
  "QuestionId": "SAE.9326b89a-e7b2-45d7-be30-ac2e1b6649a9",
  "choice": "D",
  "answer": "Telephones"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f-A",
  "QuestionId": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "choice": "A",
  "answer": "It is stronger than RSA using significantly smaller key lengths"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f-B",
  "QuestionId": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "choice": "B",
  "answer": "It has a large memory footprint"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f-C",
  "QuestionId": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "choice": "C",
  "answer": "It can help conserve battery life in mobile?"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f-D",
  "QuestionId": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "choice": "D",
  "answer": "It has a lower CPU overhead compared to RSA."
 },
 {
  "type": "ANSWER",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f-E",
  "QuestionId": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "choice": "E",
  "answer": "It is not support by most modern browsers"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f-F",
  "QuestionId": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "choice": "F",
  "answer": "ECC was introduced as an alternative to AES"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5fd35553-4735-4512-8885-a64593bb016f-G",
  "QuestionId": "SAE.5fd35553-4735-4512-8885-a64593bb016f",
  "choice": "G",
  "answer": "It can be used in Diffie Helman key exchange"
 },
 {
  "type": "ANSWER",
  "id": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8-A",
  "QuestionId": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8",
  "choice": "A",
  "answer": "They are routeable and must be tunneled"
 },
 {
  "type": "ANSWER",
  "id": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8-B",
  "QuestionId": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8",
  "choice": "B",
  "answer": "They all operate at the transport layer"
 },
 {
  "type": "ANSWER",
  "id": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8-C",
  "QuestionId": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8",
  "choice": "C",
  "answer": "They are supported in ALL IPV4 nodes"
 },
 {
  "type": "ANSWER",
  "id": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8-D",
  "QuestionId": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8",
  "choice": "D",
  "answer": "They all offer confidentiality for data"
 },
 {
  "type": "ANSWER",
  "id": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8-E",
  "QuestionId": "SAE.aedc686c-0b07-4410-86a4-8d77e56ac5d8",
  "choice": "E",
  "answer": "They utilize only symmetric encryption"
 },
 {
  "type": "ANSWER",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47-A",
  "QuestionId": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "choice": "A",
  "answer": "64"
 },
 {
  "type": "ANSWER",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47-B",
  "QuestionId": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "choice": "B",
  "answer": "72"
 },
 {
  "type": "ANSWER",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47-C",
  "QuestionId": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "choice": "C",
  "answer": "128"
 },
 {
  "type": "ANSWER",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47-D",
  "QuestionId": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "choice": "D",
  "answer": "168"
 },
 {
  "type": "ANSWER",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47-E",
  "QuestionId": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "choice": "E",
  "answer": "192"
 },
 {
  "type": "ANSWER",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47-F",
  "QuestionId": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "choice": "F",
  "answer": "256"
 },
 {
  "type": "ANSWER",
  "id": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47-G",
  "QuestionId": "SAE.b660f9e4-bfac-4b14-bb26-91eb21c55f47",
  "choice": "G",
  "answer": "512"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2-A",
  "QuestionId": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "choice": "A",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2-B",
  "QuestionId": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "choice": "B",
  "answer": "Diffie-Helman"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2-C",
  "QuestionId": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "choice": "C",
  "answer": "EAP-TLS"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2-D",
  "QuestionId": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "choice": "D",
  "answer": "PEAP"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2-E",
  "QuestionId": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "choice": "E",
  "answer": "ECC"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2-F",
  "QuestionId": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "choice": "F",
  "answer": "AES"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2-G",
  "QuestionId": "SAE.d2d79b54-cc60-4a2d-b766-f55add1fd1a2",
  "choice": "G",
  "answer": "Vernam Ciphers"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-A",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "A",
  "answer": "Key Size of 128,192,256"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-B",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "B",
  "answer": "Key Sizes from 32-448"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-C",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "C",
  "answer": "Symmetric"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-D",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "D",
  "answer": "Assymetric"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-E",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "E",
  "answer": "Patented"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-F",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "F",
  "answer": "Un-patented and license-free"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-G",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "G",
  "answer": "Used in bcyrypt"
 },
 {
  "type": "ANSWER",
  "id": "SAE.94a686da-cbca-456b-91b2-31672e380a28-H",
  "QuestionId": "SAE.94a686da-cbca-456b-91b2-31672e380a28",
  "choice": "H",
  "answer": "Used in scrypt"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7f008538-eefc-445d-b284-e62c82f2b477-A",
  "QuestionId": "SAE.7f008538-eefc-445d-b284-e62c82f2b477",
  "choice": "A",
  "answer": "Advance Encryption Standard (AES)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7f008538-eefc-445d-b284-e62c82f2b477-B",
  "QuestionId": "SAE.7f008538-eefc-445d-b284-e62c82f2b477",
  "choice": "B",
  "answer": "Blowfish"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7f008538-eefc-445d-b284-e62c82f2b477-C",
  "QuestionId": "SAE.7f008538-eefc-445d-b284-e62c82f2b477",
  "choice": "C",
  "answer": "Data Encryption Standard"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7f008538-eefc-445d-b284-e62c82f2b477-D",
  "QuestionId": "SAE.7f008538-eefc-445d-b284-e62c82f2b477",
  "choice": "D",
  "answer": "El Gamal"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7f008538-eefc-445d-b284-e62c82f2b477-E",
  "QuestionId": "SAE.7f008538-eefc-445d-b284-e62c82f2b477",
  "choice": "E",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-A",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "A",
  "answer": "Detect if a spreadsheet has been changed in an unauthorized way"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-B",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "B",
  "answer": "Provide confidentiality for a windows users accessing a linux web server via a web browswer"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-C",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "C",
  "answer": "Prevent a user from deleting a file they have no permission to access"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-D",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "D",
  "answer": "Provide a high degree of assurance that a remote system is who it says it is"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-E",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "E",
  "answer": "Recover data changed in an authorized manner."
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-F",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "F",
  "answer": "Prevent a thief from putting a stolen hard drive into a separate computer and recovering the information."
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-G",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "G",
  "answer": "Check a script is unmodified before allowing it to be executed"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-H",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "H",
  "answer": "Reduced the effectiveness of ICMP based Denial of Service attacks"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-I",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "I",
  "answer": "Prevent an authorized user from exfiltrating data from a protected network"
 },
 {
  "type": "ANSWER",
  "id": "SAE.69167999-1b06-4123-a128-cecfcf54678d-J",
  "QuestionId": "SAE.69167999-1b06-4123-a128-cecfcf54678d",
  "choice": "J",
  "answer": "Assist with complying with regulatory data security requirements"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c-A",
  "QuestionId": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c",
  "choice": "A",
  "answer": "Hash all the files using SHA-256 and look for a matching hash"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c-B",
  "QuestionId": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c",
  "choice": "B",
  "answer": "Look in the Sent Items folder in the users email program"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c-C",
  "QuestionId": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c",
  "choice": "C",
  "answer": "Search for images of similar size and file type."
 },
 {
  "type": "ANSWER",
  "id": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c-D",
  "QuestionId": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c",
  "choice": "D",
  "answer": "use a fuzzy hashing tool"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c-E",
  "QuestionId": "SAE.82070721-5c48-47f8-bbcb-54cb6942de4c",
  "choice": "E",
  "answer": "Create SHA1 hashes of each file and compare the last 8 bits of the hash value"
 },
 {
  "type": "ANSWER",
  "id": "SRM.01ac8071-2c60-4081-b84b-185322c86e55-A",
  "QuestionId": "SRM.01ac8071-2c60-4081-b84b-185322c86e55",
  "choice": "A",
  "answer": "Your pricate key"
 },
 {
  "type": "ANSWER",
  "id": "SRM.01ac8071-2c60-4081-b84b-185322c86e55-B",
  "QuestionId": "SRM.01ac8071-2c60-4081-b84b-185322c86e55",
  "choice": "B",
  "answer": "Senders private key"
 },
 {
  "type": "ANSWER",
  "id": "SRM.01ac8071-2c60-4081-b84b-185322c86e55-C",
  "QuestionId": "SRM.01ac8071-2c60-4081-b84b-185322c86e55",
  "choice": "C",
  "answer": "Root CAS private key"
 },
 {
  "type": "ANSWER",
  "id": "SRM.01ac8071-2c60-4081-b84b-185322c86e55-D",
  "QuestionId": "SRM.01ac8071-2c60-4081-b84b-185322c86e55",
  "choice": "D",
  "answer": "Senders public key"
 },
 {
  "type": "ANSWER",
  "id": "SRM.01ac8071-2c60-4081-b84b-185322c86e55-E",
  "QuestionId": "SRM.01ac8071-2c60-4081-b84b-185322c86e55",
  "choice": "E",
  "answer": "The passphrase for the message."
 },
 {
  "type": "ANSWER",
  "id": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573-A",
  "QuestionId": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573",
  "choice": "A",
  "answer": "An undisclosed vulnerability attackers can exploit to adversely affect systems"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573-B",
  "QuestionId": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573",
  "choice": "B",
  "answer": "Infecting a website frequented by a targeted group of users"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573-C",
  "QuestionId": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573",
  "choice": "C",
  "answer": "Gaining persistent control of a target system without the users knowledge"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573-D",
  "QuestionId": "SAE.1b3ac04f-0db9-402b-8307-0b497c122573",
  "choice": "D",
  "answer": "Attacking a crypt system by exploiting its physicl implementation"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31-A",
  "QuestionId": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31",
  "choice": "A",
  "answer": "Uninstall the rootkit and recover any correcupted data from backup"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31-B",
  "QuestionId": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31",
  "choice": "B",
  "answer": "Remove the system from the network and manually remove the rootkit files"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31-C",
  "QuestionId": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31",
  "choice": "C",
  "answer": "Reinstall the operating systems and restore data from a backup"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31-D",
  "QuestionId": "SAE.a710ab56-a9f9-459f-ad61-9b0a2cf4da31",
  "choice": "D",
  "answer": "Install a rootkit removal program and use it to remove the rootkit"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c-A",
  "QuestionId": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c",
  "choice": "A",
  "answer": "64"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c-B",
  "QuestionId": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c",
  "choice": "B",
  "answer": "128"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c-C",
  "QuestionId": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c",
  "choice": "C",
  "answer": "256"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c-D",
  "QuestionId": "SAE.bfb644a9-c508-4a2d-8973-3012f0431f8c",
  "choice": "D",
  "answer": "384"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08-A",
  "QuestionId": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08",
  "choice": "A",
  "answer": "ATO"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08-B",
  "QuestionId": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08",
  "choice": "B",
  "answer": "IATO"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08-C",
  "QuestionId": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08",
  "choice": "C",
  "answer": "IATT"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08-D",
  "QuestionId": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08",
  "choice": "D",
  "answer": "DATO"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08-E",
  "QuestionId": "SAE.2b4feb27-61c1-4f2a-a78a-e0dc955dcf08",
  "choice": "E",
  "answer": "NIST"
 },
 {
  "type": "ANSWER",
  "id": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b-A",
  "QuestionId": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b",
  "choice": "A",
  "answer": "Start integrity property"
 },
 {
  "type": "ANSWER",
  "id": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b-B",
  "QuestionId": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b",
  "choice": "B",
  "answer": "simple integrity property"
 },
 {
  "type": "ANSWER",
  "id": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b-C",
  "QuestionId": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b",
  "choice": "C",
  "answer": "simple security property"
 },
 {
  "type": "ANSWER",
  "id": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b-D",
  "QuestionId": "SAE.388c240e-649e-4214-9db0-a3b01598aa8b",
  "choice": "D",
  "answer": "Star security propery"
 },
 {
  "type": "ANSWER",
  "id": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98-A",
  "QuestionId": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98",
  "choice": "A",
  "answer": "Application"
 },
 {
  "type": "ANSWER",
  "id": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98-B",
  "QuestionId": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98",
  "choice": "B",
  "answer": "Data Center"
 },
 {
  "type": "ANSWER",
  "id": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98-C",
  "QuestionId": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98",
  "choice": "C",
  "answer": "Data"
 },
 {
  "type": "ANSWER",
  "id": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98-D",
  "QuestionId": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98",
  "choice": "D",
  "answer": "Hardware"
 },
 {
  "type": "ANSWER",
  "id": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98-E",
  "QuestionId": "SAE.953d4cc1-76f6-4f67-bde4-8e9124ba5c98",
  "choice": "E",
  "answer": "OS"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58-A",
  "QuestionId": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58",
  "choice": "A",
  "answer": "Use parametrized sql queries"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58-B",
  "QuestionId": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58",
  "choice": "B",
  "answer": "Descape SQL characters in field inputs"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58-C",
  "QuestionId": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58",
  "choice": "C",
  "answer": "Use raw queries"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58-D",
  "QuestionId": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58",
  "choice": "D",
  "answer": "Bind parameters to queries"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58-E",
  "QuestionId": "SAE.2631b1c1-590d-4907-9ca3-60a9ae8a2c58",
  "choice": "E",
  "answer": "Validate field inputs"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672-A",
  "QuestionId": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672",
  "choice": "A",
  "answer": "intrusion detection"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672-B",
  "QuestionId": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672",
  "choice": "B",
  "answer": "input validation"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672-C",
  "QuestionId": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672",
  "choice": "C",
  "answer": "network segmentation"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672-D",
  "QuestionId": "SAE.5e12f151-dc0a-478d-ad6a-f501d38fd672",
  "choice": "D",
  "answer": "WPA2 encryption"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9-A",
  "QuestionId": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9",
  "choice": "A",
  "answer": "network segmentation"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9-B",
  "QuestionId": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9",
  "choice": "B",
  "answer": "preventing HTTP GET requests"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9-C",
  "QuestionId": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9",
  "choice": "C",
  "answer": "Automatic Logouts"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9-D",
  "QuestionId": "SAE.8872a861-8493-41a1-8c8d-03ac2c8b79b9",
  "choice": "D",
  "answer": "User Education"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda-A",
  "QuestionId": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda",
  "choice": "A",
  "answer": "Script Based Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda-B",
  "QuestionId": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda",
  "choice": "B",
  "answer": "Static Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda-C",
  "QuestionId": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda",
  "choice": "C",
  "answer": "Mutation Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda-D",
  "QuestionId": "SAE.e056c234-8a68-4ecd-8ad1-fb4baf986fda",
  "choice": "D",
  "answer": "Generation Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930-A",
  "QuestionId": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930",
  "choice": "A",
  "answer": "Require all traffic to be encrypted."
 },
 {
  "type": "ANSWER",
  "id": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930-B",
  "QuestionId": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930",
  "choice": "B",
  "answer": "Disable search and form submission"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930-C",
  "QuestionId": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930",
  "choice": "C",
  "answer": "Install a NIDS to monitor for XSS attackes"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930-D",
  "QuestionId": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930",
  "choice": "D",
  "answer": "Install a WAF between clients and server to act as a reverse proxy"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930-E",
  "QuestionId": "SAE.4d1be06e-6fc1-48b4-a81d-7c6e29cda930",
  "choice": "E",
  "answer": "Install a statefull firewall to only allow established session to port 443"
 },
 {
  "type": "ANSWER",
  "id": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36-A",
  "QuestionId": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36",
  "choice": "A",
  "answer": "ownership"
 },
 {
  "type": "ANSWER",
  "id": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36-B",
  "QuestionId": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36",
  "choice": "B",
  "answer": "standards"
 },
 {
  "type": "ANSWER",
  "id": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36-C",
  "QuestionId": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36",
  "choice": "C",
  "answer": "administration"
 },
 {
  "type": "ANSWER",
  "id": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36-D",
  "QuestionId": "SAE.36e10bdd-d565-4ea1-9f10-b292f58c8e36",
  "choice": "D",
  "answer": "process"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9-A",
  "QuestionId": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9",
  "choice": "A",
  "answer": "IDS"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9-B",
  "QuestionId": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9",
  "choice": "B",
  "answer": "Wrappers"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9-C",
  "QuestionId": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9",
  "choice": "C",
  "answer": "Log Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9-D",
  "QuestionId": "SAE.ee0d5c51-9fb8-491e-9620-8a1787959df9",
  "choice": "D",
  "answer": "SIEM"
 },
 {
  "type": "ANSWER",
  "id": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045-A",
  "QuestionId": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045",
  "choice": "A",
  "answer": "wrappers"
 },
 {
  "type": "ANSWER",
  "id": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045-B",
  "QuestionId": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045",
  "choice": "B",
  "answer": "intrusion detection"
 },
 {
  "type": "ANSWER",
  "id": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045-C",
  "QuestionId": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045",
  "choice": "C",
  "answer": "network segmentation"
 },
 {
  "type": "ANSWER",
  "id": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045-D",
  "QuestionId": "SAE.08276296-067f-43dc-9b50-7f88b8c9f045",
  "choice": "D",
  "answer": "application firewalls"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f7536883-e971-420b-89f0-6a2094cd5209-A",
  "QuestionId": "SAE.f7536883-e971-420b-89f0-6a2094cd5209",
  "choice": "A",
  "answer": "using asymetric keys"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f7536883-e971-420b-89f0-6a2094cd5209-B",
  "QuestionId": "SAE.f7536883-e971-420b-89f0-6a2094cd5209",
  "choice": "B",
  "answer": "PGP"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f7536883-e971-420b-89f0-6a2094cd5209-C",
  "QuestionId": "SAE.f7536883-e971-420b-89f0-6a2094cd5209",
  "choice": "C",
  "answer": "using symetric keys"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f7536883-e971-420b-89f0-6a2094cd5209-D",
  "QuestionId": "SAE.f7536883-e971-420b-89f0-6a2094cd5209",
  "choice": "D",
  "answer": "Secure MIME"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b-A",
  "QuestionId": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b",
  "choice": "A",
  "answer": "elliptic cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b-B",
  "QuestionId": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b",
  "choice": "B",
  "answer": "transposition cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b-C",
  "QuestionId": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b",
  "choice": "C",
  "answer": "stream cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b-D",
  "QuestionId": "SAE.5087193b-fc48-4abb-8da8-f5e848c2784b",
  "choice": "D",
  "answer": "substitution cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6-A",
  "QuestionId": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6",
  "choice": "A",
  "answer": "principle of seperation of duties"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6-B",
  "QuestionId": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6",
  "choice": "B",
  "answer": "principle of least privilege"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6-C",
  "QuestionId": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6",
  "choice": "C",
  "answer": "Communication in event of breach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6-D",
  "QuestionId": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6",
  "choice": "D",
  "answer": "Tranquility principle"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6-E",
  "QuestionId": "SAE.c7296eb9-19c4-4280-93d1-9ee3a4cdd7b6",
  "choice": "E",
  "answer": "In a security algorithm only the key should be secret"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9-A",
  "QuestionId": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9",
  "choice": "A",
  "answer": "For each message"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9-B",
  "QuestionId": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9",
  "choice": "B",
  "answer": "When the key expires"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9-C",
  "QuestionId": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9",
  "choice": "C",
  "answer": "When a participant leaves a group"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9-D",
  "QuestionId": "SAE.7edd21a7-124d-4c91-bf3e-7d8cb9e56aa9",
  "choice": "D",
  "answer": "Secret is compromised"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591-A",
  "QuestionId": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591",
  "choice": "A",
  "answer": "4"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591-B",
  "QuestionId": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591",
  "choice": "B",
  "answer": "8"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591-C",
  "QuestionId": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591",
  "choice": "C",
  "answer": "16"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591-D",
  "QuestionId": "SAE.e9a96291-b56d-4969-8317-5fbc8b238591",
  "choice": "D",
  "answer": "128"
 },
 {
  "type": "ANSWER",
  "id": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0-A",
  "QuestionId": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0",
  "choice": "A",
  "answer": "56"
 },
 {
  "type": "ANSWER",
  "id": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0-B",
  "QuestionId": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0",
  "choice": "B",
  "answer": "128"
 },
 {
  "type": "ANSWER",
  "id": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0-C",
  "QuestionId": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0",
  "choice": "C",
  "answer": "192"
 },
 {
  "type": "ANSWER",
  "id": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0-D",
  "QuestionId": "SAE.81497ae7-d17f-4c4c-b70b-0471d2aaf0d0",
  "choice": "D",
  "answer": "256"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d1f67934-7d75-411a-ba62-806323be13a0-A",
  "QuestionId": "SAE.d1f67934-7d75-411a-ba62-806323be13a0",
  "choice": "A",
  "answer": "Non Repduation"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d1f67934-7d75-411a-ba62-806323be13a0-B",
  "QuestionId": "SAE.d1f67934-7d75-411a-ba62-806323be13a0",
  "choice": "B",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d1f67934-7d75-411a-ba62-806323be13a0-C",
  "QuestionId": "SAE.d1f67934-7d75-411a-ba62-806323be13a0",
  "choice": "C",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "SAE.d1f67934-7d75-411a-ba62-806323be13a0-D",
  "QuestionId": "SAE.d1f67934-7d75-411a-ba62-806323be13a0",
  "choice": "D",
  "answer": "Key Distribution"
 },
 {
  "type": "ANSWER",
  "id": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20-A",
  "QuestionId": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20",
  "choice": "A",
  "answer": "0"
 },
 {
  "type": "ANSWER",
  "id": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20-B",
  "QuestionId": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20",
  "choice": "B",
  "answer": "1"
 },
 {
  "type": "ANSWER",
  "id": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20-C",
  "QuestionId": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20",
  "choice": "C",
  "answer": "3"
 },
 {
  "type": "ANSWER",
  "id": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20-D",
  "QuestionId": "SAE.27f366f3-1f91-4b6c-8d0c-169fbfa00f20",
  "choice": "D",
  "answer": "5"
 },
 {
  "type": "ANSWER",
  "id": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4-A",
  "QuestionId": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4",
  "choice": "A",
  "answer": "32"
 },
 {
  "type": "ANSWER",
  "id": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4-B",
  "QuestionId": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4",
  "choice": "B",
  "answer": "64"
 },
 {
  "type": "ANSWER",
  "id": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4-C",
  "QuestionId": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4",
  "choice": "C",
  "answer": "128"
 },
 {
  "type": "ANSWER",
  "id": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4-D",
  "QuestionId": "SAE.294a2dfa-0d1b-4d21-9991-450de98459e4",
  "choice": "D",
  "answer": "256"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8-A",
  "QuestionId": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8",
  "choice": "A",
  "answer": "1"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8-B",
  "QuestionId": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8",
  "choice": "B",
  "answer": "2"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8-C",
  "QuestionId": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8",
  "choice": "C",
  "answer": "3"
 },
 {
  "type": "ANSWER",
  "id": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8-D",
  "QuestionId": "SAE.0c001ef9-a889-4ec6-860e-a2fcbedd1aa8",
  "choice": "D",
  "answer": "4"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3-A",
  "QuestionId": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3",
  "choice": "A",
  "answer": "Split Knowledge"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3-B",
  "QuestionId": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3",
  "choice": "B",
  "answer": "M of N"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3-C",
  "QuestionId": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3",
  "choice": "C",
  "answer": "Work function"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3-D",
  "QuestionId": "SAE.1c575b1a-d985-42b1-8f4b-c1e68e6417e3",
  "choice": "D",
  "answer": "Zero Knowledege Proof"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2-A",
  "QuestionId": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2",
  "choice": "A",
  "answer": "Cipher Block Chaining (CBC)"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2-B",
  "QuestionId": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2",
  "choice": "B",
  "answer": "Electronic Code Book (ECB)"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2-C",
  "QuestionId": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2",
  "choice": "C",
  "answer": "Cipher Feedback (CFB)"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2-D",
  "QuestionId": "SOP.f9edeca3-c37b-4626-846d-2d9633ab26c2",
  "choice": "D",
  "answer": "Output feedback (OFB)"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d-A",
  "QuestionId": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d",
  "choice": "A",
  "answer": "Diffusion"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d-B",
  "QuestionId": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d",
  "choice": "B",
  "answer": "Confusion"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d-C",
  "QuestionId": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d",
  "choice": "C",
  "answer": "One Way function"
 },
 {
  "type": "ANSWER",
  "id": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d-D",
  "QuestionId": "SAE.bf28f942-4766-4e8c-b193-e78fa204788d",
  "choice": "D",
  "answer": "Kerckhoffs Principle"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af-A",
  "QuestionId": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af",
  "choice": "A",
  "answer": "10"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af-B",
  "QuestionId": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af",
  "choice": "B",
  "answer": "20"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af-C",
  "QuestionId": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af",
  "choice": "C",
  "answer": "45"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af-D",
  "QuestionId": "SAE.a9f40aaf-79e4-4ed8-924c-f3c829bff7af",
  "choice": "D",
  "answer": "100"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d-A",
  "QuestionId": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d",
  "choice": "A",
  "answer": "Vernam Cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d-B",
  "QuestionId": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d",
  "choice": "B",
  "answer": "Running key cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d-C",
  "QuestionId": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d",
  "choice": "C",
  "answer": "Skiphack Cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d-D",
  "QuestionId": "SAE.a12df651-d86d-4f36-b5e7-22f3e294711d",
  "choice": "D",
  "answer": "Blowfish Cipher"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4-A",
  "QuestionId": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4",
  "choice": "A",
  "answer": "Rjindael"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4-B",
  "QuestionId": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4",
  "choice": "B",
  "answer": "Twofish"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4-C",
  "QuestionId": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4",
  "choice": "C",
  "answer": "blowfish"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4-D",
  "QuestionId": "SAE.ef33177e-8e4f-4ba5-8703-349a676916d4",
  "choice": "D",
  "answer": "Skipjack"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2-A",
  "QuestionId": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2",
  "choice": "A",
  "answer": "10"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2-B",
  "QuestionId": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2",
  "choice": "B",
  "answer": "20"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2-C",
  "QuestionId": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2",
  "choice": "C",
  "answer": "45"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2-D",
  "QuestionId": "SAE.7bb96776-7f6d-4a50-8fdc-c6e903d75be2",
  "choice": "D",
  "answer": "100"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad-A",
  "QuestionId": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad",
  "choice": "A",
  "answer": "recipients public key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad-B",
  "QuestionId": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad",
  "choice": "B",
  "answer": "senders public key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad-C",
  "QuestionId": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad",
  "choice": "C",
  "answer": "randomly generated key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad-D",
  "QuestionId": "SAE.9fa5abf7-22f2-40dd-83c2-3e88f05dc3ad",
  "choice": "D",
  "answer": "senders private key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e-A",
  "QuestionId": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e",
  "choice": "A",
  "answer": "quantum cryptography"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e-B",
  "QuestionId": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e",
  "choice": "B",
  "answer": "RSA cryptography"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e-C",
  "QuestionId": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e",
  "choice": "C",
  "answer": "elliptic curve cryptograhy"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e-D",
  "QuestionId": "SAE.7dc73fc8-3933-47fb-935f-dfeb93efd67e",
  "choice": "D",
  "answer": "AES cryptography"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776-A",
  "QuestionId": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776",
  "choice": "A",
  "answer": "prime factorization"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776-B",
  "QuestionId": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776",
  "choice": "B",
  "answer": "travelling salesman"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776-C",
  "QuestionId": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776",
  "choice": "C",
  "answer": "elliptic curve"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776-D",
  "QuestionId": "SAE.c7df2284-6093-44fa-a7f1-76b491f24776",
  "choice": "D",
  "answer": "graph isomorphism"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135-A",
  "QuestionId": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135",
  "choice": "A",
  "answer": "Common certificates"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135-B",
  "QuestionId": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135",
  "choice": "B",
  "answer": "DES"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135-C",
  "QuestionId": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135",
  "choice": "C",
  "answer": "Common critiera"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135-D",
  "QuestionId": "SAE.7ff155c3-98b1-4e9b-acf3-191f3a13a135",
  "choice": "D",
  "answer": "Clipper Chip"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80-A",
  "QuestionId": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80",
  "choice": "A",
  "answer": "Blowdart"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80-B",
  "QuestionId": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80",
  "choice": "B",
  "answer": "PBKDF2"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80-C",
  "QuestionId": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80",
  "choice": "C",
  "answer": "Bcrypt"
 },
 {
  "type": "ANSWER",
  "id": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80-D",
  "QuestionId": "SAE.8b4a6f11-fd90-4859-a85d-09a55b940a80",
  "choice": "D",
  "answer": "PBKDF2"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998-A",
  "QuestionId": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998",
  "choice": "A",
  "answer": "No read down"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998-B",
  "QuestionId": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998",
  "choice": "B",
  "answer": "No read up"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998-C",
  "QuestionId": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998",
  "choice": "C",
  "answer": "No write down"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998-D",
  "QuestionId": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998",
  "choice": "D",
  "answer": "No write up"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998-E",
  "QuestionId": "SAE.ea15a35f-6fb9-4db6-bd13-0dfe31bf6998",
  "choice": "E",
  "answer": "Strong star"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf-A",
  "QuestionId": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf",
  "choice": "A",
  "answer": "Reference Monitor"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf-B",
  "QuestionId": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf",
  "choice": "B",
  "answer": "Kerberos"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf-C",
  "QuestionId": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf",
  "choice": "C",
  "answer": "Memory Manager"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf-D",
  "QuestionId": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf",
  "choice": "D",
  "answer": "API"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf-E",
  "QuestionId": "SAE.4bb852a7-5386-4817-a7be-7e633fe100bf",
  "choice": "E",
  "answer": "Security Control"
 },
 {
  "type": "ANSWER",
  "id": "SAE.379b6147-5315-4520-95b8-715299ab6497-A",
  "QuestionId": "SAE.379b6147-5315-4520-95b8-715299ab6497",
  "choice": "A",
  "answer": "The owner of the public key is the person who signed the message"
 },
 {
  "type": "ANSWER",
  "id": "SAE.379b6147-5315-4520-95b8-715299ab6497-B",
  "QuestionId": "SAE.379b6147-5315-4520-95b8-715299ab6497",
  "choice": "B",
  "answer": "The device was sent from a particular device."
 },
 {
  "type": "ANSWER",
  "id": "SAE.379b6147-5315-4520-95b8-715299ab6497-C",
  "QuestionId": "SAE.379b6147-5315-4520-95b8-715299ab6497",
  "choice": "C",
  "answer": "The message was not altered after being signed"
 },
 {
  "type": "ANSWER",
  "id": "SAE.379b6147-5315-4520-95b8-715299ab6497-D",
  "QuestionId": "SAE.379b6147-5315-4520-95b8-715299ab6497",
  "choice": "D",
  "answer": "The receipient can prove these facts to a 3rd party."
 },
 {
  "type": "ANSWER",
  "id": "SAE.379b6147-5315-4520-95b8-715299ab6497-E",
  "QuestionId": "SAE.379b6147-5315-4520-95b8-715299ab6497",
  "choice": "E",
  "answer": "The receipient can prove the email address of the sender"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23-A",
  "QuestionId": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23",
  "choice": "A",
  "answer": "Decentralized Approach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23-B",
  "QuestionId": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23",
  "choice": "B",
  "answer": "Technical knowledge of users"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23-C",
  "QuestionId": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23",
  "choice": "C",
  "answer": "use of weak crptography"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23-D",
  "QuestionId": "SAE.f0f47dd3-42f9-469f-9084-6c118295da23",
  "choice": "D",
  "answer": "high barrier to entry"
 },
 {
  "type": "ANSWER",
  "id": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239-A",
  "QuestionId": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239",
  "choice": "A",
  "answer": "512"
 },
 {
  "type": "ANSWER",
  "id": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239-B",
  "QuestionId": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239",
  "choice": "B",
  "answer": "128"
 },
 {
  "type": "ANSWER",
  "id": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239-C",
  "QuestionId": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239",
  "choice": "C",
  "answer": "256"
 },
 {
  "type": "ANSWER",
  "id": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239-D",
  "QuestionId": "SAE.68a39515-6bdc-4dcb-8ba9-0d5ebe5fc239",
  "choice": "D",
  "answer": "224"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82391306-4098-4b35-9da1-ada8728f2785-A",
  "QuestionId": "SAE.82391306-4098-4b35-9da1-ada8728f2785",
  "choice": "A",
  "answer": "802.1x"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82391306-4098-4b35-9da1-ada8728f2785-B",
  "QuestionId": "SAE.82391306-4098-4b35-9da1-ada8728f2785",
  "choice": "B",
  "answer": "x.509"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82391306-4098-4b35-9da1-ada8728f2785-C",
  "QuestionId": "SAE.82391306-4098-4b35-9da1-ada8728f2785",
  "choice": "C",
  "answer": "x.500"
 },
 {
  "type": "ANSWER",
  "id": "SAE.82391306-4098-4b35-9da1-ada8728f2785-D",
  "QuestionId": "SAE.82391306-4098-4b35-9da1-ada8728f2785",
  "choice": "D",
  "answer": "802.11ac"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5-A",
  "QuestionId": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5",
  "choice": "A",
  "answer": "Transport Layer Security"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5-B",
  "QuestionId": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5",
  "choice": "B",
  "answer": "Online Certificate Status Protocol"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5-C",
  "QuestionId": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5",
  "choice": "C",
  "answer": "Certificate Revocation Bulletin"
 },
 {
  "type": "ANSWER",
  "id": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5-D",
  "QuestionId": "SAE.9dde39c2-549d-4fea-9233-a0920f674ef5",
  "choice": "D",
  "answer": "Certificate Revocation Lists"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07-A",
  "QuestionId": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07",
  "choice": "A",
  "answer": "Confirm integrity of the message"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07-B",
  "QuestionId": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07",
  "choice": "B",
  "answer": "Allow you to prove to 3rd party who sent the message"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07-C",
  "QuestionId": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07",
  "choice": "C",
  "answer": "Generation of Partial Digital Signature"
 },
 {
  "type": "ANSWER",
  "id": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07-D",
  "QuestionId": "SAE.1bd2a5b4-ec41-49a5-a158-7c5c79ca9b07",
  "choice": "D",
  "answer": "Viewing of plain text message content"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e-A",
  "QuestionId": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e",
  "choice": "A",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e-B",
  "QuestionId": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e",
  "choice": "B",
  "answer": "DSS"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e-C",
  "QuestionId": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e",
  "choice": "C",
  "answer": "Schnorrs Signature Algorthm"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e-D",
  "QuestionId": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e",
  "choice": "D",
  "answer": "Nyberg-Rueppel’s Signature Algorthm"
 },
 {
  "type": "ANSWER",
  "id": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e-E",
  "QuestionId": "SAE.e2b5674d-637b-40ca-a780-a81581c7ef0e",
  "choice": "E",
  "answer": "X.509"
 },
 {
  "type": "ANSWER",
  "id": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf-A",
  "QuestionId": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf",
  "choice": "A",
  "answer": "Private keys should be marked as exportable so they can be transferred to new keys"
 },
 {
  "type": "ANSWER",
  "id": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf-B",
  "QuestionId": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf",
  "choice": "B",
  "answer": "The private key is encrypted with asymmetric key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf-C",
  "QuestionId": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf",
  "choice": "C",
  "answer": "Smartcards hold multiple key pairs and their certificates"
 },
 {
  "type": "ANSWER",
  "id": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf-D",
  "QuestionId": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf",
  "choice": "D",
  "answer": "Smart cards do not support assymetric keys with length shorter then 1024"
 },
 {
  "type": "ANSWER",
  "id": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf-E",
  "QuestionId": "SAE.64b96b33-aec6-4b9d-9e83-9c007b8333bf",
  "choice": "E",
  "answer": "If the card is lost the private can easily be retrieved with compatible smart card reader"
 },
 {
  "type": "ANSWER",
  "id": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd-A",
  "QuestionId": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd",
  "choice": "A",
  "answer": "The smartcard calculates a hash of the user certificate and sends to the host computer"
 },
 {
  "type": "ANSWER",
  "id": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd-B",
  "QuestionId": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd",
  "choice": "B",
  "answer": "The computer validates the user certificate with standard PKI validation techniques"
 },
 {
  "type": "ANSWER",
  "id": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd-C",
  "QuestionId": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd",
  "choice": "C",
  "answer": "The user smartcard PIN unlocks the access to the user certificates"
 },
 {
  "type": "ANSWER",
  "id": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd-D",
  "QuestionId": "SAE.66e41c62-9aad-4987-9234-ac510337e4bd",
  "choice": "D",
  "answer": "The user private key encrypts a challenge generated by the computer"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4-A",
  "QuestionId": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4",
  "choice": "A",
  "answer": "Allow input of any length"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4-B",
  "QuestionId": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4",
  "choice": "B",
  "answer": "Collision free"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4-C",
  "QuestionId": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4",
  "choice": "C",
  "answer": "Easy to compute hash for any input"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4-D",
  "QuestionId": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4",
  "choice": "D",
  "answer": "Provide variable-length output"
 },
 {
  "type": "ANSWER",
  "id": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4-E",
  "QuestionId": "SAE.2cc696eb-ec0f-4ce6-8cf4-ab44e734f7c4",
  "choice": "E",
  "answer": "provide one-way functionality"
 },
 {
  "type": "ANSWER",
  "id": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa-A",
  "QuestionId": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa",
  "choice": "A",
  "answer": "128-256-512"
 },
 {
  "type": "ANSWER",
  "id": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa-B",
  "QuestionId": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa",
  "choice": "B",
  "answer": "128-128-256"
 },
 {
  "type": "ANSWER",
  "id": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa-C",
  "QuestionId": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa",
  "choice": "C",
  "answer": "160-512-512"
 },
 {
  "type": "ANSWER",
  "id": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa-D",
  "QuestionId": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa",
  "choice": "D",
  "answer": "160-256-512"
 },
 {
  "type": "ANSWER",
  "id": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa-E",
  "QuestionId": "SAE.3666c73f-55b9-44ab-9ff0-9854703748fa",
  "choice": "E",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c-A",
  "QuestionId": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c",
  "choice": "A",
  "answer": "Hash using MD3 encrypt with AES"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c-B",
  "QuestionId": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c",
  "choice": "B",
  "answer": "Hash using SHA1-1,2,3 and encrypt with DSA,RSA,ECDSA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c-C",
  "QuestionId": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c",
  "choice": "C",
  "answer": "Hash with PGP encrypt with Blowfish"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c-D",
  "QuestionId": "SAE.a0335572-b5d9-4fdd-939b-e1df1eb0c05c",
  "choice": "D",
  "answer": "Hash with HAVAL and encrypt with Diffie Helman"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f77ca056-76a1-4604-991f-43857ad69348-A",
  "QuestionId": "SAE.f77ca056-76a1-4604-991f-43857ad69348",
  "choice": "A",
  "answer": "Generate secret key, encrypt message, encrypt secret key with recipients public key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f77ca056-76a1-4604-991f-43857ad69348-B",
  "QuestionId": "SAE.f77ca056-76a1-4604-991f-43857ad69348",
  "choice": "B",
  "answer": "Encrypt message using recipients public key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.f77ca056-76a1-4604-991f-43857ad69348-C",
  "QuestionId": "SAE.f77ca056-76a1-4604-991f-43857ad69348",
  "choice": "C",
  "answer": "Hash message using MD5 encrypt message with hash"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560-A",
  "QuestionId": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560",
  "choice": "A",
  "answer": "Output feedback mode"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560-B",
  "QuestionId": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560",
  "choice": "B",
  "answer": "Tunnel Mode"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560-C",
  "QuestionId": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560",
  "choice": "C",
  "answer": "Network mode"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560-D",
  "QuestionId": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560",
  "choice": "D",
  "answer": "Transport Mode"
 },
 {
  "type": "ANSWER",
  "id": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560-E",
  "QuestionId": "SAE.5006fa32-5b2f-4507-893a-bd2af13d1560",
  "choice": "E",
  "answer": "Presentation Mode"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919-A",
  "QuestionId": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "choice": "A",
  "answer": "Man-in-the-middle"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919-B",
  "QuestionId": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "choice": "B",
  "answer": "Plaintext"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919-C",
  "QuestionId": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "choice": "C",
  "answer": "Meet-in-the-middle"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919-D",
  "QuestionId": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "choice": "D",
  "answer": "Brute Force Attack"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919-E",
  "QuestionId": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "choice": "E",
  "answer": "Reset Attack"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919-F",
  "QuestionId": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "choice": "F",
  "answer": "Chosen Plaintext Attach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919-G",
  "QuestionId": "SAE.6b2c1fc2-93ab-435f-a412-e828bf12f919",
  "choice": "G",
  "answer": "Birthday attack"
 },
 {
  "type": "ANSWER",
  "id": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608-A",
  "QuestionId": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608",
  "choice": "A",
  "answer": "Temperature and Humdity Mgt Approach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608-B",
  "QuestionId": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608",
  "choice": "B",
  "answer": "HVAC Approach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608-C",
  "QuestionId": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608",
  "choice": "C",
  "answer": "Hot Aisle, Cool Aisle Approach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608-D",
  "QuestionId": "SAE.34f8f5f8-7d2d-477f-ab57-7fd1fe669608",
  "choice": "D",
  "answer": "Sustainability Approach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b-A",
  "QuestionId": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b",
  "choice": "A",
  "answer": "64.6 Fahrenheit"
 },
 {
  "type": "ANSWER",
  "id": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b-B",
  "QuestionId": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b",
  "choice": "B",
  "answer": "80.6 Fahrenheit"
 },
 {
  "type": "ANSWER",
  "id": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b-C",
  "QuestionId": "SAE.72ebedd7-dd3d-45a7-add1-0cda8d7dec3b",
  "choice": "C",
  "answer": "74.8 Fahrenheit"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8-A",
  "QuestionId": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8",
  "choice": "A",
  "answer": "Class A"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8-B",
  "QuestionId": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8",
  "choice": "B",
  "answer": "Class B"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8-C",
  "QuestionId": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8",
  "choice": "C",
  "answer": "Class C"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8-D",
  "QuestionId": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8",
  "choice": "D",
  "answer": "Class D"
 },
 {
  "type": "ANSWER",
  "id": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8-E",
  "QuestionId": "SOP.d92d2764-f1b3-4052-92cf-cfb92c3c78a8",
  "choice": "E",
  "answer": "Class K"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a8411004-c988-444c-ac3a-a4abf9161342-A",
  "QuestionId": "SAE.a8411004-c988-444c-ac3a-a4abf9161342",
  "choice": "A",
  "answer": "A"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a8411004-c988-444c-ac3a-a4abf9161342-B",
  "QuestionId": "SAE.a8411004-c988-444c-ac3a-a4abf9161342",
  "choice": "B",
  "answer": "B"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a8411004-c988-444c-ac3a-a4abf9161342-C",
  "QuestionId": "SAE.a8411004-c988-444c-ac3a-a4abf9161342",
  "choice": "C",
  "answer": "C"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a8411004-c988-444c-ac3a-a4abf9161342-D",
  "QuestionId": "SAE.a8411004-c988-444c-ac3a-a4abf9161342",
  "choice": "D",
  "answer": "D"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a8411004-c988-444c-ac3a-a4abf9161342-E",
  "QuestionId": "SAE.a8411004-c988-444c-ac3a-a4abf9161342",
  "choice": "E",
  "answer": "K"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd-A",
  "QuestionId": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd",
  "choice": "A",
  "answer": "SHA2"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd-B",
  "QuestionId": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd",
  "choice": "B",
  "answer": "Whirlpool"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd-C",
  "QuestionId": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd",
  "choice": "C",
  "answer": "SHA1"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd-D",
  "QuestionId": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd",
  "choice": "D",
  "answer": "AES-CCMP"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd-E",
  "QuestionId": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd",
  "choice": "E",
  "answer": "RC5"
 },
 {
  "type": "ANSWER",
  "id": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd-F",
  "QuestionId": "SAE.a23d32d6-aa8e-4673-88be-f0632f06aafd",
  "choice": "F",
  "answer": "MD5"
 },
 {
  "type": "ANSWER",
  "id": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e-A",
  "QuestionId": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e",
  "choice": "A",
  "answer": "The new hash value will be one character different from the old hash value."
 },
 {
  "type": "ANSWER",
  "id": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e-B",
  "QuestionId": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e",
  "choice": "B",
  "answer": "The new hash value will share at least 50% of the characters of the old hash value."
 },
 {
  "type": "ANSWER",
  "id": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e-C",
  "QuestionId": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e",
  "choice": "C",
  "answer": "The new hash value will be unchanged."
 },
 {
  "type": "ANSWER",
  "id": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e-D",
  "QuestionId": "SAE.ed31c85c-7dcc-4dcd-8c72-656fc170613e",
  "choice": "D",
  "answer": "The new hash value will be completely different from the old hash value."
 },
 {
  "type": "ANSWER",
  "id": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0-A",
  "QuestionId": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0",
  "choice": "A",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0-B",
  "QuestionId": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0",
  "choice": "B",
  "answer": "Diffie-Helman"
 },
 {
  "type": "ANSWER",
  "id": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0-C",
  "QuestionId": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0",
  "choice": "C",
  "answer": "3DES"
 },
 {
  "type": "ANSWER",
  "id": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0-D",
  "QuestionId": "SAE.58a12dd9-3666-4b5d-85cd-aa44bb6f5cd0",
  "choice": "D",
  "answer": "IDEA"
 },
 {
  "type": "ANSWER",
  "id": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd-A",
  "QuestionId": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd",
  "choice": "A",
  "answer": "Richards Public Key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd-B",
  "QuestionId": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd",
  "choice": "B",
  "answer": "Richards Private Key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd-C",
  "QuestionId": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd",
  "choice": "C",
  "answer": "Sues public key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd-D",
  "QuestionId": "SAE.dce1f321-49a2-4f8b-9101-0bfd9c728bbd",
  "choice": "D",
  "answer": "Sues private key"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c8ca9146-cf0c-4716-b496-73f408237891-A",
  "QuestionId": "SAE.c8ca9146-cf0c-4716-b496-73f408237891",
  "choice": "A",
  "answer": "1024"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c8ca9146-cf0c-4716-b496-73f408237891-B",
  "QuestionId": "SAE.c8ca9146-cf0c-4716-b496-73f408237891",
  "choice": "B",
  "answer": "2048"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c8ca9146-cf0c-4716-b496-73f408237891-C",
  "QuestionId": "SAE.c8ca9146-cf0c-4716-b496-73f408237891",
  "choice": "C",
  "answer": "4096"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c8ca9146-cf0c-4716-b496-73f408237891-D",
  "QuestionId": "SAE.c8ca9146-cf0c-4716-b496-73f408237891",
  "choice": "D",
  "answer": "8192"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c-A",
  "QuestionId": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c",
  "choice": "A",
  "answer": "160"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c-B",
  "QuestionId": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c",
  "choice": "B",
  "answer": "512"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c-C",
  "QuestionId": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c",
  "choice": "C",
  "answer": "1024"
 },
 {
  "type": "ANSWER",
  "id": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c-D",
  "QuestionId": "SAE.c28283a3-48a5-4567-b001-41f6175a8c0c",
  "choice": "D",
  "answer": "2048"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4982434d-73e5-49be-838d-cd06267900dc-A",
  "QuestionId": "SAE.4982434d-73e5-49be-838d-cd06267900dc",
  "choice": "A",
  "answer": "160"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4982434d-73e5-49be-838d-cd06267900dc-B",
  "QuestionId": "SAE.4982434d-73e5-49be-838d-cd06267900dc",
  "choice": "B",
  "answer": "512"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4982434d-73e5-49be-838d-cd06267900dc-C",
  "QuestionId": "SAE.4982434d-73e5-49be-838d-cd06267900dc",
  "choice": "C",
  "answer": "1024"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4982434d-73e5-49be-838d-cd06267900dc-D",
  "QuestionId": "SAE.4982434d-73e5-49be-838d-cd06267900dc",
  "choice": "D",
  "answer": "2048"
 },
 {
  "type": "ANSWER",
  "id": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe-A",
  "QuestionId": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe",
  "choice": "A",
  "answer": "SHA-3"
 },
 {
  "type": "ANSWER",
  "id": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe-B",
  "QuestionId": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe",
  "choice": "B",
  "answer": "PGP"
 },
 {
  "type": "ANSWER",
  "id": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe-C",
  "QuestionId": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe",
  "choice": "C",
  "answer": "WEP"
 },
 {
  "type": "ANSWER",
  "id": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe-D",
  "QuestionId": "SAE.fc15eb81-8bd0-44b4-876d-d326ae9294fe",
  "choice": "D",
  "answer": "TLS"
 },
 {
  "type": "ANSWER",
  "id": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed-A",
  "QuestionId": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed",
  "choice": "A",
  "answer": "Richards public key"
 },
 {
  "type": "ANSWER",
  "id": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed-B",
  "QuestionId": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed",
  "choice": "B",
  "answer": "Richards private key"
 },
 {
  "type": "ANSWER",
  "id": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed-C",
  "QuestionId": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed",
  "choice": "C",
  "answer": "Sues public key"
 },
 {
  "type": "ANSWER",
  "id": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed-D",
  "QuestionId": "COM.759411ae-8c3c-4685-a2cc-da7b1c058fed",
  "choice": "D",
  "answer": "Sues private key"
 },
 {
  "type": "ANSWER",
  "id": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6-A",
  "QuestionId": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6",
  "choice": "A",
  "answer": "Richards public key"
 },
 {
  "type": "ANSWER",
  "id": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6-B",
  "QuestionId": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6",
  "choice": "B",
  "answer": "Richards private key"
 },
 {
  "type": "ANSWER",
  "id": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6-C",
  "QuestionId": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6",
  "choice": "C",
  "answer": "Sues public key"
 },
 {
  "type": "ANSWER",
  "id": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6-D",
  "QuestionId": "COM.fcc210af-c43a-4a79-a74f-0319d6ab05d6",
  "choice": "D",
  "answer": "Sues private key"
 },
 {
  "type": "ANSWER",
  "id": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d-A",
  "QuestionId": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d",
  "choice": "A",
  "answer": "DSA"
 },
 {
  "type": "ANSWER",
  "id": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d-B",
  "QuestionId": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d",
  "choice": "B",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d-C",
  "QuestionId": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d",
  "choice": "C",
  "answer": "El gammal"
 },
 {
  "type": "ANSWER",
  "id": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d-D",
  "QuestionId": "COM.5693599b-c8ac-4d9d-be9d-534d879a543d",
  "choice": "D",
  "answer": "ECC"
 },
 {
  "type": "ANSWER",
  "id": "COM.c927df0d-8486-4788-990d-47b5c11686f9-A",
  "QuestionId": "COM.c927df0d-8486-4788-990d-47b5c11686f9",
  "choice": "A",
  "answer": "x.500"
 },
 {
  "type": "ANSWER",
  "id": "COM.c927df0d-8486-4788-990d-47b5c11686f9-B",
  "QuestionId": "COM.c927df0d-8486-4788-990d-47b5c11686f9",
  "choice": "B",
  "answer": "x.509"
 },
 {
  "type": "ANSWER",
  "id": "COM.c927df0d-8486-4788-990d-47b5c11686f9-C",
  "QuestionId": "COM.c927df0d-8486-4788-990d-47b5c11686f9",
  "choice": "C",
  "answer": "x.900"
 },
 {
  "type": "ANSWER",
  "id": "COM.c927df0d-8486-4788-990d-47b5c11686f9-D",
  "QuestionId": "COM.c927df0d-8486-4788-990d-47b5c11686f9",
  "choice": "D",
  "answer": "x.905"
 },
 {
  "type": "ANSWER",
  "id": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a-A",
  "QuestionId": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a",
  "choice": "A",
  "answer": "ROT13"
 },
 {
  "type": "ANSWER",
  "id": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a-B",
  "QuestionId": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a",
  "choice": "B",
  "answer": "IDEA"
 },
 {
  "type": "ANSWER",
  "id": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a-C",
  "QuestionId": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a",
  "choice": "C",
  "answer": "ECC"
 },
 {
  "type": "ANSWER",
  "id": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a-D",
  "QuestionId": "COM.deb64a92-5c35-4a1d-8c55-09cd50553b9a",
  "choice": "D",
  "answer": "El Gammal"
 },
 {
  "type": "ANSWER",
  "id": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043-A",
  "QuestionId": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043",
  "choice": "A",
  "answer": "Birthday Attack"
 },
 {
  "type": "ANSWER",
  "id": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043-B",
  "QuestionId": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043",
  "choice": "B",
  "answer": "Chosen Cipher Text"
 },
 {
  "type": "ANSWER",
  "id": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043-C",
  "QuestionId": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043",
  "choice": "C",
  "answer": "Meet in the middle"
 },
 {
  "type": "ANSWER",
  "id": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043-D",
  "QuestionId": "COM.105e99a2-f3bc-4ad7-8ce4-c95aa8549043",
  "choice": "D",
  "answer": "Man in the middle"
 },
 {
  "type": "ANSWER",
  "id": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20-A",
  "QuestionId": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20",
  "choice": "A",
  "answer": "Rainbow Tables"
 },
 {
  "type": "ANSWER",
  "id": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20-B",
  "QuestionId": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20",
  "choice": "B",
  "answer": "Hierarchical Screening"
 },
 {
  "type": "ANSWER",
  "id": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20-C",
  "QuestionId": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20",
  "choice": "C",
  "answer": "TKIP"
 },
 {
  "type": "ANSWER",
  "id": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20-D",
  "QuestionId": "COM.01582e2e-31ec-4c50-a17c-ceb4c0b3ca20",
  "choice": "D",
  "answer": "Random Enhancement"
 },
 {
  "type": "ANSWER",
  "id": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb-A",
  "QuestionId": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb",
  "choice": "A",
  "answer": "Firewall to Firewall"
 },
 {
  "type": "ANSWER",
  "id": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb-B",
  "QuestionId": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb",
  "choice": "B",
  "answer": "Router to Firewall"
 },
 {
  "type": "ANSWER",
  "id": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb-C",
  "QuestionId": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb",
  "choice": "C",
  "answer": "Client to Wireless Access Point"
 },
 {
  "type": "ANSWER",
  "id": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb-D",
  "QuestionId": "COM.14799fb1-0790-46db-88a1-8726e8a86ccb",
  "choice": "D",
  "answer": "Wireless Access Point To Router"
 },
 {
  "type": "ANSWER",
  "id": "COM.4e501025-9160-4346-85b2-1018121edc4c-A",
  "QuestionId": "COM.4e501025-9160-4346-85b2-1018121edc4c",
  "choice": "A",
  "answer": "Key Management"
 },
 {
  "type": "ANSWER",
  "id": "COM.4e501025-9160-4346-85b2-1018121edc4c-B",
  "QuestionId": "COM.4e501025-9160-4346-85b2-1018121edc4c",
  "choice": "B",
  "answer": "Latency"
 },
 {
  "type": "ANSWER",
  "id": "COM.4e501025-9160-4346-85b2-1018121edc4c-C",
  "QuestionId": "COM.4e501025-9160-4346-85b2-1018121edc4c",
  "choice": "C",
  "answer": "Record Keeping"
 },
 {
  "type": "ANSWER",
  "id": "COM.4e501025-9160-4346-85b2-1018121edc4c-D",
  "QuestionId": "COM.4e501025-9160-4346-85b2-1018121edc4c",
  "choice": "D",
  "answer": "Vulnerability To Brute Force Attacks"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757-A",
  "QuestionId": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757",
  "choice": "A",
  "answer": "El Gamal"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757-B",
  "QuestionId": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757",
  "choice": "B",
  "answer": "RSA"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757-C",
  "QuestionId": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757",
  "choice": "C",
  "answer": "Elliptic Curve Cryptography"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757-D",
  "QuestionId": "COM.1e2aed4e-2a47-4c2b-9918-4db59e4df757",
  "choice": "D",
  "answer": "Merkle-Hellman Knapsack"
 },
 {
  "type": "ANSWER",
  "id": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4-A",
  "QuestionId": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4",
  "choice": "A",
  "answer": "All possible security classifications for a specific configuration"
 },
 {
  "type": "ANSWER",
  "id": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4-B",
  "QuestionId": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4",
  "choice": "B",
  "answer": "A framework for setting up a secure communication channel"
 },
 {
  "type": "ANSWER",
  "id": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4-C",
  "QuestionId": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4",
  "choice": "C",
  "answer": "The valid transition states in the Biba model"
 },
 {
  "type": "ANSWER",
  "id": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4-D",
  "QuestionId": "COM.7ad2eeef-4063-4bc8-91bc-e1a9c64843f4",
  "choice": "D",
  "answer": "TCSEC security categories"
 }
]