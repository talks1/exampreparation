export const Log = [
 {
  "type": "QUESTION",
  "id": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46",
  "question": "Which of the following issues is NOT addressed by Kerberos?",
  "answer": "A",
  "explanation": "Kereberos doesnt address how to make the system more robust."
 },
 {
  "type": "QUESTION",
  "id": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac",
  "question": "Which of the following should NOT considered as key objectives of a smart card system for employees accessing facility and systems?",
  "answer": "E",
  "explanation": "AES-192 might not be involved in the smart card solution at all. All the other make sense for a system."
 },
 {
  "type": "QUESTION",
  "id": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e",
  "question": "Organization is replacing username/password with a smart card based system.Which of the following is least likely to be a requirement for issuance of a smart card?",
  "answer": "B",
  "explanation": "WIth smart card issues we want to associate the current with a specific person and the current password is the thing least directly proving the identity of a person."
 },
 {
  "type": "QUESTION",
  "id": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33",
  "question": "The Health Insurance Portability and Accountability Act (HIPAA), requires that medical providers keep which information private?",
  "answer": "C",
  "explanation": "Defines PHI (Personal Health Information)"
 },
 {
  "type": "QUESTION",
  "id": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4",
  "question": "Which of the following is correct",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.",
  "question": "Which of the following access control mechanisms allows information owners to control access to resources by evaluating the subject, object and the environment?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40",
  "question": "An organization uses PIV cards for desktop computer logins and physical access and wants to extend PIV-based authentication to the increasing number of company-issued smartphones and tablets. Which of the following will provide the best authentication security and the most seamless user experience?",
  "answer": "C",
  "explanation": "Derived PIV is like a virtual card on the device. A software solution.\n Other options are more problematic as they are hardware based."
 },
 {
  "type": "QUESTION",
  "id": "IAM.3283dee6-1985-45fe-834c-6605e5420732",
  "question": "A system in your enterprise does not support individual user passwords but multiple admins require access at least once a month. Which of the solutions is the best to acheive Accountability?",
  "answer": "C",
  "explanation": "Password manager can record the admin requesting password. They can also change the password after each admin has done their work."
 },
 {
  "type": "QUESTION",
  "id": "IAM.770f3370-d056-46f3-8353-5ddd04312277",
  "question": "Which of the following are not characteristics of the challenge authentication protocol (CHAP)?",
  "answer": "A",
  "explanation": "The challenge is hashed using the shared secret rather than encrypted.\n The 3 way hanshake is challenge-response-accept"
 },
 {
  "type": "QUESTION",
  "id": "IAM.c99147e4-2de5-4612-9186-a9490e03b317",
  "question": "Which of the following are not principles of access control",
  "answer": "C,E",
  "explanation": "Locards principle related to attackers always leaving some evidence\nTranquility principle relates to access control rules that are invariant\n Principle of accountability is not a principle of access control"
 },
 {
  "type": "QUESTION",
  "id": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343",
  "question": "Select the authentication mechanisms which are characteristic based",
  "answer": "B,D",
  "explanation": "Characteristic based are some you are...biometric"
 },
 {
  "type": "QUESTION",
  "id": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13",
  "question": "Which of the following is not a mitigation technique that can be used to protect an application and its data.",
  "answer": "A",
  "explanation": "Fuzzing is a technique to spam an application with lots of random inputs to check if there are some vulnerabilities. THis is not a migitation technique.\nInterestingly I wouldnt consider 'secure logging' a migitation technique but this was indicated."
 },
 {
  "type": "QUESTION",
  "id": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19",
  "question": "Which resource-intensive access control mechanism implemented in databases, can allow/deny access to an object based on the data the object contains?",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92",
  "question": "What is an advantage of content-dependent access control in databases?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680",
  "question": "If security was not part of the development of a database, how is it usually handled?",
  "answer": "C",
  "explanation": "Have to ensure security in the front end to compensate for the back end"
 },
 {
  "type": "QUESTION",
  "id": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa",
  "question": "The Active Directory domain adminstrator has created a security group called 'R&D- Project Z' added members of the group 'Project Z' team to the group. He then configures a Group Policy Object (GPO) that allows only that group  to acess 'Project Z' servers. What type of access control is this an example of?",
  "answer": "C",
  "explanation": "Non-Discretionary is another name for Role Based Access Control"
 },
 {
  "type": "QUESTION",
  "id": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e",
  "question": "Which of the following is an example of multi-factor authentication",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de",
  "question": "X.509 is the standard for which of the following?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "question": "One important criteria with biometrics is how acceptable they will be to your work force. Of the following authentication types which is most likely to be met with strong resistance from the average user.",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab",
  "question": "During what phase of the access control process does a user prove his or her identity?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729",
  "question": "How many separate roles are involved in a properly implemented registration and identity proofing process?",
  "answer": "D",
  "explanation": "Request, Approval, Identity Proof and Issuance"
 },
 {
  "type": "QUESTION",
  "id": "IAM.0f947655-5374-4afd-84b4-c74eeced675f",
  "question": "Which one of the following is an example of multifactor authentication?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1",
  "question": "Jane uses an authentication token that requires her to push a button each time she wishes to login to a system. What type of token is she using?",
  "answer": "A",
  "explanation": "HOTP - HMAC based One Time Password uses shared secret and sequence number\nTOTP - Uses shared secret and time of day"
 },
 {
  "type": "QUESTION",
  "id": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b",
  "question": "Which authentication protocol requires the use of external encryption to protect passwords?",
  "answer": "B",
  "explanation": "PAP has no encryption and therefore requires encryption in addition to its use"
 },
 {
  "type": "QUESTION",
  "id": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0",
  "question": "Which choice is not an example of federated authentication?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848",
  "question": "Ricky would like to use an authentication protocol that fully encrypts the authentication session, uses the reliable TCP protocol and will work on his Cisco devices. What protocol should he choose?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.5b1b533c-12af-4a16-8927-8268092a3767",
  "question": "In the Kerberos protocol, what system performs authentication of the end user?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2",
  "question": "In SAML, what organization performs authentication of the end user?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "question": "Which of the following are required for accountability?",
  "answer": "A,C,E,F",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.a1969183-1ede-4460-a003-858632776137",
  "question": "Which of the following are NOT a mechanism to increase account security",
  "answer": "E",
  "explanation": "Require re-authentication on set time periods prevents session mis management but is not related to account security"
 },
 {
  "type": "QUESTION",
  "id": "IAM.68ce9b3a-4633-4371-a972-4b160917b052",
  "question": "Which of the following is not an important account management practice for security professionals?",
  "answer": "A",
  "explanation": "Privilege Creep is a symptom of inappropriately managing privileges while the others are practives for good account management."
 },
 {
  "type": "QUESTION",
  "id": "IAM.682a4be3-9472-4a58-956d-9706a100ba27",
  "question": "What Windows mechanism allows the easy application of security settings to groups of users?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b",
  "question": "Which one of the following is not a normal account activity attribute to monitor?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86",
  "question": "Tobias recently permanently moved from a job in accounting to a job in human resources but never had his accounting privileges revoked. What situation occurred in this case?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069",
  "question": "What command can administrators use to determine whether the SELinux kernel module is enabled?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.893d3c77-2980-426e-9386-0a619f3974e9",
  "question": "In a discretionary access control system, individual users have the ability to alter access permissions.",
  "answer": "A",
  "explanation": "Mandatory Access Control - users cannot change permissions\nWith Discretionary Access Controls - they can"
 },
 {
  "type": "QUESTION",
  "id": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d",
  "question": "What file permission does NOT allow a user to launch an application?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099",
  "question": "Dan is engaging in a password cracking attack where he uses precomputed hash values. What type of attack is Dan waging?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce",
  "question": "What type of website does the attacker use when waging a watering hole attack?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e",
  "question": "A social engineer calls an administrative assistant in your organization and obtains her password by threatening her that her boss' account will be deleted if she does not provide the password to assist with troubleshooting. What type of attack is the social engineer using?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e",
  "question": "What type of phishing attack focuses specifically on senior executives of a targeted organization?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46",
  "question": "Which of the following would not be an asset that an organization would want to protect with access controls?",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.8ede983b-5750-4d24-a373-d3999224f448",
  "question": "Which of the following is true related to a subject? A subject is always a user account. The subject is always the entity that provides or hosts the information or data. The subject is always the entity that receives information about or data from an object. A single entity can never change roles between subject and object.",
  "answer": "C",
  "explanation": "C. The subject is active and is always the entity that receives information about, or data from, the object. A subject can be a user, a program, a process, a file, a computer, a database, and so on. The object is always the entity that provides or hosts information or data. The roles of subject and"
 },
 {
  "type": "QUESTION",
  "id": "IAM.46d03007-1312-48cf-ab82-8b6025d25020",
  "question": "Which of the following types of access control uses fences, security policies, security awareness training, and antivirus software to stop an unwanted or unauthorized activity from occurring?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf",
  "question": "What type of access controls are hardware or software mechanisms used to manage access to resources and systems, and provide protection for those resources and systems?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb",
  "question": "Which of the following best expresses the primary goal when controlling access to assets?",
  "answer": "A",
  "explanation": "A primary goal when controlling access to assets is to protect against losses, including any loss of confidentiality, loss of availability, or loss of integrity. Subjects authenticate on a system, but objects do not authenticate. Subjects access objects, but objects do not access subjects. Identification and authentication is important as a first step in access control, but much more is needed to protect assets."
 },
 {
  "type": "QUESTION",
  "id": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8",
  "question": "A user logs in with a login ID and a password. What is the purpose of the login ID?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e",
  "question": "Accountability requires all of the following items except one. Which item is not required for accountability?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7",
  "question": "Which of the following best identifies the benefit of a passphrase?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18",
  "question": "Which of the following is an example of a Type 2 authentication factor?",
  "answer": "A",
  "explanation": "A Type 2 authentication factor is based on something you have, such as a smartcard or token device. Type 3 authentication is based on something you are and sometimes something you do, which uses physical and behavioral biometric methods. Type 1 authentication is based on something you know, such as passwords or PINs."
 },
 {
  "type": "QUESTION",
  "id": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca",
  "question": "Your organization issues devices to employees. These devices generate onetime passwords every 60 seconds. A server hosted within the organization knows what this password is at any given time. What type of device is this?",
  "answer": "A",
  "explanation": "A synchronous token generates and displays onetime passwords, which are synchronized with an authentication server. An asynchronous token uses a challenge-response process to generate the onetime password. Smartcards do not generate onetime passwords, and common access cards are a version of a smartcard that includes a picture of the user."
 },
 {
  "type": "QUESTION",
  "id": "IAM.dda87e4d-a132-4596-bd50-884365717aaa",
  "question": "What does the CER for a biometric device indicate?",
  "answer": "C",
  "explanation": "The point at which the biometric false rejection rate and the false acceptance rate are equal is the crossover error rate (CER). It does not indicate that sensitivity is too high or too low. A lower CER indicates a higher-quality biometric device, and a higher CER indicates a less accurate device."
 },
 {
  "type": "QUESTION",
  "id": "IAM.fe58e997-4925-4850-8273-cbb52add565b",
  "question": "What is the primary purpose of Kerberos?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710",
  "question": "Which of the following is the best choice to support a federated identity management (FIM) system?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e",
  "question": "What is the function of the network access server within a RADIUS architecture?",
  "answer": "B",
  "explanation": "The network access server is the client within a RADIUS architecture. The RADIUS server is the authentication server and it provides authentication, authorization, and accounting (AAA) services. The network access server might have a host firewall enabled, but that isn’t the primary function."
 },
 {
  "type": "QUESTION",
  "id": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de",
  "question": "Which of the following AAA protocols is based on RADIUS and supports Mobile IP and VoIP?",
  "answer": "B",
  "explanation": "Diameter is based on Remote Authentication Dial-in User Service (RADIUS), and it supports Mobile IP and Voice over IP (VoIP). Distributed access control systems such as a federated identity management system are not a specific protocol, and they don’t necessarily provide authentication, authorization, and accounting. TACACS and TACACS+ are authentication, authorization, and accounting (AAA) protocols, but they are alternatives to RADIUS, not based on RADIUS."
 },
 {
  "type": "QUESTION",
  "id": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983",
  "question": "Which of the following best describes an implicit deny principle?",
  "answer": "B",
  "explanation": "The implicit deny principle ensures that access to an object is denied unless access has been expressly allowed (or explicitly granted) to a subject. It does not allow all actions that are not denied, and it doesn’t require all actions to be denied."
 },
 {
  "type": "QUESTION",
  "id": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1",
  "question": "What is the intent of least privilege?",
  "answer": "C",
  "explanation": "The principle of least privilege ensures that users (subjects) are granted only the most restrictive rights they need to perform their work tasks and job functions. Users don’t execute system processes. The least privilege principle does not enforce the least restrictive rights but rather the most restrictive rights."
 },
 {
  "type": "QUESTION",
  "id": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c",
  "question": "A table includes multiple objects and subjects and it identifies the specific access each subject has to different objects. What is this table?",
  "answer": "B",
  "explanation": "An access control matrix includes multiple objects, and it lists subjects’ access to each of the objects. A single list of subjects for any specific object within an access control matrix is an access control list. A federation refers to a group of companies that share a federated identity management system for single sign-on. Creeping privileges refers to the excessive privileges a subject gathers over time."
 },
 {
  "type": "QUESTION",
  "id": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba",
  "question": "Who, or what, grants permissions to users in a DAC model?",
  "answer": "D",
  "explanation": "The data custodian (or owner) grants permissions to users in a Discretionary Access Control (DAC) model."
 },
 {
  "type": "QUESTION",
  "id": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2",
  "question": "Which of the following models is also known as an identity-based access control model?",
  "answer": "A",
  "explanation": "A Discretionary Access Control (DAC) model is an identity-based access control model. It allows the owner (or data custodian) of a resource to grant permissions at the discretion of the owner. The Role Based Access Control (RBAC) model is based on role or group membership. The rule-based access control model is based on rules within an ACL. The Mandatory Access Control (MAC) model uses assigned labels to identify access."
 },
 {
  "type": "QUESTION",
  "id": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86",
  "question": "A central authority determines which files a user can access. Which of the following best describes this?",
  "answer": "D",
  "explanation": "A nondiscretionary access control model uses a central authority to determine which objects (such as files) that users (and other subjects) can access."
 },
 {
  "type": "QUESTION",
  "id": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf",
  "question": "Which of the following statements is true related to the RBAC model?",
  "answer": "A",
  "explanation": "A. The Role Based Access Control (RBAC) model is based on role or group membership, and users can be members of multiple groups. Users are not limited to only a single role. RBAC models are based on the hierarchy of an organization, so they are hierarchy based. The Mandatory Access Control (MAC) model uses assigned labels to identify access."
 },
 {
  "type": "QUESTION",
  "id": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01",
  "question": "Which of the following is the best choice for a role within an organization using a RBAC model?",
  "answer": "D",
  "explanation": "A programmer is a valid role in a Role Based Access Control (RBAC) model. Administrators would place programmers’ user accounts into the Programmer role and assign privileges to this role. Roles are typically used to organize users, and the other answers are not users."
 },
 {
  "type": "QUESTION",
  "id": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3",
  "question": "What type of access control model is used on a firewall?",
  "answer": "C",
  "explanation": "Firewalls use a rule-based access control model with rules expressed in an access control list. A Mandatory Access Control (MAC) model uses labels. A Discretionary Access Control (DAC) model allows users to assign permissions. A Role Based Access Control (RBAC) model organizes users in groups."
 },
 {
  "type": "QUESTION",
  "id": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d",
  "question": "What type of access controls rely on the use of labels?",
  "answer": "C",
  "explanation": "Mandatory Access Control (MAC) models rely on the use of labels for subjects and objects. Discretionary Access Control (DAC) models allow an owner of an object to control access to the object. Nondiscretionary access controls have centralized management such as a rule-based access control model deployed on a firewall. Role Based Access Control (RBAC) models define a subject’s access based on job-related roles."
 },
 {
  "type": "QUESTION",
  "id": "IAM.78dcb889-35c7-4238-9319-56e907986260",
  "question": "Which of the following best describes a characteristic of the MAC model?",
  "answer": "D",
  "explanation": "The Mandatory Access Control (MAC) model is prohibitive, and it uses an implicit-deny philosophy (not an explicit-deny philosophy). It is not permissive and it uses labels rather than rules."
 },
 {
  "type": "QUESTION",
  "id": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d",
  "question": "Which of the following is not a valid access control model?",
  "answer": "D",
  "explanation": "Compliance-based access control model is not a valid type of access control model. The other answers list valid access control models."
 },
 {
  "type": "QUESTION",
  "id": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e",
  "question": "What would an organization do to identify weaknesses?",
  "answer": "C",
  "explanation": "A vulnerability analysis identifies weaknesses and can include periodic vulnerability scans and penetration tests. Asset valuation determines the value of assets, not weaknesses. Threat modeling attempts to identify threats."
 },
 {
  "type": "QUESTION",
  "id": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510",
  "question": "Which of the following can help mitigate the success of an online brute-force attack?",
  "answer": "B",
  "explanation": "An account lockout policy will lock an account after a user has entered an incorrect password too many times, and this blocks an online brute-force attack. Attackers use rainbow tables in offline password attacks. Password salts reduce the effectiveness of rainbow tables. Encrypting the password protects the stored password but isn’t effective against a brute-force attack without an account lockout."
 },
 {
  "type": "QUESTION",
  "id": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93",
  "question": "Which of the following would provide the best protection against rainbow table attacks?",
  "answer": "B",
  "explanation": "Using both a salt and pepper when hashing passwords provides strong protection against rainbow table attacks. MD5 is no longer considered secure, so it isn’t a good choice for hashing passwords. Account lockout helps thwart online password brute-force attacks, but a rainbow table attack is an offline attack. Role Based Access Control (RBAC) is an access control model and unrelated to password attacks."
 },
 {
  "type": "QUESTION",
  "id": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09",
  "question": "Management wants to ensure that the consultant has the correct priorities while doing her research. Of the following, what should be provided to the consultant to meet this need?",
  "answer": "A",
  "explanation": "Asset valuation identifies the actual value of assets so that they can be prioritized. For example, it will identify the value of the company’s reputation from the loss of customer data compared with the value of the secret data stolen by the malicious employee. None of the other answers is focused on high-value assets. Threat modeling results will identify potential threats."
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46-A",
  "QuestionId": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46",
  "choice": "A",
  "answer": "Availability"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46-B",
  "QuestionId": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46",
  "choice": "B",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46-C",
  "QuestionId": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46",
  "choice": "C",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46-D",
  "QuestionId": "IAM.d0d13802-c128-46a9-a13f-d13f341d8f46",
  "choice": "D",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac-A",
  "QuestionId": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac",
  "choice": "A",
  "answer": "Mechanism should provide strong resistance to fraud, tampering or other exploitation"
 },
 {
  "type": "ANSWER",
  "id": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac-B",
  "QuestionId": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac",
  "choice": "B",
  "answer": "Mechanism should allow rapid authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac-C",
  "QuestionId": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac",
  "choice": "C",
  "answer": "Credentials are issued by only authorization officials"
 },
 {
  "type": "ANSWER",
  "id": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac-D",
  "QuestionId": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac",
  "choice": "D",
  "answer": "Credential recipients must be adequately verified prior to credentials being issues"
 },
 {
  "type": "ANSWER",
  "id": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac-E",
  "QuestionId": "IAM.24b9cefe-ed9f-4129-944a-ed9bee2a64ac",
  "choice": "E",
  "answer": "Mechanisms must support SHA-256 and AES-192 or greater."
 },
 {
  "type": "ANSWER",
  "id": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e-A",
  "QuestionId": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e",
  "choice": "A",
  "answer": "Requring person to appear in person for the registration process."
 },
 {
  "type": "ANSWER",
  "id": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e-B",
  "QuestionId": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e",
  "choice": "B",
  "answer": "Requring user to provide knowledge of current password"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e-C",
  "QuestionId": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e",
  "choice": "C",
  "answer": "Recording copy of the users fingerprint during the process"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e-D",
  "QuestionId": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e",
  "choice": "D",
  "answer": "Taking a photo of the user to be included on the smart card"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e-E",
  "QuestionId": "IAM.ec78fd46-4a5d-462e-8ee4-a21d87d8e14e",
  "choice": "E",
  "answer": "Requiring user to provide passport and another form of government id"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33-A",
  "QuestionId": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33",
  "choice": "A",
  "answer": "Medical"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33-B",
  "QuestionId": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33",
  "choice": "B",
  "answer": "Personal"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33-C",
  "QuestionId": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33",
  "choice": "C",
  "answer": "Personal and medical"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33-D",
  "QuestionId": "IAM.5158e50b-d1ae-4294-938a-e5f60f7eae33",
  "choice": "D",
  "answer": "Insurance"
 },
 {
  "type": "ANSWER",
  "id": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4-A",
  "QuestionId": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4",
  "choice": "A",
  "answer": "Authorization precedes Identification precedes Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4-B",
  "QuestionId": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4",
  "choice": "B",
  "answer": "Identification precedes precedes Authorization preceds Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4-C",
  "QuestionId": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4",
  "choice": "C",
  "answer": "Identification precedes precedes Authentication preceds Authorization"
 },
 {
  "type": "ANSWER",
  "id": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4-D",
  "QuestionId": "IAM.26eb28e6-0f62-4916-b878-2dbfee03f9d4",
  "choice": "D",
  "answer": "Authentication precedes Authorization precedes Identification"
 },
 {
  "type": "ANSWER",
  "id": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.-A",
  "QuestionId": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.",
  "choice": "A",
  "answer": "Rule based access control"
 },
 {
  "type": "ANSWER",
  "id": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.-B",
  "QuestionId": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.",
  "choice": "B",
  "answer": "Attribute based access control"
 },
 {
  "type": "ANSWER",
  "id": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.-C",
  "QuestionId": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.",
  "choice": "C",
  "answer": "Role based access control"
 },
 {
  "type": "ANSWER",
  "id": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.-D",
  "QuestionId": "IAM561697b1-1cb4-49cd-bc67-a45bbbe30cb8.",
  "choice": "D",
  "answer": "Discretionary Access Control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40-A",
  "QuestionId": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40",
  "choice": "A",
  "answer": "MicroSD authentication tokens"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40-B",
  "QuestionId": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40",
  "choice": "B",
  "answer": "TOTP authentication using key fob or mobile app"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40-C",
  "QuestionId": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40",
  "choice": "C",
  "answer": "Derived PIV credentials stored securely on the device"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40-D",
  "QuestionId": "IAM.d436fc7e-31bc-4217-9e0f-f276e9063f40",
  "choice": "D",
  "answer": "USB PIV card reader connected to the device"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3283dee6-1985-45fe-834c-6605e5420732-A",
  "QuestionId": "IAM.3283dee6-1985-45fe-834c-6605e5420732",
  "choice": "A",
  "answer": "Isolate the legacy system to its own VLAN"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3283dee6-1985-45fe-834c-6605e5420732-B",
  "QuestionId": "IAM.3283dee6-1985-45fe-834c-6605e5420732",
  "choice": "B",
  "answer": "Change the password weekly and share with authorized users"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3283dee6-1985-45fe-834c-6605e5420732-C",
  "QuestionId": "IAM.3283dee6-1985-45fe-834c-6605e5420732",
  "choice": "C",
  "answer": "Utilize an enterprise password manager with password sharing features"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3283dee6-1985-45fe-834c-6605e5420732-D",
  "QuestionId": "IAM.3283dee6-1985-45fe-834c-6605e5420732",
  "choice": "D",
  "answer": "Remove the system from the network until a replacement can be identified"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3283dee6-1985-45fe-834c-6605e5420732-E",
  "QuestionId": "IAM.3283dee6-1985-45fe-834c-6605e5420732",
  "choice": "E",
  "answer": "Assign one admin to perform all tasks on the system"
 },
 {
  "type": "ANSWER",
  "id": "IAM.770f3370-d056-46f3-8353-5ddd04312277-A",
  "QuestionId": "IAM.770f3370-d056-46f3-8353-5ddd04312277",
  "choice": "A",
  "answer": "Challenges are encrypted using a symmetric algorithm"
 },
 {
  "type": "ANSWER",
  "id": "IAM.770f3370-d056-46f3-8353-5ddd04312277-B",
  "QuestionId": "IAM.770f3370-d056-46f3-8353-5ddd04312277",
  "choice": "B",
  "answer": "Authentication is negotiated via a 3-way handshake"
 },
 {
  "type": "ANSWER",
  "id": "IAM.770f3370-d056-46f3-8353-5ddd04312277-C",
  "QuestionId": "IAM.770f3370-d056-46f3-8353-5ddd04312277",
  "choice": "C",
  "answer": "Authenticator will randomly require re-authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.770f3370-d056-46f3-8353-5ddd04312277-D",
  "QuestionId": "IAM.770f3370-d056-46f3-8353-5ddd04312277",
  "choice": "D",
  "answer": "CHAP support mutual authentication by client and server"
 },
 {
  "type": "ANSWER",
  "id": "IAM.c99147e4-2de5-4612-9186-a9490e03b317-A",
  "QuestionId": "IAM.c99147e4-2de5-4612-9186-a9490e03b317",
  "choice": "A",
  "answer": "Principle of least privilege"
 },
 {
  "type": "ANSWER",
  "id": "IAM.c99147e4-2de5-4612-9186-a9490e03b317-B",
  "QuestionId": "IAM.c99147e4-2de5-4612-9186-a9490e03b317",
  "choice": "B",
  "answer": "Locards Principle"
 },
 {
  "type": "ANSWER",
  "id": "IAM.c99147e4-2de5-4612-9186-a9490e03b317-C",
  "QuestionId": "IAM.c99147e4-2de5-4612-9186-a9490e03b317",
  "choice": "C",
  "answer": "Need to know"
 },
 {
  "type": "ANSWER",
  "id": "IAM.c99147e4-2de5-4612-9186-a9490e03b317-D",
  "QuestionId": "IAM.c99147e4-2de5-4612-9186-a9490e03b317",
  "choice": "D",
  "answer": "Separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "IAM.c99147e4-2de5-4612-9186-a9490e03b317-E",
  "QuestionId": "IAM.c99147e4-2de5-4612-9186-a9490e03b317",
  "choice": "E",
  "answer": "Tranquility principle"
 },
 {
  "type": "ANSWER",
  "id": "IAM.c99147e4-2de5-4612-9186-a9490e03b317-F",
  "QuestionId": "IAM.c99147e4-2de5-4612-9186-a9490e03b317",
  "choice": "F",
  "answer": "Principle of accountability"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343-A",
  "QuestionId": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343",
  "choice": "A",
  "answer": "Password"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343-B",
  "QuestionId": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343",
  "choice": "B",
  "answer": "Rentinal Scan"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343-C",
  "QuestionId": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343",
  "choice": "C",
  "answer": "Soft token"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343-D",
  "QuestionId": "IAM.4bd05ed4-c3f3-4186-b45e-f1d1d2a8a343",
  "choice": "D",
  "answer": "vascular pattern scanner"
 },
 {
  "type": "ANSWER",
  "id": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13-A",
  "QuestionId": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13",
  "choice": "A",
  "answer": "Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13-B",
  "QuestionId": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13",
  "choice": "B",
  "answer": "ACLs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13-C",
  "QuestionId": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13",
  "choice": "C",
  "answer": "Digital Signatures"
 },
 {
  "type": "ANSWER",
  "id": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13-D",
  "QuestionId": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13",
  "choice": "D",
  "answer": "Disk Quotas"
 },
 {
  "type": "ANSWER",
  "id": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13-E",
  "QuestionId": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13",
  "choice": "E",
  "answer": "Encryption"
 },
 {
  "type": "ANSWER",
  "id": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13-F",
  "QuestionId": "SRM.82e6bd9f-2557-44c5-9ba9-b89c831e3a13",
  "choice": "F",
  "answer": "Secure Logging"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19-A",
  "QuestionId": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19",
  "choice": "A",
  "answer": "Discretionary Access Control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19-B",
  "QuestionId": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19",
  "choice": "B",
  "answer": "Mandatory Access Control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19-C",
  "QuestionId": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19",
  "choice": "C",
  "answer": "Role Based Acess Control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19-D",
  "QuestionId": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19",
  "choice": "D",
  "answer": "View Based Access Control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19-E",
  "QuestionId": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19",
  "choice": "E",
  "answer": "Content Dependent Access Control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19-F",
  "QuestionId": "IAM.f49428d3-894d-4c25-aaa8-3665d739df19",
  "choice": "F",
  "answer": "Context Based Access Control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92-A",
  "QuestionId": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92",
  "choice": "A",
  "answer": "Processing overhead."
 },
 {
  "type": "ANSWER",
  "id": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92-B",
  "QuestionId": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92",
  "choice": "B",
  "answer": "It ensures concurrency."
 },
 {
  "type": "ANSWER",
  "id": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92-C",
  "QuestionId": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92",
  "choice": "C",
  "answer": "It disallows data locking."
 },
 {
  "type": "ANSWER",
  "id": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92-D",
  "QuestionId": "IAM.96369afd-0a44-4936-8ae8-2782b13ccf92",
  "choice": "D",
  "answer": "Granular control."
 },
 {
  "type": "ANSWER",
  "id": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680-A",
  "QuestionId": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680",
  "choice": "A",
  "answer": "Through cell suppression"
 },
 {
  "type": "ANSWER",
  "id": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680-B",
  "QuestionId": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680",
  "choice": "B",
  "answer": "By a trusted back end"
 },
 {
  "type": "ANSWER",
  "id": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680-C",
  "QuestionId": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680",
  "choice": "C",
  "answer": "By a trusted front end"
 },
 {
  "type": "ANSWER",
  "id": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680-D",
  "QuestionId": "IAM.80b59f91-e8a6-47ea-bf7f-aae9b60c6680",
  "choice": "D",
  "answer": "By views"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa-A",
  "QuestionId": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa",
  "choice": "A",
  "answer": "Discretionary"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa-B",
  "QuestionId": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa",
  "choice": "B",
  "answer": "Context-Dependent"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa-C",
  "QuestionId": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa",
  "choice": "C",
  "answer": "Non-Discretionary"
 },
 {
  "type": "ANSWER",
  "id": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa-D",
  "QuestionId": "IAM.4267a455-0292-4a85-be62-44f74fb8bbfa",
  "choice": "D",
  "answer": "View Based"
 },
 {
  "type": "ANSWER",
  "id": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e-A",
  "QuestionId": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e",
  "choice": "A",
  "answer": "A split knowledge system"
 },
 {
  "type": "ANSWER",
  "id": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e-B",
  "QuestionId": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e",
  "choice": "B",
  "answer": "A username with an iris scanner"
 },
 {
  "type": "ANSWER",
  "id": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e-C",
  "QuestionId": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e",
  "choice": "C",
  "answer": "A smartcard and a PIN"
 },
 {
  "type": "ANSWER",
  "id": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e-D",
  "QuestionId": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e",
  "choice": "D",
  "answer": "A password and a PIN"
 },
 {
  "type": "ANSWER",
  "id": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e-E",
  "QuestionId": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e",
  "choice": "E",
  "answer": "A passphrase and a CAPTCHA challenge"
 },
 {
  "type": "ANSWER",
  "id": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e-F",
  "QuestionId": "IAM.40b54081-909d-4fd1-93a3-674b1c43707e",
  "choice": "F",
  "answer": "A passphrase and a pre-shared key"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de-A",
  "QuestionId": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de",
  "choice": "A",
  "answer": "Kerberos"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de-B",
  "QuestionId": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de",
  "choice": "B",
  "answer": "PKI Certificates"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de-C",
  "QuestionId": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de",
  "choice": "C",
  "answer": "Directory Services"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de-D",
  "QuestionId": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de",
  "choice": "D",
  "answer": "Diffie-Helman"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de-E",
  "QuestionId": "IAM.8aa1c3e6-f08a-4695-b5ca-232a5c0fd9de",
  "choice": "E",
  "answer": "Risk Management Framework"
 },
 {
  "type": "ANSWER",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b-A",
  "QuestionId": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "choice": "A",
  "answer": "Iris Scan"
 },
 {
  "type": "ANSWER",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b-B",
  "QuestionId": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "choice": "B",
  "answer": "Hand Geometry"
 },
 {
  "type": "ANSWER",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b-C",
  "QuestionId": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "choice": "C",
  "answer": "Palm Scan"
 },
 {
  "type": "ANSWER",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b-D",
  "QuestionId": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "choice": "D",
  "answer": "Fingerprint Scan"
 },
 {
  "type": "ANSWER",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b-E",
  "QuestionId": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "choice": "E",
  "answer": "Retina Scan"
 },
 {
  "type": "ANSWER",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b-F",
  "QuestionId": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "choice": "F",
  "answer": "Voice Analysis"
 },
 {
  "type": "ANSWER",
  "id": "IAM.87807170-05c0-4866-b498-59b06b16382b-G",
  "QuestionId": "IAM.87807170-05c0-4866-b498-59b06b16382b",
  "choice": "G",
  "answer": "Signature Dyanmics"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab-A",
  "QuestionId": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab",
  "choice": "A",
  "answer": "Authorization"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab-B",
  "QuestionId": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab",
  "choice": "B",
  "answer": "Identification"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab-C",
  "QuestionId": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab",
  "choice": "C",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab-D",
  "QuestionId": "IAM.b4e9ee30-4a94-4286-9c4e-2c6d4b2dbdab",
  "choice": "D",
  "answer": "Remediation"
 },
 {
  "type": "ANSWER",
  "id": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729-A",
  "QuestionId": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729",
  "choice": "A",
  "answer": "1"
 },
 {
  "type": "ANSWER",
  "id": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729-B",
  "QuestionId": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729",
  "choice": "B",
  "answer": "2"
 },
 {
  "type": "ANSWER",
  "id": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729-C",
  "QuestionId": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729",
  "choice": "C",
  "answer": "3"
 },
 {
  "type": "ANSWER",
  "id": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729-D",
  "QuestionId": "IAM.74d782ed-a6a3-4fcd-bf2d-8f2366fa4729",
  "choice": "D",
  "answer": "4"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0f947655-5374-4afd-84b4-c74eeced675f-A",
  "QuestionId": "IAM.0f947655-5374-4afd-84b4-c74eeced675f",
  "choice": "A",
  "answer": "ID Card and PIN"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0f947655-5374-4afd-84b4-c74eeced675f-B",
  "QuestionId": "IAM.0f947655-5374-4afd-84b4-c74eeced675f",
  "choice": "B",
  "answer": "retinal scan and fingerprint"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0f947655-5374-4afd-84b4-c74eeced675f-C",
  "QuestionId": "IAM.0f947655-5374-4afd-84b4-c74eeced675f",
  "choice": "C",
  "answer": "password and security questions"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0f947655-5374-4afd-84b4-c74eeced675f-D",
  "QuestionId": "IAM.0f947655-5374-4afd-84b4-c74eeced675f",
  "choice": "D",
  "answer": "ID Card and Key"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1-A",
  "QuestionId": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1",
  "choice": "A",
  "answer": "HOTP"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1-B",
  "QuestionId": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1",
  "choice": "B",
  "answer": "SSL"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1-C",
  "QuestionId": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1",
  "choice": "C",
  "answer": "HMAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1-D",
  "QuestionId": "IAM.7c8da458-e0de-4714-9dd4-1d6aceee12b1",
  "choice": "D",
  "answer": "TOTP"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b-A",
  "QuestionId": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b",
  "choice": "A",
  "answer": "SAML"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b-B",
  "QuestionId": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b",
  "choice": "B",
  "answer": "PAP"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b-C",
  "QuestionId": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b",
  "choice": "C",
  "answer": "CHAP"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b-D",
  "QuestionId": "IAM.1b8a32cb-5f08-4057-bef7-7955d3f9756b",
  "choice": "D",
  "answer": "Kerberos"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0-A",
  "QuestionId": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0",
  "choice": "A",
  "answer": "Google Accounts"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0-B",
  "QuestionId": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0",
  "choice": "B",
  "answer": "Twitter Accounts"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0-C",
  "QuestionId": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0",
  "choice": "C",
  "answer": "Facebook Connect"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0-D",
  "QuestionId": "IAM.1af7f6d7-bd35-4788-bc44-d633b05ed5e0",
  "choice": "D",
  "answer": "RADIUS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848-A",
  "QuestionId": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848",
  "choice": "A",
  "answer": "TACACS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848-B",
  "QuestionId": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848",
  "choice": "B",
  "answer": "RADIUS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848-C",
  "QuestionId": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848",
  "choice": "C",
  "answer": "XTACACS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848-D",
  "QuestionId": "IAM.47b9a1e4-774a-42f3-ba59-a04cf0893848",
  "choice": "D",
  "answer": "TACACS+"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5b1b533c-12af-4a16-8927-8268092a3767-A",
  "QuestionId": "IAM.5b1b533c-12af-4a16-8927-8268092a3767",
  "choice": "A",
  "answer": "TGT"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5b1b533c-12af-4a16-8927-8268092a3767-B",
  "QuestionId": "IAM.5b1b533c-12af-4a16-8927-8268092a3767",
  "choice": "B",
  "answer": "SS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5b1b533c-12af-4a16-8927-8268092a3767-C",
  "QuestionId": "IAM.5b1b533c-12af-4a16-8927-8268092a3767",
  "choice": "C",
  "answer": "TGS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.5b1b533c-12af-4a16-8927-8268092a3767-D",
  "QuestionId": "IAM.5b1b533c-12af-4a16-8927-8268092a3767",
  "choice": "D",
  "answer": "AS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2-A",
  "QuestionId": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2",
  "choice": "A",
  "answer": "Service Provider"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2-B",
  "QuestionId": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2",
  "choice": "B",
  "answer": "Principal"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2-C",
  "QuestionId": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2",
  "choice": "C",
  "answer": "Identity Provider"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2-D",
  "QuestionId": "IAM.9342a92e-143e-40e7-a459-44f73f3e94b2",
  "choice": "D",
  "answer": "Authentication Source"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-A",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "A",
  "answer": "Identification"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-B",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "B",
  "answer": "Authorization"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-C",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "C",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-D",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "D",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-E",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "E",
  "answer": "Secure Logging"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-F",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "F",
  "answer": "Auditing"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-G",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "G",
  "answer": "Scalability"
 },
 {
  "type": "ANSWER",
  "id": "IAM.53804faf-84c0-4d62-993b-299c33fae693-H",
  "QuestionId": "IAM.53804faf-84c0-4d62-993b-299c33fae693",
  "choice": "H",
  "answer": "Availability"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a1969183-1ede-4460-a003-858632776137-A",
  "QuestionId": "IAM.a1969183-1ede-4460-a003-858632776137",
  "choice": "A",
  "answer": "Enforce minimum password length"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a1969183-1ede-4460-a003-858632776137-B",
  "QuestionId": "IAM.a1969183-1ede-4460-a003-858632776137",
  "choice": "B",
  "answer": "Enforce password expiry after set periods"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a1969183-1ede-4460-a003-858632776137-C",
  "QuestionId": "IAM.a1969183-1ede-4460-a003-858632776137",
  "choice": "C",
  "answer": "Enforce minimum password complexity"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a1969183-1ede-4460-a003-858632776137-D",
  "QuestionId": "IAM.a1969183-1ede-4460-a003-858632776137",
  "choice": "D",
  "answer": "Prevent password reuse on reset"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a1969183-1ede-4460-a003-858632776137-E",
  "QuestionId": "IAM.a1969183-1ede-4460-a003-858632776137",
  "choice": "E",
  "answer": "Require re-authentication on set time periods"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a1969183-1ede-4460-a003-858632776137-F",
  "QuestionId": "IAM.a1969183-1ede-4460-a003-858632776137",
  "choice": "F",
  "answer": "Lock out users if too many incorrect attempts occur"
 },
 {
  "type": "ANSWER",
  "id": "IAM.68ce9b3a-4633-4371-a972-4b160917b052-A",
  "QuestionId": "IAM.68ce9b3a-4633-4371-a972-4b160917b052",
  "choice": "A",
  "answer": "Privilege Creep"
 },
 {
  "type": "ANSWER",
  "id": "IAM.68ce9b3a-4633-4371-a972-4b160917b052-B",
  "QuestionId": "IAM.68ce9b3a-4633-4371-a972-4b160917b052",
  "choice": "B",
  "answer": "Separation of Duties"
 },
 {
  "type": "ANSWER",
  "id": "IAM.68ce9b3a-4633-4371-a972-4b160917b052-C",
  "QuestionId": "IAM.68ce9b3a-4633-4371-a972-4b160917b052",
  "choice": "C",
  "answer": "Least Privilege"
 },
 {
  "type": "ANSWER",
  "id": "IAM.68ce9b3a-4633-4371-a972-4b160917b052-D",
  "QuestionId": "IAM.68ce9b3a-4633-4371-a972-4b160917b052",
  "choice": "D",
  "answer": "Mandatory Vacations"
 },
 {
  "type": "ANSWER",
  "id": "IAM.682a4be3-9472-4a58-956d-9706a100ba27-A",
  "QuestionId": "IAM.682a4be3-9472-4a58-956d-9706a100ba27",
  "choice": "A",
  "answer": "SCEP"
 },
 {
  "type": "ANSWER",
  "id": "IAM.682a4be3-9472-4a58-956d-9706a100ba27-B",
  "QuestionId": "IAM.682a4be3-9472-4a58-956d-9706a100ba27",
  "choice": "B",
  "answer": "GPO"
 },
 {
  "type": "ANSWER",
  "id": "IAM.682a4be3-9472-4a58-956d-9706a100ba27-C",
  "QuestionId": "IAM.682a4be3-9472-4a58-956d-9706a100ba27",
  "choice": "C",
  "answer": "MMC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.682a4be3-9472-4a58-956d-9706a100ba27-D",
  "QuestionId": "IAM.682a4be3-9472-4a58-956d-9706a100ba27",
  "choice": "D",
  "answer": "ADUC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b-A",
  "QuestionId": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b",
  "choice": "A",
  "answer": "Login Time"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b-B",
  "QuestionId": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b",
  "choice": "B",
  "answer": "Incorrect Login Attempts"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b-C",
  "QuestionId": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b",
  "choice": "C",
  "answer": "Password"
 },
 {
  "type": "ANSWER",
  "id": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b-D",
  "QuestionId": "IAM.b9dc3219-1d5a-427f-a97d-cf8afb2ea42b",
  "choice": "D",
  "answer": "Login Location"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86-A",
  "QuestionId": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86",
  "choice": "A",
  "answer": "Job Rotation"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86-B",
  "QuestionId": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86",
  "choice": "B",
  "answer": "Least Privilege"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86-C",
  "QuestionId": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86",
  "choice": "C",
  "answer": "Privilege Creep"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86-D",
  "QuestionId": "IAM.d0dbe92f-84ab-402c-96b1-899329d87b86",
  "choice": "D",
  "answer": "Separation of Duties"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069-A",
  "QuestionId": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069",
  "choice": "A",
  "answer": "secheck"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069-B",
  "QuestionId": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069",
  "choice": "B",
  "answer": "selmodule"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069-C",
  "QuestionId": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069",
  "choice": "C",
  "answer": "getenforce"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069-D",
  "QuestionId": "IAM.bb58de21-d4eb-4cd5-9abc-530187f2a069",
  "choice": "D",
  "answer": "fsck"
 },
 {
  "type": "ANSWER",
  "id": "IAM.893d3c77-2980-426e-9386-0a619f3974e9-A",
  "QuestionId": "IAM.893d3c77-2980-426e-9386-0a619f3974e9",
  "choice": "A",
  "answer": "True"
 },
 {
  "type": "ANSWER",
  "id": "IAM.893d3c77-2980-426e-9386-0a619f3974e9-B",
  "QuestionId": "IAM.893d3c77-2980-426e-9386-0a619f3974e9",
  "choice": "B",
  "answer": "False"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d-A",
  "QuestionId": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d",
  "choice": "A",
  "answer": "read and execute"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d-B",
  "QuestionId": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d",
  "choice": "B",
  "answer": "read"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d-C",
  "QuestionId": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d",
  "choice": "C",
  "answer": "modify"
 },
 {
  "type": "ANSWER",
  "id": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d-D",
  "QuestionId": "IAM.9b6a4de5-fb7c-473b-bd8b-377f1bc28d6d",
  "choice": "D",
  "answer": "full control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099-A",
  "QuestionId": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099",
  "choice": "A",
  "answer": "Rainbow Tables"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099-B",
  "QuestionId": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099",
  "choice": "B",
  "answer": "Hybrid"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099-C",
  "QuestionId": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099",
  "choice": "C",
  "answer": "Brute Force"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099-D",
  "QuestionId": "IAM.bb53e966-5c73-42ed-87af-23b8fe981099",
  "choice": "D",
  "answer": "Dictionary"
 },
 {
  "type": "ANSWER",
  "id": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce-A",
  "QuestionId": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce",
  "choice": "A",
  "answer": "software distribution site"
 },
 {
  "type": "ANSWER",
  "id": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce-B",
  "QuestionId": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce",
  "choice": "B",
  "answer": "known malicious site"
 },
 {
  "type": "ANSWER",
  "id": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce-C",
  "QuestionId": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce",
  "choice": "C",
  "answer": "site trusted by the end user"
 },
 {
  "type": "ANSWER",
  "id": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce-D",
  "QuestionId": "IAM.65aa0101-b702-47cc-8c80-c52db17d4cce",
  "choice": "D",
  "answer": "hacker forum"
 },
 {
  "type": "ANSWER",
  "id": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e-A",
  "QuestionId": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e",
  "choice": "A",
  "answer": "scarcity"
 },
 {
  "type": "ANSWER",
  "id": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e-B",
  "QuestionId": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e",
  "choice": "B",
  "answer": "liking"
 },
 {
  "type": "ANSWER",
  "id": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e-C",
  "QuestionId": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e",
  "choice": "C",
  "answer": "social proof"
 },
 {
  "type": "ANSWER",
  "id": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e-D",
  "QuestionId": "IAM.cff2b913-4c3f-40cf-b1f1-4c085e90b40e",
  "choice": "D",
  "answer": "intimidation"
 },
 {
  "type": "ANSWER",
  "id": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e-A",
  "QuestionId": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e",
  "choice": "A",
  "answer": "vishing"
 },
 {
  "type": "ANSWER",
  "id": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e-B",
  "QuestionId": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e",
  "choice": "B",
  "answer": "spear phishing"
 },
 {
  "type": "ANSWER",
  "id": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e-C",
  "QuestionId": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e",
  "choice": "C",
  "answer": "whaling"
 },
 {
  "type": "ANSWER",
  "id": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e-D",
  "QuestionId": "IAM.df358c4c-0ae4-42fc-95c2-5c6d4c119f7e",
  "choice": "D",
  "answer": "pharming"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46-A",
  "QuestionId": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46",
  "choice": "A",
  "answer": "Information"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46-B",
  "QuestionId": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46",
  "choice": "B",
  "answer": "Systems"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46-C",
  "QuestionId": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46",
  "choice": "C",
  "answer": "Devices"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46-D",
  "QuestionId": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46",
  "choice": "D",
  "answer": "Facilities"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46-E",
  "QuestionId": "IAM.6a5039d2-ce92-4753-8c9a-72af3de33b46",
  "choice": "E",
  "answer": "None of the above"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8ede983b-5750-4d24-a373-d3999224f448-A",
  "QuestionId": "IAM.8ede983b-5750-4d24-a373-d3999224f448",
  "choice": "A",
  "answer": "A subject is always a user account"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8ede983b-5750-4d24-a373-d3999224f448-B",
  "QuestionId": "IAM.8ede983b-5750-4d24-a373-d3999224f448",
  "choice": "B",
  "answer": "The subject is always the entity that hosts information or data"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8ede983b-5750-4d24-a373-d3999224f448-C",
  "QuestionId": "IAM.8ede983b-5750-4d24-a373-d3999224f448",
  "choice": "C",
  "answer": "The subject is always the entity that receives information about or data from an object"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8ede983b-5750-4d24-a373-d3999224f448-D",
  "QuestionId": "IAM.8ede983b-5750-4d24-a373-d3999224f448",
  "choice": "D",
  "answer": "A single entity can never change roles between subject and object"
 },
 {
  "type": "ANSWER",
  "id": "IAM.46d03007-1312-48cf-ab82-8b6025d25020-A",
  "QuestionId": "IAM.46d03007-1312-48cf-ab82-8b6025d25020",
  "choice": "A",
  "answer": "Preventative"
 },
 {
  "type": "ANSWER",
  "id": "IAM.46d03007-1312-48cf-ab82-8b6025d25020-B",
  "QuestionId": "IAM.46d03007-1312-48cf-ab82-8b6025d25020",
  "choice": "B",
  "answer": "Detective"
 },
 {
  "type": "ANSWER",
  "id": "IAM.46d03007-1312-48cf-ab82-8b6025d25020-C",
  "QuestionId": "IAM.46d03007-1312-48cf-ab82-8b6025d25020",
  "choice": "C",
  "answer": "Corrective"
 },
 {
  "type": "ANSWER",
  "id": "IAM.46d03007-1312-48cf-ab82-8b6025d25020-D",
  "QuestionId": "IAM.46d03007-1312-48cf-ab82-8b6025d25020",
  "choice": "D",
  "answer": "Authoritative"
 },
 {
  "type": "ANSWER",
  "id": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf-A",
  "QuestionId": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf",
  "choice": "A",
  "answer": "Administrative"
 },
 {
  "type": "ANSWER",
  "id": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf-B",
  "QuestionId": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf",
  "choice": "B",
  "answer": "Logical/Technical"
 },
 {
  "type": "ANSWER",
  "id": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf-C",
  "QuestionId": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf",
  "choice": "C",
  "answer": "Physical"
 },
 {
  "type": "ANSWER",
  "id": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf-D",
  "QuestionId": "IAM.e37e705c-a3c5-4195-af85-431d0a92b4bf",
  "choice": "D",
  "answer": "Preventative"
 },
 {
  "type": "ANSWER",
  "id": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb-A",
  "QuestionId": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb",
  "choice": "A",
  "answer": "Preserve Confidentiality, integrity, and availability of systems and data"
 },
 {
  "type": "ANSWER",
  "id": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb-B",
  "QuestionId": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb",
  "choice": "B",
  "answer": "Ensure on valid objects can authenticate in the system"
 },
 {
  "type": "ANSWER",
  "id": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb-C",
  "QuestionId": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb",
  "choice": "C",
  "answer": "Prevent unauthorized access to subjects"
 },
 {
  "type": "ANSWER",
  "id": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb-D",
  "QuestionId": "IAM.092c642f-540d-4974-aa7e-b4a9d15f41cb",
  "choice": "D",
  "answer": "Ensure that all subjects are authenticated"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8-A",
  "QuestionId": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8",
  "choice": "A",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8-B",
  "QuestionId": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8",
  "choice": "B",
  "answer": "Authorization"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8-C",
  "QuestionId": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8",
  "choice": "C",
  "answer": "Accountability"
 },
 {
  "type": "ANSWER",
  "id": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8-D",
  "QuestionId": "IAM.bf7b94d4-1ceb-4500-8a1a-f35d690fc0e8",
  "choice": "D",
  "answer": "Identification"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e-A",
  "QuestionId": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e",
  "choice": "A",
  "answer": "Identitication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e-B",
  "QuestionId": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e",
  "choice": "B",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e-C",
  "QuestionId": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e",
  "choice": "C",
  "answer": "Auditing"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e-D",
  "QuestionId": "IAM.0ac0ec8a-9e02-4870-be07-2468b83b3a0e",
  "choice": "D",
  "answer": "Authorization"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7-A",
  "QuestionId": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7",
  "choice": "A",
  "answer": "It is short"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7-B",
  "QuestionId": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7",
  "choice": "B",
  "answer": "It is easy to remember"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7-C",
  "QuestionId": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7",
  "choice": "C",
  "answer": "It includes a single set of characters"
 },
 {
  "type": "ANSWER",
  "id": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7-D",
  "QuestionId": "IAM.0fafaf7f-f9aa-4d67-a04d-ba77fc634bf7",
  "choice": "D",
  "answer": "It is easy to crack"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18-A",
  "QuestionId": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18",
  "choice": "A",
  "answer": "Something you have"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18-B",
  "QuestionId": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18",
  "choice": "B",
  "answer": "Something you are"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18-C",
  "QuestionId": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18",
  "choice": "C",
  "answer": "Something you do"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18-D",
  "QuestionId": "IAM.d0ef1137-a405-4f70-8ba6-17e2df588e18",
  "choice": "D",
  "answer": "Something you know"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca-A",
  "QuestionId": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca",
  "choice": "A",
  "answer": "Synchronous Token"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca-B",
  "QuestionId": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca",
  "choice": "B",
  "answer": "Asynchronous Token"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca-C",
  "QuestionId": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca",
  "choice": "C",
  "answer": "Smartcard"
 },
 {
  "type": "ANSWER",
  "id": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca-D",
  "QuestionId": "IAM.a48f6c2b-0544-4b67-b3f7-879cbedec1ca",
  "choice": "D",
  "answer": "Common Access Card"
 },
 {
  "type": "ANSWER",
  "id": "IAM.dda87e4d-a132-4596-bd50-884365717aaa-A",
  "QuestionId": "IAM.dda87e4d-a132-4596-bd50-884365717aaa",
  "choice": "A",
  "answer": "The sensitivity is to high"
 },
 {
  "type": "ANSWER",
  "id": "IAM.dda87e4d-a132-4596-bd50-884365717aaa-B",
  "QuestionId": "IAM.dda87e4d-a132-4596-bd50-884365717aaa",
  "choice": "B",
  "answer": "The sensitivity is to low"
 },
 {
  "type": "ANSWER",
  "id": "IAM.dda87e4d-a132-4596-bd50-884365717aaa-C",
  "QuestionId": "IAM.dda87e4d-a132-4596-bd50-884365717aaa",
  "choice": "C",
  "answer": "It indicates the point where false rejection rate equals false acceptance rate"
 },
 {
  "type": "ANSWER",
  "id": "IAM.dda87e4d-a132-4596-bd50-884365717aaa-D",
  "QuestionId": "IAM.dda87e4d-a132-4596-bd50-884365717aaa",
  "choice": "D",
  "answer": "When high enought, the biometric device is highly accurate"
 },
 {
  "type": "ANSWER",
  "id": "IAM.fe58e997-4925-4850-8273-cbb52add565b-A",
  "QuestionId": "IAM.fe58e997-4925-4850-8273-cbb52add565b",
  "choice": "A",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "IAM.fe58e997-4925-4850-8273-cbb52add565b-B",
  "QuestionId": "IAM.fe58e997-4925-4850-8273-cbb52add565b",
  "choice": "B",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "IAM.fe58e997-4925-4850-8273-cbb52add565b-C",
  "QuestionId": "IAM.fe58e997-4925-4850-8273-cbb52add565b",
  "choice": "C",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "IAM.fe58e997-4925-4850-8273-cbb52add565b-D",
  "QuestionId": "IAM.fe58e997-4925-4850-8273-cbb52add565b",
  "choice": "D",
  "answer": "Accountability"
 },
 {
  "type": "ANSWER",
  "id": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710-A",
  "QuestionId": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710",
  "choice": "A",
  "answer": "Kerberos"
 },
 {
  "type": "ANSWER",
  "id": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710-B",
  "QuestionId": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710",
  "choice": "B",
  "answer": "HTML"
 },
 {
  "type": "ANSWER",
  "id": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710-C",
  "QuestionId": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710",
  "choice": "C",
  "answer": "XML"
 },
 {
  "type": "ANSWER",
  "id": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710-D",
  "QuestionId": "IAM.2496fbc4-8b48-4c31-9df6-cff19fd42710",
  "choice": "D",
  "answer": "SAML"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e-A",
  "QuestionId": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e",
  "choice": "A",
  "answer": "Authentication Server"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e-B",
  "QuestionId": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e",
  "choice": "B",
  "answer": "Client"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e-C",
  "QuestionId": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e",
  "choice": "C",
  "answer": "AAA server"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e-D",
  "QuestionId": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e",
  "choice": "D",
  "answer": "Firewall"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e-E",
  "QuestionId": "IAM.7af826d2-2d5c-490b-a2a8-40a04dbdb64e",
  "choice": "E",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de-A",
  "QuestionId": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de",
  "choice": "A",
  "answer": "Distributed access control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de-B",
  "QuestionId": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de",
  "choice": "B",
  "answer": "Diameter"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de-C",
  "QuestionId": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de",
  "choice": "C",
  "answer": "TACACS+"
 },
 {
  "type": "ANSWER",
  "id": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de-D",
  "QuestionId": "IAM.7afb7b76-27b1-4425-8bb4-592e67ca03de",
  "choice": "D",
  "answer": "TACACS"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983-A",
  "QuestionId": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983",
  "choice": "A",
  "answer": "All actions that are not expressly denied are allowed"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983-B",
  "QuestionId": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983",
  "choice": "B",
  "answer": "All actions that are not expressly allowed are denied"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983-C",
  "QuestionId": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983",
  "choice": "C",
  "answer": "All actions must be expressly denied"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983-D",
  "QuestionId": "IAM.d4bc9d4c-2e78-498d-8f7a-b577c6369983",
  "choice": "D",
  "answer": "None of the above"
 },
 {
  "type": "ANSWER",
  "id": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1-A",
  "QuestionId": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1",
  "choice": "A",
  "answer": "Enforce the most restrictive rights required by users to run system processes."
 },
 {
  "type": "ANSWER",
  "id": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1-B",
  "QuestionId": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1",
  "choice": "B",
  "answer": "Enforce the least restrictive rights required by users to run system processes."
 },
 {
  "type": "ANSWER",
  "id": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1-C",
  "QuestionId": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1",
  "choice": "C",
  "answer": "Enforce the most restrictive rights required by users to complete assigned tasks."
 },
 {
  "type": "ANSWER",
  "id": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1-D",
  "QuestionId": "IAM.fd8fa020-0e29-43d5-a1dd-344e2254b2b1",
  "choice": "D",
  "answer": "Enforce the least restrictive rights required by users to complete assigned tasks."
 },
 {
  "type": "ANSWER",
  "id": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c-A",
  "QuestionId": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c",
  "choice": "A",
  "answer": "Access Control List"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c-B",
  "QuestionId": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c",
  "choice": "B",
  "answer": "Access control matrix"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c-C",
  "QuestionId": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c",
  "choice": "C",
  "answer": "Federation"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c-D",
  "QuestionId": "IAM.8c37636c-359e-43ae-ac6d-074e6db84b7c",
  "choice": "D",
  "answer": "Creeping Privilege"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba-A",
  "QuestionId": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba",
  "choice": "A",
  "answer": "Administrators"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba-B",
  "QuestionId": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba",
  "choice": "B",
  "answer": "Access Controls list"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba-C",
  "QuestionId": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba",
  "choice": "C",
  "answer": "Assigned Labels"
 },
 {
  "type": "ANSWER",
  "id": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba-D",
  "QuestionId": "IAM.3b70e3de-19b1-489d-b037-1094c97adcba",
  "choice": "D",
  "answer": "The data customers"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2-A",
  "QuestionId": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2",
  "choice": "A",
  "answer": "DAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2-B",
  "QuestionId": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2",
  "choice": "B",
  "answer": "RBAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2-C",
  "QuestionId": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2",
  "choice": "C",
  "answer": "Rule-based access control"
 },
 {
  "type": "ANSWER",
  "id": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2-D",
  "QuestionId": "IAM.d3c14785-e47a-46ca-a7a7-f9c31f2c19e2",
  "choice": "D",
  "answer": "MAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86-A",
  "QuestionId": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86",
  "choice": "A",
  "answer": "Access Control List"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86-B",
  "QuestionId": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86",
  "choice": "B",
  "answer": "An access control matrix"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86-C",
  "QuestionId": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86",
  "choice": "C",
  "answer": "Discretionary Access Control model"
 },
 {
  "type": "ANSWER",
  "id": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86-D",
  "QuestionId": "IAM.f5b7b065-2934-4c24-a0b9-dfdbb49dbb86",
  "choice": "D",
  "answer": "Non discretionary access control model"
 },
 {
  "type": "ANSWER",
  "id": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf-A",
  "QuestionId": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf",
  "choice": "A",
  "answer": "A RBAC model allows users membership in multiple groups."
 },
 {
  "type": "ANSWER",
  "id": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf-B",
  "QuestionId": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf",
  "choice": "B",
  "answer": "A RBAC model allows users membership in a single group."
 },
 {
  "type": "ANSWER",
  "id": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf-C",
  "QuestionId": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf",
  "choice": "C",
  "answer": "A RBAC model is nonhierarchical."
 },
 {
  "type": "ANSWER",
  "id": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf-D",
  "QuestionId": "IAM.8688b741-2aa5-4a1a-a0e6-9a5bbc246daf",
  "choice": "D",
  "answer": "A RBAC model uses labels."
 },
 {
  "type": "ANSWER",
  "id": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01-A",
  "QuestionId": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01",
  "choice": "A",
  "answer": "Web Server"
 },
 {
  "type": "ANSWER",
  "id": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01-B",
  "QuestionId": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01",
  "choice": "B",
  "answer": "Application"
 },
 {
  "type": "ANSWER",
  "id": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01-C",
  "QuestionId": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01",
  "choice": "C",
  "answer": "Database"
 },
 {
  "type": "ANSWER",
  "id": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01-D",
  "QuestionId": "IAM.369e9b55-ec3e-4288-a735-4f988ff5bf01",
  "choice": "D",
  "answer": "Programmer"
 },
 {
  "type": "ANSWER",
  "id": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3-A",
  "QuestionId": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3",
  "choice": "A",
  "answer": "MAC Model"
 },
 {
  "type": "ANSWER",
  "id": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3-B",
  "QuestionId": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3",
  "choice": "B",
  "answer": "DAC Model"
 },
 {
  "type": "ANSWER",
  "id": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3-C",
  "QuestionId": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3",
  "choice": "C",
  "answer": "Rule Based Access Model"
 },
 {
  "type": "ANSWER",
  "id": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3-D",
  "QuestionId": "IAM.97b87f54-80fd-480b-bb33-c4b436669ca3",
  "choice": "D",
  "answer": "RBAC Model"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d-A",
  "QuestionId": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d",
  "choice": "A",
  "answer": "DAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d-B",
  "QuestionId": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d",
  "choice": "B",
  "answer": "NonDiscretionary"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d-C",
  "QuestionId": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d",
  "choice": "C",
  "answer": "MAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d-D",
  "QuestionId": "IAM.ea5c8909-852b-4e75-a88a-488b5001d16d",
  "choice": "D",
  "answer": "RBAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.78dcb889-35c7-4238-9319-56e907986260-A",
  "QuestionId": "IAM.78dcb889-35c7-4238-9319-56e907986260",
  "choice": "A",
  "answer": "Employs Explicit-Deny philosophy"
 },
 {
  "type": "ANSWER",
  "id": "IAM.78dcb889-35c7-4238-9319-56e907986260-B",
  "QuestionId": "IAM.78dcb889-35c7-4238-9319-56e907986260",
  "choice": "B",
  "answer": "Permissive"
 },
 {
  "type": "ANSWER",
  "id": "IAM.78dcb889-35c7-4238-9319-56e907986260-C",
  "QuestionId": "IAM.78dcb889-35c7-4238-9319-56e907986260",
  "choice": "C",
  "answer": "Rule Based"
 },
 {
  "type": "ANSWER",
  "id": "IAM.78dcb889-35c7-4238-9319-56e907986260-D",
  "QuestionId": "IAM.78dcb889-35c7-4238-9319-56e907986260",
  "choice": "D",
  "answer": "Prohibitive"
 },
 {
  "type": "ANSWER",
  "id": "IAM.78dcb889-35c7-4238-9319-56e907986260-E",
  "QuestionId": "IAM.78dcb889-35c7-4238-9319-56e907986260",
  "choice": "E",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d-A",
  "QuestionId": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d",
  "choice": "A",
  "answer": "DAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d-B",
  "QuestionId": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d",
  "choice": "B",
  "answer": "Nondiscretionary"
 },
 {
  "type": "ANSWER",
  "id": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d-C",
  "QuestionId": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d",
  "choice": "C",
  "answer": "MAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d-D",
  "QuestionId": "IAM.97a52d4a-818c-4e66-8388-dd022e09c20d",
  "choice": "D",
  "answer": "Compliance Based Access control model"
 },
 {
  "type": "ANSWER",
  "id": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e-A",
  "QuestionId": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e",
  "choice": "A",
  "answer": "Asset Valuation"
 },
 {
  "type": "ANSWER",
  "id": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e-B",
  "QuestionId": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e",
  "choice": "B",
  "answer": "Threat Modeling"
 },
 {
  "type": "ANSWER",
  "id": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e-C",
  "QuestionId": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e",
  "choice": "C",
  "answer": "Vulnerability Analysis"
 },
 {
  "type": "ANSWER",
  "id": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e-D",
  "QuestionId": "IAM.269dcb72-80ae-4bb5-a81a-5225e904933e",
  "choice": "D",
  "answer": "Access Review"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510-A",
  "QuestionId": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510",
  "choice": "A",
  "answer": "Rainbow Table"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510-B",
  "QuestionId": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510",
  "choice": "B",
  "answer": "Account Lockout"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510-C",
  "QuestionId": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510",
  "choice": "C",
  "answer": "Salting Passwords"
 },
 {
  "type": "ANSWER",
  "id": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510-D",
  "QuestionId": "IAM.ba1829a4-557f-4c3c-9257-9045f2487510",
  "choice": "D",
  "answer": "Encryption of Passwords"
 },
 {
  "type": "ANSWER",
  "id": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93-A",
  "QuestionId": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93",
  "choice": "A",
  "answer": "Hashing Passwords with MD5"
 },
 {
  "type": "ANSWER",
  "id": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93-B",
  "QuestionId": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93",
  "choice": "B",
  "answer": "Salt and Pepper with Hashing"
 },
 {
  "type": "ANSWER",
  "id": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93-C",
  "QuestionId": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93",
  "choice": "C",
  "answer": "Account Lockout"
 },
 {
  "type": "ANSWER",
  "id": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93-D",
  "QuestionId": "IAM.42d73c30-5a31-4744-8d32-aa64e7fe5e93",
  "choice": "D",
  "answer": "Implement RBAC"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09-A",
  "QuestionId": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09",
  "choice": "A",
  "answer": "Asset Valuation"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09-B",
  "QuestionId": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09",
  "choice": "B",
  "answer": "Threat Modeling Results"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09-C",
  "QuestionId": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09",
  "choice": "C",
  "answer": "Vulnerability Analaysis Reports"
 },
 {
  "type": "ANSWER",
  "id": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09-D",
  "QuestionId": "IAM.1f19865a-48c0-49c1-bc88-74a54dd69c09",
  "choice": "D",
  "answer": "Audit Trails"
 }
]