export const Log = [
 {
  "type": "QUESTION",
  "id": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c",
  "question": "Which of the following is a violation of the principles of least privelege (choose 2)?",
  "answer": "A,D",
  "explanation": "I am tempted to answer E but the rationale that D is better is that E is more about separation of duties than violation of least privilege."
 },
 {
  "type": "QUESTION",
  "id": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0",
  "question": "Your risk analysis team is overwhelmed with the process of information gathering and calculating values for 'what if' scenarios. Which is the best course of action.",
  "answer": "C",
  "explanation": "Apparently there are helpful automated risk management tools which can multiple team effort to consider a wider set of information."
 },
 {
  "type": "QUESTION",
  "id": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb",
  "question": "Which of the following would not be consider an indication of attack",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328",
  "question": "Which of the following is NOT an element of a security planning mission statement?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196",
  "question": "In data processing systems, the value analysis should be performed in terms of which three properties?",
  "answer": "D",
  "explanation": "A relates to considerations of business management.\nB related to considers of threat analysis.\nC relates to business impact analysis."
 },
 {
  "type": "QUESTION",
  "id": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb",
  "question": "Which of the following techniques MOST clearly indicates whether specific risk reduction controls should be implemented?",
  "answer": "D",
  "explanation": "ALE only consider impact but doesnt consider costs to mitigate risk"
 },
 {
  "type": "QUESTION",
  "id": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f",
  "question": "Place the following four elements of the Business Continuity Plan in the proper order.?",
  "answer": "B",
  "explanation": "Implement is last.\nScoping is first because its a management thing which does things like defined roles and responsibilities."
 },
 {
  "type": "QUESTION",
  "id": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886",
  "question": "Houston-based Sea Breeze Industries has determined that there is a possibility that it may be hit by a hurricane once every 10 years. The losses from such an event are calculated to be $1 million. What is the SLE for this event?",
  "answer": "A",
  "explanation": "SLE is about a single loss.\nThe ARO is frequency.\nThe ALE is SLE * ARO"
 },
 {
  "type": "QUESTION",
  "id": "SRM.1d96aeb3-e95a-4366-9320-892c42431698",
  "question": "The primary goal of a security awareness program is:?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9",
  "question": "You have changed your encryption algorithm for files on your server from 3DES to AES. Which type of control are you implementing. Choose two.",
  "answer": "C,E",
  "explanation": "Encryption prevents others from reading\nThis is technical control"
 },
 {
  "type": "QUESTION",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "question": "Crime Preventation through Environmental Design seeks to deter criminal activity through techniques for environment design. Which of the following are components of CPTEDs strategy",
  "answer": "A,E,G",
  "explanation": "Matural Access Control - making it obvious what is public vs private areas\nNatural Territorical Control - Making people feel they have ownership of the area\nNatural Survelliance - people more likely to create crime if they believe they are being seen doing it"
 },
 {
  "type": "QUESTION",
  "id": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6",
  "question": "Which of the following is a preventative security control",
  "answer": "B",
  "explanation": "NTFS prevents certain actions\nCompared to No Trepassing which Deter someone but dont prevent\nBackups are a corrective control\nCCTV is a detective control\nIDS is a detective control"
 },
 {
  "type": "QUESTION",
  "id": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.",
  "question": "What would not be considered in the creation of an occupant emergency plan (OEP), Emergency Action plan",
  "answer": "D",
  "explanation": "D would be part of a continuity of operations"
 },
 {
  "type": "QUESTION",
  "id": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25",
  "question": "An employee has written a script that changes values in a database before a billing application reads the values for payment processing . After the billing process runs , the malicious script changes the database back to its original values. What type of attack is this?",
  "answer": "E",
  "explanation": "Salalmi attack is changing a number slightly over time and hoping its not noticed."
 },
 {
  "type": "QUESTION",
  "id": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a",
  "question": "Which of the following is a characteristic of a trade secret",
  "answer": "C",
  "explanation": "A copyright protects the original expression of the idea rather than the idea itself"
 },
 {
  "type": "QUESTION",
  "id": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec",
  "question": "Who is ultimately responsible for accepting the risk associated with operating a system in your enterprise?",
  "answer": "D - this is someone who formally accepts the delivery",
  "explanation": "A- System owner is responsible for multiple aspect of developing the system\nISSO - is responsible for security concerns during development\nCIO - is responsible for budgets"
 },
 {
  "type": "QUESTION",
  "id": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.",
  "question": "When you combine the SLE (Single Loss Expectancy) x ARO (Annualized Rate of Occurence) you get what",
  "answer": "E",
  "explanation": "Exposure Factor is the percentage of an asset that is damaged when an incident occurs"
 },
 {
  "type": "QUESTION",
  "id": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b",
  "question": "In addition to Physical and Adminstration controls what are the other types of controls",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44",
  "question": "Which of the following is the most important objective acheived by an IT change management policy",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31",
  "question": "All of the following should be determined during a BIA exception:",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "question": "ISO/IEEE 15408 is an international standard for computer security certification. It provides a set of requirements for security functionality of IT products during security evaluation with the goal of providing a level of assurance that the product/system performs in a certain way. What is the common name for this standard?",
  "answer": "E",
  "explanation": "https://en.wikipedia.org/wiki/Common_Criteria"
 },
 {
  "type": "QUESTION",
  "id": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9",
  "question": "Textbooks define 'due care' as the actions a reasonable and prudent person or organization would take in order to protect an asset. What is the primary reason due care is taken.",
  "answer": "B",
  "explanation": "When participating in an activity you arent as liable if you have take due care."
 },
 {
  "type": "QUESTION",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "question": "DDL (Data Definition Language) is the standard for the language to create structures in a database.  What is the term used to refer to the logical database structure.",
  "answer": "B",
  "explanation": "DCL - Data Control Lanaguage - Commits, Rollback\nPolyinstantiation - Having records with the same primary key but separate column defines sensitivity. Record provided to end user depends upon sensitivity."
 },
 {
  "type": "QUESTION",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "question": "Which of the following would be considered an adminstrative control?",
  "answer": "A, D, F",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be",
  "question": "You are researching security solutions to provide overnight security for a large fenced in area that stores physical company assets. Your solution should act as a deterrent and provide the ability to gauge the level of corrective response. Which of the following is the most appropriate solution.",
  "answer": "D",
  "explanation": "Only security guard provides a gauging of appropriate corrective action"
 },
 {
  "type": "QUESTION",
  "id": "SRM.5615f348-7a38-44cc-88a7-a10382912b02",
  "question": "You have received a PDF file that was signed using a self signed certificate. Which of the following should you do before viewing the file?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876",
  "question": "What can HASH function tell you about a file change",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf",
  "question": "WHich of the following are not Integrity controls?",
  "answer": "C,E,F",
  "explanation": "Integrity are about detecting changes or non-repudiation"
 },
 {
  "type": "QUESTION",
  "id": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3",
  "question": "A user at a terminal needs to be able to securely log into a system. Both the system login process and the user need to have confidence that untrusted software can eject itself into the authentication process.  What is this called",
  "answer": "A",
  "explanation": "A trusted path is a channel established with strict standards to allow necessary communication to occur without exposing the TCB to security vulnerabilities. A trusted path also protects system users (sometimes known as subjects) from compromise as a result of a TCB interchange."
 },
 {
  "type": "QUESTION",
  "id": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34",
  "question": "An IT Contingency Planning Process consists of 7 broad steps. Which of the following is one of those steps? Choose 2",
  "answer": "B,E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d",
  "question": "What are all the following examples of : ISO 270001, NIST 800-53, COBIT",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9",
  "question": "Which one of the following is not one of the major principles of COBIT?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "question": "As  it relates to the GDPR, which of the following have been added under the requirement to protect persional information (Choose 2)",
  "answer": "E,H",
  "explanation": "Other items were in GDPR and Privacy Act"
 },
 {
  "type": "QUESTION",
  "id": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8",
  "question": "Which of the following is a new requirement under the EU GDPR?",
  "answer": "D",
  "explanation": "Now you have the right to request your account to be completely removed. Accounts cannot be deactivated."
 },
 {
  "type": "QUESTION",
  "id": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d",
  "question": "When making decisions about how to best secure user compueters and servers, which of the following is the most important consideration?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "question": "When a data breach occurs in which situation is notification not required in many Data Breach notification laws (choose 3)?",
  "answer": "D, G, I",
  "explanation": "Generally if encrypted information is breach it is not considered a data breach if the encryption keys are not breached. It also has to be personally identifiable which is why paswords and transactions are not data breach notifications."
 },
 {
  "type": "QUESTION",
  "id": "SRM.69ed37a2-38d2-413c-8691-1350f7793143",
  "question": "What law restricts government interception of private electronic communications?",
  "answer": "B",
  "explanation": "CFAA- Prevents unauthorized access\nITADA- Identity theft\nGLBA - Sharing financial information between branches"
 },
 {
  "type": "QUESTION",
  "id": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7",
  "question": "Dru recently developed a new name for a product that he will be selling. He would like to ensure that nobody else may use the same name. What type of intellectual property protection should he seek?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35",
  "question": "Which one of the following regulations and agencies is not involved in the export control process?",
  "answer": "D",
  "explanation": "ITAR - Defense restrictions\nEAR - Dual Use\nOFAC - restrictions on trade"
 },
 {
  "type": "QUESTION",
  "id": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289",
  "question": "What should an information policy NOT contain",
  "answer": "D",
  "explanation": "THe policies are not specific to be able to stand test of time"
 },
 {
  "type": "QUESTION",
  "id": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18",
  "question": "What security principle prevents against an individual having excess security rights?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604",
  "question": "Which of the following are NOT key principles of information security (choose 2)",
  "answer": "B,E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95",
  "question": "Which of the following is an example of a business continuity control",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9",
  "question": "Which of the following does NOT involves disk striping",
  "answer": "B",
  "explanation": "RAID 1 is pure mirroring"
 },
 {
  "type": "QUESTION",
  "id": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174",
  "question": "How many disks are required to implement disk striping with parity",
  "answer": "C",
  "explanation": "RAID 3 requires at least 2 disks to strip across and a 3rd for the parity information"
 },
 {
  "type": "QUESTION",
  "id": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2",
  "question": "WHich of the following control types is RAID",
  "answer": "C",
  "explanation": "In CISSP material HA is separate from Fault Tolerance"
 },
 {
  "type": "QUESTION",
  "id": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1",
  "question": "What type of control are we using if we supplement a single firewall with a second standby firewall ready to assume responsibility if the primary firewall fails?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac",
  "question": "What is the minimum number of disks to acheive RAID level 5",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50",
  "question": "Whats the first thing you should do if you observe a information security policy violation?",
  "answer": "C",
  "explanation": "Suggestion in this material is that need to be sensitive to causing more harm than good"
 },
 {
  "type": "QUESTION",
  "id": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18",
  "question": "Which of the following is not a way to protect sensitive employee information",
  "answer": "C",
  "explanation": "The others are mechanism to limit and protect the information kept by an organization"
 },
 {
  "type": "QUESTION",
  "id": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164",
  "question": "Which one of the following agreements is most directly designed to protect confidential information after an employee has left the organization?",
  "answer": "D",
  "explanation": "Non Disclosure Agreement"
 },
 {
  "type": "QUESTION",
  "id": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c",
  "question": "Whats the SLE of an asset where the EF is 25% and the AV is 2 million dollars?",
  "answer": "B",
  "explanation": "ALE = EF * AV"
 },
 {
  "type": "QUESTION",
  "id": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe",
  "question": "Which of the following is NOT a risk management strategy",
  "answer": "F",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d",
  "question": "Which of the following are operational controls",
  "answer": "B,C,E",
  "explanation": "Operational Controls are implemented by people\nTechnical Controls are implemented by technology"
 },
 {
  "type": "QUESTION",
  "id": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49",
  "question": "What two factors are used to evaluate a risk?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023",
  "question": "What is the correct formula for computing the annualized loss expectancy?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.5efea285-a901-41a5-b759-d42c48d712f2",
  "question": "What is the first step in the NIST risk management framework?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0",
  "question": "What letter in STRIDE refers to kind of attacks attempting to deny responsibility for an action?",
  "answer": "C",
  "explanation": "S-spoofing\nT-tampering\nR-repudiation\nI-information Disclosure\nD-denial of Service\nE-elevation of privilege"
 },
 {
  "type": "QUESTION",
  "id": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856",
  "question": "Which of the following RAID solutions provides the SMALLEST net usable disk space?",
  "answer": "B",
  "explanation": "With mirroing there is loss of 50% of disk capacity with 2 disks and 66% loss if 3 disks\nWith striping and parity there is 1 of X disks lost so RAID 5 is more efficient in terms of disks space"
 },
 {
  "type": "QUESTION",
  "id": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7",
  "question": "What is the purpose of the hamming code?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410",
  "question": "What would 'system response time', 'service availablity' and 'data preservation' all be examples of?",
  "answer": "C",
  "explanation": "SLR - Service Level Requirements\nSLA - Service Level Agreement\nMOU - Letter to document aspects of a relationship...document for future understanding\nBPA - Business Partnership Agreement - separate responsibilities and renumernation for a joint development\nISA - Agreement on how organizations will interconnect"
 },
 {
  "type": "QUESTION",
  "id": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930",
  "question": "Which of the following are not likely to be data ownership provisions in a contract",
  "answer": "C",
  "explanation": "The customer should know how a vendor uses the customers information."
 },
 {
  "type": "QUESTION",
  "id": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2",
  "question": "What activity would an MSSP not normally perform for an organization",
  "answer": "D",
  "explanation": "If a company uses an MSSP (Managed Security Service Provider) it will need to independently monitor performance of the MSSP."
 },
 {
  "type": "QUESTION",
  "id": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393",
  "question": "What type of agreement is used to define availability requirements for an IT service that an organization is purchasing from a vendor?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297",
  "question": "Which of the following is not a required component in support of accountability?",
  "answer": "B",
  "explanation": "Accountability does not require privacy but rather non-repudiation"
 },
 {
  "type": "QUESTION",
  "id": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a",
  "question": "Which of the following is not a defense against collusion",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518",
  "question": "In what phase of the Capability Maturity Model for Software (SW-CMM) are quantitative measures utilized to gain a detailed understanding of the software development process.",
  "answer": "C",
  "explanation": "The Managed phase of the SW-CMM involves the use of quantitative development metrics. The Software Engineering Institute (SEI) defines the key process areas for this level"
 },
 {
  "type": "QUESTION",
  "id": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66",
  "question": "Which one of the following is a layer in the ring protection system that is not normally implemented in practice?",
  "answer": "B",
  "explanation": "Ring Protection System is the defense in depth model\nLayer 0 is the security kernel.\nLayer 1 and 2 are device drivers.\nLayer 3 is the application\nLayer 4 doesnt exist"
 },
 {
  "type": "QUESTION",
  "id": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea",
  "question": "Which of the following is best countered by adequate parameter checking?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5",
  "question": "What is the logical operation shown here?    X: 0 1 1, Y: 0 1 0?   X v Y",
  "answer": "A",
  "explanation": "V or ~ is the logical OR operator"
 },
 {
  "type": "QUESTION",
  "id": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a",
  "question": "The collection of components in the TCB that work together to implement reference monitor function is the ",
  "answer": "B",
  "explanation": "Reference monitor is the aspect of enforcing access controls"
 },
 {
  "type": "QUESTION",
  "id": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2",
  "question": "Which of the following is true?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5",
  "question": "System architecture, system integrity, convert channel analysis, trusted facility management, and trusted recovery are elements of what security criteria?",
  "answer": "B",
  "explanation": "Assurance is the degree of confidence you can place in the satification of security needs. Operational assurance focuses on the basic features and architecture that lend themselves to support security"
 },
 {
  "type": "QUESTION",
  "id": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3",
  "question": "Ring 0, from the design architecture security mechanism known as protection rings, can also be referred to as all but which of the following?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.0b1fdb59-e955-4805-aee6-475689470516",
  "question": "Audit trails, logs, CCTV, intrusion detection systems, antivirus software, penetration testing, password crackers, performance monitoring, and cyclic redundancy checks (CRCs) are examples of what?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662",
  "question": "Which type of intrusion detection system (IDS) can be considered an expert system?",
  "answer": "D",
  "explanation": "A behavior-based IDS can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events. A knowledge-based IDS uses a database of known attack methods to detect attacks. Both host-based and network-based systems can be either knowledge-based, behavior-based, or a combination of both."
 },
 {
  "type": "QUESTION",
  "id": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59",
  "question": "Which of the following are not included in BIA recovery timeframe assessments? (choose 2)",
  "answer": "B,E",
  "explanation": "Recovery doesnt worry about how frequently they occur.\nMTBF - Mean Time Betwee Failures\nMTBF - Mean Time Between System Incidents\n MTD - Maximum Tolerable Downtime\nTTR - Time to recover"
 },
 {
  "type": "QUESTION",
  "id": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d",
  "question": "Why would you implement a logon banner (choose 3)?",
  "answer": "B,E,F",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2",
  "question": "WHich of the following mechahisms provides the greatest capacity for individual accountability?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1",
  "question": "What are the opposites of Confidentiality, Integrity and Availability",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd",
  "question": "Vulnerabilities and risks are evaluated based on their threats against which of the following?",
  "answer": "A",
  "explanation": "Vulnerabilities and risks are evaluated based on their threats against one or more of the CIA Triad principles."
 },
 {
  "type": "QUESTION",
  "id": "SRM.1dcd2588-2900-4d84-a62c-909712911bca",
  "question": "Which of the following is not considered a violation of confidentiality?",
  "answer": "C",
  "explanation": "Hardware destruction is a violation of availability and possibly integrity. Violations of confidentiality include capturing network traffic, stealing password files, social engineering, port scanning, shoulder surfing, eavesdropping, and sniffing."
 },
 {
  "type": "QUESTION",
  "id": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88",
  "question": "Which of the following is not true?",
  "answer": "C",
  "explanation": "Violations of confidentiality are not limited to direct intentional attacks. Many instances of unauthorized disclosure of sensitive or confidential information are due to human error, oversight, or ineptitude."
 },
 {
  "type": "QUESTION",
  "id": "SRM.a43efd88-6245-4694-b506-25665c24d9ef",
  "question": "STRIDE is often used in relation to assessing threats against applications or operating systems. Which of the following is not an element of STRIDE?",
  "answer": "D",
  "explanation": "Disclosure is not an element of STRIDE. The elements of STRIDE are spoofing, tampering, repudiation, information disclosure, denial of service, and elevation of privilege."
 },
 {
  "type": "QUESTION",
  "id": "SRM.b06e843f-1eab-4356-b09a-21b913b15987",
  "question": "All but which of the following items requires awareness for all individuals affected?",
  "answer": "D",
  "explanation": "Users should be aware that email messages are retained, but the backup mechanism used to perform this operation does not need to be disclosed to them."
 },
 {
  "type": "QUESTION",
  "id": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079",
  "question": "What element of data categorization management can override all other forms of access control?",
  "answer": "D",
  "explanation": "Ownership grants an entity full capabilities and privileges over the object they own. The ability to take ownership is often granted to the most powerful accounts in an operating system because it can be used to overstep any access control limitations otherwise implemented."
 },
 {
  "type": "QUESTION",
  "id": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284",
  "question": "Which of the following is the most important and distinctive concept in relation to layered security?",
  "answer": "A",
  "explanation": "Layering is the deployment of multiple security mechanisms in a series. When security restrictions are performed in a series, they are performed one after the other in a linear fashion. Therefore, a single failure of a security control does not render the entire solution ineffective."
 },
 {
  "type": "QUESTION",
  "id": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed",
  "question": "Which of the following is not considered an example of data hiding?",
  "answer": "A",
  "explanation": "A. Preventing an authorized reader of an object from deleting that object is just an example of access control, not data hiding. If you can read an object, it is not hidden from you."
 },
 {
  "type": "QUESTION",
  "id": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27",
  "question": "What is the primary goal of change management?",
  "answer": "D",
  "explanation": "The prevention of security compromises is the primary goal of change management."
 },
 {
  "type": "QUESTION",
  "id": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276",
  "question": "What is the primary objective of data classification schemes?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8",
  "question": "Which of the following is typically not a characteristic considered when classifying data?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f",
  "question": "What are the two common data classification schemes?",
  "answer": "A",
  "explanation": "Military (or government) and private sector (or commercial business) are the two common data classification schemes."
 },
 {
  "type": "QUESTION",
  "id": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1",
  "question": "Which of the following is the lowest military data classification for classified data?",
  "answer": "A",
  "explanation": "Of the options listed, secret is the lowest classified military data classification. Keep in mind that items labeled as confidential, secret, and top secret are collectively known as classified, and confidential is below secret in the list."
 },
 {
  "type": "QUESTION",
  "id": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e",
  "question": "Which commercial business/private sector data classification is used to control information about individuals within an organization?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3",
  "question": "Data classifications are used to focus security controls over all but which of the following?",
  "answer": "C",
  "explanation": "Layering is a core aspect of security mechanisms, but it is not a focus of data classifications."
 },
 {
  "type": "QUESTION",
  "id": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335",
  "question": "Which of the following is the weakest element in any security solution?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b",
  "question": "When seeking to hire new employees, what is the first step?",
  "answer": "A",
  "explanation": "The first step in hiring new employees is to create a job description. Without a job description, there is no consensus on what type of individual needs to be found and hired."
 },
 {
  "type": "QUESTION",
  "id": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4",
  "question": "When an employee is to be terminated, which of the following should be done?",
  "answer": "B",
  "explanation": "You should remove or disable the employee’s network user account immediately before or at the same time they are informed of their termination."
 },
 {
  "type": "QUESTION",
  "id": "SRM.f429f56a-f210-4635-84ef-89dd28304c88",
  "question": "QUESTION",
  "answer": "",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8",
  "question": "If an organization contracts with outside entities to provide key business functions or services, such as account or technical support, what is the process called that is used to ensure that these entities support sufficient security?",
  "answer": "B",
  "explanation": "Third-party governance is the application of security oversight on third parties that your organization relies on."
 },
 {
  "type": "QUESTION",
  "id": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a",
  "question": "A portion of the ???? is the logical and practical investigation of business processes and organizational policies. This process/policy review ensures that the stated and implemented business tasks, systems, and methodologies are practical, efficient, and cost-effective, but most of all (at least in relation to security governance) that they support security through the reduction of vulnerabilities and the avoidance, reduction, or mitigation of risk.",
  "answer": "D",
  "explanation": "A portion of the documentation review is the logical and practical investigation of business processes and organizational policies."
 },
 {
  "type": "QUESTION",
  "id": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1",
  "question": "Which of the following statements is not true?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9",
  "question": "Which of the following is not an element of the risk analysis process?",
  "answer": "C",
  "explanation": "Risk analysis includes analyzing an environment for risks, evaluating each threat event as to its likelihood of occurring and the cost of the damage it would cause, assessing the cost of various countermeasures for each risk, and creating a cost/benefit report for safeguards to present to upper management. Selecting safeguards is a task of upper management based on the results of risk analysis. It is a task that falls under risk management, but it is not part of the risk analysis process."
 },
 {
  "type": "QUESTION",
  "id": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827",
  "question": "Which of the following would generally not be considered an asset in a risk analysis?",
  "answer": "D",
  "explanation": "The personal files of users are not usually considered assets of the organization and thus are not considered in a risk analysis."
 },
 {
  "type": "QUESTION",
  "id": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2",
  "question": "Which of the following represents accidental or intentional exploitations of vulnerabilities?",
  "answer": "A",
  "explanation": "Threat events are accidental or intentional exploitations of vulnerabilities."
 },
 {
  "type": "QUESTION",
  "id": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0",
  "question": "When a safeguard or a countermeasure is not present or is not sufficient, what remains?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SRM.21ee9e81-194e-432c-9391-83d95e158e32",
  "question": "Which of the following is not a valid definition for risk?",
  "answer": "B",
  "explanation": "Anything that removes a vulnerability or protects against one or more specific threats is considered a safeguard or a countermeasure, not a risk."
 },
 {
  "type": "QUESTION",
  "id": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3",
  "question": "When evaluating safeguards, what is the rule that should be followed in most cases?",
  "answer": "C",
  "explanation": "The annual costs of safeguards should not exceed the expected annual cost of asset loss."
 },
 {
  "type": "QUESTION",
  "id": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f",
  "question": "How is single loss expectancy (SLE) calculated?",
  "answer": "B",
  "explanation": "SLE is calculated using the formula SLE = asset value ($) exposure factor (SLE = AV EF)."
 },
 {
  "type": "QUESTION",
  "id": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6",
  "question": "How is the value of a safeguard to a company calculated?",
  "answer": "A",
  "explanation": "The value of a safeguard to an organization is calculated by ALE before safeguard – ALE after implementing the safeguard – annual cost of safeguard (ALE1 – ALE2) – ACS ."
 },
 {
  "type": "QUESTION",
  "id": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8",
  "question": "What security control is directly focused on preventing collusion?",
  "answer": "C",
  "explanation": "The likelihood that a co-worker will be willing to collaborate on an illegal or abusive scheme is reduced because of the higher risk of detection created by the combination of separation of duties, restricted job responsibilities, and job rotation."
 },
 {
  "type": "QUESTION",
  "id": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02",
  "question": "What process or event is typically hosted by an organization and is targeted to groups of employees with similar job functions?",
  "answer": "C",
  "explanation": "Training is teaching employees to perform their work tasks and to comply with the security policy. Training is typically hosted by an organization and is targeted to groups of employees with similar job functions."
 },
 {
  "type": "QUESTION",
  "id": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0",
  "question": "Which of the following is not specifically or directly related to managing the security function of an organization?",
  "answer": "A",
  "explanation": "Managing the security function often includes assessment of budget, metrics, resources, and information security strategies,"
 },
 {
  "type": "QUESTION",
  "id": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656",
  "question": "You’ve performed a basic quantitative risk analysis on a specific threat/vulnerability/risk relation. You select a possible countermeasure. When performing the calculations again, which of the following factors will change?",
  "answer": "D",
  "explanation": "A countermeasure directly affects the annualized rate of occurrence, primarily because the countermeasure is designed to prevent the occurrence of the risk, thus reducing its frequency per year."
 },
 {
  "type": "QUESTION",
  "id": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240",
  "question": "What is the first step that individusla responsible for the development of a business continuity plan should perform?",
  "answer": "B",
  "explanation": "The business organization analysis helps the initial planners select appropriate BCP team members and then guides the overall BCP process."
 },
 {
  "type": "QUESTION",
  "id": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136",
  "question": "Once the BCP team is selected, what should be the first item placed on the team’s agenda?",
  "answer": "B",
  "explanation": "The first task of the BCP team should be the review and validation of the business organization analysis initially performed by those individuals responsible for spearheading the BCP effort. This ensures that the initial effort, undertaken by a small group of individuals, reflects the beliefs of the entire BCP team."
 },
 {
  "type": "QUESTION",
  "id": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b",
  "question": "What is the term used to describe the responsibility of a firm’s officers and directors to ensure that adequate measures are in place to minimize the effect of a disaster on the organization’s continued viability?",
  "answer": "C",
  "explanation": "A firm’s officers and directors are legally bound to exercise due diligence in conducting their activities. This concept creates a fiduciary responsibility on their part to ensure that adequate business continuity plans are in place."
 },
 {
  "type": "QUESTION",
  "id": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8",
  "question": "Which one of the following BIA terms identifies the amount of money a business expects to lose to a given risk each year?",
  "answer": "C",
  "explanation": "The annualized loss expectancy (ALE) represents the amount of money a business expects to lose to a given risk each year. This figure is quite useful when performing a quantitative prioritization of business continuity resource allocation."
 },
 {
  "type": "ANSWER",
  "id": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c-A",
  "QuestionId": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c",
  "choice": "A",
  "answer": "Given an auditor read and write permissions for system log files?"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c-B",
  "QuestionId": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c",
  "choice": "B",
  "answer": "Installing access control units on elevators limting staff to job-related floors"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c-C",
  "QuestionId": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c",
  "choice": "C",
  "answer": "Requiring users to enter only a username and password to log into a system"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c-D",
  "QuestionId": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c",
  "choice": "D",
  "answer": "Placing a linux subsystem in the Domain Admins group in Active Directory"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c-E",
  "QuestionId": "SRM.d7a4a7cc-7ada-4092-9f8e-6fb76b0e8e1c",
  "choice": "E",
  "answer": "Granting software developers access to production systems"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0-A",
  "QuestionId": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0",
  "choice": "A",
  "answer": "Increase the size of the team"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0-B",
  "QuestionId": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0",
  "choice": "B",
  "answer": "Switch to a fully qualitative approach"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0-C",
  "QuestionId": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0",
  "choice": "C",
  "answer": "Implement automated risk analysis tool"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0-D",
  "QuestionId": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0",
  "choice": "D",
  "answer": "Re-evalute the scope of the RA teams work"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0-E",
  "QuestionId": "SRM.b1363405-31a5-4d46-a602-54e02c6603a0",
  "choice": "E",
  "answer": "Limit the number of risks considered."
 },
 {
  "type": "ANSWER",
  "id": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb-A",
  "QuestionId": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb",
  "choice": "A",
  "answer": "Detection of an ongoing spear phishing campaign against employees"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb-B",
  "QuestionId": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb",
  "choice": "B",
  "answer": "A NIDS detects a buffer overflow exploit in an inbound packet"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb-C",
  "QuestionId": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb",
  "choice": "C",
  "answer": "Unusual amounts of ssh traffic leaving a network"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb-D",
  "QuestionId": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb",
  "choice": "D",
  "answer": "Log files show the same username/password trying to log in to 20 different servers in 20 seconds"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb-E",
  "QuestionId": "SRM.a05df008-d665-4558-b048-af9ac10d8ddb",
  "choice": "E",
  "answer": "A zero day exploit has been identified for software widely used in your organization"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328-A",
  "QuestionId": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328",
  "choice": "A",
  "answer": "Objectives Statement"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328-B",
  "QuestionId": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328",
  "choice": "B",
  "answer": "Background Statement"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328-C",
  "QuestionId": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328",
  "choice": "C",
  "answer": "Scope Statement"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328-D",
  "QuestionId": "SRM.5a9c811e-9cb2-448f-9542-15de619ab328",
  "choice": "D",
  "answer": "Confidentiality Statement"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196-A",
  "QuestionId": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196",
  "choice": "A",
  "answer": "Profit, loss, ROI"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196-B",
  "QuestionId": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196",
  "choice": "B",
  "answer": "Intentional, accidental, natural disaster"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196-C",
  "QuestionId": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196",
  "choice": "C",
  "answer": "Assets, personnel, services provided"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196-D",
  "QuestionId": "SRM.ed16244f-a2e6-45e3-acef-57434d43e196",
  "choice": "D",
  "answer": "Availability, integrity, confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb-A",
  "QuestionId": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb",
  "choice": "A",
  "answer": "Threat and vulnerability analysis."
 },
 {
  "type": "ANSWER",
  "id": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb-B",
  "QuestionId": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb",
  "choice": "B",
  "answer": "Risk evaluation."
 },
 {
  "type": "ANSWER",
  "id": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb-C",
  "QuestionId": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb",
  "choice": "C",
  "answer": "ALE calculation."
 },
 {
  "type": "ANSWER",
  "id": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb-D",
  "QuestionId": "SRM.3316234c-e3d1-4cf0-b51f-9a5d692bcfdb",
  "choice": "D",
  "answer": "Countermeasure cost/benefit analysis."
 },
 {
  "type": "ANSWER",
  "id": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f-A",
  "QuestionId": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f",
  "choice": "A",
  "answer": "Scope and plan initiation, plan approval and implementation, business impact assessment, business continuity plan development"
 },
 {
  "type": "ANSWER",
  "id": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f-B",
  "QuestionId": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f",
  "choice": "B",
  "answer": "Scope and plan initiation, business impact assessment, business continuity plan development, plan approval and implementation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f-C",
  "QuestionId": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f",
  "choice": "C",
  "answer": "Business impact assessment, scope and plan initiation, business continuity plan development, plan approval and implementation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f-D",
  "QuestionId": "SRM.66aab13a-2ade-4d82-8ee1-161156a0d90f",
  "choice": "D",
  "answer": "Plan approval and implementation, business impact assessment, scope and plan initiation, business continuity plan development"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886-A",
  "QuestionId": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886",
  "choice": "A",
  "answer": "$1 million"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886-B",
  "QuestionId": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886",
  "choice": "B",
  "answer": "$10 million"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886-C",
  "QuestionId": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886",
  "choice": "C",
  "answer": "$100,000"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886-D",
  "QuestionId": "SRM.b2a2b036-9fc3-4784-bf2c-686b82d08886",
  "choice": "D",
  "answer": "$10,000"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d96aeb3-e95a-4366-9320-892c42431698-A",
  "QuestionId": "SRM.1d96aeb3-e95a-4366-9320-892c42431698",
  "choice": "A",
  "answer": "A platform for disclosing exposure and risk analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d96aeb3-e95a-4366-9320-892c42431698-B",
  "QuestionId": "SRM.1d96aeb3-e95a-4366-9320-892c42431698",
  "choice": "B",
  "answer": "To make everyone aware of potential risk and exposure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d96aeb3-e95a-4366-9320-892c42431698-C",
  "QuestionId": "SRM.1d96aeb3-e95a-4366-9320-892c42431698",
  "choice": "C",
  "answer": "Platform for disclosing exposure and risk analysis."
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d96aeb3-e95a-4366-9320-892c42431698-D",
  "QuestionId": "SRM.1d96aeb3-e95a-4366-9320-892c42431698",
  "choice": "D",
  "answer": "A way for communicating security procedures"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9-A",
  "QuestionId": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9",
  "choice": "A",
  "answer": "Detective"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9-B",
  "QuestionId": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9",
  "choice": "B",
  "answer": "Corrective"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9-C",
  "QuestionId": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9",
  "choice": "C",
  "answer": "Preventative"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9-D",
  "QuestionId": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9",
  "choice": "D",
  "answer": "Administrative"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9-E",
  "QuestionId": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9",
  "choice": "E",
  "answer": "Technical"
 },
 {
  "type": "ANSWER",
  "id": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9-F",
  "QuestionId": "IAM.6e3cf218-a0aa-49da-a5b9-2f9047225ce9",
  "choice": "F",
  "answer": "Physical"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734-A",
  "QuestionId": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "choice": "A",
  "answer": "Natural Access Control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734-B",
  "QuestionId": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "choice": "B",
  "answer": "End User Security Awareness Training"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734-C",
  "QuestionId": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "choice": "C",
  "answer": "Building Code Security Reviews"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734-D",
  "QuestionId": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "choice": "D",
  "answer": "Community Activism"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734-E",
  "QuestionId": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "choice": "E",
  "answer": "Natural Territorial Enforcement"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734-F",
  "QuestionId": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "choice": "F",
  "answer": "Environmental Inconveniences"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734-G",
  "QuestionId": "SRM.3e0a4ecd-9f3a-4ca7-a695-bc28c70f4734",
  "choice": "G",
  "answer": "Natural Survelliance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6-A",
  "QuestionId": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6",
  "choice": "A",
  "answer": "CCTV Cameras"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6-B",
  "QuestionId": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6",
  "choice": "B",
  "answer": "NTFS File Permissions"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6-C",
  "QuestionId": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6",
  "choice": "C",
  "answer": "Data backups"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6-D",
  "QuestionId": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6",
  "choice": "D",
  "answer": "No Trepassing Signs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6-E",
  "QuestionId": "SRM.8be5ea80-975d-4df5-82d7-ef07a5f1f5a6",
  "choice": "E",
  "answer": "Network Intrusion Detection"
 },
 {
  "type": "ANSWER",
  "id": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.-A",
  "QuestionId": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.",
  "choice": "A",
  "answer": "Evacuation / Shelter in Place for individuals"
 },
 {
  "type": "ANSWER",
  "id": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.-B",
  "QuestionId": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.",
  "choice": "B",
  "answer": "Defining locations for command center"
 },
 {
  "type": "ANSWER",
  "id": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.-C",
  "QuestionId": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.",
  "choice": "C",
  "answer": "Contingency plans for alternate modes of communication"
 },
 {
  "type": "ANSWER",
  "id": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.-D",
  "QuestionId": "SRM3996910f-775a-49f6-8b89-8839f2f9b956.",
  "choice": "D",
  "answer": "Steps to ensure the primary operations of the facility continue to function"
 },
 {
  "type": "ANSWER",
  "id": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25-A",
  "QuestionId": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25",
  "choice": "A",
  "answer": "Time of CHeck/Time of Use"
 },
 {
  "type": "ANSWER",
  "id": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25-B",
  "QuestionId": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25",
  "choice": "B",
  "answer": "Buffer Overflow"
 },
 {
  "type": "ANSWER",
  "id": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25-C",
  "QuestionId": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25",
  "choice": "C",
  "answer": "XSS"
 },
 {
  "type": "ANSWER",
  "id": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25-D",
  "QuestionId": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25",
  "choice": "D",
  "answer": "Salami Attach"
 },
 {
  "type": "ANSWER",
  "id": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25-E",
  "QuestionId": "SRM.be642a5c-a7be-41ce-bd4c-ff7e11590a25",
  "choice": "E",
  "answer": "Data Diddlin"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a-A",
  "QuestionId": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a",
  "choice": "A",
  "answer": "Trade secrets are not afforded protection once published to the world"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a-B",
  "QuestionId": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a",
  "choice": "B",
  "answer": "Trade secrets are exempt from non-disclosure agreements"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a-C",
  "QuestionId": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a",
  "choice": "C",
  "answer": "Trade secrets are generally unknown to the world"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a-D",
  "QuestionId": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a",
  "choice": "D",
  "answer": "Trade secrets are protected for 20 years"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a-E",
  "QuestionId": "SRM.4645437e-fb7b-44a8-8b06-52ea73e4385a",
  "choice": "E",
  "answer": "Trade secrets protect the original expression of the idea that than the idea itself"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec-A",
  "QuestionId": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec",
  "choice": "A",
  "answer": "System Owner"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec-B",
  "QuestionId": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec",
  "choice": "B",
  "answer": "ISSO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec-C",
  "QuestionId": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec",
  "choice": "C",
  "answer": "Software Developer"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec-D",
  "QuestionId": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec",
  "choice": "D",
  "answer": "Authorizing Official"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec-E",
  "QuestionId": "SRM.89f6cd7f-dc59-4845-872e-7526a41e31ec",
  "choice": "E",
  "answer": "CIO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.-A",
  "QuestionId": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.",
  "choice": "A",
  "answer": "Return on Investment (ROI)"
 },
 {
  "type": "ANSWER",
  "id": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.-B",
  "QuestionId": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.",
  "choice": "B",
  "answer": "Exposure Factor (EF)"
 },
 {
  "type": "ANSWER",
  "id": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.-C",
  "QuestionId": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.",
  "choice": "C",
  "answer": "Risk"
 },
 {
  "type": "ANSWER",
  "id": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.-D",
  "QuestionId": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.",
  "choice": "D",
  "answer": "Total Cost of Ownership"
 },
 {
  "type": "ANSWER",
  "id": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.-E",
  "QuestionId": "SRM.297af2a5-b7ed-44a8-aca5-164cb1ceb963.",
  "choice": "E",
  "answer": "Annualized Loss Expectancy (ALE)"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b-A",
  "QuestionId": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b",
  "choice": "A",
  "answer": "Preventative"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b-B",
  "QuestionId": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b",
  "choice": "B",
  "answer": "Detective"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b-C",
  "QuestionId": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b",
  "choice": "C",
  "answer": "Corrective"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b-D",
  "QuestionId": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b",
  "choice": "D",
  "answer": "Technical"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b-E",
  "QuestionId": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b",
  "choice": "E",
  "answer": "Compensating"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b-F",
  "QuestionId": "SRM.c9a90254-c773-4c8e-bc6c-b3dd670ebb6b",
  "choice": "F",
  "answer": "Recovery"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44-A",
  "QuestionId": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44",
  "choice": "A",
  "answer": "Minimize costs associated with changes"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44-B",
  "QuestionId": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44",
  "choice": "B",
  "answer": "Support rapid change while minimizing service disruption"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44-C",
  "QuestionId": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44",
  "choice": "C",
  "answer": "Create best practice to ensure that data confidentiality is preserved"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44-D",
  "QuestionId": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44",
  "choice": "D",
  "answer": "Provides guidance when emergency changes must be made"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44-E",
  "QuestionId": "SRM.7e205a4e-6937-4ec0-bb26-21409a4c9d44",
  "choice": "E",
  "answer": "Improved change documentation and post change reporting"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31-A",
  "QuestionId": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31",
  "choice": "A",
  "answer": "The backup strategies required for data recovery"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31-B",
  "QuestionId": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31",
  "choice": "B",
  "answer": "Which business processes a system supports"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31-C",
  "QuestionId": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31",
  "choice": "C",
  "answer": "The financial impact to the business if a system becomes unavailable"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31-D",
  "QuestionId": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31",
  "choice": "D",
  "answer": "The resources required to restore the system to operation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31-E",
  "QuestionId": "SRM.3a9fa221-919f-409c-8b8a-6b03aaca2b31",
  "choice": "E",
  "answer": "The maximum amount of time the system can be unavailable."
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-A",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "A",
  "answer": "NIST"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-B",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "B",
  "answer": "RMF"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-C",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "C",
  "answer": "TCSEC"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-D",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "D",
  "answer": "ITSEC"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-E",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "E",
  "answer": "Common Criteria"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-F",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "F",
  "answer": "Certification and Accreditation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-G",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "G",
  "answer": "PCI"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b-H",
  "QuestionId": "SRM.2284c248-cf87-4499-ba5f-a7c90144369b",
  "choice": "H",
  "answer": "COBIT"
 },
 {
  "type": "ANSWER",
  "id": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9-A",
  "QuestionId": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9",
  "choice": "A",
  "answer": "Minimize Downtime"
 },
 {
  "type": "ANSWER",
  "id": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9-B",
  "QuestionId": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9",
  "choice": "B",
  "answer": "Reduce Liability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9-C",
  "QuestionId": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9",
  "choice": "C",
  "answer": "Manage Costs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9-D",
  "QuestionId": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9",
  "choice": "D",
  "answer": "Decrease Administrative Overhead"
 },
 {
  "type": "ANSWER",
  "id": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9-E",
  "QuestionId": "SRM.9c275835-cd54-46fa-be04-d5319fefb7d9",
  "choice": "E",
  "answer": "Eliminate Risk"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-A",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "A",
  "answer": "DCL"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-B",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "B",
  "answer": "Schema"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-C",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "C",
  "answer": "Normalization"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-D",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "D",
  "answer": "Polyinstantiation"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-E",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "E",
  "answer": "Cardinality"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-F",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "F",
  "answer": "Relational"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-G",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "G",
  "answer": "SQL"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa-H",
  "QuestionId": "SDL.fe1aaff9-7538-4d04-845a-6d921166a7aa",
  "choice": "H",
  "answer": "DML"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658-A",
  "QuestionId": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "choice": "A",
  "answer": "Background Checks"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658-B",
  "QuestionId": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "choice": "B",
  "answer": "Network Firewall"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658-C",
  "QuestionId": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "choice": "C",
  "answer": "Audible Alarms"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658-D",
  "QuestionId": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "choice": "D",
  "answer": "Security Awareness Training"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658-E",
  "QuestionId": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "choice": "E",
  "answer": "Security Guards"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658-F",
  "QuestionId": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "choice": "F",
  "answer": "Risk Management"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658-G",
  "QuestionId": "SRM.4b3b623d-646f-4ddc-8225-b837adfe2658",
  "choice": "G",
  "answer": "Encryption of Personnel Records"
 },
 {
  "type": "ANSWER",
  "id": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be-A",
  "QuestionId": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be",
  "choice": "A",
  "answer": "Use guard dogs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be-B",
  "QuestionId": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be",
  "choice": "B",
  "answer": "CCTV Cameras that record when movement is detected"
 },
 {
  "type": "ANSWER",
  "id": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be-C",
  "QuestionId": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be",
  "choice": "C",
  "answer": "Install razer wire on top of fences"
 },
 {
  "type": "ANSWER",
  "id": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be-D",
  "QuestionId": "SRM.52de8b1d-f4d9-4086-bc3e-894111d6b3be",
  "choice": "D",
  "answer": "Hire a security guard"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5615f348-7a38-44cc-88a7-a10382912b02-A",
  "QuestionId": "SRM.5615f348-7a38-44cc-88a7-a10382912b02",
  "choice": "A",
  "answer": "Hash the PDF and compare the result to the hash recovered from the signature"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5615f348-7a38-44cc-88a7-a10382912b02-B",
  "QuestionId": "SRM.5615f348-7a38-44cc-88a7-a10382912b02",
  "choice": "B",
  "answer": "Add the user certificates to the list of trusted certificaes on your system"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5615f348-7a38-44cc-88a7-a10382912b02-C",
  "QuestionId": "SRM.5615f348-7a38-44cc-88a7-a10382912b02",
  "choice": "C",
  "answer": "Manually verify the certificate using and out of band method"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5615f348-7a38-44cc-88a7-a10382912b02-D",
  "QuestionId": "SRM.5615f348-7a38-44cc-88a7-a10382912b02",
  "choice": "D",
  "answer": "Use the private key to validat the file signature before opening the file"
 },
 {
  "type": "ANSWER",
  "id": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876-A",
  "QuestionId": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876",
  "choice": "A",
  "answer": "WHat in the file has been changed"
 },
 {
  "type": "ANSWER",
  "id": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876-B",
  "QuestionId": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876",
  "choice": "B",
  "answer": "The content has been changed"
 },
 {
  "type": "ANSWER",
  "id": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876-C",
  "QuestionId": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876",
  "choice": "C",
  "answer": "How much of the file has been changed"
 },
 {
  "type": "ANSWER",
  "id": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876-D",
  "QuestionId": "SOP.346ecd5b-dfbb-468f-8ca2-8a211876b876",
  "choice": "D",
  "answer": "The file has been viewed"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf-A",
  "QuestionId": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf",
  "choice": "A",
  "answer": "Hashing"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf-B",
  "QuestionId": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf",
  "choice": "B",
  "answer": "Digital Signature"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf-C",
  "QuestionId": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf",
  "choice": "C",
  "answer": "Access controls"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf-D",
  "QuestionId": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf",
  "choice": "D",
  "answer": "Digital Certificates"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf-E",
  "QuestionId": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf",
  "choice": "E",
  "answer": "Redundant components"
 },
 {
  "type": "ANSWER",
  "id": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf-F",
  "QuestionId": "SOP.a9319e54-0a77-4f2b-9c33-caccad910fbf",
  "choice": "F",
  "answer": "OS Patching"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3-A",
  "QuestionId": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3",
  "choice": "A",
  "answer": "Trusted Path"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3-B",
  "QuestionId": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3",
  "choice": "B",
  "answer": "API"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3-C",
  "QuestionId": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3",
  "choice": "C",
  "answer": "Covert Channel"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3-D",
  "QuestionId": "SRM.bdc84081-2b30-4323-9e9c-6c36b14657d3",
  "choice": "D",
  "answer": "Protection Domain"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34-A",
  "QuestionId": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34",
  "choice": "A",
  "answer": "Define Metrics to be gathered"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34-B",
  "QuestionId": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34",
  "choice": "B",
  "answer": "Develop Recovery Strategies"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34-C",
  "QuestionId": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34",
  "choice": "C",
  "answer": "Respond to management with migitation steps"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34-D",
  "QuestionId": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34",
  "choice": "D",
  "answer": "Perform functionality and security testing"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34-E",
  "QuestionId": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34",
  "choice": "E",
  "answer": "Identifying Preventative Controls"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34-F",
  "QuestionId": "SRM.f81231cb-37ae-442f-9541-656d2b0c6a34",
  "choice": "F",
  "answer": "Obtain formal authorization to operate (ATO)"
 },
 {
  "type": "ANSWER",
  "id": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d-A",
  "QuestionId": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d",
  "choice": "A",
  "answer": "Security Policy"
 },
 {
  "type": "ANSWER",
  "id": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d-B",
  "QuestionId": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d",
  "choice": "B",
  "answer": "Information Security Program"
 },
 {
  "type": "ANSWER",
  "id": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d-C",
  "QuestionId": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d",
  "choice": "C",
  "answer": "Security Control Framework"
 },
 {
  "type": "ANSWER",
  "id": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d-D",
  "QuestionId": "SRM.437bf04e-df99-4e88-868c-fc09dcbc385d",
  "choice": "D",
  "answer": "Risk Management Framework"
 },
 {
  "type": "ANSWER",
  "id": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9-A",
  "QuestionId": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9",
  "choice": "A",
  "answer": "Meeting stakholder needs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9-B",
  "QuestionId": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9",
  "choice": "B",
  "answer": "applying a single integrated framework"
 },
 {
  "type": "ANSWER",
  "id": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9-C",
  "QuestionId": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9",
  "choice": "C",
  "answer": "maximizing profitability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9-D",
  "QuestionId": "SRM.15bf55f1-8c15-4e1d-b2c5-913686b4fca9",
  "choice": "D",
  "answer": "separately governance from management"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-A",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "A",
  "answer": "Physical Characeristics"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-B",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "B",
  "answer": "Genertics"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-C",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "C",
  "answer": "Identification Number"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-D",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "D",
  "answer": "Mental Status"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-E",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "E",
  "answer": "Location Data"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-F",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "F",
  "answer": "Economic Status"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-G",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "G",
  "answer": "Cultural or Social Identity"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f-H",
  "QuestionId": "SRM.a3809290-fd10-429a-9c87-b2ee6294693f",
  "choice": "H",
  "answer": "Online Identifiers"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8-A",
  "QuestionId": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8",
  "choice": "A",
  "answer": "Data consent cannot be disclosed without data subjects consent"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8-B",
  "QuestionId": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8",
  "choice": "B",
  "answer": "Data can only be used for the purpose stated when collected"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8-C",
  "QuestionId": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8",
  "choice": "C",
  "answer": "Subject can access their data and make corrections"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8-D",
  "QuestionId": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8",
  "choice": "D",
  "answer": "Subjects have the right to be forgotten"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8-E",
  "QuestionId": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8",
  "choice": "E",
  "answer": "Subjects must consent to data collection"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8-F",
  "QuestionId": "SRM.bce309ce-7cc5-4082-8e04-d9e0ae2a59e8",
  "choice": "F",
  "answer": "Collected data should be kept secure from potential abuse"
 },
 {
  "type": "ANSWER",
  "id": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d-A",
  "QuestionId": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d",
  "choice": "A",
  "answer": "Security should not decrease the usability of the system"
 },
 {
  "type": "ANSWER",
  "id": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d-B",
  "QuestionId": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d",
  "choice": "B",
  "answer": "Intangible risks should be mitigated first"
 },
 {
  "type": "ANSWER",
  "id": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d-C",
  "QuestionId": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d",
  "choice": "C",
  "answer": "Should cover all regularoty requirements"
 },
 {
  "type": "ANSWER",
  "id": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d-D",
  "QuestionId": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d",
  "choice": "D",
  "answer": "Cost must be manage and should make sense for the given risk"
 },
 {
  "type": "ANSWER",
  "id": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d-E",
  "QuestionId": "SRM.64448da4-a58b-4e4f-9e16-6ed6aef81d9d",
  "choice": "E",
  "answer": "All risk should be eliminated by mitigating mechanisms"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-A",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "A",
  "answer": "Place of Birth"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-B",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "B",
  "answer": "Address"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-C",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "C",
  "answer": "Social Security Information"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-D",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "D",
  "answer": "Encrypted Credit Card Information"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-E",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "E",
  "answer": "Social Media Posts"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-F",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "F",
  "answer": "Drivers License Numbers"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-G",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "G",
  "answer": "Transactions"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-H",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "H",
  "answer": "Bank Accounts"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a-I",
  "QuestionId": "SRM.e0f7970b-7c32-478e-9005-d6d5a8fc9a8a",
  "choice": "I",
  "answer": "Passwords"
 },
 {
  "type": "ANSWER",
  "id": "SRM.69ed37a2-38d2-413c-8691-1350f7793143-A",
  "QuestionId": "SRM.69ed37a2-38d2-413c-8691-1350f7793143",
  "choice": "A",
  "answer": "ITADA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.69ed37a2-38d2-413c-8691-1350f7793143-B",
  "QuestionId": "SRM.69ed37a2-38d2-413c-8691-1350f7793143",
  "choice": "B",
  "answer": "EPCA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.69ed37a2-38d2-413c-8691-1350f7793143-C",
  "QuestionId": "SRM.69ed37a2-38d2-413c-8691-1350f7793143",
  "choice": "C",
  "answer": "CFAA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.69ed37a2-38d2-413c-8691-1350f7793143-D",
  "QuestionId": "SRM.69ed37a2-38d2-413c-8691-1350f7793143",
  "choice": "D",
  "answer": "GLBA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7-A",
  "QuestionId": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7",
  "choice": "A",
  "answer": "Patent"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7-B",
  "QuestionId": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7",
  "choice": "B",
  "answer": "Copyright"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7-C",
  "QuestionId": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7",
  "choice": "C",
  "answer": "Trademark"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7-D",
  "QuestionId": "SRM.d5f0c16c-2732-46fe-b3f6-228df7b274b7",
  "choice": "D",
  "answer": "Trade Secret"
 },
 {
  "type": "ANSWER",
  "id": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35-A",
  "QuestionId": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35",
  "choice": "A",
  "answer": "ITAR"
 },
 {
  "type": "ANSWER",
  "id": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35-B",
  "QuestionId": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35",
  "choice": "B",
  "answer": "OFAC"
 },
 {
  "type": "ANSWER",
  "id": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35-C",
  "QuestionId": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35",
  "choice": "C",
  "answer": "EAR"
 },
 {
  "type": "ANSWER",
  "id": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35-D",
  "QuestionId": "SRM.076184a2-eb3c-47e4-a69c-e42c387b5a35",
  "choice": "D",
  "answer": "FCC"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289-A",
  "QuestionId": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289",
  "choice": "A",
  "answer": "Process for policy exceptions and violations"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289-B",
  "QuestionId": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289",
  "choice": "B",
  "answer": "Description of security roles"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289-C",
  "QuestionId": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289",
  "choice": "C",
  "answer": "Designation of individuals responsible for security"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289-D",
  "QuestionId": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289",
  "choice": "D",
  "answer": "Specific details of security controls"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289-E",
  "QuestionId": "SRM.0f5cc65f-5ca4-48b8-9dc3-1a7a0b964289",
  "choice": "E",
  "answer": "Authority for the creation of policies"
 },
 {
  "type": "ANSWER",
  "id": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18-A",
  "QuestionId": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18",
  "choice": "A",
  "answer": "Least Privilege"
 },
 {
  "type": "ANSWER",
  "id": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18-B",
  "QuestionId": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18",
  "choice": "B",
  "answer": "Job Rotation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18-C",
  "QuestionId": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18",
  "choice": "C",
  "answer": "Separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18-D",
  "QuestionId": "SRM.24af6a88-fd23-4da8-968c-b2926a078a18",
  "choice": "D",
  "answer": "Mandatory Vacations"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604-A",
  "QuestionId": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604",
  "choice": "A",
  "answer": "Principle of Least Privelege"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604-B",
  "QuestionId": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604",
  "choice": "B",
  "answer": "Locards Principle"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604-C",
  "QuestionId": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604",
  "choice": "C",
  "answer": "Need to know"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604-D",
  "QuestionId": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604",
  "choice": "D",
  "answer": "Separation of dutie"
 },
 {
  "type": "ANSWER",
  "id": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604-E",
  "QuestionId": "SRM.3a2ec3ba-5e0f-480a-8466-f62b962c8604",
  "choice": "E",
  "answer": "Tranquility Principle"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95-A",
  "QuestionId": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95",
  "choice": "A",
  "answer": "Only allow a use to create a vendor or pay a vendor but not both"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95-B",
  "QuestionId": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95",
  "choice": "B",
  "answer": "Classify data from a system"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95-C",
  "QuestionId": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95",
  "choice": "C",
  "answer": "Succession plan for key personnel"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95-D",
  "QuestionId": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95",
  "choice": "D",
  "answer": "Establish information security policy"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95-E",
  "QuestionId": "SRM.d9473124-717b-4543-9c27-0ce1cf55df95",
  "choice": "E",
  "answer": "Log user access to information systems"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9-A",
  "QuestionId": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9",
  "choice": "A",
  "answer": "RAID 0"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9-B",
  "QuestionId": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9",
  "choice": "B",
  "answer": "RAID 1"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9-C",
  "QuestionId": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9",
  "choice": "C",
  "answer": "RAID 2"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9-D",
  "QuestionId": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9",
  "choice": "D",
  "answer": "RAID 5"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9-E",
  "QuestionId": "SRM.f74187ef-83df-450d-91b8-898fb0c1b2d9",
  "choice": "E",
  "answer": "RAID 6"
 },
 {
  "type": "ANSWER",
  "id": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174-A",
  "QuestionId": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174",
  "choice": "A",
  "answer": "1"
 },
 {
  "type": "ANSWER",
  "id": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174-B",
  "QuestionId": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174",
  "choice": "B",
  "answer": "2"
 },
 {
  "type": "ANSWER",
  "id": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174-C",
  "QuestionId": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174",
  "choice": "C",
  "answer": "3"
 },
 {
  "type": "ANSWER",
  "id": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174-D",
  "QuestionId": "SRM.220d74fc-5e64-499c-8d1b-7bf423040174",
  "choice": "D",
  "answer": "4"
 },
 {
  "type": "ANSWER",
  "id": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2-A",
  "QuestionId": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2",
  "choice": "A",
  "answer": "Access control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2-B",
  "QuestionId": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2",
  "choice": "B",
  "answer": "Backup Control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2-C",
  "QuestionId": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2",
  "choice": "C",
  "answer": "Fault Tolerance Control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2-D",
  "QuestionId": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2",
  "choice": "D",
  "answer": "High Availability Control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2-E",
  "QuestionId": "SRM.16875f77-d734-4ba0-bbe9-be612ff9a9f2",
  "choice": "E",
  "answer": "Quality of Service Control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1-A",
  "QuestionId": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1",
  "choice": "A",
  "answer": "Component Redunancy Control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1-B",
  "QuestionId": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1",
  "choice": "B",
  "answer": "Load Balancing"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1-C",
  "QuestionId": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1",
  "choice": "C",
  "answer": "High Availability Control"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1-D",
  "QuestionId": "SRM.c21d6959-38a7-4fab-bcc4-d246e679c1c1",
  "choice": "D",
  "answer": "Clustering"
 },
 {
  "type": "ANSWER",
  "id": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac-A",
  "QuestionId": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac",
  "choice": "A",
  "answer": "1"
 },
 {
  "type": "ANSWER",
  "id": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac-B",
  "QuestionId": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac",
  "choice": "B",
  "answer": "2"
 },
 {
  "type": "ANSWER",
  "id": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac-C",
  "QuestionId": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac",
  "choice": "C",
  "answer": "3"
 },
 {
  "type": "ANSWER",
  "id": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac-D",
  "QuestionId": "SRM.499567ab-0538-44c9-983d-1a997b3dc3ac",
  "choice": "D",
  "answer": "4"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50-A",
  "QuestionId": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50",
  "choice": "A",
  "answer": "Inform the violator of the policy"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50-B",
  "QuestionId": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50",
  "choice": "B",
  "answer": "Investigate the consequences of this violation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50-C",
  "QuestionId": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50",
  "choice": "C",
  "answer": "Bring it up with your manager"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50-D",
  "QuestionId": "SRM.f4b8895b-3a34-4606-8c05-ca0c0fb63a50",
  "choice": "D",
  "answer": "Do nothing"
 },
 {
  "type": "ANSWER",
  "id": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18-A",
  "QuestionId": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18",
  "choice": "A",
  "answer": "Minimization"
 },
 {
  "type": "ANSWER",
  "id": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18-B",
  "QuestionId": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18",
  "choice": "B",
  "answer": "Limitation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18-C",
  "QuestionId": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18",
  "choice": "C",
  "answer": "Truncation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18-D",
  "QuestionId": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18",
  "choice": "D",
  "answer": "Encryption"
 },
 {
  "type": "ANSWER",
  "id": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18-E",
  "QuestionId": "SRM.11d5fd2c-b153-4509-b7cb-115c9e2fdb18",
  "choice": "E",
  "answer": "Masking"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164-A",
  "QuestionId": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164",
  "choice": "A",
  "answer": "BAA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164-B",
  "QuestionId": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164",
  "choice": "B",
  "answer": "Asset Return"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164-C",
  "QuestionId": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164",
  "choice": "C",
  "answer": "SLA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164-D",
  "QuestionId": "SRM.c402d128-bd67-47f7-8794-2a0fc12c8164",
  "choice": "D",
  "answer": "NDA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c-A",
  "QuestionId": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c",
  "choice": "A",
  "answer": "250,000"
 },
 {
  "type": "ANSWER",
  "id": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c-B",
  "QuestionId": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c",
  "choice": "B",
  "answer": "500,000"
 },
 {
  "type": "ANSWER",
  "id": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c-C",
  "QuestionId": "SRM.265f9110-6910-4a07-98a6-0c2837171b0c",
  "choice": "C",
  "answer": "1 million"
 },
 {
  "type": "ANSWER",
  "id": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe-A",
  "QuestionId": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe",
  "choice": "A",
  "answer": "Risk Avoidance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe-B",
  "QuestionId": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe",
  "choice": "B",
  "answer": "Risk Deterence"
 },
 {
  "type": "ANSWER",
  "id": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe-C",
  "QuestionId": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe",
  "choice": "C",
  "answer": "Risk Acceptance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe-D",
  "QuestionId": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe",
  "choice": "D",
  "answer": "Risk Mitigation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe-E",
  "QuestionId": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe",
  "choice": "E",
  "answer": "Risk Transference"
 },
 {
  "type": "ANSWER",
  "id": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe-F",
  "QuestionId": "SRM.440ec91f-8e23-47db-9049-8908f8cae8fe",
  "choice": "F",
  "answer": "Risk Assessment"
 },
 {
  "type": "ANSWER",
  "id": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d-A",
  "QuestionId": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d",
  "choice": "A",
  "answer": "Firewall"
 },
 {
  "type": "ANSWER",
  "id": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d-B",
  "QuestionId": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d",
  "choice": "B",
  "answer": "Code Review"
 },
 {
  "type": "ANSWER",
  "id": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d-C",
  "QuestionId": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d",
  "choice": "C",
  "answer": "System Administrator"
 },
 {
  "type": "ANSWER",
  "id": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d-D",
  "QuestionId": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d",
  "choice": "D",
  "answer": "Automatic Code Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d-E",
  "QuestionId": "SRM.09a99e2b-b7a1-4af4-90cb-961f7579881d",
  "choice": "E",
  "answer": "Pentration Test"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49-A",
  "QuestionId": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49",
  "choice": "A",
  "answer": "Criticality and Likelihood"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49-B",
  "QuestionId": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49",
  "choice": "B",
  "answer": "Impact and Criticality"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49-C",
  "QuestionId": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49",
  "choice": "C",
  "answer": "Frequency and Likelihood"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49-D",
  "QuestionId": "SRM.1fa5cf55-a605-4926-9600-2a64c43f4a49",
  "choice": "D",
  "answer": "Likelihood and Impact"
 },
 {
  "type": "ANSWER",
  "id": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023-A",
  "QuestionId": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023",
  "choice": "A",
  "answer": "ALE = ARO * AV"
 },
 {
  "type": "ANSWER",
  "id": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023-B",
  "QuestionId": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023",
  "choice": "B",
  "answer": "ALE = SLE * ARO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023-C",
  "QuestionId": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023",
  "choice": "C",
  "answer": "ALE = EF SLE ARO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023-D",
  "QuestionId": "SRM.46dbe115-6907-456c-96f6-b2a92eeed023",
  "choice": "D",
  "answer": "ALE = AV - SLE"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5efea285-a901-41a5-b759-d42c48d712f2-A",
  "QuestionId": "SRM.5efea285-a901-41a5-b759-d42c48d712f2",
  "choice": "A",
  "answer": "Authorize Information System"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5efea285-a901-41a5-b759-d42c48d712f2-B",
  "QuestionId": "SRM.5efea285-a901-41a5-b759-d42c48d712f2",
  "choice": "B",
  "answer": "Categorize information system"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5efea285-a901-41a5-b759-d42c48d712f2-C",
  "QuestionId": "SRM.5efea285-a901-41a5-b759-d42c48d712f2",
  "choice": "C",
  "answer": "Monitor Security Controls"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5efea285-a901-41a5-b759-d42c48d712f2-D",
  "QuestionId": "SRM.5efea285-a901-41a5-b759-d42c48d712f2",
  "choice": "D",
  "answer": "Select security controls"
 },
 {
  "type": "ANSWER",
  "id": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0-A",
  "QuestionId": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0",
  "choice": "A",
  "answer": "S"
 },
 {
  "type": "ANSWER",
  "id": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0-B",
  "QuestionId": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0",
  "choice": "B",
  "answer": "T"
 },
 {
  "type": "ANSWER",
  "id": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0-C",
  "QuestionId": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0",
  "choice": "C",
  "answer": "R"
 },
 {
  "type": "ANSWER",
  "id": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0-D",
  "QuestionId": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0",
  "choice": "D",
  "answer": "I"
 },
 {
  "type": "ANSWER",
  "id": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0-E",
  "QuestionId": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0",
  "choice": "E",
  "answer": "D"
 },
 {
  "type": "ANSWER",
  "id": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0-F",
  "QuestionId": "SRM.44e27cc1-54dd-4703-8eb1-fa51443c0dd0",
  "choice": "F",
  "answer": "E"
 },
 {
  "type": "ANSWER",
  "id": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856-A",
  "QuestionId": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856",
  "choice": "A",
  "answer": "RAID 0"
 },
 {
  "type": "ANSWER",
  "id": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856-B",
  "QuestionId": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856",
  "choice": "B",
  "answer": "RAID 1"
 },
 {
  "type": "ANSWER",
  "id": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856-C",
  "QuestionId": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856",
  "choice": "C",
  "answer": "RAID 3"
 },
 {
  "type": "ANSWER",
  "id": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856-D",
  "QuestionId": "SRM.12da5fde-ec0c-4ab4-b9d2-aea41a57b856",
  "choice": "D",
  "answer": "RAID 5"
 },
 {
  "type": "ANSWER",
  "id": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7-A",
  "QuestionId": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7",
  "choice": "A",
  "answer": "It is used for data transposition in encryption processes"
 },
 {
  "type": "ANSWER",
  "id": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7-B",
  "QuestionId": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7",
  "choice": "B",
  "answer": "It is used as data encoding mechanism for 802.11 WLANs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7-C",
  "QuestionId": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7",
  "choice": "C",
  "answer": "It is used to detect and correct errors in data"
 },
 {
  "type": "ANSWER",
  "id": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7-D",
  "QuestionId": "SRM.25c0bde3-8f5c-4bc6-a1c3-d07de9a2d9a7",
  "choice": "D",
  "answer": "It is used to calculate CRC checksums in Ethernet frames"
 },
 {
  "type": "ANSWER",
  "id": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410-A",
  "QuestionId": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410",
  "choice": "A",
  "answer": "MOU"
 },
 {
  "type": "ANSWER",
  "id": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410-B",
  "QuestionId": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410",
  "choice": "B",
  "answer": "SLA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410-C",
  "QuestionId": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410",
  "choice": "C",
  "answer": "SLR"
 },
 {
  "type": "ANSWER",
  "id": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410-D",
  "QuestionId": "SRM.021d83ca-9540-4801-b5ef-e5b1aefe2410",
  "choice": "D",
  "answer": "BPA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930-A",
  "QuestionId": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930",
  "choice": "A",
  "answer": "Customer retains unihibited data ownership"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930-B",
  "QuestionId": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930",
  "choice": "B",
  "answer": "Vendors right to use information is limited to activities performed on behalf of the customer"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930-C",
  "QuestionId": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930",
  "choice": "C",
  "answer": "Vendors right to use information is limited to activities performed without the customers knowledge"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930-D",
  "QuestionId": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930",
  "choice": "D",
  "answer": "Vendor must delete information at the end of the contract"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930-E",
  "QuestionId": "SRM.ac32a9e3-5c1a-4ec4-b2e3-3dc2042af930",
  "choice": "E",
  "answer": "Vendor is not allowed to share information with 3rd parties"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2-A",
  "QuestionId": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2",
  "choice": "A",
  "answer": "Manage an entire security infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2-B",
  "QuestionId": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2",
  "choice": "B",
  "answer": "Monitor System Logs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2-C",
  "QuestionId": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2",
  "choice": "C",
  "answer": "Manage firewalls and networks"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2-D",
  "QuestionId": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2",
  "choice": "D",
  "answer": "Monitor security performance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2-E",
  "QuestionId": "SRM.f9adbf3d-262a-4299-b2e2-3601bcdfd8f2",
  "choice": "E",
  "answer": "Perform identity and access management"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393-A",
  "QuestionId": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393",
  "choice": "A",
  "answer": "MOU"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393-B",
  "QuestionId": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393",
  "choice": "B",
  "answer": "SLA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393-C",
  "QuestionId": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393",
  "choice": "C",
  "answer": "BPA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393-D",
  "QuestionId": "SRM.ffe33446-5a34-4a03-8598-55afb22ec393",
  "choice": "D",
  "answer": "ISA"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297-A",
  "QuestionId": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297",
  "choice": "A",
  "answer": "Auditing"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297-B",
  "QuestionId": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297",
  "choice": "B",
  "answer": "Privacy"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297-C",
  "QuestionId": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297",
  "choice": "C",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297-D",
  "QuestionId": "SRM.ea1ad329-b35e-43c7-8bcc-7bed02f91297",
  "choice": "D",
  "answer": "Authorization"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a-A",
  "QuestionId": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a",
  "choice": "A",
  "answer": "Separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a-B",
  "QuestionId": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a",
  "choice": "B",
  "answer": "Restricted job responsibilities"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a-C",
  "QuestionId": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a",
  "choice": "C",
  "answer": "Group user accounts"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a-D",
  "QuestionId": "SRM.ceed8f8d-f3a7-451d-8fe9-0c6206eaac6a",
  "choice": "D",
  "answer": "Job rotation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518-A",
  "QuestionId": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518",
  "choice": "A",
  "answer": "Repeatable"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518-B",
  "QuestionId": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518",
  "choice": "B",
  "answer": "Defined"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518-C",
  "QuestionId": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518",
  "choice": "C",
  "answer": "Managed"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518-D",
  "QuestionId": "SRM.6ab5a63e-4e5f-4d64-a060-3839585ad518",
  "choice": "D",
  "answer": "Optimized"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66-A",
  "QuestionId": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66",
  "choice": "A",
  "answer": "Layer 0"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66-B",
  "QuestionId": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66",
  "choice": "B",
  "answer": "Layer 1"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66-C",
  "QuestionId": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66",
  "choice": "C",
  "answer": "Layer 3"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66-D",
  "QuestionId": "SRM.2d49ba54-ca14-497c-98f8-f5b1db535f66",
  "choice": "D",
  "answer": "Layer 4"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea-A",
  "QuestionId": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea",
  "choice": "A",
  "answer": "Time of check to time of use"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea-B",
  "QuestionId": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea",
  "choice": "B",
  "answer": "Buffer overflow"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea-C",
  "QuestionId": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea",
  "choice": "C",
  "answer": "SYN Flood"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea-D",
  "QuestionId": "SRM.a73338b2-0a7c-4f36-9a09-1a7ea6ffc0ea",
  "choice": "D",
  "answer": "Denial of Service"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5-A",
  "QuestionId": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5",
  "choice": "A",
  "answer": "0 1 1"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5-B",
  "QuestionId": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5",
  "choice": "B",
  "answer": "0 1 0"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5-C",
  "QuestionId": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5",
  "choice": "C",
  "answer": "0 0 0"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5-D",
  "QuestionId": "SRM.1512e3fe-eb36-455a-b7d7-1fd6e0c10ce5",
  "choice": "D",
  "answer": "0 0 1"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a-A",
  "QuestionId": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a",
  "choice": "A",
  "answer": "Security Perimeter"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a-B",
  "QuestionId": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a",
  "choice": "B",
  "answer": "Security Kernel"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a-C",
  "QuestionId": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a",
  "choice": "C",
  "answer": "Access Matrix"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a-D",
  "QuestionId": "SRM.f160cfc2-c0af-481f-b657-f6511fbd5f6a",
  "choice": "D",
  "answer": "Constrained Interface"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2-A",
  "QuestionId": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2",
  "choice": "A",
  "answer": "The less complex a system is the more vulnerabilities it has"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2-B",
  "QuestionId": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2",
  "choice": "B",
  "answer": "The more complex a system is, the less assurance it provides"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2-C",
  "QuestionId": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2",
  "choice": "C",
  "answer": "The less complex a system, the less trust it provides"
 },
 {
  "type": "ANSWER",
  "id": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2-D",
  "QuestionId": "SRM.ac9449f1-c0f1-4938-822e-5c1b24b9a1e2",
  "choice": "D",
  "answer": "The more complex a system, the less attack surface it provides"
 },
 {
  "type": "ANSWER",
  "id": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5-A",
  "QuestionId": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5",
  "choice": "A",
  "answer": "Quality Assurance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5-B",
  "QuestionId": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5",
  "choice": "B",
  "answer": "Operationational Assurance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5-C",
  "QuestionId": "SRM.cbac2557-f2f5-447d-87b7-f3429dc923b5",
  "choice": "C",
  "answer": "Lifecycle Assurance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3-A",
  "QuestionId": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3",
  "choice": "A",
  "answer": "Priviledge Mode"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3-B",
  "QuestionId": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3",
  "choice": "B",
  "answer": "Supervisory Mode"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3-C",
  "QuestionId": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3",
  "choice": "C",
  "answer": "System Mode"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3-D",
  "QuestionId": "SRM.6a71d9f1-be3e-4f32-a7f8-8de638d56bb3",
  "choice": "D",
  "answer": "User Mode"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0b1fdb59-e955-4805-aee6-475689470516-A",
  "QuestionId": "SRM.0b1fdb59-e955-4805-aee6-475689470516",
  "choice": "A",
  "answer": "Preventative Controls"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0b1fdb59-e955-4805-aee6-475689470516-B",
  "QuestionId": "SRM.0b1fdb59-e955-4805-aee6-475689470516",
  "choice": "B",
  "answer": "Detective Controls"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0b1fdb59-e955-4805-aee6-475689470516-C",
  "QuestionId": "SRM.0b1fdb59-e955-4805-aee6-475689470516",
  "choice": "C",
  "answer": "Corrective Controls"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662-A",
  "QuestionId": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662",
  "choice": "A",
  "answer": "Host based"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662-B",
  "QuestionId": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662",
  "choice": "B",
  "answer": "Network based"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662-C",
  "QuestionId": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662",
  "choice": "C",
  "answer": "Knowlege Based System"
 },
 {
  "type": "ANSWER",
  "id": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662-D",
  "QuestionId": "SRM.4220dd3c-0d63-451e-8899-5034fb4b0662",
  "choice": "D",
  "answer": "Behaviour Based System"
 },
 {
  "type": "ANSWER",
  "id": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59-A",
  "QuestionId": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59",
  "choice": "A",
  "answer": "RPO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59-B",
  "QuestionId": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59",
  "choice": "B",
  "answer": "MTBF"
 },
 {
  "type": "ANSWER",
  "id": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59-C",
  "QuestionId": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59",
  "choice": "C",
  "answer": "MTD"
 },
 {
  "type": "ANSWER",
  "id": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59-D",
  "QuestionId": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59",
  "choice": "D",
  "answer": "RTO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59-E",
  "QuestionId": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59",
  "choice": "E",
  "answer": "MTBSI"
 },
 {
  "type": "ANSWER",
  "id": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59-F",
  "QuestionId": "SRM.392d91a3-1bc0-4c29-90e0-750b99da5c59",
  "choice": "F",
  "answer": "TTR"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d-A",
  "QuestionId": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d",
  "choice": "A",
  "answer": "Provide a welcome message to connecting users?"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d-B",
  "QuestionId": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d",
  "choice": "B",
  "answer": "Notifying user of active monitoring"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d-C",
  "QuestionId": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d",
  "choice": "C",
  "answer": "Provider system information upon connection"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d-D",
  "QuestionId": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d",
  "choice": "D",
  "answer": "Deter hackers attempting to connect"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d-E",
  "QuestionId": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d",
  "choice": "E",
  "answer": "Establishing \"no expectation of privacy\""
 },
 {
  "type": "ANSWER",
  "id": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d-F",
  "QuestionId": "SRM.0c89ff40-c9bb-451c-8568-fff07346df3d",
  "choice": "F",
  "answer": "Defining who is allowed to acces the system"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2-A",
  "QuestionId": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2",
  "choice": "A",
  "answer": "Hashing Files to ensure integrity"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2-B",
  "QuestionId": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2",
  "choice": "B",
  "answer": "Loggint activity per IP address"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2-C",
  "QuestionId": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2",
  "choice": "C",
  "answer": "Setting permissions on folders"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2-D",
  "QuestionId": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2",
  "choice": "D",
  "answer": "Individual sign on per user"
 },
 {
  "type": "ANSWER",
  "id": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2-E",
  "QuestionId": "SRM.bc9a1ca8-d0de-40ba-a3f6-734a4fdbe5a2",
  "choice": "E",
  "answer": "Limiting the number of employees that have keys to the building."
 },
 {
  "type": "ANSWER",
  "id": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1-A",
  "QuestionId": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1",
  "choice": "A",
  "answer": "Alteration, Destruction and Disclosure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1-B",
  "QuestionId": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1",
  "choice": "B",
  "answer": "Compromise, Inaccuracy and Destruction"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1-C",
  "QuestionId": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1",
  "choice": "C",
  "answer": "Disclosure, Alteration and Destruction"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1-D",
  "QuestionId": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1",
  "choice": "D",
  "answer": "Inaccuracy, Compomise, Disclosure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1-E",
  "QuestionId": "SRM.5fd1518c-a24a-4dac-9393-0ac5c3360aa1",
  "choice": "E",
  "answer": "Top Secret, Secret, Unclassifed"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd-A",
  "QuestionId": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd",
  "choice": "A",
  "answer": "One or more of the CIA Triad"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd-B",
  "QuestionId": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd",
  "choice": "B",
  "answer": "Data usefulness"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd-C",
  "QuestionId": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd",
  "choice": "C",
  "answer": "Due care"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd-D",
  "QuestionId": "SRM.6e6f65c2-7d31-4d41-8128-f838abfe75bd",
  "choice": "D",
  "answer": "Extent of liability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1dcd2588-2900-4d84-a62c-909712911bca-A",
  "QuestionId": "SRM.1dcd2588-2900-4d84-a62c-909712911bca",
  "choice": "A",
  "answer": "Stealing passwords"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1dcd2588-2900-4d84-a62c-909712911bca-B",
  "QuestionId": "SRM.1dcd2588-2900-4d84-a62c-909712911bca",
  "choice": "B",
  "answer": "Eavesdropping"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1dcd2588-2900-4d84-a62c-909712911bca-C",
  "QuestionId": "SRM.1dcd2588-2900-4d84-a62c-909712911bca",
  "choice": "C",
  "answer": "Hardware destruction"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1dcd2588-2900-4d84-a62c-909712911bca-D",
  "QuestionId": "SRM.1dcd2588-2900-4d84-a62c-909712911bca",
  "choice": "D",
  "answer": "Social Engineering"
 },
 {
  "type": "ANSWER",
  "id": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88-A",
  "QuestionId": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88",
  "choice": "A",
  "answer": "Violations of confidentiality include human error"
 },
 {
  "type": "ANSWER",
  "id": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88-B",
  "QuestionId": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88",
  "choice": "B",
  "answer": "Violations of confidentiality include management oversight"
 },
 {
  "type": "ANSWER",
  "id": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88-C",
  "QuestionId": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88",
  "choice": "C",
  "answer": "Violations of confidentiality are limited to direct intentional acts"
 },
 {
  "type": "ANSWER",
  "id": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88-D",
  "QuestionId": "SRM.afbe33bf-47c7-487a-9af8-43fd9f15ae88",
  "choice": "D",
  "answer": "Violations of confidentiality can occur when a transmission is not properly encrypted."
 },
 {
  "type": "ANSWER",
  "id": "SRM.a43efd88-6245-4694-b506-25665c24d9ef-A",
  "QuestionId": "SRM.a43efd88-6245-4694-b506-25665c24d9ef",
  "choice": "A",
  "answer": "Spoofing"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a43efd88-6245-4694-b506-25665c24d9ef-B",
  "QuestionId": "SRM.a43efd88-6245-4694-b506-25665c24d9ef",
  "choice": "B",
  "answer": "Elevation of privelege"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a43efd88-6245-4694-b506-25665c24d9ef-C",
  "QuestionId": "SRM.a43efd88-6245-4694-b506-25665c24d9ef",
  "choice": "C",
  "answer": "Repudiation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a43efd88-6245-4694-b506-25665c24d9ef-D",
  "QuestionId": "SRM.a43efd88-6245-4694-b506-25665c24d9ef",
  "choice": "D",
  "answer": "Disclosure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b06e843f-1eab-4356-b09a-21b913b15987-A",
  "QuestionId": "SRM.b06e843f-1eab-4356-b09a-21b913b15987",
  "choice": "A",
  "answer": "Restricting personal email"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b06e843f-1eab-4356-b09a-21b913b15987-B",
  "QuestionId": "SRM.b06e843f-1eab-4356-b09a-21b913b15987",
  "choice": "B",
  "answer": "Recording Phone Conversations"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b06e843f-1eab-4356-b09a-21b913b15987-C",
  "QuestionId": "SRM.b06e843f-1eab-4356-b09a-21b913b15987",
  "choice": "C",
  "answer": "Gathering information about surfing habits"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b06e843f-1eab-4356-b09a-21b913b15987-D",
  "QuestionId": "SRM.b06e843f-1eab-4356-b09a-21b913b15987",
  "choice": "D",
  "answer": "The backup mechanism used to retain email"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079-A",
  "QuestionId": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079",
  "choice": "A",
  "answer": "Classification"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079-B",
  "QuestionId": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079",
  "choice": "B",
  "answer": "Physical Access"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079-C",
  "QuestionId": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079",
  "choice": "C",
  "answer": "Custodian Responsibility"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079-D",
  "QuestionId": "SRM.d1a8fc80-1b17-4578-9dc2-302e978ac079",
  "choice": "D",
  "answer": "Taking Ownership"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284-A",
  "QuestionId": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284",
  "choice": "A",
  "answer": "Multiple"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284-B",
  "QuestionId": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284",
  "choice": "B",
  "answer": "Series"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284-C",
  "QuestionId": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284",
  "choice": "C",
  "answer": "Parallel"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284-D",
  "QuestionId": "SRM.5b0ebc21-dde2-417f-9564-10bf9fefe284",
  "choice": "D",
  "answer": "Filter"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed-A",
  "QuestionId": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed",
  "choice": "A",
  "answer": "Preventing an authorized reader of an object from deleting that object"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed-B",
  "QuestionId": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed",
  "choice": "B",
  "answer": "Keeping a database from being accessed by unauthorized visitors"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed-C",
  "QuestionId": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed",
  "choice": "C",
  "answer": "Restricting a subject at a lower classification level from accessing data at a higher classification level"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed-D",
  "QuestionId": "SRM.f801ec13-2de3-4c88-9e42-22fbc27458ed",
  "choice": "D",
  "answer": "Preventing an application from accessing hardware directly"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27-A",
  "QuestionId": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27",
  "choice": "A",
  "answer": "Maintaining documentation"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27-B",
  "QuestionId": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27",
  "choice": "B",
  "answer": "Keeping users informed of change"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27-C",
  "QuestionId": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27",
  "choice": "C",
  "answer": "Allowing rollback of failed changes"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27-D",
  "QuestionId": "SRM.e9c88248-894e-43e8-a771-4e83b3805f27",
  "choice": "D",
  "answer": "Preventing security compromises"
 },
 {
  "type": "ANSWER",
  "id": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276-A",
  "QuestionId": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276",
  "choice": "A",
  "answer": "To control access to objects for authorized subjects"
 },
 {
  "type": "ANSWER",
  "id": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276-B",
  "QuestionId": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276",
  "choice": "B",
  "answer": "To formalize and stratify the process of securing data based on assigned labels of importance and sensitivity"
 },
 {
  "type": "ANSWER",
  "id": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276-C",
  "QuestionId": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276",
  "choice": "C",
  "answer": "To establish a transaction trail for auditing accountability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276-D",
  "QuestionId": "SRM.af9b6735-1a22-4c65-994a-d4a42988b276",
  "choice": "D",
  "answer": "To manipulate access controls to provide for the most efficient means to grant or restrict functionality"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8-A",
  "QuestionId": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8",
  "choice": "A",
  "answer": "Value"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8-B",
  "QuestionId": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8",
  "choice": "B",
  "answer": "Size of object"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8-C",
  "QuestionId": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8",
  "choice": "C",
  "answer": "Useful lifetime"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8-D",
  "QuestionId": "SRM.f4300b72-7ded-4db3-8384-3dddf14b53a8",
  "choice": "D",
  "answer": "National Security implications"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f-A",
  "QuestionId": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f",
  "choice": "A",
  "answer": "Military and private sector"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f-B",
  "QuestionId": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f",
  "choice": "B",
  "answer": "Personal and government"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f-C",
  "QuestionId": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f",
  "choice": "C",
  "answer": "Private sector and unrestricted sector"
 },
 {
  "type": "ANSWER",
  "id": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f-D",
  "QuestionId": "SRM.2de77ea2-371d-4f15-b32f-80b8c95b8e2f",
  "choice": "D",
  "answer": "Classified and unclassifed"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1-A",
  "QuestionId": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1",
  "choice": "A",
  "answer": "Sensitive"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1-B",
  "QuestionId": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1",
  "choice": "B",
  "answer": "Secret"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1-C",
  "QuestionId": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1",
  "choice": "C",
  "answer": "Proprietary"
 },
 {
  "type": "ANSWER",
  "id": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1-D",
  "QuestionId": "SRM.e3e319e2-f691-4149-96a1-746c5bd90dd1",
  "choice": "D",
  "answer": "Private"
 },
 {
  "type": "ANSWER",
  "id": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e-A",
  "QuestionId": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e",
  "choice": "A",
  "answer": "Confidential"
 },
 {
  "type": "ANSWER",
  "id": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e-B",
  "QuestionId": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e",
  "choice": "B",
  "answer": "Private"
 },
 {
  "type": "ANSWER",
  "id": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e-C",
  "QuestionId": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e",
  "choice": "C",
  "answer": "Sensitive"
 },
 {
  "type": "ANSWER",
  "id": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e-D",
  "QuestionId": "SRM.91dfbe88-f177-42a1-9b65-244e2674537e",
  "choice": "D",
  "answer": "Propietary"
 },
 {
  "type": "ANSWER",
  "id": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3-A",
  "QuestionId": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3",
  "choice": "A",
  "answer": "Storage"
 },
 {
  "type": "ANSWER",
  "id": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3-B",
  "QuestionId": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3",
  "choice": "B",
  "answer": "Processing"
 },
 {
  "type": "ANSWER",
  "id": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3-C",
  "QuestionId": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3",
  "choice": "C",
  "answer": "Layering"
 },
 {
  "type": "ANSWER",
  "id": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3-D",
  "QuestionId": "SRM.647c3dd9-63f9-48e5-a6a5-bcf9d5e70cb3",
  "choice": "D",
  "answer": "Transfer"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335-A",
  "QuestionId": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335",
  "choice": "A",
  "answer": "Software products"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335-B",
  "QuestionId": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335",
  "choice": "B",
  "answer": "Internet Connection"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335-C",
  "QuestionId": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335",
  "choice": "C",
  "answer": "Security policies"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335-D",
  "QuestionId": "SRM.8d3ca788-809a-44d1-b756-fe118e1e2335",
  "choice": "D",
  "answer": "Humans"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b-A",
  "QuestionId": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b",
  "choice": "A",
  "answer": "Create Job Description"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b-B",
  "QuestionId": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b",
  "choice": "B",
  "answer": "Set Position Characteristics"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b-C",
  "QuestionId": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b",
  "choice": "C",
  "answer": "Screen Candidates"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b-D",
  "QuestionId": "SRM.89fcf732-c0a8-43e5-8889-e6e94c61292b",
  "choice": "D",
  "answer": "Request referrals"
 },
 {
  "type": "ANSWER",
  "id": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4-A",
  "QuestionId": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4",
  "choice": "A",
  "answer": "Inform the employee a few hours before they are officially terminated."
 },
 {
  "type": "ANSWER",
  "id": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4-B",
  "QuestionId": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4",
  "choice": "B",
  "answer": "Disable the employee’s network access just as they are informed of the termination."
 },
 {
  "type": "ANSWER",
  "id": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4-C",
  "QuestionId": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4",
  "choice": "C",
  "answer": "Send out a broadcast email informing everyone that a specific employee is to be terminated."
 },
 {
  "type": "ANSWER",
  "id": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4-D",
  "QuestionId": "SRM.830b1cca-1aed-40a1-af10-086f62fe46b4",
  "choice": "D",
  "answer": "Wait until you and the employee are the only people remaining in the building before announcing the termination."
 },
 {
  "type": "ANSWER",
  "id": "SRM.f429f56a-f210-4635-84ef-89dd28304c88-A",
  "QuestionId": "SRM.f429f56a-f210-4635-84ef-89dd28304c88",
  "choice": "A",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "SRM.f429f56a-f210-4635-84ef-89dd28304c88-B",
  "QuestionId": "SRM.f429f56a-f210-4635-84ef-89dd28304c88",
  "choice": "B",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "SRM.f429f56a-f210-4635-84ef-89dd28304c88-C",
  "QuestionId": "SRM.f429f56a-f210-4635-84ef-89dd28304c88",
  "choice": "C",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "SRM.f429f56a-f210-4635-84ef-89dd28304c88-D",
  "QuestionId": "SRM.f429f56a-f210-4635-84ef-89dd28304c88",
  "choice": "D",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8-A",
  "QuestionId": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8",
  "choice": "A",
  "answer": "Asset Identification"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8-B",
  "QuestionId": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8",
  "choice": "B",
  "answer": "Third Party Governance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8-C",
  "QuestionId": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8",
  "choice": "C",
  "answer": "Exit Interview"
 },
 {
  "type": "ANSWER",
  "id": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8-D",
  "QuestionId": "SRM.f322fef5-68ce-4cb6-884d-9c52f75047a8",
  "choice": "D",
  "answer": "Qualitative Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a-A",
  "QuestionId": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a",
  "choice": "A",
  "answer": "Hybrid Assessment"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a-B",
  "QuestionId": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a",
  "choice": "B",
  "answer": "Risk aversion process"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a-C",
  "QuestionId": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a",
  "choice": "C",
  "answer": "Countermeasure selection"
 },
 {
  "type": "ANSWER",
  "id": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a-D",
  "QuestionId": "SRM.d50d8db9-622b-4311-b9a9-9b5ad942c82a",
  "choice": "D",
  "answer": "Documentation review"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1-A",
  "QuestionId": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1",
  "choice": "A",
  "answer": "IT security can provide protection only against logical or technical attacks"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1-B",
  "QuestionId": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1",
  "choice": "B",
  "answer": "THe process by which the goals of risk management are acheived is known as risk analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1-C",
  "QuestionId": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1",
  "choice": "C",
  "answer": "Risks to an IT infarstructure are all computer based"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1-D",
  "QuestionId": "SRM.a49fe36e-90e1-4c74-bc7d-08d9048322e1",
  "choice": "D",
  "answer": "An asset is anything used in a business process or task"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9-A",
  "QuestionId": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9",
  "choice": "A",
  "answer": "Analyzing an environment for risks"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9-B",
  "QuestionId": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9",
  "choice": "B",
  "answer": "Creating a cost/benefit report for safeguards to present to upper management"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9-C",
  "QuestionId": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9",
  "choice": "C",
  "answer": "Selecting appropriate safeguards and implementing them"
 },
 {
  "type": "ANSWER",
  "id": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9-D",
  "QuestionId": "SRM.a7aa8f62-5046-4052-847b-500ce71296c9",
  "choice": "D",
  "answer": "Evaluating each threat event as to its likelihood of ocurring and cost of the resulting damage"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827-A",
  "QuestionId": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827",
  "choice": "A",
  "answer": "A development process"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827-B",
  "QuestionId": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827",
  "choice": "B",
  "answer": "An IT infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827-C",
  "QuestionId": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827",
  "choice": "C",
  "answer": "A proprietary system resource"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827-D",
  "QuestionId": "SRM.6fcd9916-1dc5-406b-b9ac-1dbbfb726827",
  "choice": "D",
  "answer": "Users personal files"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2-A",
  "QuestionId": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2",
  "choice": "A",
  "answer": "Threat events"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2-B",
  "QuestionId": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2",
  "choice": "B",
  "answer": "Risks"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2-C",
  "QuestionId": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2",
  "choice": "C",
  "answer": "Threat agents"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2-D",
  "QuestionId": "SRM.6d96df52-34f6-46a2-a8cf-5d13b77a8fc2",
  "choice": "D",
  "answer": "Vulnerabilities"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0-A",
  "QuestionId": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0",
  "choice": "A",
  "answer": "Vulnerability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0-B",
  "QuestionId": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0",
  "choice": "B",
  "answer": "Exposure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0-C",
  "QuestionId": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0",
  "choice": "C",
  "answer": "Risk"
 },
 {
  "type": "ANSWER",
  "id": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0-D",
  "QuestionId": "SRM.5618b312-1ea8-4041-b315-c7cd2b6f70c0",
  "choice": "D",
  "answer": "Penetration"
 },
 {
  "type": "ANSWER",
  "id": "SRM.21ee9e81-194e-432c-9391-83d95e158e32-A",
  "QuestionId": "SRM.21ee9e81-194e-432c-9391-83d95e158e32",
  "choice": "A",
  "answer": "An assessment of probability, possible or chance"
 },
 {
  "type": "ANSWER",
  "id": "SRM.21ee9e81-194e-432c-9391-83d95e158e32-B",
  "QuestionId": "SRM.21ee9e81-194e-432c-9391-83d95e158e32",
  "choice": "B",
  "answer": "Anything that removes vulneability or protects against one or more threats"
 },
 {
  "type": "ANSWER",
  "id": "SRM.21ee9e81-194e-432c-9391-83d95e158e32-C",
  "QuestionId": "SRM.21ee9e81-194e-432c-9391-83d95e158e32",
  "choice": "C",
  "answer": "Risk = threat * vulnerability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.21ee9e81-194e-432c-9391-83d95e158e32-D",
  "QuestionId": "SRM.21ee9e81-194e-432c-9391-83d95e158e32",
  "choice": "D",
  "answer": "Every instance of exposure"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3-A",
  "QuestionId": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3",
  "choice": "A",
  "answer": "The expected annual cost of asset loss should not exceed the annual cost of safeguards"
 },
 {
  "type": "ANSWER",
  "id": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3-B",
  "QuestionId": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3",
  "choice": "B",
  "answer": "The annual costs of safeguards should equal the value of the asset."
 },
 {
  "type": "ANSWER",
  "id": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3-C",
  "QuestionId": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3",
  "choice": "C",
  "answer": "The annual costs of safeguards should not exceed the expected annual cost of asset loss."
 },
 {
  "type": "ANSWER",
  "id": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3-D",
  "QuestionId": "SRM.0b6cf8e9-7a5f-47c7-8eb1-23b6faf0c7d3",
  "choice": "D",
  "answer": "The annual costs of safeguards should not exceed 10 percent of the security budget."
 },
 {
  "type": "ANSWER",
  "id": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f-A",
  "QuestionId": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f",
  "choice": "A",
  "answer": "Threat + Vulnerability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f-B",
  "QuestionId": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f",
  "choice": "B",
  "answer": "Asset Value * exposure factor"
 },
 {
  "type": "ANSWER",
  "id": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f-C",
  "QuestionId": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f",
  "choice": "C",
  "answer": "Annual Rate of Occurance * vulneability"
 },
 {
  "type": "ANSWER",
  "id": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f-D",
  "QuestionId": "SRM.769f41bc-9a0a-45ec-8b8e-b43a20c3667f",
  "choice": "D",
  "answer": "Annualized Rate of Occurance Asset Value Exposure Factor"
 },
 {
  "type": "ANSWER",
  "id": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6-A",
  "QuestionId": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6",
  "choice": "A",
  "answer": "ALE before safeguard – ALE after implementing the safeguard – annual cost of safeguard"
 },
 {
  "type": "ANSWER",
  "id": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6-B",
  "QuestionId": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6",
  "choice": "B",
  "answer": "ALE before safeguard * ARO of safeguard"
 },
 {
  "type": "ANSWER",
  "id": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6-C",
  "QuestionId": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6",
  "choice": "C",
  "answer": "ALE after implementing safeguard + annual cost of safeguard – controls gap"
 },
 {
  "type": "ANSWER",
  "id": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6-D",
  "QuestionId": "SRM.20ac7b60-2c57-49fd-8f58-807b17d497c6",
  "choice": "D",
  "answer": "Total risk – controls gap"
 },
 {
  "type": "ANSWER",
  "id": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8-A",
  "QuestionId": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8",
  "choice": "A",
  "answer": "Principle of least privilege"
 },
 {
  "type": "ANSWER",
  "id": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8-B",
  "QuestionId": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8",
  "choice": "B",
  "answer": "Job Description"
 },
 {
  "type": "ANSWER",
  "id": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8-C",
  "QuestionId": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8",
  "choice": "C",
  "answer": "Separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8-D",
  "QuestionId": "SRM.030d5ed9-5777-419c-b6a8-285cd19622b8",
  "choice": "D",
  "answer": "Qualitative Risk Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02-A",
  "QuestionId": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02",
  "choice": "A",
  "answer": "Education"
 },
 {
  "type": "ANSWER",
  "id": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02-B",
  "QuestionId": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02",
  "choice": "B",
  "answer": "Awareness"
 },
 {
  "type": "ANSWER",
  "id": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02-C",
  "QuestionId": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02",
  "choice": "C",
  "answer": "Training"
 },
 {
  "type": "ANSWER",
  "id": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02-D",
  "QuestionId": "SRM.36dafe34-ca9f-4076-8080-421c777d7e02",
  "choice": "D",
  "answer": "Termination"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0-A",
  "QuestionId": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0",
  "choice": "A",
  "answer": "Worker job satisfaction"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0-B",
  "QuestionId": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0",
  "choice": "B",
  "answer": "Metrics"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0-C",
  "QuestionId": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0",
  "choice": "C",
  "answer": "Information Security Strategies"
 },
 {
  "type": "ANSWER",
  "id": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0-D",
  "QuestionId": "SRM.89c352c1-248e-4571-8849-b2536a01ddd0",
  "choice": "D",
  "answer": "Budget"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656-A",
  "QuestionId": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656",
  "choice": "A",
  "answer": "EF"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656-B",
  "QuestionId": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656",
  "choice": "B",
  "answer": "SLE"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656-C",
  "QuestionId": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656",
  "choice": "C",
  "answer": "Asset Value"
 },
 {
  "type": "ANSWER",
  "id": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656-D",
  "QuestionId": "SRM.8ce11ce9-2b60-47dd-9afb-23e5e70b3656",
  "choice": "D",
  "answer": "ARO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240-A",
  "QuestionId": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240",
  "choice": "A",
  "answer": "BCP Team Selection"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240-B",
  "QuestionId": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240",
  "choice": "B",
  "answer": "Business organization analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240-C",
  "QuestionId": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240",
  "choice": "C",
  "answer": "Resource Requirements Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240-D",
  "QuestionId": "SRM.1d509ace-668c-47ec-9aa0-d5a4ea513240",
  "choice": "D",
  "answer": "Legal and Regulatory assessment"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136-A",
  "QuestionId": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136",
  "choice": "A",
  "answer": "Business Impact Assessment"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136-B",
  "QuestionId": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136",
  "choice": "B",
  "answer": "Business organization analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136-C",
  "QuestionId": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136",
  "choice": "C",
  "answer": "Resource Requirements Analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136-D",
  "QuestionId": "SRM.7eaf16d9-d8ac-4bc3-9f56-278734a2b136",
  "choice": "D",
  "answer": "Legal and Regulatory assessment"
 },
 {
  "type": "ANSWER",
  "id": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b-A",
  "QuestionId": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b",
  "choice": "A",
  "answer": "Corporate Responsibility"
 },
 {
  "type": "ANSWER",
  "id": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b-B",
  "QuestionId": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b",
  "choice": "B",
  "answer": "Review and validation of the business organizational analysis"
 },
 {
  "type": "ANSWER",
  "id": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b-C",
  "QuestionId": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b",
  "choice": "C",
  "answer": "Due diligence"
 },
 {
  "type": "ANSWER",
  "id": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b-D",
  "QuestionId": "SRM.121395c6-3071-4381-b248-f19fe71cfb8b",
  "choice": "D",
  "answer": "Going concern responsilbity"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8-A",
  "QuestionId": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8",
  "choice": "A",
  "answer": "ARO"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8-B",
  "QuestionId": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8",
  "choice": "B",
  "answer": "SLE"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8-C",
  "QuestionId": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8",
  "choice": "C",
  "answer": "ALE"
 },
 {
  "type": "ANSWER",
  "id": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8-D",
  "QuestionId": "SRM.b5ae5554-2b53-46d0-ae79-b0bc314be1b8",
  "choice": "D",
  "answer": "EF"
 }
]