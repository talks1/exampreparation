export const Log = [
 {
  "type": "QUESTION",
  "id": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39",
  "question": "At which stage in the SDLC should a privacy impact assessment first be conducted",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8",
  "question": "A small but talented company has developed proprietary code for you. You are concerned that in the future the employees and maybe even the company may break up and move on. What is the best way for you to protect the value of your purchased product?",
  "answer": "D",
  "explanation": "Software escrow is usually requested by the buyers, who intend to ensure the continuity of the software maintenance over time, even if the software house that has developed the application goes out of the business or fails to maintain and update the code."
 },
 {
  "type": "QUESTION",
  "id": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f",
  "question": "If a web application is not doing appropriate input validation what is the most likely consequence of this",
  "answer": "A",
  "explanation": "According to OWASP SQL injection is the most frequently occurring type of attack when there is inappropriate validation.\nCross Site Scripting also occurs but is less frequent.\n [The OWASP Top Ten provides this information( https://owasp.org/www-project-top-ten/2017/A1_2017-Injection )"
 },
 {
  "type": "QUESTION",
  "id": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7",
  "question": "You are designing a relational db and what to ensure that the design is logical, efficient, and optimized. WHich of the following processes should you follow to acheive this.",
  "answer": "B",
  "explanation": "This is a process where some data is converted into relational form by considering the constraints expected at different normal forms to make the data towards a normalized form. There are NF1,2,3,4,5"
 },
 {
  "type": "QUESTION",
  "id": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b",
  "question": "Which of the following differentiates DOM based XSS from stored or reflected XSS attackes",
  "answer": "B",
  "explanation": "Apparrently DOM based attacks write scripts into the DOM ...where they are not usually found. SOme browser versions dont handle this properly."
 },
 {
  "type": "QUESTION",
  "id": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a",
  "question": "You are completing design of the Security Architecture. What phase of the SDLC are you in?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc",
  "question": "Which of the following memory types is used by programs on your system.",
  "answer": "B",
  "explanation": "The OS manages memory and hides the actual physical or virtual (on disk) memory location behind a logical address"
 },
 {
  "type": "QUESTION",
  "id": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4",
  "question": "You have tasked your security team with reviewing an internal web application. If configured which of the following would be considered GREATEST weakness in the session management configuration",
  "answer": "D",
  "explanation": "If session info is in the URL sending a URL to someone means they access a site as yourself."
 },
 {
  "type": "QUESTION",
  "id": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de",
  "question": "An approach based on lean and agile principles in which business owners and the development, operations and quality assurance departments collaborate?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b",
  "question": "How can security violations due to object reuse be mitigated",
  "answer": "D",
  "explanation": "If we had secure information in RAM we zeroize it to eliminate possiblity it might be used by another process."
 },
 {
  "type": "QUESTION",
  "id": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa",
  "question": "Which of the following is a characteristic of an interpreted programming language",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d",
  "question": "Software prototyping was introduced to overcome some limitations of the waterfall approach to software development.",
  "answer": "A, D,E",
  "explanation": [
   {
    "value": "loss of the big picture"
   },
   {
    "value": "content in prototype doesnt make it into final code base"
   },
   {
    "value": "scope screep"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "question": "The Montreal Protocol, an international treaty in the 1980's endevours to protect the earths ozone layer from depletion. This includes the replacement of the Halon-base fire suppression systems. The EPA SNAP (Significant New Alternatives Policy) provides a list of alternatives. Which of the following are suitable Halon replacements according to SNAP? Pick 6",
  "answer": "B,C,D,E,G,I",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4",
  "question": "Which of the following best describes the domains of a relation in a relational database?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063",
  "question": "Which one of the following is not a standard application hardening technique?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b",
  "question": "What software development methodology uses four stages in an iterative process?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e",
  "question": "What phase of the capability maturity model introduces the reuse of code across projects?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db",
  "question": "What component of a change management program includes final testing that the software functions properly?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505",
  "question": "What is the most effective defense against cross-site scripting attacks?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8",
  "question": "What character is essential in input for a SQL injection attack?",
  "answer": "B",
  "explanation": "The quote can be used to terminate one statement and start a new one."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185",
  "question": "Alan is analyzing his web server logs and sees several strange entries that contain strings similar to ../../\" in URL requests. What type of attack was attempted against his server?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed",
  "question": "Which choice is not a significant risk associated with browser add-ons and extensions?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2",
  "question": "Developers wishing to sign their code must have a ",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52",
  "question": "What portion of the change management process allows developers to prioritize tasks?",
  "answer": "C",
  "explanation": "The request control provides users with a framework to request changes and developers with the opportunity to prioritize those requests."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b",
  "question": "What approach to failure management places the system in a high level of security?",
  "answer": "C",
  "explanation": "In a fail-secure state, the system remains in a high level of security until an administrator intervenes."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655",
  "question": "What software development model uses a seven-stage approach with a feedback loop that allows progress one step backward?",
  "answer": "B",
  "explanation": "The waterfall model uses a seven-stage approach to software development and includes a feedback loop that allows development to return to the previous phase to correct defects discovered during the subsequent phase."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702",
  "question": "Richard believes that a database user is misusing his privileges to gain information about the company’s overall business trends by issuing queries that combine data from a large number of records. What process is the database user taking advantage of?",
  "answer": "",
  "explanation": "In this case, the process the database user is taking advantage of is aggregation. Aggregation attacks involve the use of specialized database functions to combine information from a large number of database records to reveal information that may be more sensitive than the information in individual records"
 },
 {
  "type": "QUESTION",
  "id": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3",
  "question": "What database technique can be used to prevent unauthorized users from determining classified information by noticing the absence of information normally available to them?",
  "answer": "C",
  "explanation": "Polyinstantiation allows the insertion of multiple records that appear to have the same primary key values into a database at different classification levels."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8",
  "question": "Which one of the following is not a principle of Agile development?",
  "answer": "D",
  "explanation": "In Agile, the highest priority is to satisfy the customer through early and continuous delivery of valuable software."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78",
  "question": "What type of information is used to form the basis of an expert system’s decision-making process?",
  "answer": "C",
  "explanation": "Expert systems use a knowledge base consisting of a series of “if/then” statements to form decisions based on the previous experience of human experts."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247",
  "question": "In which phase of the SW-CMM does an organization use quantitative measures to gain a detailed understanding of the development process?",
  "answer": "D",
  "explanation": "In the Managed phase, level 4 of the SW-CMM, the organization uses quantitative measures to gain a detailed understanding of the development process."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d",
  "question": "Which of the following acts as a proxy between an application and a database to support interaction and simplify the work of programmers?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8",
  "question": "In what type of software testing does the tester have access to the underlying source code?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653",
  "question": "What type of chart provides a graphical illustration of a schedule that helps to plan, coordinate, and track project tasks?",
  "answer": "A",
  "explanation": "A. A Gantt chart is a type of bar chart that shows the interrelationships over time between projects and schedules. It provides a graphical illustration of a schedule that helps to plan, coordinate, and track specific tasks in a project."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69",
  "question": "Which database security risk occurs when data from a higher classification level is mixed with data from a lower classification level?",
  "answer": "C",
  "explanation": "Contamination is the mixing of data from a higher classification level and/or need-to-know requirement with data from a lower classification level and/or need-to-know requirement."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.7857613f-efd6-471a-a1b7-022defccd868",
  "question": "What database security technology involves creating two or more rows with seemingly identical primary keys that contain different data for users with different security clearances?",
  "answer": "A",
  "explanation": "Database developers use polyinstantiation, the creation of multiple records that seem to have the same primary key, to protect against inference attacks."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc",
  "question": "Which one of the following is not part of the change management process?",
  "answer": "C",
  "explanation": "Configuration audit is part of the configuration management process rather than the change control process."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f",
  "question": "What transaction management principle ensures that two transactions do not interfere with each other as they operate on the same data?",
  "answer": "C",
  "explanation": "The isolation principle states that two transactions operating on the same data must be temporarily separated from each other such that one does not interfere with the other."
 },
 {
  "type": "QUESTION",
  "id": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42",
  "question": "Tom built a database table consisting of the names, telephone numbers, and customer IDs for his business. The table contains information on 30 customers. What is the degree of this table?",
  "answer": "B",
  "explanation": "The cardinality of a table refers to the number of rows in the table while the degree of a table is the number of columns."
 },
 {
  "type": "ANSWER",
  "id": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39-A",
  "QuestionId": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39",
  "choice": "A",
  "answer": "Initiation"
 },
 {
  "type": "ANSWER",
  "id": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39-B",
  "QuestionId": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39",
  "choice": "B",
  "answer": "Development/Acquisition"
 },
 {
  "type": "ANSWER",
  "id": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39-C",
  "QuestionId": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39",
  "choice": "C",
  "answer": "Implementation/Assessment"
 },
 {
  "type": "ANSWER",
  "id": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39-D",
  "QuestionId": "SDL.f74710ec-e2fb-4700-a394-0bcece067f39",
  "choice": "D",
  "answer": "Operation/Maintenance"
 },
 {
  "type": "ANSWER",
  "id": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8-A",
  "QuestionId": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8",
  "choice": "A",
  "answer": "Demand the source code"
 },
 {
  "type": "ANSWER",
  "id": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8-B",
  "QuestionId": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8",
  "choice": "B",
  "answer": "Hire the lead developer"
 },
 {
  "type": "ANSWER",
  "id": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8-C",
  "QuestionId": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8",
  "choice": "C",
  "answer": "Only buy from well-established vendors"
 },
 {
  "type": "ANSWER",
  "id": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8-D",
  "QuestionId": "SDL.6c3c0336-0996-4766-b74f-2a87cae9daf8",
  "choice": "D",
  "answer": "Use a software escrow"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f-A",
  "QuestionId": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f",
  "choice": "A",
  "answer": "SQL Injection"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f-B",
  "QuestionId": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f",
  "choice": "B",
  "answer": "Memory Leaks"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f-C",
  "QuestionId": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f",
  "choice": "C",
  "answer": "SYN Floods"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f-D",
  "QuestionId": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f",
  "choice": "D",
  "answer": "Cross Site Scripting"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f-E",
  "QuestionId": "SDL.a15bd4d6-af81-47b7-b62b-4f1e006cb17f",
  "choice": "E",
  "answer": "Heartbleed"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7-A",
  "QuestionId": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7",
  "choice": "A",
  "answer": "Top Down Approach"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7-B",
  "QuestionId": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7",
  "choice": "B",
  "answer": "Normalization"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7-C",
  "QuestionId": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7",
  "choice": "C",
  "answer": "Multiple Primary Keys"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7-D",
  "QuestionId": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7",
  "choice": "D",
  "answer": "Waterfall"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7-E",
  "QuestionId": "SDL.fbbcc230-cb9a-48d6-b347-510d0c3035d7",
  "choice": "E",
  "answer": "Baselines"
 },
 {
  "type": "ANSWER",
  "id": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b-A",
  "QuestionId": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b",
  "choice": "A",
  "answer": "DOM based XSS is stored in the web server and sent to a victim when visiting the web page"
 },
 {
  "type": "ANSWER",
  "id": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b-B",
  "QuestionId": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b",
  "choice": "B",
  "answer": "DOM based XSS will not be visible in the HTML source of the page"
 },
 {
  "type": "ANSWER",
  "id": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b-C",
  "QuestionId": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b",
  "choice": "C",
  "answer": "DOM based XSS rely upon tricking a user into clickingon a malicious link"
 },
 {
  "type": "ANSWER",
  "id": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b-D",
  "QuestionId": "SDL.13c68efb-0afc-46ed-8e39-17e70b46528b",
  "choice": "D",
  "answer": "DOM based XSS are server side flaw and stored and reflected are client side"
 },
 {
  "type": "ANSWER",
  "id": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a-A",
  "QuestionId": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a",
  "choice": "A",
  "answer": "Initiation"
 },
 {
  "type": "ANSWER",
  "id": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a-B",
  "QuestionId": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a",
  "choice": "B",
  "answer": "Development/Acquisition"
 },
 {
  "type": "ANSWER",
  "id": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a-C",
  "QuestionId": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a",
  "choice": "C",
  "answer": "Implement/Assessment"
 },
 {
  "type": "ANSWER",
  "id": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a-D",
  "QuestionId": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a",
  "choice": "D",
  "answer": "Operations & Maintenance"
 },
 {
  "type": "ANSWER",
  "id": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a-E",
  "QuestionId": "SDL.d0819d9f-58c2-4aca-b6fb-c4ef5775da6a",
  "choice": "E",
  "answer": "Disposal"
 },
 {
  "type": "ANSWER",
  "id": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc-A",
  "QuestionId": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc",
  "choice": "A",
  "answer": "Physical Addresses"
 },
 {
  "type": "ANSWER",
  "id": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc-B",
  "QuestionId": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc",
  "choice": "B",
  "answer": "Logical Addresses"
 },
 {
  "type": "ANSWER",
  "id": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc-C",
  "QuestionId": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc",
  "choice": "C",
  "answer": "Relative Addresses"
 },
 {
  "type": "ANSWER",
  "id": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc-D",
  "QuestionId": "SDL.c26b10e5-59cd-4aa3-b254-5c9e3e008ddc",
  "choice": "D",
  "answer": "Indirect Addresses"
 },
 {
  "type": "ANSWER",
  "id": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4-A",
  "QuestionId": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4",
  "choice": "A",
  "answer": "Sessions are tracked and managed in cookies"
 },
 {
  "type": "ANSWER",
  "id": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4-B",
  "QuestionId": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4",
  "choice": "B",
  "answer": "The server supports only HTTPS connections"
 },
 {
  "type": "ANSWER",
  "id": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4-C",
  "QuestionId": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4",
  "choice": "C",
  "answer": "The server does not run a client side firewall"
 },
 {
  "type": "ANSWER",
  "id": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4-D",
  "QuestionId": "SDL.0d74f6d9-ab88-4f95-9fa2-d9aa62b84db4",
  "choice": "D",
  "answer": "Sessions are managed via URL rewrites"
 },
 {
  "type": "ANSWER",
  "id": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de-A",
  "QuestionId": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de",
  "choice": "A",
  "answer": "Data Mining"
 },
 {
  "type": "ANSWER",
  "id": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de-B",
  "QuestionId": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de",
  "choice": "B",
  "answer": "DevOps"
 },
 {
  "type": "ANSWER",
  "id": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de-C",
  "QuestionId": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de",
  "choice": "C",
  "answer": "Computer Virus"
 },
 {
  "type": "ANSWER",
  "id": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de-D",
  "QuestionId": "SDL.47d92f97-ee2b-40e1-bcc5-e418b65543de",
  "choice": "D",
  "answer": "Covert Channel"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b-A",
  "QuestionId": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b",
  "choice": "A",
  "answer": "Reboot the system after deleting a folder containing a large number of files."
 },
 {
  "type": "ANSWER",
  "id": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b-B",
  "QuestionId": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b",
  "choice": "B",
  "answer": "Hold Option+Command while pressing delete on the MacOS system"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b-C",
  "QuestionId": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b",
  "choice": "C",
  "answer": "Hold the shift key while pressing delete on Windows"
 },
 {
  "type": "ANSWER",
  "id": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b-D",
  "QuestionId": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b",
  "choice": "D",
  "answer": "Zeroize RAM after a process finishes using it."
 },
 {
  "type": "ANSWER",
  "id": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b-E",
  "QuestionId": "SDL.a35aeae1-f2aa-433e-9b1c-7a613c31425b",
  "choice": "E",
  "answer": "Delete the file and then create a new empty file with the same name"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa-A",
  "QuestionId": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa",
  "choice": "A",
  "answer": "Turns high level source code into binary executable"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa-B",
  "QuestionId": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa",
  "choice": "B",
  "answer": "Executes code one line at a time"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa-C",
  "QuestionId": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa",
  "choice": "C",
  "answer": "Are in the form of instructions executed directly by the hardware"
 },
 {
  "type": "ANSWER",
  "id": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa-D",
  "QuestionId": "SDL.fff298bd-93f6-4eb0-91bb-bb289a0e0faa",
  "choice": "D",
  "answer": "Converts assembly language into machine language"
 },
 {
  "type": "ANSWER",
  "id": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d-A",
  "QuestionId": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d",
  "choice": "A",
  "answer": "Missing functionality may be identified"
 },
 {
  "type": "ANSWER",
  "id": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d-B",
  "QuestionId": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d",
  "choice": "B",
  "answer": "Prototypes can be reused to build the actual application"
 },
 {
  "type": "ANSWER",
  "id": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d-C",
  "QuestionId": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d",
  "choice": "C",
  "answer": "Requirements analysis is reduced"
 },
 {
  "type": "ANSWER",
  "id": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d-D",
  "QuestionId": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d",
  "choice": "D",
  "answer": "Defects can be identified earlier, reducing time and cost of development"
 },
 {
  "type": "ANSWER",
  "id": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d-E",
  "QuestionId": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d",
  "choice": "E",
  "answer": "User feedback is quicker, allowing necessary changes to be identified sooner"
 },
 {
  "type": "ANSWER",
  "id": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d-F",
  "QuestionId": "SDL.38faa4c5-d9d8-4874-bd9f-6f989b360c5d",
  "choice": "F",
  "answer": "Flexibility of development allows project to be easily expand beyond plans"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-A",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "A",
  "answer": "BFR (Bromine Flame Retardent)"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-B",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "B",
  "answer": "Carbon Dioxide"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-C",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "C",
  "answer": "FM-200"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-D",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "D",
  "answer": "Aero K"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-E",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "E",
  "answer": "Argonite"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-F",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "F",
  "answer": "FM-100"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-G",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "G",
  "answer": "FE-13"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-H",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "H",
  "answer": "HFC-32"
 },
 {
  "type": "ANSWER",
  "id": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f-I",
  "QuestionId": "SOP.f93d44ab-dd45-46c4-a108-2dc68d55339f",
  "choice": "I",
  "answer": "Inergen"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4-A",
  "QuestionId": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4",
  "choice": "A",
  "answer": "A named set of possible values for an attribute, all of the same type."
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4-B",
  "QuestionId": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4",
  "choice": "B",
  "answer": "All tuple in a relation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4-C",
  "QuestionId": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4",
  "choice": "C",
  "answer": "All the attributes of a relation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4-D",
  "QuestionId": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4",
  "choice": "D",
  "answer": "The cardinality of a realtion"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4-E",
  "QuestionId": "SDLC.6632505b-509f-4b08-8ac4-855f681464a4",
  "choice": "E",
  "answer": "The degree of attributes in a relation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063-A",
  "QuestionId": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063",
  "choice": "A",
  "answer": "Encrypt Sensitive Information"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063-B",
  "QuestionId": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063",
  "choice": "B",
  "answer": "Conduct Cross Site Scripting"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063-C",
  "QuestionId": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063",
  "choice": "C",
  "answer": "Apply Security Patches Promptly"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063-D",
  "QuestionId": "SDLC.b091a4cb-50c9-4822-8764-adac0f1b6063",
  "choice": "D",
  "answer": "Validate User Input"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b-A",
  "QuestionId": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b",
  "choice": "A",
  "answer": "Agile"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b-B",
  "QuestionId": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b",
  "choice": "B",
  "answer": "Spiral"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b-C",
  "QuestionId": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b",
  "choice": "C",
  "answer": "Waterfall"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b-D",
  "QuestionId": "SDLC.6653bd82-496f-47dd-9fe0-e604d66bb46b",
  "choice": "D",
  "answer": "DevOps"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e-A",
  "QuestionId": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e",
  "choice": "A",
  "answer": "Optimizing"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e-B",
  "QuestionId": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e",
  "choice": "B",
  "answer": "Defined"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e-C",
  "QuestionId": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e",
  "choice": "C",
  "answer": "Initial"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e-D",
  "QuestionId": "SDLC.5624c30b-3920-49e8-9446-265489d3e99e",
  "choice": "D",
  "answer": "Repeatable"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db-A",
  "QuestionId": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db",
  "choice": "A",
  "answer": "Change Management"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db-B",
  "QuestionId": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db",
  "choice": "B",
  "answer": "Iteration Management"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db-C",
  "QuestionId": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db",
  "choice": "C",
  "answer": "Request Management"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db-D",
  "QuestionId": "SDLC.2a834edc-72ae-466f-9576-3262b9f2f5db",
  "choice": "D",
  "answer": "Release Management"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505-A",
  "QuestionId": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505",
  "choice": "A",
  "answer": "input validation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505-B",
  "QuestionId": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505",
  "choice": "B",
  "answer": "query parameterization"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505-C",
  "QuestionId": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505",
  "choice": "C",
  "answer": "antivirus software"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505-D",
  "QuestionId": "SDLC.24f41563-7e96-434d-bf89-d1df2441e505",
  "choice": "D",
  "answer": "vulnerability scanning"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8-A",
  "QuestionId": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8",
  "choice": "A",
  "answer": "exclamation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8-B",
  "QuestionId": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8",
  "choice": "B",
  "answer": "quote"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8-C",
  "QuestionId": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8",
  "choice": "C",
  "answer": "asterix"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8-D",
  "QuestionId": "SDLC.4d9232bd-daf5-47ba-b044-0e4e7b505ad8",
  "choice": "D",
  "answer": "doublequotes"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185-A",
  "QuestionId": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185",
  "choice": "A",
  "answer": "directory traversal"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185-B",
  "QuestionId": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185",
  "choice": "B",
  "answer": "cross-site scriptiong"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185-C",
  "QuestionId": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185",
  "choice": "C",
  "answer": "buffer overflow"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185-D",
  "QuestionId": "SDLC.554cd8e2-3b2c-4a71-a645-31c576f0b185",
  "choice": "D",
  "answer": "SQL injection"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed-A",
  "QuestionId": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed",
  "choice": "A",
  "answer": "resale of legitmate extensions"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed-B",
  "QuestionId": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed",
  "choice": "B",
  "answer": "sandbox execution"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed-C",
  "QuestionId": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed",
  "choice": "C",
  "answer": "overly broad permissions"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed-D",
  "QuestionId": "SDLC.385df37c-085f-49a7-ae08-9854d8c5d0ed",
  "choice": "D",
  "answer": "malicious author"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2-A",
  "QuestionId": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2",
  "choice": "A",
  "answer": "patent"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2-B",
  "QuestionId": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2",
  "choice": "B",
  "answer": "digital certificate"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2-C",
  "QuestionId": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2",
  "choice": "C",
  "answer": "software license"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2-D",
  "QuestionId": "SDLC.c5cc83af-21a6-4887-968d-2001eac8faf2",
  "choice": "D",
  "answer": "shared secret"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e-A",
  "QuestionId": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e",
  "choice": "A",
  "answer": "Unit Testing"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e-B",
  "QuestionId": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e",
  "choice": "B",
  "answer": "UAT"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e-C",
  "QuestionId": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e",
  "choice": "C",
  "answer": "Load Testing"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e-D",
  "QuestionId": "SDLC.e89f6f64-b183-4690-a800-460cf82b258e",
  "choice": "D",
  "answer": "Integration Testing"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52-A",
  "QuestionId": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52",
  "choice": "A",
  "answer": "Release Control"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52-B",
  "QuestionId": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52",
  "choice": "B",
  "answer": "Configuration Control"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52-C",
  "QuestionId": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52",
  "choice": "C",
  "answer": "Request Control"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52-D",
  "QuestionId": "SDLC.041c2f7d-78bf-4aff-bc13-c28044ca3b52",
  "choice": "D",
  "answer": "Change Audit"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b-A",
  "QuestionId": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b",
  "choice": "A",
  "answer": "Fail Open"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b-B",
  "QuestionId": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b",
  "choice": "B",
  "answer": "Fail Mitigation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b-C",
  "QuestionId": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b",
  "choice": "C",
  "answer": "Fail Secure"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b-D",
  "QuestionId": "SDLC.b9718aab-e6b0-4ed0-af48-31393104d65b",
  "choice": "D",
  "answer": "Fail Clear"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655-A",
  "QuestionId": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655",
  "choice": "A",
  "answer": "Boyce Codd"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655-B",
  "QuestionId": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655",
  "choice": "B",
  "answer": "Waterfall"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655-C",
  "QuestionId": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655",
  "choice": "C",
  "answer": "Spiral"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655-D",
  "QuestionId": "SDLC.b12b3957-0f14-4db8-948f-788d52e18655",
  "choice": "D",
  "answer": "Agile"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702-A",
  "QuestionId": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702",
  "choice": "A",
  "answer": "Inference"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702-B",
  "QuestionId": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702",
  "choice": "B",
  "answer": "Contamination"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702-C",
  "QuestionId": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702",
  "choice": "C",
  "answer": "Polyinstantiation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702-D",
  "QuestionId": "SDLC.7f151b2f-6f5b-479d-9d3f-06530922a702",
  "choice": "D",
  "answer": "Aggregration"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3-A",
  "QuestionId": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3",
  "choice": "A",
  "answer": "Inference"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3-B",
  "QuestionId": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3",
  "choice": "B",
  "answer": "Manipulation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3-C",
  "QuestionId": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3",
  "choice": "C",
  "answer": "Polyinstantiation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3-D",
  "QuestionId": "SDLC.2be3aad3-a22e-4a39-9fc4-9a2160e131c3",
  "choice": "D",
  "answer": "Aggegration"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8-A",
  "QuestionId": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8",
  "choice": "A",
  "answer": "Satisfy the customer through early and continuous delivery"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8-B",
  "QuestionId": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8",
  "choice": "B",
  "answer": "Business people and developers work together"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8-C",
  "QuestionId": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8",
  "choice": "C",
  "answer": "Pay continuous attention to technical excellence"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8-D",
  "QuestionId": "SDLC.d7f29669-e2f4-4ff9-bec5-b0db5aa012f8",
  "choice": "D",
  "answer": "Prioritize security over other requirements"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78-A",
  "QuestionId": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78",
  "choice": "A",
  "answer": "A series of weighted layered computations"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78-B",
  "QuestionId": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78",
  "choice": "B",
  "answer": "Combined input from a number of human experts weighted according to past performance"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78-C",
  "QuestionId": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78",
  "choice": "C",
  "answer": "A series of if/then rules codified in a knowledge base"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78-D",
  "QuestionId": "SDLC.a72b599e-0e60-4a7d-bef5-6df1ad474b78",
  "choice": "D",
  "answer": "A biological decision-making process that simulates the reasoning process used by the human mind"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247-A",
  "QuestionId": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247",
  "choice": "A",
  "answer": "initial"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247-B",
  "QuestionId": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247",
  "choice": "B",
  "answer": "repeatable"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247-C",
  "QuestionId": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247",
  "choice": "C",
  "answer": "defined"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247-D",
  "QuestionId": "SDLC.1e7aded0-1c97-4149-8840-bd40d29ba247",
  "choice": "D",
  "answer": "managed"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d-A",
  "QuestionId": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d",
  "choice": "A",
  "answer": "SDLC"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d-B",
  "QuestionId": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d",
  "choice": "B",
  "answer": "ODBC"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d-C",
  "QuestionId": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d",
  "choice": "C",
  "answer": "DSS"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d-D",
  "QuestionId": "SDLC.ab759ad0-8e73-40b8-bde4-9bfde359068d",
  "choice": "D",
  "answer": "Abstraction"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8-A",
  "QuestionId": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8",
  "choice": "A",
  "answer": "Static Testing"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8-B",
  "QuestionId": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8",
  "choice": "B",
  "answer": "Dynamic Testing"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8-C",
  "QuestionId": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8",
  "choice": "C",
  "answer": "Cross Site Scripting"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8-D",
  "QuestionId": "SDLC.d3c18c67-87f0-4ae2-8b64-2b87c7a6f1a8",
  "choice": "D",
  "answer": "Black Box Testing"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653-A",
  "QuestionId": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653",
  "choice": "A",
  "answer": "Gantt"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653-B",
  "QuestionId": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653",
  "choice": "B",
  "answer": "Venn"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653-C",
  "QuestionId": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653",
  "choice": "C",
  "answer": "Bar"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653-D",
  "QuestionId": "SDLC.58c968d3-9cbd-44f3-ae30-4b9f3a19b653",
  "choice": "D",
  "answer": "PERT"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69-A",
  "QuestionId": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69",
  "choice": "A",
  "answer": "Aggregation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69-B",
  "QuestionId": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69",
  "choice": "B",
  "answer": "Inference"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69-C",
  "QuestionId": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69",
  "choice": "C",
  "answer": "Contamination"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69-D",
  "QuestionId": "SDLC.6353856b-0129-4fa6-a425-f442013f3d69",
  "choice": "D",
  "answer": "Polyinstantiation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7857613f-efd6-471a-a1b7-022defccd868-A",
  "QuestionId": "SDLC.7857613f-efd6-471a-a1b7-022defccd868",
  "choice": "A",
  "answer": "Polyinstantiation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7857613f-efd6-471a-a1b7-022defccd868-B",
  "QuestionId": "SDLC.7857613f-efd6-471a-a1b7-022defccd868",
  "choice": "B",
  "answer": "Cell supresssion"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7857613f-efd6-471a-a1b7-022defccd868-C",
  "QuestionId": "SDLC.7857613f-efd6-471a-a1b7-022defccd868",
  "choice": "C",
  "answer": "Aggregation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.7857613f-efd6-471a-a1b7-022defccd868-D",
  "QuestionId": "SDLC.7857613f-efd6-471a-a1b7-022defccd868",
  "choice": "D",
  "answer": "Views"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc-A",
  "QuestionId": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc",
  "choice": "A",
  "answer": "Request Control"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc-B",
  "QuestionId": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc",
  "choice": "B",
  "answer": "Release Control"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc-C",
  "QuestionId": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc",
  "choice": "C",
  "answer": "Configuration Audit"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc-D",
  "QuestionId": "SDLC.f2140e07-3f8f-423b-8621-b49663d61efc",
  "choice": "D",
  "answer": "Change control"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f-A",
  "QuestionId": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f",
  "choice": "A",
  "answer": "Atomicity"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f-B",
  "QuestionId": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f",
  "choice": "B",
  "answer": "Consistency"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f-C",
  "QuestionId": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f",
  "choice": "C",
  "answer": "Isolation"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f-D",
  "QuestionId": "SDLC.1810d72a-0316-4070-b379-05c5d40c212f",
  "choice": "D",
  "answer": "Durability"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42-A",
  "QuestionId": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42",
  "choice": "A",
  "answer": "2"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42-B",
  "QuestionId": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42",
  "choice": "B",
  "answer": "3"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42-C",
  "QuestionId": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42",
  "choice": "C",
  "answer": "Thirty"
 },
 {
  "type": "ANSWER",
  "id": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42-D",
  "QuestionId": "SDLC.d59e90a0-89db-4ebb-ac8d-c6b3e507bd42",
  "choice": "D",
  "answer": "Undefined"
 }
]