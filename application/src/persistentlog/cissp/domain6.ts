export const Log = [
 {
  "type": "QUESTION",
  "id": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13",
  "question": "Which of the following is key shortcoming of behaviour based (anomoly based) intrusion detection systems?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "question": "You are developing an attack tree for a web application and as part of the process you are attempting to anticipate your potential attackers. Which of the following will you need to identify in order to characterize a likely adversary?",
  "answer": "B,D,F",
  "explanation": "This is the basis of the MOM acronym. Means, opportunity and motive."
 },
 {
  "type": "QUESTION",
  "id": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155",
  "question": "Which is not true of penetration testing?",
  "answer": "C,E",
  "explanation": "A is important so you dont break things\nB is important so you can mitigate known weaknesses and mitigate before the penetration test\nFor E there are always limits which need to be discussed before attempting a penetration test"
 },
 {
  "type": "QUESTION",
  "id": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d",
  "question": "The \"Rules of Engagement\" are a critical component of any penetration test. Which is not something that needs to be agreed upon before  a pentest begins",
  "answer": "B",
  "explanation": "The tools dont really make any difference to the customer"
 },
 {
  "type": "QUESTION",
  "id": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9",
  "question": "In software development what is one of the primary differences between white-box and black-box testing?",
  "answer": "A",
  "explanation": "Black box testing is more about the functionality\nWhite box testing is more about issues with the code base."
 },
 {
  "type": "QUESTION",
  "id": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74",
  "question": "What type of detected incident allows the most time for an investigation?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de",
  "question": "As part of evidence collection process an investigator opens a terminal  on the suspect machine and issues commands to display the current network settings, ARP cache, resolver cache and routing table. He uses his cell phone to take pictures. Which of the following is true regarding his actions?",
  "answer": "B",
  "explanation": "Dont rely on binaries on the system. The idea is to bring your own binaries on a read only media"
 },
 {
  "type": "QUESTION",
  "id": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5",
  "question": "A developer in your company has written a script that changes a numeric value before it is read and used by another program. After the value is read, the developers script changes the value back to its original value. What is this an example of?",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410",
  "question": "A ???? is an externally occurring force that jeopardizes the security of your systems.",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762",
  "question": "In a ????  penetration test, the attacker has no prior knowledge of the environment.",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.17fe3314-3394-4ce2-ac19-ffbe4b08f7dd",
  "question": "Safe mode scans provide the most comprehensive look at system security.",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808",
  "question": "What is the first step of a Fagan inspection?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d",
  "question": "What type of fuzz testing captures real software input and modifies it?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f",
  "question": "What disaster recovery metric provides the targeted amount of time to restore a service after a failure?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1",
  "question": "What type of backup includes only those files that have changed since the most recent full or incremental backup?",
  "answer": "D",
  "explanation": "Incremental backups only capture changed files since last incremental backup.\nDifferential backups capture changes sinces last full backup."
 },
 {
  "type": "QUESTION",
  "id": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae",
  "question": "Which one of the following disaster recovery tests involves the actual activation of the DR site?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "ANSWER",
  "id": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13-A",
  "QuestionId": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13",
  "choice": "A",
  "answer": "requires regular signature database updates"
 },
 {
  "type": "ANSWER",
  "id": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13-B",
  "QuestionId": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13",
  "choice": "B",
  "answer": "High number of false positives"
 },
 {
  "type": "ANSWER",
  "id": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13-C",
  "QuestionId": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13",
  "choice": "C",
  "answer": "Limited to detecting signatures of known attacks"
 },
 {
  "type": "ANSWER",
  "id": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13-D",
  "QuestionId": "SAT.a78fa02d-6d39-4893-9db6-38ebfa7e0f13",
  "choice": "D",
  "answer": "Changes in network traffic pattern go unnoticed"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf-A",
  "QuestionId": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "choice": "A",
  "answer": "Ease of vulnerability discovery"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf-B",
  "QuestionId": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "choice": "B",
  "answer": "Attacker Motive"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf-C",
  "QuestionId": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "choice": "C",
  "answer": "Damage Potential"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf-D",
  "QuestionId": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "choice": "D",
  "answer": "Opportunity"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf-E",
  "QuestionId": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "choice": "E",
  "answer": "Trust Boundaries"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf-F",
  "QuestionId": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "choice": "F",
  "answer": "Means"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf-G",
  "QuestionId": "SAT.4ad1a539-3c12-4891-a3a9-6caa07ef97bf",
  "choice": "G",
  "answer": "Exploitability"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155-A",
  "QuestionId": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155",
  "choice": "A",
  "answer": "Define clearly established boundaries before the test begins"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155-B",
  "QuestionId": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155",
  "choice": "B",
  "answer": "Typically performed after a vulnerability assessment is completed"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155-C",
  "QuestionId": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155",
  "choice": "C",
  "answer": "Utilize only passive tools combined with social engineering"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155-D",
  "QuestionId": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155",
  "choice": "D",
  "answer": "Define success criteria based on test objectives"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155-E",
  "QuestionId": "SAT.4ce34ae8-70c4-4eb9-9473-1cdd886b8155",
  "choice": "E",
  "answer": "Rules of engagement are not defined in a black box test"
 },
 {
  "type": "ANSWER",
  "id": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d-A",
  "QuestionId": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d",
  "choice": "A",
  "answer": "The scope and range of the penetration test"
 },
 {
  "type": "ANSWER",
  "id": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d-B",
  "QuestionId": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d",
  "choice": "B",
  "answer": "The tools that will be used"
 },
 {
  "type": "ANSWER",
  "id": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d-C",
  "QuestionId": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d",
  "choice": "C",
  "answer": "The time frame/duration of the pen test"
 },
 {
  "type": "ANSWER",
  "id": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d-D",
  "QuestionId": "SAT.deca50b7-b067-45c5-a42f-16bda760c66d",
  "choice": "D",
  "answer": "Limits of liability of the pen testers"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9-A",
  "QuestionId": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9",
  "choice": "A",
  "answer": "White box testing gives testers access to source code"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9-B",
  "QuestionId": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9",
  "choice": "B",
  "answer": "Black box testers fully deconstruct the app to identify vulnerabilities"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9-C",
  "QuestionId": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9",
  "choice": "C",
  "answer": "White box testers are limited to pre-defined use cases"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9-D",
  "QuestionId": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9",
  "choice": "D",
  "answer": "Black box testers are more thorough and proficient"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9-E",
  "QuestionId": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9",
  "choice": "E",
  "answer": "White box testing is done by developers"
 },
 {
  "type": "ANSWER",
  "id": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9-F",
  "QuestionId": "SAT.f1bb7f5c-d585-43eb-a9b4-9d9c2d3156c9",
  "choice": "F",
  "answer": "Black box testing includes the line of business in the evaluation process"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74-A",
  "QuestionId": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74",
  "choice": "A",
  "answer": "Compromise"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74-B",
  "QuestionId": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74",
  "choice": "B",
  "answer": "Denial Of Service"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74-C",
  "QuestionId": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74",
  "choice": "C",
  "answer": "Malicious Code"
 },
 {
  "type": "ANSWER",
  "id": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74-D",
  "QuestionId": "SAT.ee13bc11-df97-4f1e-9697-df3630686b74",
  "choice": "D",
  "answer": "Scanning"
 },
 {
  "type": "ANSWER",
  "id": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de-A",
  "QuestionId": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de",
  "choice": "A",
  "answer": "By viewing the data, disclosure rules have been violated."
 },
 {
  "type": "ANSWER",
  "id": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de-B",
  "QuestionId": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de",
  "choice": "B",
  "answer": "Running command has altered the system"
 },
 {
  "type": "ANSWER",
  "id": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de-C",
  "QuestionId": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de",
  "choice": "C",
  "answer": "Chain of custody has been broken"
 },
 {
  "type": "ANSWER",
  "id": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de-D",
  "QuestionId": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de",
  "choice": "D",
  "answer": "Photos are not considered legally authentic."
 },
 {
  "type": "ANSWER",
  "id": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de-E",
  "QuestionId": "SAT.be55d545-d60c-4760-b0ec-745a0b7854de",
  "choice": "E",
  "answer": "Commands should only be executed through remote SSH."
 },
 {
  "type": "ANSWER",
  "id": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5-A",
  "QuestionId": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5",
  "choice": "A",
  "answer": "Hacking"
 },
 {
  "type": "ANSWER",
  "id": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5-B",
  "QuestionId": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5",
  "choice": "B",
  "answer": "Salalmi Slicing"
 },
 {
  "type": "ANSWER",
  "id": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5-C",
  "QuestionId": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5",
  "choice": "C",
  "answer": "Time of Check/Time of Use"
 },
 {
  "type": "ANSWER",
  "id": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5-D",
  "QuestionId": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5",
  "choice": "D",
  "answer": "Poly Instantiation"
 },
 {
  "type": "ANSWER",
  "id": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5-E",
  "QuestionId": "SOP.32a11d5b-acd7-4b24-90cb-3b64cfd1b3b5",
  "choice": "E",
  "answer": "Data Diddling"
 },
 {
  "type": "ANSWER",
  "id": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410-A",
  "QuestionId": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410",
  "choice": "A",
  "answer": "Vulnerability"
 },
 {
  "type": "ANSWER",
  "id": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410-B",
  "QuestionId": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410",
  "choice": "B",
  "answer": "Countermeasure"
 },
 {
  "type": "ANSWER",
  "id": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410-C",
  "QuestionId": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410",
  "choice": "C",
  "answer": "Risk"
 },
 {
  "type": "ANSWER",
  "id": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410-D",
  "QuestionId": "SAT.a00e916b-7537-4873-a83c-3fa6918a2410",
  "choice": "D",
  "answer": "Threat"
 },
 {
  "type": "ANSWER",
  "id": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762-A",
  "QuestionId": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762",
  "choice": "A",
  "answer": "rainbox box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762-B",
  "QuestionId": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762",
  "choice": "B",
  "answer": "black box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762-C",
  "QuestionId": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762",
  "choice": "C",
  "answer": "white box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762-D",
  "QuestionId": "SAT.2ff07fc2-a832-4426-b6c8-c39e14016762",
  "choice": "D",
  "answer": "grey box"
 },
 {
  "type": "ANSWER",
  "id": "SAT.17fe3314-3394-4ce2-ac19-ffbe4b08f7dd-A",
  "QuestionId": "SAT.17fe3314-3394-4ce2-ac19-ffbe4b08f7dd",
  "choice": "A",
  "answer": "TRUE"
 },
 {
  "type": "ANSWER",
  "id": "SAT.17fe3314-3394-4ce2-ac19-ffbe4b08f7dd-B",
  "QuestionId": "SAT.17fe3314-3394-4ce2-ac19-ffbe4b08f7dd",
  "choice": "B",
  "answer": "FALSE"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808-A",
  "QuestionId": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808",
  "choice": "A",
  "answer": "Planning"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808-B",
  "QuestionId": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808",
  "choice": "B",
  "answer": "Meeting"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808-C",
  "QuestionId": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808",
  "choice": "C",
  "answer": "Overview"
 },
 {
  "type": "ANSWER",
  "id": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808-D",
  "QuestionId": "SAT.6614d4cf-3c89-4077-a5e3-d58bd3b20808",
  "choice": "D",
  "answer": "Preparation"
 },
 {
  "type": "ANSWER",
  "id": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d-A",
  "QuestionId": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d",
  "choice": "A",
  "answer": "Mutation Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d-B",
  "QuestionId": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d",
  "choice": "B",
  "answer": "Switch Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d-C",
  "QuestionId": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d",
  "choice": "C",
  "answer": "Generation Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d-D",
  "QuestionId": "SAT.702f29ba-d39d-4d1c-aed7-b20f1799ba0d",
  "choice": "D",
  "answer": "Twist Fuzzing"
 },
 {
  "type": "ANSWER",
  "id": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f-A",
  "QuestionId": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f",
  "choice": "A",
  "answer": "RPO"
 },
 {
  "type": "ANSWER",
  "id": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f-B",
  "QuestionId": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f",
  "choice": "B",
  "answer": "MTO"
 },
 {
  "type": "ANSWER",
  "id": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f-C",
  "QuestionId": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f",
  "choice": "C",
  "answer": "RTO"
 },
 {
  "type": "ANSWER",
  "id": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f-D",
  "QuestionId": "SAT.d1356dd4-9a5c-4fd7-8963-7542ff54f86f",
  "choice": "D",
  "answer": "TLS"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1-A",
  "QuestionId": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1",
  "choice": "A",
  "answer": "differential"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1-B",
  "QuestionId": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1",
  "choice": "B",
  "answer": "partial"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1-C",
  "QuestionId": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1",
  "choice": "C",
  "answer": "full"
 },
 {
  "type": "ANSWER",
  "id": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1-D",
  "QuestionId": "SAT.4aa036ef-245b-4491-ac42-175c45dcedb1",
  "choice": "D",
  "answer": "incremental"
 },
 {
  "type": "ANSWER",
  "id": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae-A",
  "QuestionId": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae",
  "choice": "A",
  "answer": "read-through"
 },
 {
  "type": "ANSWER",
  "id": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae-B",
  "QuestionId": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae",
  "choice": "B",
  "answer": "parallel test"
 },
 {
  "type": "ANSWER",
  "id": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae-C",
  "QuestionId": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae",
  "choice": "C",
  "answer": "simulation"
 },
 {
  "type": "ANSWER",
  "id": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae-D",
  "QuestionId": "SAT.75ff3172-603b-48dc-9c93-0ebbf67365ae",
  "choice": "D",
  "answer": "walk through"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267-A",
  "QuestionId": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267",
  "choice": "A",
  "answer": "CRIs"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267-B",
  "QuestionId": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267",
  "choice": "B",
  "answer": "KRIs"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267-C",
  "QuestionId": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267",
  "choice": "C",
  "answer": "KPIs"
 },
 {
  "type": "ANSWER",
  "id": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267-D",
  "QuestionId": "SAE.4ea5257d-ba1f-4b27-862c-cd009a6cb267",
  "choice": "D",
  "answer": "CPIs"
 },
 {
  "type": "ANSWER",
  "id": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf-A",
  "QuestionId": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf",
  "choice": "A",
  "answer": "Compensating"
 },
 {
  "type": "ANSWER",
  "id": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf-B",
  "QuestionId": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf",
  "choice": "B",
  "answer": "Technical"
 },
 {
  "type": "ANSWER",
  "id": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf-C",
  "QuestionId": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf",
  "choice": "C",
  "answer": "Administrative"
 },
 {
  "type": "ANSWER",
  "id": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf-D",
  "QuestionId": "SAE.144a153c-a9d8-44c7-b359-9eeff23567cf",
  "choice": "D",
  "answer": "Logical"
 }
]