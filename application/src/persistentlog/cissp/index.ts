
import {Log as domain1} from './domain1'
import {Log as domain2} from './domain2'
import {Log as domain3} from './domain3'
import {Log as domain4} from './domain4'
import {Log as domain5} from './domain5'
import {Log as domain6} from './domain6'
import {Log as domain7} from './domain7'
import {Log as domain8} from './domain8'
import {Log as ethics} from './ethics'
import {Log as testbank} from './testbank'


export const Log= [
    ...domain1,
    ...domain2,
    ...domain3,
    ...domain4,
    ...domain5,
    ...domain6,
    ...domain7,
    ...domain8,
    ...ethics,
    ...testbank
]