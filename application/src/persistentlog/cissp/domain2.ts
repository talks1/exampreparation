export const Log = [
 {
  "type": "QUESTION",
  "id": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379",
  "question": "ISO 27018:2019 provides guidance for cloud service providers on handling PII stored in their cloud. Which of the following is not a key principle definedin ISO 27018?",
  "answer": "C",
  "explanation": "C is to detailed to be part of this specification"
 },
 {
  "type": "QUESTION",
  "id": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe",
  "question": "Which of the following is considered the most common form of fault-tolerant RAID technology?",
  "answer": "B",
  "explanation": "3 disks striping and parity survives 1 disk failure"
 },
 {
  "type": "QUESTION",
  "id": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24",
  "question": "Which of the following is a responsibility of an information owner",
  "answer": "B",
  "explanation": "Custodian does nightly backups and labels information\nPolicy more likely to be defined at the enterprise level"
 },
 {
  "type": "QUESTION",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "question": "PCI DSS is a mandated information security standard that applies to organizations that accept credit cards.  The security requirements for compliance with PCI DSS allow merchants to only store and retain certain types of cardholder information.  Which are allowed to be stored by a merchant? (Choose 3)",
  "answer": "C,F,G",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571",
  "question": "Which one of the most following is the MOST important security consideration when selecting a new computer facility?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20",
  "question": "Your fire system is being upgraded and you are considering options.  One of the requirements is  notification of the local fire department when there is an event. Which type of system will provide this functionality?",
  "answer": "C",
  "explanation": "Protected Premises Fire Alaram System is a local only solution\nCentral Station Fire Alarm System - alarm is monitoring company \nAuxiliary Fire Alarm System - alarm is local but also the fire department is notified"
 },
 {
  "type": "QUESTION",
  "id": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0",
  "question": "Which one of the following is not a commonly used business classification level?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad",
  "question": "Which one of the following is not one of the GAPP Privacy Principles",
  "answer": "D",
  "explanation": "Management - policy and procedures in place to protect privacy\nNotice - notify subject their information is being recorded\nChoice - subjects should be given choices around the data that is recorded and stored\nCollection - information should only be used for the purpose it is intended\nUse, Retentation and Disposal - \nAccess - subjects should be able to review and update\nDisclosure - only share information if that sharing is consistency and subject has agreed\nSecurity - must secure private information\nData Quality - reasonable steps to ensure private information is update to date\nMonitoring - monitoring must be in place"
 },
 {
  "type": "QUESTION",
  "id": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62",
  "question": "What U.S. federal government agency publishes security standards that are widely used throughout the government and private industry?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f",
  "question": "What letter is used to describe the file permissions given to all users on a Linux system?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5",
  "question": "What is the formula used to compute the ALE?",
  "answer": "A",
  "explanation": "SLE is AV x EF and ALE is SLE x ARO"
 },
 {
  "type": "QUESTION",
  "id": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0",
  "question": "What is the first step of the business impact assessment process?",
  "answer": "A",
  "explanation": "The priorities for the business must first be determined to order the risks subsequently identified"
 },
 {
  "type": "QUESTION",
  "id": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8",
  "question": "What kind of recovery facility enables an organization to resume operations as quickly as possible, if not immediately, upon failure of the primary facility?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701",
  "question": "You have been given a hard drive that spins at 15000RPM. You task is to erase the data such that the erase data cannot be recovered using readily available \"keyboard recovery\" tool.  At a minimum what must you do.",
  "answer": "C",
  "explanation": "Purging is a more itense operation than clearing.\nClearing removes data in free space , swap space and any in use space."
 },
 {
  "type": "ANSWER",
  "id": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379-A",
  "QuestionId": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379",
  "choice": "A",
  "answer": "CSPs require customer consent to to use PII"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379-B",
  "QuestionId": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379",
  "choice": "B",
  "answer": "Customers maintain control over how information is used"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379-C",
  "QuestionId": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379",
  "choice": "C",
  "answer": "Maintain encrypted backups of customer data"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379-D",
  "QuestionId": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379",
  "choice": "D",
  "answer": "Transparency regarding where data resides"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379-E",
  "QuestionId": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379",
  "choice": "E",
  "answer": "Communication in event of breach"
 },
 {
  "type": "ANSWER",
  "id": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379-F",
  "QuestionId": "SAE.7b265a2b-b2cf-4308-88d3-d68f8f112379",
  "choice": "F",
  "answer": "Independent annual audit"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe-A",
  "QuestionId": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe",
  "choice": "A",
  "answer": "RAID 0"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe-B",
  "QuestionId": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe",
  "choice": "B",
  "answer": "RAID 5"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe-C",
  "QuestionId": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe",
  "choice": "C",
  "answer": "RAID 6"
 },
 {
  "type": "ANSWER",
  "id": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe-D",
  "QuestionId": "SOP.3fc2d7ce-6477-4626-b960-70abe0d13bfe",
  "choice": "D",
  "answer": "RAID 0+1"
 },
 {
  "type": "ANSWER",
  "id": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24-A",
  "QuestionId": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24",
  "choice": "A",
  "answer": "Perform Regular Backups of Data"
 },
 {
  "type": "ANSWER",
  "id": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24-B",
  "QuestionId": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24",
  "choice": "B",
  "answer": "Identify classification level of data"
 },
 {
  "type": "ANSWER",
  "id": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24-C",
  "QuestionId": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24",
  "choice": "C",
  "answer": "Develop an information classification policy"
 },
 {
  "type": "ANSWER",
  "id": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24-D",
  "QuestionId": "ASE.15c7653f-b8db-45a8-9b2e-c2baf1566d24",
  "choice": "D",
  "answer": "Labelling information with its classification level"
 },
 {
  "type": "ANSWER",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d-A",
  "QuestionId": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "choice": "A",
  "answer": "Card Holders PIN"
 },
 {
  "type": "ANSWER",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d-B",
  "QuestionId": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "choice": "B",
  "answer": "Card Holders account number in plain text"
 },
 {
  "type": "ANSWER",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d-C",
  "QuestionId": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "choice": "C",
  "answer": "Card Expiration Date"
 },
 {
  "type": "ANSWER",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d-D",
  "QuestionId": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "choice": "D",
  "answer": "Full Magmentic Step Data"
 },
 {
  "type": "ANSWER",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d-E",
  "QuestionId": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "choice": "E",
  "answer": "CAV2/CVS2/CVV2/CID"
 },
 {
  "type": "ANSWER",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d-F",
  "QuestionId": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "choice": "F",
  "answer": "Card Holders Name"
 },
 {
  "type": "ANSWER",
  "id": "ASE.50517f05-c6f7-414e-95e0-043619a9889d-G",
  "QuestionId": "ASE.50517f05-c6f7-414e-95e0-043619a9889d",
  "choice": "G",
  "answer": "Card Holders Account encrypted"
 },
 {
  "type": "ANSWER",
  "id": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571-A",
  "QuestionId": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571",
  "choice": "A",
  "answer": "Local Law enforment response times"
 },
 {
  "type": "ANSWER",
  "id": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571-B",
  "QuestionId": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571",
  "choice": "B",
  "answer": "Adjacent to competitor facilities"
 },
 {
  "type": "ANSWER",
  "id": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571-C",
  "QuestionId": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571",
  "choice": "C",
  "answer": "Aircraft Flight Paths"
 },
 {
  "type": "ANSWER",
  "id": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571-D",
  "QuestionId": "ASE.6bb00c0c-ead2-4f03-b58e-ed386609b571",
  "choice": "D",
  "answer": "Utility Infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20-A",
  "QuestionId": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20",
  "choice": "A",
  "answer": "Protected Premises Fire Alaram System"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20-B",
  "QuestionId": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20",
  "choice": "B",
  "answer": "Central Station Fire Alarm System"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20-C",
  "QuestionId": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20",
  "choice": "C",
  "answer": "Auxilliary Fire Alarm System"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20-D",
  "QuestionId": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20",
  "choice": "D",
  "answer": "Public Fire Alarm System"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20-E",
  "QuestionId": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20",
  "choice": "E",
  "answer": "Municipal Fire Alarm System"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20-F",
  "QuestionId": "ASE.e236b52c-28e0-4763-a96e-0d92cdb4fe20",
  "choice": "F",
  "answer": "Combination Fire Alarm System"
 },
 {
  "type": "ANSWER",
  "id": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0-A",
  "QuestionId": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0",
  "choice": "A",
  "answer": "Top Secret"
 },
 {
  "type": "ANSWER",
  "id": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0-B",
  "QuestionId": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0",
  "choice": "B",
  "answer": "Sensitive"
 },
 {
  "type": "ANSWER",
  "id": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0-C",
  "QuestionId": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0",
  "choice": "C",
  "answer": "Internal"
 },
 {
  "type": "ANSWER",
  "id": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0-D",
  "QuestionId": "ASE.874a3a76-32fa-4e03-b956-2003d16429d0",
  "choice": "D",
  "answer": "Highly Sensitive"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad-A",
  "QuestionId": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad",
  "choice": "A",
  "answer": "Notice"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad-B",
  "QuestionId": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad",
  "choice": "B",
  "answer": "Management"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad-C",
  "QuestionId": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad",
  "choice": "C",
  "answer": "Quality"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad-D",
  "QuestionId": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad",
  "choice": "D",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad-E",
  "QuestionId": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad",
  "choice": "E",
  "answer": "Monitoring"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad-F",
  "QuestionId": "ASE.a5f01d9e-4206-4a7f-83b3-d5e862b84bad",
  "choice": "F",
  "answer": "Choice"
 },
 {
  "type": "ANSWER",
  "id": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62-A",
  "QuestionId": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62",
  "choice": "A",
  "answer": "FCC"
 },
 {
  "type": "ANSWER",
  "id": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62-B",
  "QuestionId": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62",
  "choice": "B",
  "answer": "NIST"
 },
 {
  "type": "ANSWER",
  "id": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62-C",
  "QuestionId": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62",
  "choice": "C",
  "answer": "CIA"
 },
 {
  "type": "ANSWER",
  "id": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62-D",
  "QuestionId": "ASE.4d27a5e7-3c0b-4c5c-82e0-412f10087f62",
  "choice": "D",
  "answer": "FTC"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f-A",
  "QuestionId": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f",
  "choice": "A",
  "answer": "u"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f-B",
  "QuestionId": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f",
  "choice": "B",
  "answer": "o"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f-C",
  "QuestionId": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f",
  "choice": "C",
  "answer": "g"
 },
 {
  "type": "ANSWER",
  "id": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f-D",
  "QuestionId": "ASE.e39a7d27-ebd5-4d8f-b8c6-d7d840001f8f",
  "choice": "D",
  "answer": "r"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5-A",
  "QuestionId": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5",
  "choice": "A",
  "answer": "ALE = AV x EF x ARO"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5-B",
  "QuestionId": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5",
  "choice": "B",
  "answer": "ALE = ARO x EF"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5-C",
  "QuestionId": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5",
  "choice": "C",
  "answer": "ALE = AV x ARO"
 },
 {
  "type": "ANSWER",
  "id": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5-D",
  "QuestionId": "ASE.a413e75e-6992-4e34-a6cb-9155bf3fdbf5",
  "choice": "D",
  "answer": "ALE = EF x ARO"
 },
 {
  "type": "ANSWER",
  "id": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0-A",
  "QuestionId": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0",
  "choice": "A",
  "answer": "Identification of priorities"
 },
 {
  "type": "ANSWER",
  "id": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0-B",
  "QuestionId": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0",
  "choice": "B",
  "answer": "Likelihood assessment"
 },
 {
  "type": "ANSWER",
  "id": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0-C",
  "QuestionId": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0",
  "choice": "C",
  "answer": "Risk identification"
 },
 {
  "type": "ANSWER",
  "id": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0-D",
  "QuestionId": "ASE.8c7c474d-8fcc-4e53-87a4-a421b41958f0",
  "choice": "D",
  "answer": "Resource prioritization"
 },
 {
  "type": "ANSWER",
  "id": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8-A",
  "QuestionId": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8",
  "choice": "A",
  "answer": "Hot Site"
 },
 {
  "type": "ANSWER",
  "id": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8-B",
  "QuestionId": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8",
  "choice": "B",
  "answer": "Warm Site"
 },
 {
  "type": "ANSWER",
  "id": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8-C",
  "QuestionId": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8",
  "choice": "C",
  "answer": "Colde Site"
 },
 {
  "type": "ANSWER",
  "id": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8-D",
  "QuestionId": "ASE.14764538-bf76-44d4-87a1-7b85d36409b8",
  "choice": "D",
  "answer": "All of the above"
 },
 {
  "type": "ANSWER",
  "id": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701-A",
  "QuestionId": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701",
  "choice": "A",
  "answer": "use a data purging tool"
 },
 {
  "type": "ANSWER",
  "id": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701-B",
  "QuestionId": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701",
  "choice": "B",
  "answer": "Physically destroy the drive"
 },
 {
  "type": "ANSWER",
  "id": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701-C",
  "QuestionId": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701",
  "choice": "C",
  "answer": "use a data clearing tool"
 },
 {
  "type": "ANSWER",
  "id": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701-D",
  "QuestionId": "ASE.d44626e8-f614-487b-ae8b-90ba24ed6701",
  "choice": "D",
  "answer": "Format the drive using a different file system"
 }
]