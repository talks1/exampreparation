export const Log = [
 {
  "type": "QUESTION",
  "id": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d",
  "question": "You need to provide internet connectivity fo workstations, devices and linux servers. You are allocated a public network with a /28 prefix. There are 2000 employess. Which of the following will allow this.",
  "answer": "E",
  "explanation": "As sessions are established the network device can map the device IP address and port to one of the 14 available public IPS and a specific port.\n NAT alone doesnt work because there arent enough IP addresses.\nHTTP Proxy is closed but only works for IP traffic\nSOCKS is possible but to uncommon to be used."
 },
 {
  "type": "QUESTION",
  "id": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db",
  "question": "Your admin is concerned about data being compromised if the servers private key is obtained. Which of the following is the BEST way to mitigate this issue.",
  "answer": "C",
  "explanation": "Perfect forward secrecy is a mechanism to use empheral private/public key pairs over https rather than using a single key pair."
 },
 {
  "type": "QUESTION",
  "id": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539",
  "question": "802.2 and 802.3 correspond to which of the following standards",
  "answer": "B, E",
  "explanation": "802.3 is standard for VLAN\n802.1x is port based access control\n802.5 is token ring\n802.15 is bluetooth\n802.16 is wimax\n802.1q is VLAN"
 },
 {
  "type": "QUESTION",
  "id": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c",
  "question": "Which of these are NOT characteristics of IPV6",
  "answer": "D",
  "explanation": "IPV6 Header is fixed to 40 bytes\nIPV6 does not have checksums as there are checksums at other layers\n128 bit source address\nThere is a 20 bit flow label in IPV6\n In IPV6 the TTL field from IPV4 has become Hop Limit. It is only 1 byte"
 },
 {
  "type": "QUESTION",
  "id": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead",
  "question": "Which of the following are characteristics of 'star' topologies",
  "answer": "B",
  "explanation": "Collisions can happen between router/switch and node.\nAny cabling can be used.\nThere is reliance on the route/switch at the center of the star"
 },
 {
  "type": "QUESTION",
  "id": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c",
  "question": "Which of the following is not likely to be found in a DMZ.",
  "answer": "E",
  "explanation": "Front end smtp relay to do initial mail processing\nDNS - yes to provide resolution to the internet of your sites although DNS is generally provided by another provide\nWeb Server - yes\nFTP Server - yes\n AD Domain Controller - No"
 },
 {
  "type": "QUESTION",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "question": "You have developed a web app for a customer to view private information, you want to minimize chances attacker  within radio range will be able to eaves drop the wireless Lan and intercept their private data. Which is the best approach to acheive this.",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09",
  "question": "WHich of the following firewall types provides you the LEAST amount of control over network traffic",
  "answer": "C",
  "explanation": "MAC Filter is not a firewall\nPac"
 },
 {
  "type": "QUESTION",
  "id": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938",
  "question": "Which of these best describes the function of the domain name system",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81",
  "question": "You need to establish a transparent link between two physical sites. User at each site need to be able to able a variety of servers, printers and other IP enabled equipment at each location.  Minimizing user involvement is a key consideration.  Both sites have internet connectivity. WHich is the best option",
  "answer": "E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.dce89f0b-462c-466d-ba71-d4804898621e",
  "question": "Which of the following is true regarding firewalls?",
  "answer": "A",
  "explanation": "In the DoD TCP/IP Network model the internet layer is the network layer in the OSI Model."
 },
 {
  "type": "QUESTION",
  "id": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780",
  "question": "Which of the following is a benefit of implementing DNSSEC (DNS Security)",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f",
  "question": "IPSec is comprised of a number of different protocols which work collectively to establish a level of security desired by a system administator. Which of the following components of an IPsec connection is responsible for authenticating parties and esablishing security associations?",
  "answer": "C",
  "explanation": "IKE - negotiating key exchange\n AH - mechanism to provide integrity of a message\nESP - deals with confidentiality\nIKE may used Diffie Helman as the negotiated approach but IKE is the mechanism"
 },
 {
  "type": "QUESTION",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "question": "Which of the following algorthms were considered by NIST to become the new Advance Encryption Standard (choose 4)",
  "answer": "A,C,F,D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57",
  "question": "A server admin has reported a persistent and large number of half open TCP connections coming from many different Internet IP addresses. Which is the best way to address this issues?",
  "answer": "E",
  "explanation": "The upstream firewall keeps track of half open connections to a server and drops new ones if the limit is exceeded.\nNot a great solution because legitimate requests may be dropped in an attack."
 },
 {
  "type": "QUESTION",
  "id": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c",
  "question": "Which of the following is a characteristic of both TCP and UDP (choose 2)",
  "answer": "B,D",
  "explanation": "Normal DNS name resolution is over UDP but Zone Transfer occur over TCP\nYes both use a header checksum.\n Windowing (recipient requesting different packet size) only happens in TCP."
 },
 {
  "type": "QUESTION",
  "id": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e",
  "question": "Which of the following is not a benefit of FHRP (First Hop Redundancy Protocol) in your network infrastructure?",
  "answer": "A",
  "explanation": "FHRP - a virtual router is defined and the virtual gateway address is shared beween some number of routers. One router is active while the others are inactive until they sense the active one is offline and they take over.\n HSRP- Hot Standy Routing Protocol\nVRRP - Virtual Redundant Routing Protocol\nGLBP = Gateway Load Balancing Routing Protocol"
 },
 {
  "type": "QUESTION",
  "id": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376",
  "question": "ICMP Echo Request sent to the network broadcast address  of a spoofed victim causing all nodes to respond to the victim with an echo reply",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564",
  "question": "Data represented at layer 4 of the open system interconnetion (OSI) model",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.3b0071c1-e977-403d-892e-c5233ea0091a",
  "question": "Provides a standard method for transporting multiprotocol datagrams over point to point",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.072569a5-66ab-4fe6-b067-553bb06de65d",
  "question": "Separates network into three components: raw data, how the data is sent and what purpose the data serves. This involves focus on data, control and application management functions or 'planes'?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.ee198893-665a-4c25-84b2-cb944f0e8475",
  "question": "What is a lightweight encapsulation protocol and lacks the reliable data transport of the TCP layer?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "question": "which of the following are not private IP addresses defined by RFC 1918? Choose all that apply",
  "answer": "A,B,D,F,H",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6",
  "question": "Which of the following does Perfect Forward Secrecy not involve?  (choose several)",
  "answer": "A,E",
  "explanation": "Perfect forward secrecy is a mechanism to use empheral private/public key pairs over https rather than using a single key pair."
 },
 {
  "type": "QUESTION",
  "id": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be",
  "question": "Security domains are critical constructs in a physical network and within alogical environment, as in an operating system. Which of the following best describes how addressing allows for isolation?",
  "answer": "B",
  "explanation": "Addressing is a way to enforce, \"if you dont have this address you dont belong here.\" network domains we use IP ranges and subnet masks to control how networksegments communicate to each other. Within software processes can only communicateto the address space that the security kernel will allow. Addressing is one way to enforce domain access and its boundaries."
 },
 {
  "type": "QUESTION",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "question": "Which of the following (pick 5) are not allowed to pass outbound through a firewall to the internet?",
  "answer": "C, E, F, G, J",
  "explanation": "EIGP - Enhance Interior Gateway Routing Protocol OSPF - Open Shortest Path First - A Routing protocol"
 },
 {
  "type": "QUESTION",
  "id": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d",
  "question": "You have recently discovered that your 802.11 WLAN is 'RF visible' several floors above your office location. Which of the following will be most helpful to you when trying to reduce your RF cell size?",
  "answer": "C,F",
  "explanation": "Fat AP have lots of extra features (nothing to do with RF cellsize)\nChannels are changed when there is interference.\n 2 main influences of RF cell size is annetenna and power"
 },
 {
  "type": "QUESTION",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "question": "You have 11 VLANs in your 6-switch network. Users needing to be assigned to the different VLANs are not all centralized.  Which of the following will you need to do in order to ensure both intra-VLAN and inter-VLAN communication (Choose 3)",
  "answer": "D,G",
  "explanation": "Layer 2 deals with MAC addresses so isnt helpful with routing\nSTP (Spanning Tree Protocol) is about loop avoidance.\n The 802.1q acheive intra routing (traffic between switches is tagged to pc on different switches are connected to VLAN)\nBy mapping subnets ID to VLANs you create the different network.\nYou can route between the networks via layer 3 routing acheive inter vlan routing (from VLAN 5 to 6)"
 },
 {
  "type": "QUESTION",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "question": "Which of the following are valid IP 6 address types?",
  "answer": "A,C,F",
  "explanation": "Global Unicast : UDP;\nLink-Local: equivalent to 127.0.0.1;\nUnique-Local - replace private addresses but are globally unique rather than stricly private;\nSite-Local - was a concept that was dropped as IPV6 matured;\nBroadcast - is not a concept in IPV but can be acheived via multicast;"
 },
 {
  "type": "QUESTION",
  "id": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e",
  "question": "Two different companies are preparing to connect their offices together via an Internet VPN. Before establishing the connection and beginning to share data, which of the following should be in place?",
  "answer": "A",
  "explanation": "ISA - Interconnect Service Agreement"
 },
 {
  "type": "QUESTION",
  "id": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8",
  "question": "Company issued laptops with integrated VLAN adapters will be used outside the office and you assume the networks to which they connect will not be trusted. Which of the following is the best recommendation for securing the laptops and their transmitted data?",
  "answer": "C",
  "explanation": "Dont trust anyone"
 },
 {
  "type": "QUESTION",
  "id": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae",
  "question": "What is the last phase of the TCP 3 way handshake",
  "answer": "C",
  "explanation": "SYN is sent first\nSYN/ACK is sent first from the destination host\nACK is then send from the initiator host"
 },
 {
  "type": "QUESTION",
  "id": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232",
  "question": "Which of the following is not a composition theory related to security models?",
  "answer": "B",
  "explanation": "Cascading: Input for one system comes from the output of another system.\nFeedback: One system provides input to another system, which reciprocates by reversing those roles (so that system A first provides input for system B and then system B provides input to system A).\nHookup: One system sends input to another system but also sends input to external entities."
 },
 {
  "type": "QUESTION",
  "id": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382",
  "question": "Which of the following are valid implementations of Triple DES (choose 2)?",
  "answer": "A,C",
  "explanation": "DES Keys are 56 bits.\nDES-EEE. Encryption-Encryption-Encryption\nDES-EDE. 2 keys used. First key is used in first and 3rd step."
 },
 {
  "type": "QUESTION",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "question": "IPV6 introduces lots of new rules for address structures. WHich of the following addresses are valid destnation IPV6 address used for sending data t antoher not or nodes on the internet?",
  "answer": "B,F",
  "explanation": "A is a local address which does not route globally\nB starts with 0 and has eight segments and is valid globally addressable\nC fda7 is example of locally routable address\nD has a g in in which is not a hex value\nE has double :: which is not allowed\nF ff (multi-cast) 1 (transient/temporary) e (global address) is valid\nG ff (multi-cast) 9 (local to the link) so not correct answer"
 },
 {
  "type": "QUESTION",
  "id": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2",
  "question": "Which of these ports numbers for well known services is correct",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8",
  "question": "What TCP flag indicates that a packet is requesting a new connection?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088",
  "question": "What technology provides the translation that assigns public IP addresses to privately addressed systems that wish to communicate on the Internet?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.e658aa3b-2895-4588-87ba-62821d005aa3",
  "question": "Which one of the following ports is not normally used by email systems?",
  "answer": "B",
  "explanation": "25 SMTP\n110 POP\n143 IMAP"
 },
 {
  "type": "QUESTION",
  "id": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23",
  "question": "What command sends ICMP Echo Request packets?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68",
  "question": "What network device can connect together multiple networks?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.ee177746-209e-4fe2-a297-43fa6117b342",
  "question": "What is the default policy for a firewall",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297",
  "question": "Which of these help you remember the OSI layers",
  "answer": "A,C",
  "explanation": "A - is from bottom up\nC - is from top down"
 },
 {
  "type": "QUESTION",
  "id": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb",
  "question": "Which of the following is not a physical layer technology",
  "answer": "E",
  "explanation": "ARP is data link layer\nHSSI - High Speed Serial Interface\nSONET - Synchronous Optical Network"
 },
 {
  "type": "QUESTION",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "question": "Which of the following are characteristics of a MAC address (choose 5)",
  "answer": "B,C,E,G,J",
  "explanation": "MAC are 48 bits, \nexpressed in hexidecimal\ncontainer 24 bit OUI - (Organizationally Unique Identifier)\n802.3 (ethernet) use MAC address\n802.11 (wireless) use MAC address\n802.15 (bluetooth) use MAC address"
 },
 {
  "type": "QUESTION",
  "id": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5",
  "question": "By what mechanism does an IPv6 node resolve an IP address to a MAC address?",
  "answer": "",
  "explanation": [
   {
    "value": "ICMP multicast"
   },
   {
    "value": "Destination Address id fixed value plus ????"
   },
   {
    "value": "So the request only goes to the node that you are trying determine MAC address for\n Dont use ARP for IPV6\nmDNS is multicast DNS\n SLAAC - kind of like DHCP for IPV6\n FF02::1 - all nodes multicast (kinda like a broadcast)\n IPV6 and IPV4 are both Ethernet at layer 2 (Data Link Layer) and both use MAC."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82",
  "question": "Which of these are synomyms?",
  "answer": "A,C,D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.278ad11c-296a-47dc-b1c3-950883cba67d",
  "question": "What security principle does a firewall implement with traffic when it does not have a rule that explicitly defines an action for that communication?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8",
  "question": "What network port is used for SSL/TLS VPN connections?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.84ed7e36-57f7-4e11-895b-abab4994de63",
  "question": "Which one of the following functions is not normally found in a UTM device?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "question": "Both IPV4 and IPV6 operate at Layer 3 of the OSI Model. WHich of the following is NOT a field in an IPv4 Header",
  "answer": "C",
  "explanation": "TTL is a field in an IPV4 header but not in an IPV6 header\nProtocol is a field in an IPV4 header but not in an IPV6 header\nFlow Label is a field in an IPc6\nFragement offset exists in IPV4 but not IPV6\nChecksum exists in IPV4 and not IPV6"
 },
 {
  "type": "QUESTION",
  "id": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48",
  "question": "Which one of the following is a public IP address?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf",
  "question": "How would a network technician write the subnet mask 255.255.0.0 in slash notation?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.ce24b9e2-b511-4622-bcea-96d81580c873",
  "question": "Which one of the following devices carries VLANs on a network?",
  "answer": "C",
  "explanation": "VLANs are implement at layer 2"
 },
 {
  "type": "QUESTION",
  "id": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee",
  "question": "What is the piece of software running on a device that enables it to connect to a NAC-protected network?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6",
  "question": "Which of the following controls would not constitute defense-in-depth for network access control?",
  "answer": "C",
  "explanation": "IDS is not about access control"
 },
 {
  "type": "QUESTION",
  "id": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4",
  "question": "What control can administrators use when encryption is not feasible for a VoIP network?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71",
  "question": "What type of storage network requires the use of dedicated connections?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.5d14e23b-8e75-49cc-b643-004775b3758a",
  "question": "What type of firewall rule error occurs when a service is decommissioned but the related firewall rules are not removed?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760",
  "question": "What router technology can be used to perform basic firewall functionality?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b",
  "question": "What technique should network administrators use on switches to limit the exposure of sensitive network traffic?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21",
  "question": "What technology can help prevent denial of service attacks on a network?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.86391be3-56d2-454a-b3ff-abf583b0c458",
  "question": "What information is not found in network flow data?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.06723673-69bd-40e0-ab53-e681f98b467a",
  "question": "What message can an SNMP agent send to a network management system to report an unusual event?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f",
  "question": "What type of packet do participating systems send during a Smurf attack?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d",
  "question": "Which one of the following techniques is useful in preventing replay attacks?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672",
  "question": "What attack uses carefully crafted packets that have all available option flags set to 1?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493",
  "question": "Which one of the following is the most secure way for web servers and web browsers to communicate with each other?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9",
  "question": "What IPsec protocol provides confidentiality protection for the content of packets?",
  "answer": "D",
  "explanation": "In IPSEC, ESP protects confidentiality of packets while AH protects integrity of header and packets"
 },
 {
  "type": "QUESTION",
  "id": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c",
  "question": "Cindy would like to transfer files between two systems over a network. Which one of the following protocols performs this action over a secure, encrypted connection?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.c5e91be2-9c47-4815-b0a5-59569684973b",
  "question": "Gary is setting up a wireless network for his home office. He wants to limit access to the network to specific pieces of hardware. What technology can he use to achieve this goal?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.529de368-e839-4851-817f-4b1e90a1b35b",
  "question": "Brad is configuring a new wireless network for his small business. What wireless security standard should he use?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.8accd396-a064-435e-a5c8-2ba11833fc60",
  "question": "Fran is choosing an authentication protocol for her organization's wireless network. Which one of the following protocols is the most secure?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.400548d4-5bfd-437f-b802-5dc811f86846",
  "question": "How many digits are allowed in a WiFi Protected Setup (WPS) PIN?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.c7978e16-7694-4851-85e7-2815cf0395ad",
  "question": "Renee notices a suspicious individual moving around the vicinity of her company's buildings with a large antenna mounted in his car. Users are not reporting any problems with the network. What type of attack is likely taking place?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33",
  "question": "What toolkit enables attackers to easily automate evil twin attacks?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.210624f0-8094-4995-aa99-2733b8afd3e9",
  "question": "What command is used to apply operating system updates on some Linux distributions?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f",
  "question": "What type of malware prevention is most effective against known viruses?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b",
  "question": "What is the name of the application control technology built-in to Microsoft Windows?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8",
  "question": "Which one of the following security controls is built into Microsoft Windows?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d",
  "question": "What additional patching requirements apply only in a virtualized environment?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a",
  "question": "Which of these describes how a code is not a cipher",
  "answer": "B",
  "explanation": "A code such as the '10-4' code used in police communication provides clarity and efficiency"
 },
 {
  "type": "QUESTION",
  "id": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08",
  "question": "What encryption technique does WPA use to protect wireless communications?",
  "answer": "A",
  "explanation": "Wi-Fi Protected Access (WPA) uses the Temporal Key Integrity Protocol (TKIP) to protect wireless communications. WPA2 uses AES encryption."
 },
 {
  "type": "ANSWER",
  "id": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d-A",
  "QuestionId": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d",
  "choice": "A",
  "answer": "NAT - Configure static translations"
 },
 {
  "type": "ANSWER",
  "id": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d-B",
  "QuestionId": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d",
  "choice": "B",
  "answer": "PAT = Translate MAC,IP & TCP/UDP Ports"
 },
 {
  "type": "ANSWER",
  "id": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d-C",
  "QuestionId": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d",
  "choice": "C",
  "answer": "HTTP Proxy - Forward all internet traffic to the proxy"
 },
 {
  "type": "ANSWER",
  "id": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d-D",
  "QuestionId": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d",
  "choice": "D",
  "answer": "NAT - translate ip addresses dynamically from a pool"
 },
 {
  "type": "ANSWER",
  "id": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d-E",
  "QuestionId": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d",
  "choice": "E",
  "answer": "PAT - Translate IP & TCP/UDP ports to one or more public addresses"
 },
 {
  "type": "ANSWER",
  "id": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d-F",
  "QuestionId": "CMS.d34f9706-00c3-4998-a860-7a661ca1dd9d",
  "choice": "F",
  "answer": "SOCK Proxy - Relay all device traffic through a SOCK Proxy"
 },
 {
  "type": "ANSWER",
  "id": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db-A",
  "QuestionId": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db",
  "choice": "A",
  "answer": "Increate key size to 2048"
 },
 {
  "type": "ANSWER",
  "id": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db-B",
  "QuestionId": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db",
  "choice": "B",
  "answer": "Install a new certificate with a lifetime not longer than 90 days"
 },
 {
  "type": "ANSWER",
  "id": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db-C",
  "QuestionId": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db",
  "choice": "C",
  "answer": "Enable perfect forward secrecy on the HTTPS server"
 },
 {
  "type": "ANSWER",
  "id": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db-D",
  "QuestionId": "CMS.19f87bbd-1169-4ee4-b9a8-c7a7c503f1db",
  "choice": "D",
  "answer": "Enable certificate pinning"
 },
 {
  "type": "ANSWER",
  "id": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539-A",
  "QuestionId": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539",
  "choice": "A",
  "answer": "802.2 is a security standard for port based access control"
 },
 {
  "type": "ANSWER",
  "id": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539-B",
  "QuestionId": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539",
  "choice": "B",
  "answer": "802.3 is a wired ethernet standard for Media Access Control (MAC)"
 },
 {
  "type": "ANSWER",
  "id": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539-C",
  "QuestionId": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539",
  "choice": "C",
  "answer": "802.2 is a LAN standard for token ring"
 },
 {
  "type": "ANSWER",
  "id": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539-D",
  "QuestionId": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539",
  "choice": "D",
  "answer": "802.3 is a wireless standanrd for wireless LANs"
 },
 {
  "type": "ANSWER",
  "id": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539-E",
  "QuestionId": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539",
  "choice": "E",
  "answer": "802.2 is an internet standard for Logical Link Control"
 },
 {
  "type": "ANSWER",
  "id": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539-G",
  "QuestionId": "CMS.7facfd8b-8fd3-4f70-a83c-179e741af539",
  "choice": "G",
  "answer": "802.2 is an wireless standard for WiMax"
 },
 {
  "type": "ANSWER",
  "id": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c-A",
  "QuestionId": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c",
  "choice": "A",
  "answer": "Fixed Size Header"
 },
 {
  "type": "ANSWER",
  "id": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c-B",
  "QuestionId": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c",
  "choice": "B",
  "answer": "No IPV6 header checksum"
 },
 {
  "type": "ANSWER",
  "id": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c-C",
  "QuestionId": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c",
  "choice": "C",
  "answer": "128 bit source address"
 },
 {
  "type": "ANSWER",
  "id": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c-D",
  "QuestionId": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c",
  "choice": "D",
  "answer": "16-bit TTL field in Header"
 },
 {
  "type": "ANSWER",
  "id": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c-E",
  "QuestionId": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c",
  "choice": "E",
  "answer": "Extension headers"
 },
 {
  "type": "ANSWER",
  "id": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c-F",
  "QuestionId": "CMS.5612f193-37dd-4bdf-bc48-6109373e8a7c",
  "choice": "F",
  "answer": "20 bit flow label"
 },
 {
  "type": "ANSWER",
  "id": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead-A",
  "QuestionId": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead",
  "choice": "A",
  "answer": "Collision Free"
 },
 {
  "type": "ANSWER",
  "id": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead-B",
  "QuestionId": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead",
  "choice": "B",
  "answer": "Resistant to multi-node failure"
 },
 {
  "type": "ANSWER",
  "id": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead-C",
  "QuestionId": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead",
  "choice": "C",
  "answer": "Can only be used with copper cabling"
 },
 {
  "type": "ANSWER",
  "id": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead-D",
  "QuestionId": "CMS.226306b9-1453-4605-9b6b-d135f1a80ead",
  "choice": "D",
  "answer": "Limited reliance on central aggregegated device"
 },
 {
  "type": "ANSWER",
  "id": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c-A",
  "QuestionId": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c",
  "choice": "A",
  "answer": "Front end smtp relay"
 },
 {
  "type": "ANSWER",
  "id": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c-B",
  "QuestionId": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c",
  "choice": "B",
  "answer": "DNS"
 },
 {
  "type": "ANSWER",
  "id": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c-C",
  "QuestionId": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c",
  "choice": "C",
  "answer": "Web Server"
 },
 {
  "type": "ANSWER",
  "id": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c-D",
  "QuestionId": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c",
  "choice": "D",
  "answer": "FTP Server"
 },
 {
  "type": "ANSWER",
  "id": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c-E",
  "QuestionId": "CMS.63645adb-6e57-465f-aca4-6978cbc8ab8c",
  "choice": "E",
  "answer": "Active Directory"
 },
 {
  "type": "ANSWER",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5-A",
  "QuestionId": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "choice": "A",
  "answer": "Require SSH for all connections to the web server"
 },
 {
  "type": "ANSWER",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5-B",
  "QuestionId": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "choice": "B",
  "answer": "Implement TLS on the web server"
 },
 {
  "type": "ANSWER",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5-C",
  "QuestionId": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "choice": "C",
  "answer": "Advise customers to implement WPA2 with AES on their wireless Lans"
 },
 {
  "type": "ANSWER",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5-D",
  "QuestionId": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "choice": "D",
  "answer": "Digitally sign all traffic with RSA keys"
 },
 {
  "type": "ANSWER",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5-E",
  "QuestionId": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "choice": "E",
  "answer": "Require customers to connect vai IP Sec VPN"
 },
 {
  "type": "ANSWER",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5-F",
  "QuestionId": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "choice": "F",
  "answer": "Implement 802.1x on your switches and provide steps for your customers to do the same"
 },
 {
  "type": "ANSWER",
  "id": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5-G",
  "QuestionId": "CMS.c7676d70-a7da-4319-b7a5-cbd7144cafc5",
  "choice": "G",
  "answer": "Use an IDS to monitor for evidence of ARP flooding attacks"
 },
 {
  "type": "ANSWER",
  "id": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09-A",
  "QuestionId": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09",
  "choice": "A",
  "answer": "Application Layer Firewalls"
 },
 {
  "type": "ANSWER",
  "id": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09-B",
  "QuestionId": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09",
  "choice": "B",
  "answer": "MAC filters on a L2 switch"
 },
 {
  "type": "ANSWER",
  "id": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09-C",
  "QuestionId": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09",
  "choice": "C",
  "answer": "Packet Filtering routers"
 },
 {
  "type": "ANSWER",
  "id": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09-D",
  "QuestionId": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09",
  "choice": "D",
  "answer": "Stateful firewalls"
 },
 {
  "type": "ANSWER",
  "id": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09-E",
  "QuestionId": "CMS.3e1fc379-59d8-4624-8fe6-968c2da78d09",
  "choice": "E",
  "answer": "Proxy Servers"
 },
 {
  "type": "ANSWER",
  "id": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938-A",
  "QuestionId": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938",
  "choice": "A",
  "answer": "Resolve computer NetBios names to IP addresses"
 },
 {
  "type": "ANSWER",
  "id": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938-B",
  "QuestionId": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938",
  "choice": "B",
  "answer": "Centralized database of domain names managed by ICANN"
 },
 {
  "type": "ANSWER",
  "id": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938-C",
  "QuestionId": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938",
  "choice": "C",
  "answer": "A distributed hierichal database for resolving fully qualified domain names"
 },
 {
  "type": "ANSWER",
  "id": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938-D",
  "QuestionId": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938",
  "choice": "D",
  "answer": "An LDAP database used for storing objects and their attributes"
 },
 {
  "type": "ANSWER",
  "id": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938-E",
  "QuestionId": "CMS.db538a1a-8c72-4d28-af67-cc47c71cb938",
  "choice": "E",
  "answer": "A port-to-service mapping agent that locates network services in a distributed network"
 },
 {
  "type": "ANSWER",
  "id": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81-A",
  "QuestionId": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81",
  "choice": "A",
  "answer": "A SOCK5 proxy hosted at a cloud service provider"
 },
 {
  "type": "ANSWER",
  "id": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81-B",
  "QuestionId": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81",
  "choice": "B",
  "answer": "Encrypted Remote Desktop Connections"
 },
 {
  "type": "ANSWER",
  "id": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81-C",
  "QuestionId": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81",
  "choice": "C",
  "answer": "Configure a reverse proxy at each location to offer services to users"
 },
 {
  "type": "ANSWER",
  "id": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81-D",
  "QuestionId": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81",
  "choice": "D",
  "answer": "Individual TLS VPN connections on a per-user basis"
 },
 {
  "type": "ANSWER",
  "id": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81-E",
  "QuestionId": "CMS.573cc41f-5b97-468b-a478-b442e80e6d81",
  "choice": "E",
  "answer": "An IPSec VPN between the 2 sites"
 },
 {
  "type": "ANSWER",
  "id": "COM.dce89f0b-462c-466d-ba71-d4804898621e-A",
  "QuestionId": "COM.dce89f0b-462c-466d-ba71-d4804898621e",
  "choice": "A",
  "answer": "They can protect a network at the internet layer of the TCP/IP networking model"
 },
 {
  "type": "ANSWER",
  "id": "COM.dce89f0b-462c-466d-ba71-d4804898621e-B",
  "QuestionId": "COM.dce89f0b-462c-466d-ba71-d4804898621e",
  "choice": "B",
  "answer": "They optimize performance at the internal/external trust boundary."
 },
 {
  "type": "ANSWER",
  "id": "COM.dce89f0b-462c-466d-ba71-d4804898621e-C",
  "QuestionId": "COM.dce89f0b-462c-466d-ba71-d4804898621e",
  "choice": "C",
  "answer": "Firewalls eliminate all network based attacks from external networks"
 },
 {
  "type": "ANSWER",
  "id": "COM.dce89f0b-462c-466d-ba71-d4804898621e-D",
  "QuestionId": "COM.dce89f0b-462c-466d-ba71-d4804898621e",
  "choice": "D",
  "answer": "Firewalls are optimized, standalone devices"
 },
 {
  "type": "ANSWER",
  "id": "COM.dce89f0b-462c-466d-ba71-d4804898621e-E",
  "QuestionId": "COM.dce89f0b-462c-466d-ba71-d4804898621e",
  "choice": "E",
  "answer": "They can be configured as the default gateway for external nodes"
 },
 {
  "type": "ANSWER",
  "id": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780-A",
  "QuestionId": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780",
  "choice": "A",
  "answer": "Using encryption, DNSSEC prevents service providers from mining your DNS queries"
 },
 {
  "type": "ANSWER",
  "id": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780-B",
  "QuestionId": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780",
  "choice": "B",
  "answer": "DNSSEC prevents you from going to malicious web sites by redirecting your connections attempts"
 },
 {
  "type": "ANSWER",
  "id": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780-C",
  "QuestionId": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780",
  "choice": "C",
  "answer": "DNSSEC speeds up name resolution by compressing queries and answers"
 },
 {
  "type": "ANSWER",
  "id": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780-D",
  "QuestionId": "COM.282baa90-2514-4ec7-ab44-c2273bfb2780",
  "choice": "D",
  "answer": "DNSSec authenticates server response using digital signatures"
 },
 {
  "type": "ANSWER",
  "id": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f-A",
  "QuestionId": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f",
  "choice": "A",
  "answer": "Authentication Header (AH)"
 },
 {
  "type": "ANSWER",
  "id": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f-B",
  "QuestionId": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f",
  "choice": "B",
  "answer": "Encapsulating Security Payload (ESP)"
 },
 {
  "type": "ANSWER",
  "id": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f-C",
  "QuestionId": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f",
  "choice": "C",
  "answer": "Internet Key Exchanges (IKE)"
 },
 {
  "type": "ANSWER",
  "id": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f-D",
  "QuestionId": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f",
  "choice": "D",
  "answer": "User Datagram Protocol (UDP)"
 },
 {
  "type": "ANSWER",
  "id": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f-E",
  "QuestionId": "COMS.cc06beda-880c-4536-98dc-e4d013e67a0f",
  "choice": "E",
  "answer": "Diffie-Helman Key Exchange"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-A",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "A",
  "answer": "RC6"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-B",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "B",
  "answer": "Blowfish"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-C",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "C",
  "answer": "Twofish"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-D",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "D",
  "answer": "Serpent"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-E",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "E",
  "answer": "scrypt"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-F",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "F",
  "answer": "Rijndael"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-G",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "G",
  "answer": "Whirlpool"
 },
 {
  "type": "ANSWER",
  "id": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20-H",
  "QuestionId": "COM.f49394ca-17fd-474c-9cfa-0b3677d88e20",
  "choice": "H",
  "answer": "SHA-512"
 },
 {
  "type": "ANSWER",
  "id": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57-A",
  "QuestionId": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57",
  "choice": "A",
  "answer": "Configure the app to retransmit slower connections over UDP"
 },
 {
  "type": "ANSWER",
  "id": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57-B",
  "QuestionId": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57",
  "choice": "B",
  "answer": "Increase the installed RAM on the web server"
 },
 {
  "type": "ANSWER",
  "id": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57-C",
  "QuestionId": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57",
  "choice": "C",
  "answer": "Decrease the size of the TCP backlog queue"
 },
 {
  "type": "ANSWER",
  "id": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57-D",
  "QuestionId": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57",
  "choice": "D",
  "answer": "Enable reverse path forwarding on the internet router"
 },
 {
  "type": "ANSWER",
  "id": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57-E",
  "QuestionId": "COMS.f3ebec42-211a-4842-8e8c-914e011eea57",
  "choice": "E",
  "answer": "Enable SYN flood protection on an upstream firewall"
 },
 {
  "type": "ANSWER",
  "id": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c-A",
  "QuestionId": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c",
  "choice": "A",
  "answer": "Both use sequence and acknowledgement numbers"
 },
 {
  "type": "ANSWER",
  "id": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c-B",
  "QuestionId": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c",
  "choice": "B",
  "answer": "Both carrry DNS payloads"
 },
 {
  "type": "ANSWER",
  "id": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c-C",
  "QuestionId": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c",
  "choice": "C",
  "answer": "Both implement windowing"
 },
 {
  "type": "ANSWER",
  "id": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c-D",
  "QuestionId": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c",
  "choice": "D",
  "answer": "Both implement a header checksum"
 },
 {
  "type": "ANSWER",
  "id": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c-E",
  "QuestionId": "COM.9742214c-e77c-4e2d-8901-a788f1d6fb3c",
  "choice": "E",
  "answer": "Both use control bits to track connection state"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e-A",
  "QuestionId": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e",
  "choice": "A",
  "answer": "Hosts always have at least 2 gateway IP addresses for fault tolerance"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e-B",
  "QuestionId": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e",
  "choice": "B",
  "answer": "Routers share a virtual IP address allowing either one to use the address"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e-C",
  "QuestionId": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e",
  "choice": "C",
  "answer": "A standy router providers failover support to the active router"
 },
 {
  "type": "ANSWER",
  "id": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e-D",
  "QuestionId": "SOP.b9a2bfd4-906d-4974-9c07-d337ebbf7e5e",
  "choice": "D",
  "answer": "An active router can be manually preempted for hardware maintenance tasks"
 },
 {
  "type": "ANSWER",
  "id": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376-A",
  "QuestionId": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376",
  "choice": "A",
  "answer": "Smurf"
 },
 {
  "type": "ANSWER",
  "id": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376-B",
  "QuestionId": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376",
  "choice": "B",
  "answer": "Bit"
 },
 {
  "type": "ANSWER",
  "id": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376-C",
  "QuestionId": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376",
  "choice": "C",
  "answer": "Switches"
 },
 {
  "type": "ANSWER",
  "id": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376-D",
  "QuestionId": "COM.8891d8b3-6afc-43a5-afd7-e34788d14376",
  "choice": "D",
  "answer": "Frame"
 },
 {
  "type": "ANSWER",
  "id": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564-A",
  "QuestionId": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564",
  "choice": "A",
  "answer": "Switches"
 },
 {
  "type": "ANSWER",
  "id": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564-B",
  "QuestionId": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564",
  "choice": "B",
  "answer": "Concentrators"
 },
 {
  "type": "ANSWER",
  "id": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564-C",
  "QuestionId": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564",
  "choice": "C",
  "answer": "Firewalls"
 },
 {
  "type": "ANSWER",
  "id": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564-D",
  "QuestionId": "COM.c05c926b-7996-4c5a-bdb9-d1349a9dc564",
  "choice": "D",
  "answer": "Segment"
 },
 {
  "type": "ANSWER",
  "id": "COM.3b0071c1-e977-403d-892e-c5233ea0091a-A",
  "QuestionId": "COM.3b0071c1-e977-403d-892e-c5233ea0091a",
  "choice": "A",
  "answer": "Multiprotocol Label Switching (MPLS)"
 },
 {
  "type": "ANSWER",
  "id": "COM.3b0071c1-e977-403d-892e-c5233ea0091a-B",
  "QuestionId": "COM.3b0071c1-e977-403d-892e-c5233ea0091a",
  "choice": "B",
  "answer": "Port Address translation (PAT)"
 },
 {
  "type": "ANSWER",
  "id": "COM.3b0071c1-e977-403d-892e-c5233ea0091a-C",
  "QuestionId": "COM.3b0071c1-e977-403d-892e-c5233ea0091a",
  "choice": "C",
  "answer": "Point to point protocol (PPP)"
 },
 {
  "type": "ANSWER",
  "id": "COM.3b0071c1-e977-403d-892e-c5233ea0091a-D",
  "QuestionId": "COM.3b0071c1-e977-403d-892e-c5233ea0091a",
  "choice": "D",
  "answer": "Internet Control Message Protocol"
 },
 {
  "type": "ANSWER",
  "id": "COM.072569a5-66ab-4fe6-b067-553bb06de65d-A",
  "QuestionId": "COM.072569a5-66ab-4fe6-b067-553bb06de65d",
  "choice": "A",
  "answer": "Port Address Translation"
 },
 {
  "type": "ANSWER",
  "id": "COM.072569a5-66ab-4fe6-b067-553bb06de65d-B",
  "QuestionId": "COM.072569a5-66ab-4fe6-b067-553bb06de65d",
  "choice": "B",
  "answer": "Software Defined WAN"
 },
 {
  "type": "ANSWER",
  "id": "COM.072569a5-66ab-4fe6-b067-553bb06de65d-C",
  "QuestionId": "COM.072569a5-66ab-4fe6-b067-553bb06de65d",
  "choice": "C",
  "answer": "WIFI"
 },
 {
  "type": "ANSWER",
  "id": "COM.072569a5-66ab-4fe6-b067-553bb06de65d-D",
  "QuestionId": "COM.072569a5-66ab-4fe6-b067-553bb06de65d",
  "choice": "D",
  "answer": "Software Defined Networks"
 },
 {
  "type": "ANSWER",
  "id": "COM.ee198893-665a-4c25-84b2-cb944f0e8475-A",
  "QuestionId": "COM.ee198893-665a-4c25-84b2-cb944f0e8475",
  "choice": "A",
  "answer": "Fibre Channel of Ethernet"
 },
 {
  "type": "ANSWER",
  "id": "COM.ee198893-665a-4c25-84b2-cb944f0e8475-B",
  "QuestionId": "COM.ee198893-665a-4c25-84b2-cb944f0e8475",
  "choice": "B",
  "answer": "Port Address Translation"
 },
 {
  "type": "ANSWER",
  "id": "COM.ee198893-665a-4c25-84b2-cb944f0e8475-C",
  "QuestionId": "COM.ee198893-665a-4c25-84b2-cb944f0e8475",
  "choice": "C",
  "answer": "MultiProtocol Label Switching"
 },
 {
  "type": "ANSWER",
  "id": "COM.ee198893-665a-4c25-84b2-cb944f0e8475-D",
  "QuestionId": "COM.ee198893-665a-4c25-84b2-cb944f0e8475",
  "choice": "D",
  "answer": "Code Division Multiple Access"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-A",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "A",
  "answer": "172.32.31.45"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-B",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "B",
  "answer": "192.188.4.3"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-C",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "C",
  "answer": "10.16.31.22"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-D",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "D",
  "answer": "169.254.67.89"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-E",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "E",
  "answer": "172.29.35.9"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-F",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "F",
  "answer": "131.107.33.1"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-G",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "G",
  "answer": "192.168.44.200"
 },
 {
  "type": "ANSWER",
  "id": "COM.4b75e180-4320-42fa-8417-c99eb5facf04-H",
  "QuestionId": "COM.4b75e180-4320-42fa-8417-c99eb5facf04",
  "choice": "H",
  "answer": "172.6.32.1"
 },
 {
  "type": "ANSWER",
  "id": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6-A",
  "QuestionId": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6",
  "choice": "A",
  "answer": "Port knocking"
 },
 {
  "type": "ANSWER",
  "id": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6-B",
  "QuestionId": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6",
  "choice": "B",
  "answer": "HTTPS"
 },
 {
  "type": "ANSWER",
  "id": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6-C",
  "QuestionId": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6",
  "choice": "C",
  "answer": "Using empheral public private key"
 },
 {
  "type": "ANSWER",
  "id": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6-D",
  "QuestionId": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6",
  "choice": "D",
  "answer": "generation of multiple public private key pairs"
 },
 {
  "type": "ANSWER",
  "id": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6-E",
  "QuestionId": "COM.ff606ab3-cdbb-4852-85fc-e159cba954e6",
  "choice": "E",
  "answer": "Using static public private key"
 },
 {
  "type": "ANSWER",
  "id": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be-A",
  "QuestionId": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be",
  "choice": "A",
  "answer": "In a network domains are isolated by using IP ranges and in an operating systemdomains can be isolated by using MAC addresses."
 },
 {
  "type": "ANSWER",
  "id": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be-B",
  "QuestionId": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be",
  "choice": "B",
  "answer": "In a network domains are isolated by using subnet masks and in an operating system domains can be isolated by using memory addresses."
 },
 {
  "type": "ANSWER",
  "id": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be-C",
  "QuestionId": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be",
  "choice": "C",
  "answer": "Addressing is a way to implement ACLs, which enforce domain access attempts and boundaries"
 },
 {
  "type": "ANSWER",
  "id": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be-D",
  "QuestionId": "COM.fd9a9a1c-73da-4ad4-ae1e-7e0e9ca618be",
  "choice": "D",
  "answer": "Addressing creates virtual machines, which creates isolated domains."
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-A",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "A",
  "answer": "HTTP"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-B",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "B",
  "answer": "FTP"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-C",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "C",
  "answer": "SNMP"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-D",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "D",
  "answer": "DNS"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-E",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "E",
  "answer": "EIGRP"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-F",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "F",
  "answer": "RADIUS"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-G",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "G",
  "answer": "OSPF"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-H",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "H",
  "answer": "SSH"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-I",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "I",
  "answer": "SMTP"
 },
 {
  "type": "ANSWER",
  "id": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c-J",
  "QuestionId": "COM.813ccd83-2cc7-4242-afba-0d8c8b12268c",
  "choice": "J",
  "answer": "LDAP"
 },
 {
  "type": "ANSWER",
  "id": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d-A",
  "QuestionId": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d",
  "choice": "A",
  "answer": "Configure PEAP"
 },
 {
  "type": "ANSWER",
  "id": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d-B",
  "QuestionId": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d",
  "choice": "B",
  "answer": "Move the access point to the center of the office"
 },
 {
  "type": "ANSWER",
  "id": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d-C",
  "QuestionId": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d",
  "choice": "C",
  "answer": "Reconsider antenna type and placement"
 },
 {
  "type": "ANSWER",
  "id": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d-D",
  "QuestionId": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d",
  "choice": "D",
  "answer": "Change to a different channel"
 },
 {
  "type": "ANSWER",
  "id": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d-E",
  "QuestionId": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d",
  "choice": "E",
  "answer": "Switch from 'fat' to 'then' APs"
 },
 {
  "type": "ANSWER",
  "id": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d-F",
  "QuestionId": "COM.d7652845-e09c-4dc3-855f-a12f3ffa344d",
  "choice": "F",
  "answer": "Reduce the power produced by the AP"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d-A",
  "QuestionId": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "choice": "A",
  "answer": "Configure Layer 2 Switch to route between the VLANs"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d-B",
  "QuestionId": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "choice": "B",
  "answer": "Route all traffic through the native VLAN"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d-C",
  "QuestionId": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "choice": "C",
  "answer": "Configure STP to tag the traffice on switch-to-switch links"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d-D",
  "QuestionId": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "choice": "D",
  "answer": "Configure a Layer-3 switch to route between the VLANs"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d-E",
  "QuestionId": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "choice": "E",
  "answer": "Allocate a unique subnet id for each VLAN"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d-F",
  "QuestionId": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "choice": "F",
  "answer": "Configure 802.1q on each port connected to a PC or server"
 },
 {
  "type": "ANSWER",
  "id": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d-G",
  "QuestionId": "SOP.c8907c2c-2141-453d-9d56-16d3755e0f8d",
  "choice": "G",
  "answer": "Configure 802.1q on each trunk link"
 },
 {
  "type": "ANSWER",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7-A",
  "QuestionId": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "choice": "A",
  "answer": "Global Unicast"
 },
 {
  "type": "ANSWER",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7-B",
  "QuestionId": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "choice": "B",
  "answer": "IPX"
 },
 {
  "type": "ANSWER",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7-C",
  "QuestionId": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "choice": "C",
  "answer": "Link-Local"
 },
 {
  "type": "ANSWER",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7-D",
  "QuestionId": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "choice": "D",
  "answer": "Broadcast"
 },
 {
  "type": "ANSWER",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7-E",
  "QuestionId": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "choice": "E",
  "answer": "Site-Local"
 },
 {
  "type": "ANSWER",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7-F",
  "QuestionId": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "choice": "F",
  "answer": "Unique-Local"
 },
 {
  "type": "ANSWER",
  "id": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7-G",
  "QuestionId": "SRM.55e11197-69fe-4bd2-a30b-eff4f59c39e7",
  "choice": "G",
  "answer": "NSAP-address"
 },
 {
  "type": "ANSWER",
  "id": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e-A",
  "QuestionId": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e",
  "choice": "A",
  "answer": "ISA"
 },
 {
  "type": "ANSWER",
  "id": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e-B",
  "QuestionId": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e",
  "choice": "B",
  "answer": "SLA"
 },
 {
  "type": "ANSWER",
  "id": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e-C",
  "QuestionId": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e",
  "choice": "C",
  "answer": "BIA"
 },
 {
  "type": "ANSWER",
  "id": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e-D",
  "QuestionId": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e",
  "choice": "D",
  "answer": "DLP"
 },
 {
  "type": "ANSWER",
  "id": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e-E",
  "QuestionId": "COM.4fbd12da-aef4-4d3e-aa6a-fa383915e74e",
  "choice": "E",
  "answer": "IDS"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8-A",
  "QuestionId": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8",
  "choice": "A",
  "answer": "Enable full disk encryption"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8-B",
  "QuestionId": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8",
  "choice": "B",
  "answer": "Configure the systems to use PEAP when connecting to the untrusted WLANs"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8-C",
  "QuestionId": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8",
  "choice": "C",
  "answer": "Require users to establish a VPN connection when connected to an untrusted network"
 },
 {
  "type": "ANSWER",
  "id": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8-D",
  "QuestionId": "SRM.6caa6ba8-200f-455c-8147-c58d29e0d9f8",
  "choice": "D",
  "answer": "Configure the clients to use AES when connecting to any WLAN"
 },
 {
  "type": "ANSWER",
  "id": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae-A",
  "QuestionId": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae",
  "choice": "A",
  "answer": "SYN"
 },
 {
  "type": "ANSWER",
  "id": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae-B",
  "QuestionId": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae",
  "choice": "B",
  "answer": "NAK"
 },
 {
  "type": "ANSWER",
  "id": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae-C",
  "QuestionId": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae",
  "choice": "C",
  "answer": "ACK"
 },
 {
  "type": "ANSWER",
  "id": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae-D",
  "QuestionId": "COM.d1c700fc-0d41-48ba-b82c-76055a340bae",
  "choice": "D",
  "answer": "SYN/ACK"
 },
 {
  "type": "ANSWER",
  "id": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232-A",
  "QuestionId": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232",
  "choice": "A",
  "answer": "Cascading"
 },
 {
  "type": "ANSWER",
  "id": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232-B",
  "QuestionId": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232",
  "choice": "B",
  "answer": "Iterative"
 },
 {
  "type": "ANSWER",
  "id": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232-C",
  "QuestionId": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232",
  "choice": "C",
  "answer": "Feedback"
 },
 {
  "type": "ANSWER",
  "id": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232-D",
  "QuestionId": "SAE.07373ea5-11a1-47ce-bb19-5f0be709f232",
  "choice": "D",
  "answer": "Hookup"
 },
 {
  "type": "ANSWER",
  "id": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382-A",
  "QuestionId": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382",
  "choice": "A",
  "answer": "DES-EEE. ALl 3 are unique. Key is effectively 168 bits."
 },
 {
  "type": "ANSWER",
  "id": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382-B",
  "QuestionId": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382",
  "choice": "B",
  "answer": "DES-EDE. Key1=Key2, Key 3 is unique"
 },
 {
  "type": "ANSWER",
  "id": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382-C",
  "QuestionId": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382",
  "choice": "C",
  "answer": "DES-EDE. Key1=Key3, Key 2 is unique"
 },
 {
  "type": "ANSWER",
  "id": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382-D",
  "QuestionId": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382",
  "choice": "D",
  "answer": "DES-EED. Key1=Key2, Key 3 is unique"
 },
 {
  "type": "ANSWER",
  "id": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382-E",
  "QuestionId": "SOP.de7eaa6c-23e5-405c-b0f0-414df0f61382",
  "choice": "E",
  "answer": "DES-EEE. ALl 3 are the same. Keys are 168 bits"
 },
 {
  "type": "ANSWER",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0-A",
  "QuestionId": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "choice": "A",
  "answer": "fe80::46c9:db66:2002"
 },
 {
  "type": "ANSWER",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0-B",
  "QuestionId": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "choice": "B",
  "answer": "2002:46a8:8:722:d740:9be1:dfd1:d964"
 },
 {
  "type": "ANSWER",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0-C",
  "QuestionId": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "choice": "C",
  "answer": "fda7:4967:fe1c:1::200"
 },
 {
  "type": "ANSWER",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0-D",
  "QuestionId": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "choice": "D",
  "answer": "2620:0000:1234:cfg9:afc4:1:a:1100"
 },
 {
  "type": "ANSWER",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0-E",
  "QuestionId": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "choice": "E",
  "answer": "3000:2341:5621:1:a84c::23::1"
 },
 {
  "type": "ANSWER",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0-F",
  "QuestionId": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "choice": "F",
  "answer": "ff1e:40:2002:abcd:dead:beef:1:11ee"
 },
 {
  "type": "ANSWER",
  "id": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0-G",
  "QuestionId": "COM.ae7bdc45-b40e-427a-9dea-93d82f3d78d0",
  "choice": "G",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2-A",
  "QuestionId": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2",
  "choice": "A",
  "answer": "RDP-1433\nFTP-22\nSSH-21\nDNS-53\nSMTP-25"
 },
 {
  "type": "ANSWER",
  "id": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2-B",
  "QuestionId": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2",
  "choice": "B",
  "answer": "RDP-3389\nFTP-22\nSSH-21\nDNS-53\nSMTP-25"
 },
 {
  "type": "ANSWER",
  "id": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2-C",
  "QuestionId": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2",
  "choice": "C",
  "answer": "RDP-3389\nFTP-21\nSSH-22\nDNS-53\nSMTP-25"
 },
 {
  "type": "ANSWER",
  "id": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2-D",
  "QuestionId": "COM.a7fa2a73-c330-40b2-9fa5-8bcffb0067e2",
  "choice": "D",
  "answer": "RDP-3389\nFTP-443\nSSH-21\nDNS-53\nSMTP-25"
 },
 {
  "type": "ANSWER",
  "id": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8-A",
  "QuestionId": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8",
  "choice": "A",
  "answer": "RST"
 },
 {
  "type": "ANSWER",
  "id": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8-B",
  "QuestionId": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8",
  "choice": "B",
  "answer": "URG"
 },
 {
  "type": "ANSWER",
  "id": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8-C",
  "QuestionId": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8",
  "choice": "C",
  "answer": "PSH"
 },
 {
  "type": "ANSWER",
  "id": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8-D",
  "QuestionId": "COM.39790d74-81b3-4bcb-8cbc-6715920818d8",
  "choice": "D",
  "answer": "SYN"
 },
 {
  "type": "ANSWER",
  "id": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088-A",
  "QuestionId": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088",
  "choice": "A",
  "answer": "SSL"
 },
 {
  "type": "ANSWER",
  "id": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088-B",
  "QuestionId": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088",
  "choice": "B",
  "answer": "TLS"
 },
 {
  "type": "ANSWER",
  "id": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088-C",
  "QuestionId": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088",
  "choice": "C",
  "answer": "NAT"
 },
 {
  "type": "ANSWER",
  "id": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088-D",
  "QuestionId": "COM.1692cd1b-b566-469e-bbeb-e03b941a0088",
  "choice": "D",
  "answer": "HTTP"
 },
 {
  "type": "ANSWER",
  "id": "COM.e658aa3b-2895-4588-87ba-62821d005aa3-A",
  "QuestionId": "COM.e658aa3b-2895-4588-87ba-62821d005aa3",
  "choice": "A",
  "answer": "143"
 },
 {
  "type": "ANSWER",
  "id": "COM.e658aa3b-2895-4588-87ba-62821d005aa3-B",
  "QuestionId": "COM.e658aa3b-2895-4588-87ba-62821d005aa3",
  "choice": "B",
  "answer": "139"
 },
 {
  "type": "ANSWER",
  "id": "COM.e658aa3b-2895-4588-87ba-62821d005aa3-C",
  "QuestionId": "COM.e658aa3b-2895-4588-87ba-62821d005aa3",
  "choice": "C",
  "answer": "25"
 },
 {
  "type": "ANSWER",
  "id": "COM.e658aa3b-2895-4588-87ba-62821d005aa3-D",
  "QuestionId": "COM.e658aa3b-2895-4588-87ba-62821d005aa3",
  "choice": "D",
  "answer": "110"
 },
 {
  "type": "ANSWER",
  "id": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23-A",
  "QuestionId": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23",
  "choice": "A",
  "answer": "telnet"
 },
 {
  "type": "ANSWER",
  "id": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23-B",
  "QuestionId": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23",
  "choice": "B",
  "answer": "ftp"
 },
 {
  "type": "ANSWER",
  "id": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23-C",
  "QuestionId": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23",
  "choice": "C",
  "answer": "ssh"
 },
 {
  "type": "ANSWER",
  "id": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23-D",
  "QuestionId": "COM.dc0805f6-9fa0-4d84-abfd-b42413e52e23",
  "choice": "D",
  "answer": "ping"
 },
 {
  "type": "ANSWER",
  "id": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68-A",
  "QuestionId": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68",
  "choice": "A",
  "answer": "switch"
 },
 {
  "type": "ANSWER",
  "id": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68-B",
  "QuestionId": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68",
  "choice": "B",
  "answer": "ap"
 },
 {
  "type": "ANSWER",
  "id": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68-C",
  "QuestionId": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68",
  "choice": "C",
  "answer": "router"
 },
 {
  "type": "ANSWER",
  "id": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68-D",
  "QuestionId": "COM.379442a5-22ec-4b76-a6f5-d505c3ab0c68",
  "choice": "D",
  "answer": "wireless controller"
 },
 {
  "type": "ANSWER",
  "id": "COM.ee177746-209e-4fe2-a297-43fa6117b342-A",
  "QuestionId": "COM.ee177746-209e-4fe2-a297-43fa6117b342",
  "choice": "A",
  "answer": "Default Disallo"
 },
 {
  "type": "ANSWER",
  "id": "COM.ee177746-209e-4fe2-a297-43fa6117b342-B",
  "QuestionId": "COM.ee177746-209e-4fe2-a297-43fa6117b342",
  "choice": "B",
  "answer": "Default Deny"
 },
 {
  "type": "ANSWER",
  "id": "COM.ee177746-209e-4fe2-a297-43fa6117b342-C",
  "QuestionId": "COM.ee177746-209e-4fe2-a297-43fa6117b342",
  "choice": "C",
  "answer": "Default Prevent"
 },
 {
  "type": "ANSWER",
  "id": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297-A",
  "QuestionId": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297",
  "choice": "A",
  "answer": "Please Do Not Teach Surly People Acronyms."
 },
 {
  "type": "ANSWER",
  "id": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297-B",
  "QuestionId": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297",
  "choice": "B",
  "answer": "Kerckchoffs Principal"
 },
 {
  "type": "ANSWER",
  "id": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297-C",
  "QuestionId": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297",
  "choice": "C",
  "answer": "All Presidents Since Truman Never Did Pot"
 },
 {
  "type": "ANSWER",
  "id": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297-D",
  "QuestionId": "COM.5954f06f-e01c-4717-860d-eb11fe6a7297",
  "choice": "D",
  "answer": "Make Hay while the sun shines."
 },
 {
  "type": "ANSWER",
  "id": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb-A",
  "QuestionId": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb",
  "choice": "A",
  "answer": "HSSI"
 },
 {
  "type": "ANSWER",
  "id": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb-B",
  "QuestionId": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb",
  "choice": "B",
  "answer": "SONET"
 },
 {
  "type": "ANSWER",
  "id": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb-C",
  "QuestionId": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb",
  "choice": "C",
  "answer": "X.21"
 },
 {
  "type": "ANSWER",
  "id": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb-D",
  "QuestionId": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb",
  "choice": "D",
  "answer": "V.25"
 },
 {
  "type": "ANSWER",
  "id": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb-E",
  "QuestionId": "COM.78b374b1-1eaf-407a-aae1-9a80b59542bb",
  "choice": "E",
  "answer": "ARP"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-A",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "A",
  "answer": "32 bits in length"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-B",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "B",
  "answer": "48 bits in length"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-C",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "C",
  "answer": "written in hexdecimal format"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-D",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "D",
  "answer": "written in decimal format"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-E",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "E",
  "answer": "contains 24 bit OUI"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-F",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "F",
  "answer": "contains 16 bit OUI"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-G",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "G",
  "answer": "Multiple 802 standards use MAC address"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-H",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "H",
  "answer": "Only PCs and Servers have MAC addresses"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-I",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "I",
  "answer": "A nodes MAC address must be globally unique"
 },
 {
  "type": "ANSWER",
  "id": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e-J",
  "QuestionId": "COM.405c8c6a-06f1-41a6-bed8-f4af858aa77e",
  "choice": "J",
  "answer": "A nodes MAC address must be locally unique."
 },
 {
  "type": "ANSWER",
  "id": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5-A",
  "QuestionId": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5",
  "choice": "A",
  "answer": "ARP"
 },
 {
  "type": "ANSWER",
  "id": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5-B",
  "QuestionId": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5",
  "choice": "B",
  "answer": "mDNS"
 },
 {
  "type": "ANSWER",
  "id": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5-C",
  "QuestionId": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5",
  "choice": "C",
  "answer": "Solicited Node Multicast Address"
 },
 {
  "type": "ANSWER",
  "id": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5-D",
  "QuestionId": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5",
  "choice": "D",
  "answer": "SLAAC"
 },
 {
  "type": "ANSWER",
  "id": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5-E",
  "QuestionId": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5",
  "choice": "E",
  "answer": "MultiCast query to FF02::1"
 },
 {
  "type": "ANSWER",
  "id": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5-F",
  "QuestionId": "COM.04cbd2d5-24fa-4d7f-9f13-06cfd0266db5",
  "choice": "F",
  "answer": "IPv6 does not use MAC addresses"
 },
 {
  "type": "ANSWER",
  "id": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82-A",
  "QuestionId": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82",
  "choice": "A",
  "answer": "Anomyly Detection"
 },
 {
  "type": "ANSWER",
  "id": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82-B",
  "QuestionId": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82",
  "choice": "B",
  "answer": "Signature Detection"
 },
 {
  "type": "ANSWER",
  "id": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82-C",
  "QuestionId": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82",
  "choice": "C",
  "answer": "Behavior Detection Systems"
 },
 {
  "type": "ANSWER",
  "id": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82-D",
  "QuestionId": "COM.97f1b61a-a8ed-4951-9784-a63e8672ee82",
  "choice": "D",
  "answer": "Heuristic Detection Systems"
 },
 {
  "type": "ANSWER",
  "id": "COM.278ad11c-296a-47dc-b1c3-950883cba67d-A",
  "QuestionId": "COM.278ad11c-296a-47dc-b1c3-950883cba67d",
  "choice": "A",
  "answer": "least privilege"
 },
 {
  "type": "ANSWER",
  "id": "COM.278ad11c-296a-47dc-b1c3-950883cba67d-B",
  "QuestionId": "COM.278ad11c-296a-47dc-b1c3-950883cba67d",
  "choice": "B",
  "answer": "implicit deny"
 },
 {
  "type": "ANSWER",
  "id": "COM.278ad11c-296a-47dc-b1c3-950883cba67d-C",
  "QuestionId": "COM.278ad11c-296a-47dc-b1c3-950883cba67d",
  "choice": "C",
  "answer": "separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "COM.278ad11c-296a-47dc-b1c3-950883cba67d-D",
  "QuestionId": "COM.278ad11c-296a-47dc-b1c3-950883cba67d",
  "choice": "D",
  "answer": "informed consent"
 },
 {
  "type": "ANSWER",
  "id": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8-A",
  "QuestionId": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8",
  "choice": "A",
  "answer": "80"
 },
 {
  "type": "ANSWER",
  "id": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8-B",
  "QuestionId": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8",
  "choice": "B",
  "answer": "443"
 },
 {
  "type": "ANSWER",
  "id": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8-C",
  "QuestionId": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8",
  "choice": "C",
  "answer": "88"
 },
 {
  "type": "ANSWER",
  "id": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8-D",
  "QuestionId": "COM.5c26a4f1-a15f-4616-be82-aa785004a6e8",
  "choice": "D",
  "answer": "1521"
 },
 {
  "type": "ANSWER",
  "id": "COM.84ed7e36-57f7-4e11-895b-abab4994de63-A",
  "QuestionId": "COM.84ed7e36-57f7-4e11-895b-abab4994de63",
  "choice": "A",
  "answer": "Firewall"
 },
 {
  "type": "ANSWER",
  "id": "COM.84ed7e36-57f7-4e11-895b-abab4994de63-B",
  "QuestionId": "COM.84ed7e36-57f7-4e11-895b-abab4994de63",
  "choice": "B",
  "answer": "Intrusion Detection"
 },
 {
  "type": "ANSWER",
  "id": "COM.84ed7e36-57f7-4e11-895b-abab4994de63-C",
  "QuestionId": "COM.84ed7e36-57f7-4e11-895b-abab4994de63",
  "choice": "C",
  "answer": "SSL TErmination"
 },
 {
  "type": "ANSWER",
  "id": "COM.84ed7e36-57f7-4e11-895b-abab4994de63-D",
  "QuestionId": "COM.84ed7e36-57f7-4e11-895b-abab4994de63",
  "choice": "D",
  "answer": "Content Filtering"
 },
 {
  "type": "ANSWER",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70-A",
  "QuestionId": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "choice": "A",
  "answer": "TTL"
 },
 {
  "type": "ANSWER",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70-B",
  "QuestionId": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "choice": "B",
  "answer": "Protocol ID"
 },
 {
  "type": "ANSWER",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70-C",
  "QuestionId": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "choice": "C",
  "answer": "Flow Label"
 },
 {
  "type": "ANSWER",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70-D",
  "QuestionId": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "choice": "D",
  "answer": "Version"
 },
 {
  "type": "ANSWER",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70-E",
  "QuestionId": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "choice": "E",
  "answer": "Source IP Address"
 },
 {
  "type": "ANSWER",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70-F",
  "QuestionId": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "choice": "F",
  "answer": "Fragment Offset"
 },
 {
  "type": "ANSWER",
  "id": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70-G",
  "QuestionId": "COM.ada873ff-0d49-4f8a-bbf1-31c5b9980e70",
  "choice": "G",
  "answer": "Checksum"
 },
 {
  "type": "ANSWER",
  "id": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48-A",
  "QuestionId": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48",
  "choice": "A",
  "answer": "172.18.144.144"
 },
 {
  "type": "ANSWER",
  "id": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48-B",
  "QuestionId": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48",
  "choice": "B",
  "answer": "142.19.15.4"
 },
 {
  "type": "ANSWER",
  "id": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48-C",
  "QuestionId": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48",
  "choice": "C",
  "answer": "192.168.14.129"
 },
 {
  "type": "ANSWER",
  "id": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48-D",
  "QuestionId": "COM.1a3a72de-ec0f-4f28-a49a-fb49a5c5ce48",
  "choice": "D",
  "answer": "10.194.99.16"
 },
 {
  "type": "ANSWER",
  "id": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf-A",
  "QuestionId": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf",
  "choice": "A",
  "answer": "/8"
 },
 {
  "type": "ANSWER",
  "id": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf-B",
  "QuestionId": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf",
  "choice": "B",
  "answer": "/32"
 },
 {
  "type": "ANSWER",
  "id": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf-C",
  "QuestionId": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf",
  "choice": "C",
  "answer": "/16"
 },
 {
  "type": "ANSWER",
  "id": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf-D",
  "QuestionId": "COM.9ee2447d-9443-45e5-bef7-6b0fb8a35baf",
  "choice": "D",
  "answer": "/24"
 },
 {
  "type": "ANSWER",
  "id": "COM.ce24b9e2-b511-4622-bcea-96d81580c873-A",
  "QuestionId": "COM.ce24b9e2-b511-4622-bcea-96d81580c873",
  "choice": "A",
  "answer": "hub"
 },
 {
  "type": "ANSWER",
  "id": "COM.ce24b9e2-b511-4622-bcea-96d81580c873-B",
  "QuestionId": "COM.ce24b9e2-b511-4622-bcea-96d81580c873",
  "choice": "B",
  "answer": "router"
 },
 {
  "type": "ANSWER",
  "id": "COM.ce24b9e2-b511-4622-bcea-96d81580c873-C",
  "QuestionId": "COM.ce24b9e2-b511-4622-bcea-96d81580c873",
  "choice": "C",
  "answer": "switch"
 },
 {
  "type": "ANSWER",
  "id": "COM.ce24b9e2-b511-4622-bcea-96d81580c873-D",
  "QuestionId": "COM.ce24b9e2-b511-4622-bcea-96d81580c873",
  "choice": "D",
  "answer": "firewall"
 },
 {
  "type": "ANSWER",
  "id": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee-A",
  "QuestionId": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee",
  "choice": "A",
  "answer": "SNMP agent"
 },
 {
  "type": "ANSWER",
  "id": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee-B",
  "QuestionId": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee",
  "choice": "B",
  "answer": "authentication server"
 },
 {
  "type": "ANSWER",
  "id": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee-C",
  "QuestionId": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee",
  "choice": "C",
  "answer": "authenticator"
 },
 {
  "type": "ANSWER",
  "id": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee-D",
  "QuestionId": "COM.ad5e3a8e-d86e-4ade-812a-06e1e0b95fee",
  "choice": "D",
  "answer": "supplicant"
 },
 {
  "type": "ANSWER",
  "id": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6-A",
  "QuestionId": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6",
  "choice": "A",
  "answer": "MAC filtering"
 },
 {
  "type": "ANSWER",
  "id": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6-B",
  "QuestionId": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6",
  "choice": "B",
  "answer": "802.1x"
 },
 {
  "type": "ANSWER",
  "id": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6-C",
  "QuestionId": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6",
  "choice": "C",
  "answer": "IDS"
 },
 {
  "type": "ANSWER",
  "id": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6-D",
  "QuestionId": "COM.0d8e3107-e799-46e4-89a0-b6061c2d90f6",
  "choice": "D",
  "answer": "NAC"
 },
 {
  "type": "ANSWER",
  "id": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4-A",
  "QuestionId": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4",
  "choice": "A",
  "answer": "VOIP Firewall"
 },
 {
  "type": "ANSWER",
  "id": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4-B",
  "QuestionId": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4",
  "choice": "B",
  "answer": "VOIP Spoofing"
 },
 {
  "type": "ANSWER",
  "id": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4-C",
  "QuestionId": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4",
  "choice": "C",
  "answer": "Separate voice VLANS"
 },
 {
  "type": "ANSWER",
  "id": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4-D",
  "QuestionId": "COM.7a54ccc5-f122-4dda-a2b4-47a46017aab4",
  "choice": "D",
  "answer": "voice broadcasting"
 },
 {
  "type": "ANSWER",
  "id": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71-A",
  "QuestionId": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71",
  "choice": "A",
  "answer": "iSSCI"
 },
 {
  "type": "ANSWER",
  "id": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71-B",
  "QuestionId": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71",
  "choice": "B",
  "answer": "Fibre Channel over Ethernet"
 },
 {
  "type": "ANSWER",
  "id": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71-C",
  "QuestionId": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71",
  "choice": "C",
  "answer": "CIFS"
 },
 {
  "type": "ANSWER",
  "id": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71-D",
  "QuestionId": "COM.7afcedf8-3a8b-4019-9a22-90f1f45bdd71",
  "choice": "D",
  "answer": "Fibre Channel"
 },
 {
  "type": "ANSWER",
  "id": "COM.5d14e23b-8e75-49cc-b643-004775b3758a-A",
  "QuestionId": "COM.5d14e23b-8e75-49cc-b643-004775b3758a",
  "choice": "A",
  "answer": "Shadowed Rule"
 },
 {
  "type": "ANSWER",
  "id": "COM.5d14e23b-8e75-49cc-b643-004775b3758a-B",
  "QuestionId": "COM.5d14e23b-8e75-49cc-b643-004775b3758a",
  "choice": "B",
  "answer": "Orphaned Rule"
 },
 {
  "type": "ANSWER",
  "id": "COM.5d14e23b-8e75-49cc-b643-004775b3758a-C",
  "QuestionId": "COM.5d14e23b-8e75-49cc-b643-004775b3758a",
  "choice": "C",
  "answer": "Promiscuous Rule"
 },
 {
  "type": "ANSWER",
  "id": "COM.5d14e23b-8e75-49cc-b643-004775b3758a-D",
  "QuestionId": "COM.5d14e23b-8e75-49cc-b643-004775b3758a",
  "choice": "D",
  "answer": "Typographical Rule"
 },
 {
  "type": "ANSWER",
  "id": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760-A",
  "QuestionId": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760",
  "choice": "A",
  "answer": "spanning tree"
 },
 {
  "type": "ANSWER",
  "id": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760-B",
  "QuestionId": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760",
  "choice": "B",
  "answer": "access control lists"
 },
 {
  "type": "ANSWER",
  "id": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760-C",
  "QuestionId": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760",
  "choice": "C",
  "answer": "IPS"
 },
 {
  "type": "ANSWER",
  "id": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760-D",
  "QuestionId": "COM.d8e72c89-d51c-4941-b900-bac9b04ae760",
  "choice": "D",
  "answer": "flood guard"
 },
 {
  "type": "ANSWER",
  "id": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b-A",
  "QuestionId": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b",
  "choice": "A",
  "answer": "spanning tree"
 },
 {
  "type": "ANSWER",
  "id": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b-B",
  "QuestionId": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b",
  "choice": "B",
  "answer": "loop prevention"
 },
 {
  "type": "ANSWER",
  "id": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b-C",
  "QuestionId": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b",
  "choice": "C",
  "answer": "VLAN pruning"
 },
 {
  "type": "ANSWER",
  "id": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b-D",
  "QuestionId": "COM.1842cf52-869d-491d-9df7-a8fd7a4cbb1b",
  "choice": "D",
  "answer": "VLAN hopping"
 },
 {
  "type": "ANSWER",
  "id": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21-A",
  "QuestionId": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21",
  "choice": "A",
  "answer": "BGP"
 },
 {
  "type": "ANSWER",
  "id": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21-B",
  "QuestionId": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21",
  "choice": "B",
  "answer": "Flood Guard"
 },
 {
  "type": "ANSWER",
  "id": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21-C",
  "QuestionId": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21",
  "choice": "C",
  "answer": "VLAN pruning"
 },
 {
  "type": "ANSWER",
  "id": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21-D",
  "QuestionId": "COM.10cfeb42-5eeb-4fd8-8025-9a0105bbde21",
  "choice": "D",
  "answer": "VLAN hopping"
 },
 {
  "type": "ANSWER",
  "id": "COM.86391be3-56d2-454a-b3ff-abf583b0c458-A",
  "QuestionId": "COM.86391be3-56d2-454a-b3ff-abf583b0c458",
  "choice": "A",
  "answer": "source address"
 },
 {
  "type": "ANSWER",
  "id": "COM.86391be3-56d2-454a-b3ff-abf583b0c458-B",
  "QuestionId": "COM.86391be3-56d2-454a-b3ff-abf583b0c458",
  "choice": "B",
  "answer": "destination port"
 },
 {
  "type": "ANSWER",
  "id": "COM.86391be3-56d2-454a-b3ff-abf583b0c458-C",
  "QuestionId": "COM.86391be3-56d2-454a-b3ff-abf583b0c458",
  "choice": "C",
  "answer": "packet content"
 },
 {
  "type": "ANSWER",
  "id": "COM.86391be3-56d2-454a-b3ff-abf583b0c458-D",
  "QuestionId": "COM.86391be3-56d2-454a-b3ff-abf583b0c458",
  "choice": "D",
  "answer": "destination address"
 },
 {
  "type": "ANSWER",
  "id": "COM.06723673-69bd-40e0-ab53-e681f98b467a-A",
  "QuestionId": "COM.06723673-69bd-40e0-ab53-e681f98b467a",
  "choice": "A",
  "answer": "Trap"
 },
 {
  "type": "ANSWER",
  "id": "COM.06723673-69bd-40e0-ab53-e681f98b467a-B",
  "QuestionId": "COM.06723673-69bd-40e0-ab53-e681f98b467a",
  "choice": "B",
  "answer": "Get Request"
 },
 {
  "type": "ANSWER",
  "id": "COM.06723673-69bd-40e0-ab53-e681f98b467a-C",
  "QuestionId": "COM.06723673-69bd-40e0-ab53-e681f98b467a",
  "choice": "C",
  "answer": "Set Request"
 },
 {
  "type": "ANSWER",
  "id": "COM.06723673-69bd-40e0-ab53-e681f98b467a-D",
  "QuestionId": "COM.06723673-69bd-40e0-ab53-e681f98b467a",
  "choice": "D",
  "answer": "Response"
 },
 {
  "type": "ANSWER",
  "id": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f-A",
  "QuestionId": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f",
  "choice": "A",
  "answer": "ICMP Information Reply"
 },
 {
  "type": "ANSWER",
  "id": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f-B",
  "QuestionId": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f",
  "choice": "B",
  "answer": "ICMP Status Check"
 },
 {
  "type": "ANSWER",
  "id": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f-C",
  "QuestionId": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f",
  "choice": "C",
  "answer": "ICMP Echo Request"
 },
 {
  "type": "ANSWER",
  "id": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f-D",
  "QuestionId": "COM.52274c4d-2d62-43c0-aeae-1f4a4fbff25f",
  "choice": "D",
  "answer": "ICMP Timestamp\nC"
 },
 {
  "type": "ANSWER",
  "id": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d-A",
  "QuestionId": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d",
  "choice": "A",
  "answer": "Mobile Device Management"
 },
 {
  "type": "ANSWER",
  "id": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d-B",
  "QuestionId": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d",
  "choice": "B",
  "answer": "Full Disk Encryption"
 },
 {
  "type": "ANSWER",
  "id": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d-C",
  "QuestionId": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d",
  "choice": "C",
  "answer": "Session Tokens"
 },
 {
  "type": "ANSWER",
  "id": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d-D",
  "QuestionId": "COM.f0447397-fdb0-414a-aaad-2c7db6fdc52d",
  "choice": "D",
  "answer": "Man in the middle"
 },
 {
  "type": "ANSWER",
  "id": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672-A",
  "QuestionId": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672",
  "choice": "A",
  "answer": "DNS Poisoning"
 },
 {
  "type": "ANSWER",
  "id": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672-B",
  "QuestionId": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672",
  "choice": "B",
  "answer": "Christmas Tree"
 },
 {
  "type": "ANSWER",
  "id": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672-C",
  "QuestionId": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672",
  "choice": "C",
  "answer": "ARP Poisoning"
 },
 {
  "type": "ANSWER",
  "id": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672-D",
  "QuestionId": "COM.c86f8838-e1dd-40fa-861d-b13ae7fb5672",
  "choice": "D",
  "answer": "URL Hijacking"
 },
 {
  "type": "ANSWER",
  "id": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493-A",
  "QuestionId": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493",
  "choice": "A",
  "answer": "SSLv1"
 },
 {
  "type": "ANSWER",
  "id": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493-B",
  "QuestionId": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493",
  "choice": "B",
  "answer": "SSLv2"
 },
 {
  "type": "ANSWER",
  "id": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493-C",
  "QuestionId": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493",
  "choice": "C",
  "answer": "TLS"
 },
 {
  "type": "ANSWER",
  "id": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493-D",
  "QuestionId": "COM.aa3596f0-2356-48ea-a8d4-c6a458e25493",
  "choice": "D",
  "answer": "SSLv3"
 },
 {
  "type": "ANSWER",
  "id": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9-A",
  "QuestionId": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9",
  "choice": "A",
  "answer": "ISAKMP"
 },
 {
  "type": "ANSWER",
  "id": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9-B",
  "QuestionId": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9",
  "choice": "B",
  "answer": "IKE"
 },
 {
  "type": "ANSWER",
  "id": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9-C",
  "QuestionId": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9",
  "choice": "C",
  "answer": "AH"
 },
 {
  "type": "ANSWER",
  "id": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9-D",
  "QuestionId": "COM.e27197a0-c063-46d8-bbad-f06dee3817d9",
  "choice": "D",
  "answer": "ESP"
 },
 {
  "type": "ANSWER",
  "id": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c-A",
  "QuestionId": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c",
  "choice": "A",
  "answer": "FTP"
 },
 {
  "type": "ANSWER",
  "id": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c-B",
  "QuestionId": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c",
  "choice": "B",
  "answer": "SCP"
 },
 {
  "type": "ANSWER",
  "id": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c-C",
  "QuestionId": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c",
  "choice": "C",
  "answer": "TFTP"
 },
 {
  "type": "ANSWER",
  "id": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c-D",
  "QuestionId": "COM.8d18fa4e-2191-4ba4-9081-8a6722ab354c",
  "choice": "D",
  "answer": "SSH"
 },
 {
  "type": "ANSWER",
  "id": "COM.c5e91be2-9c47-4815-b0a5-59569684973b-A",
  "QuestionId": "COM.c5e91be2-9c47-4815-b0a5-59569684973b",
  "choice": "A",
  "answer": "WEP"
 },
 {
  "type": "ANSWER",
  "id": "COM.c5e91be2-9c47-4815-b0a5-59569684973b-B",
  "QuestionId": "COM.c5e91be2-9c47-4815-b0a5-59569684973b",
  "choice": "B",
  "answer": "SSID broadcasting"
 },
 {
  "type": "ANSWER",
  "id": "COM.c5e91be2-9c47-4815-b0a5-59569684973b-C",
  "QuestionId": "COM.c5e91be2-9c47-4815-b0a5-59569684973b",
  "choice": "C",
  "answer": "MAC filtering"
 },
 {
  "type": "ANSWER",
  "id": "COM.c5e91be2-9c47-4815-b0a5-59569684973b-D",
  "QuestionId": "COM.c5e91be2-9c47-4815-b0a5-59569684973b",
  "choice": "D",
  "answer": "WPA"
 },
 {
  "type": "ANSWER",
  "id": "COM.529de368-e839-4851-817f-4b1e90a1b35b-A",
  "QuestionId": "COM.529de368-e839-4851-817f-4b1e90a1b35b",
  "choice": "A",
  "answer": "WPA2"
 },
 {
  "type": "ANSWER",
  "id": "COM.529de368-e839-4851-817f-4b1e90a1b35b-B",
  "QuestionId": "COM.529de368-e839-4851-817f-4b1e90a1b35b",
  "choice": "B",
  "answer": "WPA"
 },
 {
  "type": "ANSWER",
  "id": "COM.529de368-e839-4851-817f-4b1e90a1b35b-C",
  "QuestionId": "COM.529de368-e839-4851-817f-4b1e90a1b35b",
  "choice": "C",
  "answer": "WEP"
 },
 {
  "type": "ANSWER",
  "id": "COM.529de368-e839-4851-817f-4b1e90a1b35b-D",
  "QuestionId": "COM.529de368-e839-4851-817f-4b1e90a1b35b",
  "choice": "D",
  "answer": "WEP2"
 },
 {
  "type": "ANSWER",
  "id": "COM.8accd396-a064-435e-a5c8-2ba11833fc60-A",
  "QuestionId": "COM.8accd396-a064-435e-a5c8-2ba11833fc60",
  "choice": "A",
  "answer": "TACAS"
 },
 {
  "type": "ANSWER",
  "id": "COM.8accd396-a064-435e-a5c8-2ba11833fc60-B",
  "QuestionId": "COM.8accd396-a064-435e-a5c8-2ba11833fc60",
  "choice": "B",
  "answer": "PEAP"
 },
 {
  "type": "ANSWER",
  "id": "COM.8accd396-a064-435e-a5c8-2ba11833fc60-C",
  "QuestionId": "COM.8accd396-a064-435e-a5c8-2ba11833fc60",
  "choice": "C",
  "answer": "EAP-MD5"
 },
 {
  "type": "ANSWER",
  "id": "COM.8accd396-a064-435e-a5c8-2ba11833fc60-D",
  "QuestionId": "COM.8accd396-a064-435e-a5c8-2ba11833fc60",
  "choice": "D",
  "answer": "LEAP"
 },
 {
  "type": "ANSWER",
  "id": "COM.400548d4-5bfd-437f-b802-5dc811f86846-A",
  "QuestionId": "COM.400548d4-5bfd-437f-b802-5dc811f86846",
  "choice": "A",
  "answer": "11"
 },
 {
  "type": "ANSWER",
  "id": "COM.400548d4-5bfd-437f-b802-5dc811f86846-B",
  "QuestionId": "COM.400548d4-5bfd-437f-b802-5dc811f86846",
  "choice": "B",
  "answer": "4"
 },
 {
  "type": "ANSWER",
  "id": "COM.400548d4-5bfd-437f-b802-5dc811f86846-C",
  "QuestionId": "COM.400548d4-5bfd-437f-b802-5dc811f86846",
  "choice": "C",
  "answer": "6"
 },
 {
  "type": "ANSWER",
  "id": "COM.400548d4-5bfd-437f-b802-5dc811f86846-D",
  "QuestionId": "COM.400548d4-5bfd-437f-b802-5dc811f86846",
  "choice": "D",
  "answer": "8"
 },
 {
  "type": "ANSWER",
  "id": "COM.c7978e16-7694-4851-85e7-2815cf0395ad-A",
  "QuestionId": "COM.c7978e16-7694-4851-85e7-2815cf0395ad",
  "choice": "A",
  "answer": "WPS Cracking"
 },
 {
  "type": "ANSWER",
  "id": "COM.c7978e16-7694-4851-85e7-2815cf0395ad-B",
  "QuestionId": "COM.c7978e16-7694-4851-85e7-2815cf0395ad",
  "choice": "B",
  "answer": "Jamming"
 },
 {
  "type": "ANSWER",
  "id": "COM.c7978e16-7694-4851-85e7-2815cf0395ad-C",
  "QuestionId": "COM.c7978e16-7694-4851-85e7-2815cf0395ad",
  "choice": "C",
  "answer": "War Driving"
 },
 {
  "type": "ANSWER",
  "id": "COM.c7978e16-7694-4851-85e7-2815cf0395ad-D",
  "QuestionId": "COM.c7978e16-7694-4851-85e7-2815cf0395ad",
  "choice": "D",
  "answer": "War chalking"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33-A",
  "QuestionId": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33",
  "choice": "A",
  "answer": "Karma"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33-B",
  "QuestionId": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33",
  "choice": "B",
  "answer": "NIDS"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33-C",
  "QuestionId": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33",
  "choice": "C",
  "answer": "HIPS"
 },
 {
  "type": "ANSWER",
  "id": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33-D",
  "QuestionId": "COM.1e8131e6-2ab1-4c40-b33d-54697d2abc33",
  "choice": "D",
  "answer": "iStumbler"
 },
 {
  "type": "ANSWER",
  "id": "COM.210624f0-8094-4995-aa99-2733b8afd3e9-A",
  "QuestionId": "COM.210624f0-8094-4995-aa99-2733b8afd3e9",
  "choice": "A",
  "answer": "yum"
 },
 {
  "type": "ANSWER",
  "id": "COM.210624f0-8094-4995-aa99-2733b8afd3e9-B",
  "QuestionId": "COM.210624f0-8094-4995-aa99-2733b8afd3e9",
  "choice": "B",
  "answer": "update"
 },
 {
  "type": "ANSWER",
  "id": "COM.210624f0-8094-4995-aa99-2733b8afd3e9-C",
  "QuestionId": "COM.210624f0-8094-4995-aa99-2733b8afd3e9",
  "choice": "C",
  "answer": "systeminfo"
 },
 {
  "type": "ANSWER",
  "id": "COM.210624f0-8094-4995-aa99-2733b8afd3e9-D",
  "QuestionId": "COM.210624f0-8094-4995-aa99-2733b8afd3e9",
  "choice": "D",
  "answer": "ps"
 },
 {
  "type": "ANSWER",
  "id": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f-A",
  "QuestionId": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f",
  "choice": "A",
  "answer": "heuristic detection"
 },
 {
  "type": "ANSWER",
  "id": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f-B",
  "QuestionId": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f",
  "choice": "B",
  "answer": "signature detection"
 },
 {
  "type": "ANSWER",
  "id": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f-C",
  "QuestionId": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f",
  "choice": "C",
  "answer": "anomaly detection"
 },
 {
  "type": "ANSWER",
  "id": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f-D",
  "QuestionId": "COM.b2874552-aa8d-4beb-abfa-b1ff67db1d2f",
  "choice": "D",
  "answer": "behavour analsysis"
 },
 {
  "type": "ANSWER",
  "id": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b-A",
  "QuestionId": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b",
  "choice": "A",
  "answer": "BitContrl"
 },
 {
  "type": "ANSWER",
  "id": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b-B",
  "QuestionId": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b",
  "choice": "B",
  "answer": "BitLocker"
 },
 {
  "type": "ANSWER",
  "id": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b-C",
  "QuestionId": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b",
  "choice": "C",
  "answer": "AppLocker"
 },
 {
  "type": "ANSWER",
  "id": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b-D",
  "QuestionId": "COM.e6c7ca35-8468-42d3-8481-7d6a0ed1161b",
  "choice": "D",
  "answer": "AppControl"
 },
 {
  "type": "ANSWER",
  "id": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8-A",
  "QuestionId": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8",
  "choice": "A",
  "answer": "Host IDS"
 },
 {
  "type": "ANSWER",
  "id": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8-B",
  "QuestionId": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8",
  "choice": "B",
  "answer": "Host IPS"
 },
 {
  "type": "ANSWER",
  "id": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8-C",
  "QuestionId": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8",
  "choice": "C",
  "answer": "Host firewall"
 },
 {
  "type": "ANSWER",
  "id": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8-D",
  "QuestionId": "COM.329d52a8-3c63-4746-9aa9-455cffccd3f8",
  "choice": "D",
  "answer": "MDM"
 },
 {
  "type": "ANSWER",
  "id": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d-A",
  "QuestionId": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d",
  "choice": "A",
  "answer": "patching the hypervisor"
 },
 {
  "type": "ANSWER",
  "id": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d-B",
  "QuestionId": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d",
  "choice": "B",
  "answer": "patching firewalls"
 },
 {
  "type": "ANSWER",
  "id": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d-C",
  "QuestionId": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d",
  "choice": "C",
  "answer": "patching applications"
 },
 {
  "type": "ANSWER",
  "id": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d-D",
  "QuestionId": "COM.4aca9633-561e-4569-b0a7-1ce61bb2df4d",
  "choice": "D",
  "answer": "patching the operating system"
 },
 {
  "type": "ANSWER",
  "id": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a-A",
  "QuestionId": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a",
  "choice": "A",
  "answer": "Hides information"
 },
 {
  "type": "ANSWER",
  "id": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a-B",
  "QuestionId": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a",
  "choice": "B",
  "answer": "Allows efficient communication"
 },
 {
  "type": "ANSWER",
  "id": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a-C",
  "QuestionId": "COM.54f2f196-0104-4ee9-a5ef-98d4888ba62a",
  "choice": "C",
  "answer": "Provides integrity"
 },
 {
  "type": "ANSWER",
  "id": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08-A",
  "QuestionId": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08",
  "choice": "A",
  "answer": "TKIP"
 },
 {
  "type": "ANSWER",
  "id": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08-B",
  "QuestionId": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08",
  "choice": "B",
  "answer": "DES"
 },
 {
  "type": "ANSWER",
  "id": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08-C",
  "QuestionId": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08",
  "choice": "C",
  "answer": "3DES"
 },
 {
  "type": "ANSWER",
  "id": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08-D",
  "QuestionId": "COM.7f4371e6-75e6-4cdc-84f0-212528f7dc08",
  "choice": "D",
  "answer": "AES"
 }
]