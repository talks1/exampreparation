export const Log = [
 {
  "type": "QUESTION",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d",
  "question": "What tool can an organization use to reduce or avoid risk when working with external vendors, consultants, or contractors?",
  "answer": "C",
  "explanation": "A service-level agreement (SLA) is a contract with an external entity and is an important part of risk reduction and risk avoidance. By clearly defining the expectations and penalties for external parties, everyone involved knows what is expected of them and what the consequences are in the event of a failure to meet those expectations."
 },
 {
  "type": "QUESTION",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294",
  "question": "What form of testing stresses demands on the disaster recovery team to derive an appropriate response from a disaster scenario?",
  "answer": "C",
  "explanation": "Simulation tests are similar to the structured walk-throughs. In simulation tests, disaster recovery team members are presented with a scenario and asked to develop an appropriate response."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.015",
  "question": "An organization is hosting a website that accesses resources from multiple organizations. They want users to be able to access all of the resources but only log on once. What should they use?",
  "answer": "D",
  "explanation": "Single sign-on (SSO) allows a user to log on once and access multiple resources. This is not called a federal database but might be related to federation if multiple authentication systems must be integrated to establish the SSO. Kerberos is an effective SSO system within a single organization but not between organizations. Diameter is a newer alternative to RADIUS used for centralized authentication often for remote connections; thus, it is not appropriate for this scenario."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.107",
  "question": "What form of proximity device is able to generate its own electricity from a magnetic field?",
  "answer": "C",
  "explanation": "A field-powered device has electronics that activate when the device enters the electromagnetic field that the reader generates. Such devices actually generate electricity from an EM field to power themselves."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.038",
  "question": "What aspect of security governance is based on the idea that senior management is responsible for the success or failure of a security endeavor?",
  "answer": "D",
  "explanation": "he top-down approach is the aspect of security governance that is based on the idea that senior management is responsible for the success or failure of a security endeavor."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.007",
  "question": "In what scenario would you perform bulk transfers of backup data to a secure off-site location?",
  "answer": "Electronic vaulting describes the transfer of backup data to a remote backup site in a bulk-transfer fashion.",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.046",
  "question": "Which of the following is not a benefit of VLANs?",
  "answer": "B",
  "explanation": "VLANs do not impose encryption on data or traffic. Encrypted traffic can occur within a VLAN, but encryption is not imposed by the VLAN."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.063",
  "question": "Which of the following types of IDS is effective only against known attack methods?",
  "answer": "C",
  "explanation": "A knowledge-based IDS is effective only against known attack methods, which is its primary drawback."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.038",
  "question": "Administrators often consider the relationship between assets, risk, vulnerability, and threat under what model?",
  "answer": "D",
  "explanation": "The operations security triple is a conceptual security model that encompasses three concepts (assets, risk, and vulnerability) and their interdependent relationship within a structured, formalized organization."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.084",
  "question": "What form of intellectual property is used to protect words, slogans, and logos?",
  "answer": "C",
  "explanation": "Trademarks are used to protect the words, slogans, and logos that represent a company and its products or services."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.150",
  "question": "What form of cloud service provides the customer with the ability to run their own custom code but does not require that they manage the execution environment or operating system?",
  "answer": "B",
  "explanation": "Platform as a Service is the concept of providing a computing platform and software solution stack to a virtual or cloud-based service. Essentially, it involves paying for a service that provides all the aspects of a platform (that is, OS and complete solution package). A PaaS solution grants the customer the ability to run custom code of their choosing without needing to manage the environment."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.055",
  "question": "Which of the following statements is true?",
  "answer": "B",
  "explanation": "A closed system is designed to work well with a narrow range of other systems, generally all from the same manufacturer. The standards for closed systems are often proprietary and not normally disclosed. However, a closed system (as a concept) does not define whether or not its programming code can be viewed. An open system (as a concept) also does not define whether or not its programming code can be viewed. An open source program can be distributed for free or for a fee. A closed source program can be reverse engineered or decompiled."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.084",
  "question": "What technique may be used if an individual wants to prove knowledge of a fact to another individual without revealing the fact itself?",
  "answer": "D",
  "explanation": "Zero-knowledge proofs confirm that an individual possesses certain factual knowledge without revealing the knowledge."
 },
 {
  "type": "QUESTION",
  "id": "b475934.CISSPSG8E.be6.104",
  "question": "Which one of the following is not a goal of cryptographic systems?",
  "answer": "C",
  "explanation": "The four goals of cryptographic systems are confidentiality, integrity, authentication, and nonrepudiation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.026",
  "question": "Which of the following is not considered a type of auditing activity?",
  "answer": "D",
  "explanation": "Deployment of countermeasures is not considered a type of auditing activity but is instead an attempt to prevent security problems. Auditing includes recording event data in logs, using data reduction methods to extract meaningful data, and analyzing logs."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.096",
  "question": "Which of the following is an example of a code?",
  "answer": "C",
  "explanation": "The 10 system is a code used in radio communications for brevity and clarity."
 },
 {
  "type": "QUESTION",
  "id": "b475934.CISSPSG8E.be5.059",
  "question": "You are concerned about the risk that fire poses to your $25 million data center facility. Based on expert opinion, you determine that there is a 1 percent chance that fire will occur each year. Experts advise you that a fire would destroy half of your building. What is the single loss expectancy of your facility to fire?",
  "answer": "B",
  "explanation": "The single loss expectancy is the amount of damage that would occur in one fire. The scenario states that a fire would destroy half the building, resulting in $12,500,000 of damage"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.061",
  "question": "Your manager is concerned that the business impact assessment recently completed by the BCP team doesn’t adequately take into account the loss of goodwill among customers that might result from a particular type of disaster. Where should items like this be addressed?",
  "answer": "D",
  "explanation": "The qualitative analysis portion of the business impact assessment (BIA) allows you to introduce intangible concerns, such as loss of customer goodwill, into the BIA planning process."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.039",
  "question": "What strategy basically consists of multiple layers of antivirus, malware, and spyware protection distributed throughout a given network environment?",
  "answer": "B",
  "explanation": "A concentric circle security model comprises several mutually independent security applications, processes, or services that operate toward a single common goal."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.114",
  "question": "Most of the higher-order security models, such as Bell-LaPadula and Biba, are based on which of the following?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.047",
  "question": "Which is not a part of an electronic access control lock?",
  "answer": "D",
  "explanation": "An electronic access control (EAC) lock comprises three elements: an electromagnet to keep the door closed, a credential reader to authenticate subjects and to disable the electromagnet, and a door-closed sensor to reenable the electromagnet."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.030",
  "question": "Which of the following is not a typical security concern with VoIP?",
  "answer": "A",
  "explanation": "VLAN hopping is a switch security issue, not a VoIP security issue. Caller ID falsification, vishing, and SPIT (spam over Internet telephony) are VoIP security concerns."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c08.10",
  "question": "What is a security perimeter? (Choose all that apply.)",
  "answer": "A,B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.029",
  "question": "When an intruder gains unauthorized access to a facility by asking an employee to hold open a door because their arms are full of packages, this is known as what type of attack?",
  "answer": "D",
  "explanation": "Piggybacking is when an authorized person knowingly lets you through.\nTailgating is when an authorized persons unknowingly lets you through."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.at.27",
  "question": "Which of the following is a procedure designed to test and perhaps bypass a system’s security controls?",
  "answer": "C",
  "explanation": "Penetration testing is the attempt to bypass security controls to test overall system security."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c09.11",
  "question": "Which one of the following storage devices is most likely to require encryption technology in order to maintain data security in a networked environment?",
  "answer": "C",
  "explanation": "Removable drives are easily taken out of their authorized physical location, and it is often not possible to apply operating system access controls to them. Therefore, encryption is often the only security measure short of physical security that can be afforded to them. Backup tapes are most often well controlled through physical security measures. Hard disks and RAM chips are often secured through operating system access controls."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.049",
  "question": "Which of the following is not a form of spoofed traffic filtering?",
  "answer": "D",
  "explanation": "Using a block list or black list is a valid form of security filtering; it is just not a form of spoofing filtering."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.077",
  "question": "Which one of the following alternate processing arrangements is rarely implemented?",
  "answer": "D",
  "explanation": "Mutual assistance agreements are rarely implemented because they are difficult to enforce in the event of a disaster requiring site activation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c09.05",
  "question": "What is a security risk of an embedded system that is not commonly found in a standard PC?",
  "answer": "C",
  "explanation": "Because an embedded system is in control of a mechanism in the physical world, a security breach could cause harm to people and property. This typically is not true of a standard PC. Power loss, internet access, and software flaws are security risks of both embedded systems and standard PCs."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.063",
  "question": "Which one of the following business impact assessment (BIA) tasks should be performed last?",
  "answer": "C",
  "explanation": "Resource prioritization is the final step of the business impact assessment (BIA) process."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.124",
  "question": "Which one of the following assumptions is not necessary before you trust the public key contained on a digital certificate?",
  "answer": "B",
  "explanation": "You do not need to trust the sender of a digital certificate as long as the certificate meets the other requirements listed and you trust the certification authority."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.040",
  "question": "What is the first priority in any business continuity plan?",
  "answer": "B",
  "explanation": "The primary concern in any business continuity or disaster recovery effort is ensuring personal safety for everyone involved."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.005",
  "question": "On what port do DHCP clients request a configuration?",
  "answer": "C",
  "explanation": "Dynamic Host Configuration Protocol (DHCP) uses port 68 for client request broadcast and port 67 for server point-to-point response."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.117",
  "question": "Which of the following is not one of the benefits of maintaining an object’s integrity?",
  "answer": "C",
  "explanation": "Option C is not an example of protecting integrity; it is the principle of availability."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.135",
  "question": "Which one of the following is not a major asset category normally covered by the BCP (business continuity plan)?",
  "answer": "B",
  "explanation": "The BCP normally covers three major asset categories: people, infrastructure, and buildings/facilities."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.010",
  "question": "Which software development life cycle model allows for multiple iterations of the development process, resulting in multiple prototypes, each produced according to a complete design and testing process?",
  "answer": "D",
  "explanation": "The spiral model allows developers to repeat iterations of another life cycle model (such as the waterfall model) to produce a number of fully tested prototypes."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.134",
  "question": "Which of the composition theories refers to the management of information flow from one system to another?",
  "answer": "C",
  "explanation": "Cascading is when input for one system comes from the output of another system. Feedback is when one system provides input to another system, which reciprocates by reversing those roles (so that system A first provides input for system B and then system B provides input to system A). Hookup is when one system sends input to another system but also sends input to external entities. Waterfall is a form of project management not related to information flow."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.017",
  "question": "What is a data object passed from the Transport layer to the Network layer called when it reaches the Network layer?",
  "answer": "C",
  "explanation": "A data object is called a datagram or a packet in the Network layer. It is called a PDU in layers 5 through 7. It is called a segment in the Transport layer and a frame in the Data Link layer."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c11.13",
  "question": "Which of the following is not true regarding firewalls?",
  "answer": "B",
  "explanation": "Most firewalls offer extensive logging, auditing, and monitoring capabilities as well as alarms and even basic IDS functions. Firewalls are unable to block viruses or malicious code transmitted through otherwise authorized communication channels, prevent unauthorized but accidental or intended disclosure of information by users, prevent attacks by malicious users already behind the firewall, or protect data after it passed out of or into the private network."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.032",
  "question": "Which of the following cryptographic attacks can be used when you have access to an encrypted message but no other information?",
  "answer": "B",
  "explanation": "Frequency analysis may be used on encrypted messages. The other techniques listed require additional information, such as the plaintext or the ability to choose the ciphertext."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.147",
  "question": "Federation is a means to accomplish ???.",
  "answer": "C",
  "explanation": "Federation or federated identity is a means of linking a subject’s accounts from several sites, services, or entities in a single account. Thus, it is a means to accomplish single sign-on. Accountability logging is used to relate digital activities to humans. ACL verification is a means to verify that correct permissions are assigned to subjects. Trusted OS hardening is the removal of unneeded components and securing the remaining elements."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c08.05",
  "question": "What is an access object?",
  "answer": "A",
  "explanation": "An object is a resource a user or process wants to access. Option A describes an access object."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.130",
  "question": "What type of trusted recovery process always requires the intervention of an administrator?",
  "answer": "B",
  "explanation": "A manual type of trusted recovery process (described in the Common Criteria) always requires the intervention of an administrator. Automated recovery can perform some type of trusted recovery for at least one type of failure. Function recovery provides automated recovery for specific functions and will roll back changes to a secure state if recovery fails. Controlled is not a specific type of trusted recovery process."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.003",
  "question": "What is a threat modeling methodology that focuses on a risk-based approach instead of depending upon an aggregated threat model and that provides a method of performing a security audit in a reliable and repeatable procedure?",
  "answer": "B",
  "explanation": "Trike is a threat modeling methodology that focuses on a risk-based approach instead of depending upon the aggregated threat model used in STRIDE and DREAD; it provides a method of performing a security audit in a reliable and repeatable procedure. Visual, Agile, and Simple Threat (VAST) is a threat modeling concept based on Agile project management and programming principles."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c10.05",
  "question": "Which of the following does not need to be true in order to maintain the most efficient and secure server room?",
  "answer": "A",
  "explanation": "A computer room does not need to be human compatible to be efficient and secure. Having a human-incompatible server room provides a greater level of protection against attacks."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c14.14",
  "question": "Which of the following is not a valid access control model?",
  "answer": "D",
  "explanation": "Compliance-Based Access Control model is not a valid type of access control model. The other answers list valid access control models."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.051",
  "question": "Among the following scenarios, which course of action is insufficient to increase your posture against brute-force and dictionary-driven attacks?",
  "answer": "D",
  "explanation": "Requiring authentication timeouts bears no direct result on password attack protection. Strong password enforcement, restricted physical access, and two-factor authentication help improve security posture against automated attacks."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.101",
  "question": "In a relational database, what type of key is used to uniquely identify a record in a table and can have multiple instances per table?",
  "answer": "A",
  "explanation": "A candidate key is a subset of attributes that can be used to uniquely identify any record in a table. No two records in the same table will ever contain the same values for all attributes composing a candidate key. Each table may have one or more candidate keys, which are chosen from column headings."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.056",
  "question": "What sort of criminal act is committed when an attacker is able to intercept more than just network traffic?",
  "answer": "B",
  "explanation": "Eavesdropping is the ability to intercept network and telephony communications along with physical and electronic documents and even personal conversations."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.049",
  "question": "What database security feature uses a locking mechanism to prevent simultaneous edits of cells?",
  "answer": "C",
  "explanation": "Concurrency uses a “lock” feature to allow an authorized user to make changes and then “unlock” the data elements only after the changes are complete. This is done so another user is unable able to access the database to view and/or make changes to the same elements at the same time."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c08.02",
  "question": "What is system accreditation?",
  "answer": "",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.148",
  "question": "When an attacker selects a target, they must perform reconnaissance to learn as much as possible about the systems and their configuration before launching attacks. What is the gathering of information from any publicly available resource, such as websites, social networks, discussion forums, file services, and public databases?",
  "answer": "C",
  "explanation": "Open source intelligence is the gathering of information from any publicly available resource. This includes websites, social networks, discussion forums, file services, public databases, and other online sources. This also includes non-Internet sources, such as libraries and periodicals."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.009",
  "question": "What frequency does an 802.11n-compliant device employ that allows it to maintain support for legacy 802.11g devices?",
  "answer": "D",
  "explanation": "802.11n can use the 2.4 GHz and 5 GHz frequencies. The 2.4 GHz frequency is also used by 802.11g and 802.11b. The 5 GHz frequency is used by 802.11a, 802.11n, and 802.11ac"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.012",
  "question": "In a discussion of high-speed telco links or network carrier services, what does fault tolerance mean?",
  "answer": "B",
  "explanation": "In a discussion of high-speed telco links or network carrier services, fault tolerance means to have redundant connections."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.062",
  "question": "Which of the following should be generally worker/user accessible?",
  "answer": "",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.022",
  "question": "Spoofing is primarily used to perform what activity?",
  "answer": "C",
  "explanation": "Spoofing grants the attacker the ability to hide their identity through misdirection and is used in many different types of attacks. Spoofing doesn’t send large amounts of data to a victim. It can be used as part of a buffer overflow attack to hide the identity of the attacker but doesn’t cause buffer overflows directly. If user account names and passwords are stolen, the attacker can use them to impersonate the user."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.052",
  "question": "Which access control system discourages the violation of security processes, policies, and procedures?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.137",
  "question": "Abnormal or unauthorized activities detectable to an IDS include which of the following? (Choose all that apply.)",
  "answer": "A,B,C",
  "explanation": "IDSs watch for violations of confidentiality, integrity, and availability. Attacks recognized by IDSs can come from external connections (such as the Internet or partner networks), viruses, malicious code, trusted internal subjects attempting to perform unauthorized activities, and unauthorized access attempts from trusted locations."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.024",
  "question": "Which one of the following patterns of activity is indicative of a scanning attack?",
  "answer": "A",
  "explanation": "A high number of blocked connection attempts may indicate that an attacker is scanning systems that do not offer a particular service on a particular port. Port 22 is the TCP port usually used by the Secure Shell (SSH) protocol, a common target of scanning attacks."
 },
 {
  "type": "QUESTION",
  "id": "b475934.CISSPSG8E.be4.052",
  "question": "What type of attack occurs when malicious users position themselves between a client and server and then interrupt the session and take it over?",
  "answer": "C",
  "explanation": "In a hijack attack, which is an offshoot of a man-in-the-middle attack, a malicious user is positioned between a client and server and then interrupts the session and takes it over. A man-in-the middle attack doesn’t interrupt the session and take it over. Spoofing hides the identity of the attacker. Cracking commonly refers to discovering a password but can also mean any type of attack."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.120",
  "question": "You are implementing AES encryption for files that your organization plans to store in a cloud storage service and wish to have the strongest encryption possible. What key length should you choose?",
  "answer": "B",
  "explanation": "The strongest keys supported by the Advanced Encryption Standard are 256 bits. The valid AES key lengths are 128, 192, and 256 bits."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.081",
  "question": "What is the limit on the number of candidate keys that can exist in a single database table?",
  "answer": "D",
  "explanation": "There is no limit to the number of candidate keys that a table can have. However, each table can have only one primary key."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.094",
  "question": "What law protects the privacy rights of students?",
  "answer": "D",
  "explanation": "The Family Educational Rights and Privacy Act (FERPA) protects the rights of students and the parents of minor students."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.123",
  "question": "What rule of evidence states that a written agreement is assumed to contain all terms of the agreement?",
  "answer": "C",
  "explanation": "The parol evidence rule states that when an agreement between parties is put into written form, the written document is assumed to contain all the terms of the agreement, and no verbal agreements may modify the written agreement."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.078",
  "question": "Your database administrators recommend performing bulk transfer backups to a remote site on a daily basis. What type of strategy are they recommending?",
  "answer": "B",
  "explanation": "Electronic vaulting uses bulk transfers to copy database contents to a remote site on a periodic basis.\nRemote journaling is similar bit only involves transport incremental changes"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.001",
  "question": "What is access?",
  "answer": "B",
  "explanation": "Access is the transfer of information from an object to a subject. An object is a passive resource that does not have functions. Access is not unrestricted. Access control includes more than administration of access control lists (ACLs)."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.035",
  "question": "When it can be legally established that a human performed a specific action that was discovered via auditing, accountability has been established. What additional benefit is derived from this investigation and verification process?",
  "answer": "A",
  "explanation": "When audit trails legally prove accountability, then you also reap the benefit of nonrepudiation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.123",
  "question": "What means of risk response transfers the burden of risk to another entity?",
  "answer": "B",
  "explanation": "Risk assignment or transferring risk is the placement of the cost of loss a risk represents onto another entity or organization. Purchasing insurance and outsourcing are common forms of assigning or transferring risk."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.103",
  "question": "The absence of which of the following can result in the perception that due care is not being maintained?",
  "answer": "A",
  "explanation": "Failing to perform periodic security audits can result in the perception that due care is not being maintained. Such audits alert personnel that senior management is practicing due diligence in maintaining system security. An organization should not indiscriminately deploy all available controls but should choose the most effective ones based on risks. Performance reviews are useful managerial practices but not directly related to due care. Audit reports should not be shared with the public."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.076",
  "question": "A security management plan that discusses the needs of an organization to maintain security, the desire to improve control of authorized access, and the goal of implementing token-based security is what type of plan?",
  "answer": "C",
  "explanation": "A strategic plan is a long-term plan that is fairly stable. It defines the organization’s goals, mission, and objectives. It is useful for about five years if it is maintained and updated annually. The strategic plan also serves as the planning horizon. Long-term goals and visions of the future are discussed in a strategic plan."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.137",
  "question": "Which type of connection created by a packet-switching networking system reuses the same basic parameters or virtual pathway each time it connects?",
  "answer": "C",
  "explanation": "A PVC reestablishes a link using the same basic parameters or virtual pathway each time it connects. SVCs use unique settings each time. Bandwidth on-demand links can be either PVCs or SVCs. CSU/DSU is not a packet-switching technology."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.132",
  "question": "APTs are most closely related to what type of attack category?",
  "answer": "A",
  "explanation": "Advanced persistent threats (APTs) are often associated with government and military actors."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.037",
  "question": "You are concerned about the risk that a hurricane poses to your corporate headquarters in south Florida. The building itself is valued at $15 million. After consulting with the National Weather Service, you determine that there is a 10 percent likelihood that a hurricane will strike over the course of a year. You hired a team of architects and engineers who determined that the average hurricane would destroy approximately 50 percent of the building. What is the annualized rate of occurrence (ARO)?",
  "answer": "B",
  "explanation": "The ARO is, quite simply, the probability that a given disaster will strike over the course of a year. According to the experts quoted in the scenario, this chance is 10 percent."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.008",
  "question": "Which of the following includes a record of system activity and can be used to detect security violations, software flaws, and performance problems?",
  "answer": "D",
  "explanation": "Audit trails provide a comprehensive record of system activity and can help detect a wide variety of security violations, software flaws, and performance problems. An audit trail includes a variety of logs, including change logs, security logs, and system logs, but any one of these individual logs can’t detect all the issues mentioned in the question."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.047",
  "question": "What is the final stage in the life cycle of backup media, occurring after or as a means of sanitization?",
  "answer": "B",
  "explanation": "Destruction is the final stage in the life cycle of backup media. Destruction should occur after proper sanitization or as a means of sanitization."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.058",
  "question": "There are generally three forms of governance within an enterprise organization, all of which have common goals, such as to ensure continued growth and expansion over time and to maintain resiliency to threats and the market. Which of the following is not one of these common forms of governance?",
  "answer": "B",
  "explanation": "The three common forms of governance are IT, corporate, and security. Facility is not usually considered a form of governance, or it is already contained within one of the other three."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.122",
  "question": "Biometric authentication devices fall under what top-level authentication factor type?",
  "answer": "C",
  "explanation": "Biometric authentication devices represent a Type 3 (something you are) authentication factor."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.072",
  "question": "Which environment requires exact, specific clearance for an object’s security domain?",
  "answer": "C",
  "explanation": "Compartmentalized environments require specific security clearances over compartments or domains instead of objects."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.144",
  "question": "When attempting to impose accountability on users, what key issue must be addressed?",
  "answer": "C",
  "explanation": "To effectively hold users accountable, your security must be legally defensible. Primarily, you must be able to prove in a court that your authentication process cannot be easily compromised. Thus, your audit trails of actions can then be tied to a human."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.104",
  "question": "An employee retained access to sensitive data from previous job assignments. Investigators later caught him selling some of this sensitive data to competitors. What could have prevented the employee from stealing and selling the secret data?",
  "answer": "D",
  "explanation": "A user entitlement audit can detect when employees have excessive privileges. Asset valuation identifies the value of assets. Threat modeling identifies threats to valuable assets. Vulnerability analysis detects vulnerabilities or weaknesses that can be exploited by threats."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.143",
  "question": "The Roscommon Rangers baseball team is concerned about the risk that a storm might result in the cancellation of a baseball game. There is a 30 percent chance that the storm will occur, and if it does, the team must refund all single-game tickets because the game cannot be rescheduled. Season ticket holders will not receive a refund and account for 20 percent of ticket sales. The ticket sales for the game are $1,500,000. What is the single loss expectancy in this scenario?",
  "answer": "C",
  "explanation": "The single loss expectancy is calculated as the product of the exposure factor (80 percent) and the asset value ($1,500,000). In this example, the single loss expectancy is $1,200,000."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.028",
  "question": "The Bell-LaPadula access control model was designed to protect what aspect of security?",
  "answer": "A",
  "explanation": "The Bell-LaPadula model is focused on maintaining the confidentiality of objects. Bell-LaPadula does not address the aspects of object integrity or availability."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.136",
  "question": "Where is a good location for a turnstile?",
  "answer": "C",
  "explanation": "Turnstiles are most appropriate on secondary or side exits where a security guard is not available or is unable to maintain constant surveillance. The other options listed are not as ideal for the use of a turnstile."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.059",
  "question": "Which of the following elements is not necessary in the BCP documentation?",
  "answer": "D",
  "explanation": "Details of mobile sites are part of a disaster recovery plan, rather than a business continuity plan, since they are not deployed until after a disaster strikes."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.073",
  "question": "Subjects can be held accountable for their actions toward other subjects and objects while they are authenticated to a system. What process facilitates this accountability?",
  "answer": "B",
  "explanation": "Monitoring the activities of subjects and objects, as well as of core system functions that maintain the operating environment and the security mechanisms, helps establish accountability on the system."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.059",
  "question": "Abnormal or unauthorized activities detectable to an IDS include which of the following? (Choose all that apply.)",
  "answer": "A,B,C",
  "explanation": "IDSs watch for violations of confidentiality, integrity, and availability. Attacks recognized by IDSs can come from external connections (such as the Internet or partner networks), viruses, malicious code, trusted internal subjects attempting to perform unauthorized activities, and unauthorized access attempts from trusted locations."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.028",
  "question": "Using a network packet sniffer, you intercept a communication. After examining the IP packet’s header, you notice that the flag byte has the binary value of 00000100. What does this indicate?",
  "answer": "C",
  "explanation": "The flag value of 00000100 indicates a RST, or reset flag, has been transmitted. This is not necessarily an indication of malicious activity."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.067",
  "question": "An organization has a patch management program but wants to implement another method to ensure that systems are kept up-to-date. What could they use?",
  "answer": "D",
  "explanation": "Vulnerability scanners can check systems to ensure they are up-to-date with current patches (along with other checks) and are an effective tool to verify the patch management program. Change and configuration management programs ensure systems are configured as expected and unauthorized changes are not allowed. A port scanner is often part of a vulnerability scanner, but it will check only for open ports, not patches."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.088",
  "question": "What type of evidence must be authenticated by a witness who can uniquely identify it or through a documented chain of custody?",
  "answer": "C",
  "explanation": "Real evidence must be either uniquely identified by a witness or authenticated through a documented chain of custody."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.113",
  "question": "What type of malicious code uses a filename similar to that of a legitimate system file?",
  "answer": "B",
  "explanation": "Companion viruses use filenames that mimic the filenames of legitimate system files."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.131",
  "question": "What type of decision-making involves the emotional impact of events on a firm’s workforce and client base?",
  "answer": "C",
  "explanation": "Qualitative decision-making takes non-numerical factors, such as emotional impact, into consideration."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.087",
  "question": "NAT allows the use of private IP addresses for internal use while maintaining the ability to communicate with the Internet. Where are the private IP addresses defined?",
  "answer": "C",
  "explanation": "NAT offers numerous benefits, including that you can use the private IP addresses defined in RFC 1918 in a private network and still be able to communicate with the Internet."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.049",
  "question": "What form of access control is concerned with the data stored by a field rather than any other issue?",
  "answer": "A",
  "explanation": "Content-dependent access control is focused on the internal data of each field."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.053",
  "question": "What is a divestiture?",
  "answer": "A",
  "explanation": "A divestiture is an asset or employee reduction."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.041",
  "question": "Of the following, which provides some protection against tampering?",
  "answer": "C",
  "explanation": "A security label is usually a permanent part of the object to which it is attached, thus providing some protection against tampering. The other options do not offer such protection."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.135",
  "question": "Which of the following is not a feature of packet switching?",
  "answer": "B",
  "explanation": "Packet switching has variable delays; circuit switching has fixed known delays."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.023",
  "question": "What is the length of a patent in the United States?",
  "answer": "C",
  "explanation": "U.S. patents are generally issued for a period of 20 years."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.058",
  "question": "In which phase of the business impact assessment do you compute loss expectancies?",
  "answer": "C",
  "explanation": "Loss expectancies are a measure of impact and are calculated during the impact assessment phase."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.099",
  "question": "Which of the following security models is most often used for general commercial applications?",
  "answer": "D",
  "explanation": "Of the four models mentioned, Biba and Clark-Wilson are most commonly used for commercial applications because both focus on data integrity. Of these two, Clark-Wilson offers more control and does a better job of maintaining integrity, so it’s used most often for commercial applications. Bell-LaPadula is used most often for military applications. Brewer and Nash applies only to datasets (usually within database management systems) where conflict-of-interest classes prevent subjects from accessing more than one dataset that might lead to a conflict-of-interest situation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.083",
  "question": "In the Biba model, what rule prevents a user from reading from lower levels of classification?",
  "answer": "B",
  "explanation": "The Biba simple property rule is “no read down.” The Biba star axiom is \"no write up\". \"No read up\" is the simple rule for Bell LaPadula. \"No write down\" is the star rule for Bell LaPadula."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.132",
  "question": "What is the most likely problem to using VPNs when a separate firewall is present?",
  "answer": "A",
  "explanation": "Firewalls are unable to filter on encrypted traffic within a VPN, which is a drawback. VPNs can cross firewalls. Firewalls do not have to always block outbound VPN connections. Firewalls usually only minimally affect the throughput of a VPN and then only when filtering is possible."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.033",
  "question": "What are the only procedures that are allowed to modify a constrained data item (CDI) in the Clark-Wilson model?",
  "answer": "A",
  "explanation": "Transformation procedures (TPs) are the only procedures that are allowed to modify a CDI. The limited access to CDIs through TPs forms the backbone of the Clark-Wilson integrity model."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.085",
  "question": "What is the second phase of the IDEAL software development model?",
  "answer": "B",
  "explanation": "The second phase of the IDEAL software development model is the Diagnosing stage."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.091",
  "question": "Which of the following is not a critical member of a BCP team?",
  "answer": "C",
  "explanation": "While it is important to have executive-level support, it is not necessary (and quite unlikely!) to have the CEO on the team."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.056",
  "question": "What is the term used to refer to the formal assignment of responsibility to an individual or group?",
  "answer": "B",
  "explanation": "Ownership is the formal assignment of responsibility to an individual or group."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.054",
  "question": "A process can function or operate as ????.",
  "answer": "C",
  "explanation": "A process can function or operate as a subject or object. In fact, many elements within an IT environment, including users, can be subjects or objects in different access control situations."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.087",
  "question": "Which of the following algorithms/protocols provides inherent support for nonrepudiation?",
  "answer": "B",
  "explanation": "The Digital Signature Algorithm (as specified in FIPS 186-2) is the only one of the algorithms listed here that supports true digital signatures, providing integrity verification and nonrepudiation. HMAC allows for the authentication of message digests but supports only integrity verification. MD5 and SHA-1 are message digest creation algorithms and can be used in the generation of digital signatures but provide no guarantees of integrity or nonrepudiation in and of themselves."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.024",
  "question": "Which of the following groupings restricts access to roles?",
  "answer": "A",
  "explanation": "Role-based access control restricts access to roles by grouping subjects (such as users). Groups are assigned privileges but privileges aren’t grouped in roles. Programs aren’t grouped in roles. Objects such as files are often grouped within folders, but objects are not assigned as roles."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.026",
  "question": "What category of malicious software includes rogue antivirus software?",
  "answer": "C",
  "explanation": "Rogue antivirus software is an example of a Trojan horse. Users are tricked into installing it, and once installed, it steals sensitive information and/or prompts the user for payment."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.088",
  "question": "Change management should ensure that which of the following is possible?",
  "answer": "D",
  "explanation": "Change management is responsible for making it possible to roll back any change to a previous secured state."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.133",
  "question": "What form of password attack utilizes a preassembled lexicon of terms and their permutations?",
  "answer": "B",
  "explanation": "Dictionary word lists are precompiled lists of common passwords and their permutations and serve as the foundation for a dictionary attack on accounts."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.044",
  "question": "Which conceptual security model offers the best preventive protection against viral infection and outbreak?",
  "answer": "B",
  "explanation": "A concentric circle security model represents the best practice known as defense in depth, a layered approach to protecting IT infrastructure."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.105",
  "question": "What is the vulnerability or feature of a PBX system that allows for an external entity to piggyback onto the PBX system and make long-distance calls without being charged for the tolls?",
  "answer": "D",
  "explanation": "Remote dialing (aka hoteling) is the vulnerability or feature of a PBX system that allows for an external entity to piggyback onto the PBX system and make long-distance calls without being charged for the tolls."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.140",
  "question": "What Clark-Wilson model feature helps protect against insider attacks by restricting the amount of authority any user possesses?",
  "answer": "D",
  "explanation": "The Clark-Wilson model enforces separation of duties to further protect the integrity of data. This model employs limited interfaces or programs to control and maintain object integrity."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.045",
  "question": "What regulation formalizes the prudent man rule that requires senior executives to take personal responsibility for their actions?",
  "answer": "B",
  "explanation": "The Federal Sentencing Guidelines released in 1991 formalized the prudent man rule, which requires senior executives to take personal responsibility for ensuring the due care that ordinary, prudent individuals would exercise in the same situation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.106",
  "question": "What occurs when the relationship between the plain text and the key is complicated enough that an attacker can’t merely continue altering the plain text and analyzing the resulting cipher text to determine the key? (Choose all that apply.)",
  "answer": "A,D",
  "explanation": "Confusion and diffusion are two principles underlying most cryptosystems.\n Transposition is a way to acheive diffussion."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.061",
  "question": "On manual review systems, failure recognition is whose primary responsibility?",
  "answer": "D",
  "explanation": "The observer or auditor of a manual review system is directly responsible for recognizing the failure of that system."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.090",
  "question": "What protocol manages the security associations used by IPsec?",
  "answer": "A",
  "explanation": "The Internet Security Association and Key Management Protocol (ISAKMP) provides background security support services for IPsec, including managing security associations."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.126",
  "question": "The ???? model focuses on preventing interference in support of integrity. This model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.",
  "answer": "D",
  "explanation": "The Sutherland model focuses on preventing interference in support of integrity. This model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.\n Biba relates to integrity and is non-discretionary lattice based model which applies simple star rule. No write down."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.120",
  "question": "Which of the following models uses labels to define classifications levels of objects?",
  "answer": "A",
  "explanation": "A mandatory access control model uses labels to classify objects such as data. Subjects with matching labels can access these objects. None of the other models uses labels."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.017",
  "question": "Which VPN protocol can be implemented as the payload encryption mechanism when L2TP is used to craft the VPN connection for an IP communication?",
  "answer": "C",
  "explanation": "IPsec is a VPN protocol that can be implemented as the payload encryption mechanism when L2TP is used to craft the VPN connection for an IP communication."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.080",
  "question": "What cryptographic goal does the challenge-response protocol support?",
  "answer": "C",
  "explanation": "The challenge-response protocol is an authentication protocol that uses cryptographic techniques to allow parties to assure each other of their identity."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.011",
  "question": "Which of the following definitions best explains the purpose of an intrusion detection system?",
  "answer": "D",
  "explanation": "An intrusion detection system (IDS) is a product that automates the inspection of audit logs and real-time event information to detect intrusion attempts. Option A defines a firewall, option B defines an IP telephony security gateway, and option C defines a content filtering system."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.095",
  "question": "Which IPsec protocol provides assurances of nonrepudiation?",
  "answer": "A",
  "explanation": "The Authentication Header (AH) provides assurances of message integrity and nonrepudiation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.048",
  "question": "What form of infrastructure mode wireless networking deployment supports large physical environments through the use of a single SSID but numerous access points?",
  "answer": "C",
  "explanation": "Enterprise extended infrastructure mode exists when a wireless network is designed to support a large physical environment through the use of a single SSID but numerous access points."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.067",
  "question": "Which of the following is the principle that objects are not disclosed to unauthorized subjects?",
  "answer": "D",
  "explanation": "Confidentiality is the principle that objects are not disclosed to unauthorized subjects."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.089",
  "question": "What layer of the ring protection scheme includes programs running in supervisory mode?",
  "answer": "A",
  "explanation": "Supervisory mode programs are run by the security kernel, at Level 0 of the ring protection scheme."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.023",
  "question": "Coordinated attack efforts against an infrastructure or system that limit or restrict its capacity to process legitimate traffic are what form of network-driven attack?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.058",
  "question": "hat evidence standard do most civil investigations follow?",
  "answer": "C",
  "explanation": "Criminal cases use 'beyond reasonable doubt'\nwhile civil use 'more likely than not'"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.080",
  "question": "Matthew recently completed writing a new song and posted it on his website. He wants to ensure that he preserves his copyright in the work. As a U.S. citizen, which of the following is the minimum that he must do to preserve his copyright in the song?",
  "answer": "D",
  "explanation": "Matthew is not required to do anything. Copyright protection is automatic as soon as he creates the work."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.002",
  "question": "Which source of interference is generated by electrical appliances, light sources, electrical cables and circuits, and so on?",
  "answer": "B",
  "explanation": "RFI"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.029",
  "question": "Which one of the following would be considered a system compromise?",
  "answer": "B",
  "explanation": "Sharing an account with a relative allows unauthorized access to a system, meeting the definition of a compromise. Forged spam email does not necessarily require access to your organization’s computing resources. Probing a network for vulnerable services is a scanning attack. Infection of a system by a virus is a malicious code attack."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.032",
  "question": "What term describes the processor mode used to run the system tools used by administrators seeking to make configuration changes to a machine?",
  "answer": "A",
  "explanation": "All user applications, regardless of the security permissions assigned to the user, execute in user mode. Supervisory mode, kernel mode, and privileged mode are all terms that describe the mode used by the processor to execute instructions that originate from the operating system."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.146",
  "question": "Which of the following is a means for IPv6 and IPv4 to be able to coexist on the same network? (Choose all that apply.)",
  "answer": "A,B,D",
  "explanation": "The means by which IPv6 and IPv4 can coexist on the same network is to use one or more of three primary options. Dual stack is to have most systems operate both IPv4 and IPv6 and use the appropriate protocol for each conversation. Tunneling allows most systems to operate a single stack of either IPv4 or IPv6 and use an encapsulation tunnel to access systems of the other protocol. Network Address Translation-Protocol Translation (NAT-PT) (RFC-2766) can be used to convert between IPv4 and IPv6 network segments similar to how NAT converts between internal and external addresses. There is no distinct IPsec version 6 because IPsec is native to IPv6, and IPsec does not assist or support a network operating both IPv4 and IPv6 on its own."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c08.12",
  "question": "What is the best definition of a security model?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.075",
  "question": "Which type of intrusion detection system (IDS) can be considered an expert system?",
  "answer": "D",
  "explanation": "A behavior-based IDS can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events. A knowledge-based IDS uses a database of known attack methods to detect attacks. Both host-based and network-based systems can be either knowledge-based, behavior-based, or a combination of both."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c01.12",
  "question": "Which of the following is the most important and distinctive concept in relation to layered security?",
  "answer": "B",
  "explanation": "Layering is the deployment of multiple security mechanisms in a series. When security restrictions are performed in a series, they are performed one after the other in a linear fashion. Therefore, a single failure of a security control does not render the entire solution ineffective."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.060",
  "question": "What phase of the Electronic Discovery Reference Model puts evidence in a format that may be shared with others?",
  "answer": "A",
  "explanation": "Production places the information in a format that may be shared with others."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.078",
  "question": "The Jones Institute has six employees and uses a symmetric key encryption system to ensure confidentiality of communications. If each employee needs to communicate privately with every other employee, how many keys are necessary?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.038",
  "question": "What is risk?",
  "answer": "C",
  "explanation": "Risk is the likelihood that any specific threat will exploit a specific vulnerability to cause harm to an asset."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c20.12",
  "question": "In which phase of the SW-CMM does an organization use quantitative measures to gain a detailed understanding of the development process?",
  "answer": "D",
  "explanation": "In the Managed phase, level 4 of the SW-CMM, the organization uses quantitative measures to gain a detailed understanding of the development process."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c09.06",
  "question": "Which of the following describes a community cloud?",
  "answer": "A",
  "explanation": "A community cloud is a cloud environment maintained, used, and paid for by a group of users or organizations for their shared benefit, such as collaboration and data exchange. A private cloud is a cloud service within a corporate network and isolated from the internet. A public could is a cloud service that is accessible to the general public typically over an internet connection. A hybrid cloud is a cloud service that is partially hosted within an organization for private use and that uses external services to offer recourses to outsiders."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.061",
  "question": "Which of the following does not reflect the protected elements under an access control methodology?",
  "answer": "B",
  "explanation": "Access control provides security through confidentiality, integrity, and accountability."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.145",
  "question": "Which of the following is not a valid reason why log files should be protected?",
  "answer": "D",
  "explanation": "Log files include information on what happened, when and where it happened, who was involved, and sometimes how, but they do not include why an attack occurred; they do not include the motivation of an attacker. Log files should be protected because they can be used to reconstruct events, attackers may try to modify them, and unprotected logs cannot be used as evidence."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.122",
  "question": "What protocol is slowly replacing certificate revocation lists as a real-time method of verifying the status of a digital certificate?",
  "answer": "C",
  "explanation": "The Online Certificate Status Protocol (OSCP) provides real-time query/response services to digital certificate users. This overcomes the latency inherent in the traditional certificate revocation list download and cross-check process."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c04.06",
  "question": "What law protects the right of citizens to privacy by placing restrictions on the authority granted to government agencies to search private residences and facilities?",
  "answer": "B",
  "explanation": "The Fourth Amendment to the U.S. Constitution sets the “probable cause” standard that law enforcement officers must follow when conducting searches and/or seizures of private property. It also states that those officers must obtain a warrant before gaining involuntary access to such property."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.087",
  "question": "What are the well-known ports?",
  "answer": "A",
  "explanation": "Ports 0 to 1,023 are the well-known ports."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.021",
  "question": "Which of the following security assessment and testing program components may be performed by security professionals in the IT organization?",
  "answer": "D",
  "explanation": "IT staff may perform security assessments to evaluate the security of their systems and applications. Audits must be performed by internal or external auditors who are independent of the IT organization. Criminal investigations must be performed by certified law enforcement personnel."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.095",
  "question": "What technique is commonly used by polymorphic viruses to escape detection?",
  "answer": "B",
  "explanation": "While viruses may use all of the techniques described in this question to escape detection, the use of encryption is characteristic of polymorphic viruses."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.035",
  "question": "What Biba property protects a subject from accessing an object at a lower integrity level?",
  "answer": "B",
  "explanation": "The Simple Integrity Property states that a subject cannot read an object of a lower integrity level (no read down)."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.100",
  "question": "The ",
  "answer": "B",
  "explanation": "The Goguen-Meseguer model is based on predetermining the set or domain of objects that a subject can access. The set or domain is a list of those objects that a subject can access. This model is based on automation theory and domain separation. This means subjects are able to perform only predetermined actions against predetermined objects."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.033",
  "question": "Why do operating systems need security mechanisms?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.062",
  "question": "In which IPsec mode is the content of an encapsulated packet encrypted but not the header?",
  "answer": "A",
  "explanation": "In transport mode, the IP packet data is encrypted, but the header of the packet is not."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.013",
  "question": "Which of the following combinations of terms defines the operations security triple?",
  "answer": "C",
  "explanation": "The primary purpose for operations security is to safeguard information assets that reside in a system day to day, to identify and safeguard any vulnerabilities that might be present in that system, and to prevent any exploitation of threats. Administrators often call the relationship between assets, vulnerabilities, and threats an operations security triple. CIA relates to a model used to develop security policy, and AAA defines a framework for managing access to computer resources, enforcing policy, auditing usage, and providing usage data for charge backs or billing. Due care, due diligence, and operations controls are part of operations security but not in the same sense as in the relationship between assets, vulnerabilities, and threats."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.096",
  "question": "What type of virus modifies operating system routines to trick antivirus software into thinking everything is functioning normally?",
  "answer": "B",
  "explanation": "Stealth viruses alter operating system file access routines so that when an antivirus package scans the system, it is provided with the information it would see on a clean system rather than with infected versions of data."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.125",
  "question": "Your organization has recently implemented an SDN to separate the control plane from the data plane. Which of the following models will the SDN most likely use?",
  "answer": "A",
  "explanation": "A software-defined network (SDN) typically uses an attribute-based access control (ABAC) model. SDNs don’t normally use the discretionary access control (DAC), mandatory access control, or role-based access control (RBAC) models."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.150",
  "question": "Which of the following is most directly associated with providing or supporting perfect forward secrecy?",
  "answer": "B",
  "explanation": "Elliptic Curve Diffie-Hellman Ephemeral, or Elliptic Curve Ephemeral Diffie-Hellman (ECDHE), implements perfect forward secrecy through the use of elliptic curve cryptography (ECC). PBKDF2 is an example of a key-stretching technology not directly supporting perfect forward secrecy. HMAC is a hashing function. OCSP is used to check for certificate revocation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.096",
  "question": "Which wireless security protocol makes use of AES cryptography?",
  "answer": "WPA2 replaces RC4 and TKIP (used by the original WPA) with AES and CCMP cryptography.",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.098",
  "question": "Which network topology offers multiple routes to each node to protect from multiple segment failures?",
  "answer": "D",
  "explanation": "Mesh topologies provide redundant connections to systems, allowing multiple segment failures without seriously affecting connectivity."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.081",
  "question": "What technology can be used to minimize the impact of a server failure immediately before the next backup was scheduled?",
  "answer": "A",
  "explanation": "Clustering servers adds a degree of fault tolerance, protecting against the impact of a single server failure."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.088",
  "question": "A host organization that houses on-site security staff has what form of security system?",
  "answer": "D",
  "explanation": "This describes a proprietary system, which is almost the same concept as a central station, but with the host organization has its own onsite security staff waiting to respond to security breaches. This is not an auxiliary system as there is no mention of contacting official emergency services, like police or fire/rescue. This is not a centralized system, as that is not a type of security system. A central station is a security system with offsite monitoring by a professional security services company. The term centralized is often used in contrast to decentralized in describing how something is managed, such as access control or authentication. This is not a localized system as there is no mention of an audible alarm."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.068",
  "question": "Which of the following is the type of antivirus response function that removes the malicious code but leaves damage unrepaired?",
  "answer": "B",
  "explanation": "Removal removes the malicious code but does not repair the damage caused by it. Cleaning not only removes the code, but it also repairs any damage the code has caused."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.099",
  "question": "What type of attack allows the transmission of sensitive data between classification levels through the direct or indirect manipulation of a shared storage media?",
  "answer": "D",
  "explanation": "Covert channel attacks involve the illicit transfer of data by manipulating storage or timing channels."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.121",
  "question": "Identification is the first step toward what ultimate goal?",
  "answer": "A",
  "explanation": "Accountability is the ultimate goal of a process started by identification."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.067",
  "question": "What common vulnerability has no direct countermeasure and little safeguards or validators?",
  "answer": "C",
  "explanation": "Both omissions and errors are difficult aspects to protect against, particularly as they deal with human and circumstantial origins."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.082",
  "question": "Mark recently completed work on a piece of computer software that he thinks will be especially valuable. He wants to protect the source code against others rewriting it in a different form. What is the best form of intellectual property protection he could seek in this case?",
  "answer": "B",
  "explanation": "Mark’s best option is to treat the software source code as a trade secret. Copyright protection requires that he disclose the source in a public filing. Patent protection does not cover the actual program."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.079",
  "question": "Which process in the evaluation of a security system represents the formal acceptance of a certified configuration?",
  "answer": "C",
  "explanation": "Accreditation is the formal declaration by the Designated Approving Authority (DAA) that an IT system is approved to operate in a particular security mode using a prescribed set of safeguards at an acceptable level of risk."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.043",
  "question": "In a(n) ",
  "answer": "A",
  "explanation": "In a trusted system, all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.070",
  "question": "The ????? model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited.",
  "answer": "B",
  "explanation": "The Sutherland model is based on the idea of defining a set of system states, initial states, and state transitions. Through the use of and limitations to only these predetermined secure states, integrity is maintained, and interference is prohibited."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.101",
  "question": "Which one of the following backup types does not alter the archive bit on backed-up files?",
  "answer": "D",
  "explanation": "Differential backups store all files that have been modified since the time of the most recent full backup. They do not alter the archive bit."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.150",
  "question": "Which form of physical security control focuses on facility construction and selection, site management, personnel controls, awareness training, and emergency response and procedures?",
  "answer": "C",
  "explanation": "Administrative physical security controls include facility construction and selection, site management, personnel controls, awareness training, and emergency response and procedures."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.128",
  "question": "What is the primary function of a gateway as a network device?",
  "answer": "B",
  "explanation": "The gateway is a network device (or service) that works at the Application layer. However, an Application layer gateway is a very specific type of component. It serves as a protocol translation tool. For example, an IP-to-IPX gateway takes inbound communications from TCP/IP and translates them over to IPX/SPX for outbound transmission."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.126",
  "question": "If a specific step-by-step guide does not exist that prescribes how to accomplish a necessary task, which of the following is used to create such a document?",
  "answer": "D",
  "explanation": "A guideline offers recommendations on how standards and baselines are implemented and serves as an operational guide for both security professionals and users. Guidelines are flexible so they can be customized for each unique system or condition and can be used in the creation of new procedures (i.e., step-by-step guides)."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.108",
  "question": "Which one of the following items is not a critical piece of information in the chain of evidence?",
  "answer": "C",
  "explanation": "The chain of evidence does not require that the evidence collector know or document the relationship of the evidence to the crime."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.011",
  "question": "A tunnel mode VPN is used to connect which types of systems?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.120",
  "question": "The following eight primary protection rules or actions define the boundaries of what security model?",
  "answer": "A",
  "explanation": "The Graham-Denning model is focused on the secure creation and deletion of both subjects and objects. Ultimately, Graham-Denning is a collection of eight primary protection rules or actions (listed in the question) that define the boundaries of certain secure actions."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.028",
  "question": "Which component of the CIA Triad has the most avenues or vectors of attack and compromise?",
  "answer": "C",
  "explanation": "Availability has the most avenues or vectors of attack and compromise. Availability can be affected by damaging the resource, compromising the resource host, interfering with communications, or attacking the client."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.140",
  "question": "On a much smaller scale, ",
  "answer": "A",
  "explanation": "Recovery access control is deployed to repair or restore resources, functions, and capabilities after a violation of security policies."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.040",
  "question": "Why should an enterprise network implement endpoint security?",
  "answer": "A",
  "explanation": "Endpoint security is implemented in order to provide sufficient security on each individual host, rather than relying on network-based security only."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.036",
  "question": "During threat modeling, several options exist for ranking or rating the severity and priority of threats. Which of the following not a threat modeling ranking system?",
  "answer": "C",
  "explanation": "Qualitative analysis is part of risk management/risk assessment, but it is not specifically a means of ranking or rating the severity and priority of threats under threat modelling. The three common means of ranking or rating the severity and priority of threats are DREAD, Probability * Damage Potential, and High/medium/low."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.099",
  "question": "What government agency provides daily updates on wildfires in the United States?",
  "answer": "B",
  "explanation": "The National Interagency Fire Center provides daily updates on wildfires occurring in the United States.\nFEMA provides information on floods"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.031",
  "question": "Which of the following encryption packages provides full disk encryption and is built into Microsoft Windows?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.092",
  "question": "What network devices operate within the Physical layer?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.130",
  "question": "What process state can be dependent on peripherals?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.114",
  "question": "What type of disaster recovery test is the simplest to perform?",
  "answer": "B",
  "explanation": "In the read-through test, you distribute copies of the disaster recovery plan to key personnel for review but do not actually meet or perform live testing."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.052",
  "question": "Among the following concepts, which element is not essential for an audit report?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.003",
  "question": "Which of the following is the least acceptable form of biometric device?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.033",
  "question": "Which of the following is a documented set of best IT security practices crafted by Information Systems Audit and Control Association (ISACA) and IT Governance Institute (ITGI)?",
  "answer": "B",
  "explanation": "Control Objectives for Information and Related Technology (COBIT) is a documented set of best IT security practices crafted by Information Systems Audit and Control Association (ISACA) and IT Governance Institute (ITGI)."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.068",
  "question": "What testing exercise would you perform that involves personnel relocation and remote site activation?",
  "answer": "A",
  "explanation": "Parallel tests represent the next level in testing and involve actually relocating personnel to the alternate recovery site and implementing site activation procedures."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.141",
  "question": "The act of ",
  "answer": "A",
  "explanation": "The act of measuring and evaluating security metrics is the practice of assessing the completeness and effectiveness of the security program. This should also include measuring it against common security guidelines and tracking the success of its controls."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.121",
  "question": "How is metadata generated?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.129",
  "question": "What form of attack is always possible when using a non-802.1x implementation of a wireless network?",
  "answer": "A",
  "explanation": "Password guessing is always a potential attack if a wireless network is not otherwise using some other form of authentication, typically accessed via 802.1x."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c10.19",
  "question": "What is the best type of water-based fire suppression system for a computer facility?",
  "answer": "C",
  "explanation": "A preaction system is a combination dry pipe/wet pipe system. The system exists as a dry pipe until the initial stages of a fire (smoke, heat, and so on) are detected, and then the pipes are filled with water. The water is released only after the sprinkler head activation triggers are melted by sufficient heat."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.084",
  "question": "Your organization was recently attacked by an APT. A root-cause analysis indicated that the attack was successful because an employee responded inappropriately to a malicious spam email. What phase of incident response does a root-cause analysis occur?",
  "answer": "D",
  "explanation": "A root-cause analysis typically occurs during the remediation stage. Incidents are identified and verified in the detection stage. During incident response, personnel assess the damage and collect evidence. Personnel isolate compromised systems during the mitigation stage."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.105",
  "question": "The ????? of a process consist of limits set on the memory addresses and resources it can access. This also states or defines the area within which a process is confined.",
  "answer": "B",
  "explanation": "The bounds of a process consist of limits set on the memory addresses and resources it can access. The bounds state or define the area within which a process is confined"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.035",
  "question": "Methodical examination and review of environmental security and regulatory compliance, and a form of directive control, is known as what?",
  "answer": "C",
  "explanation": "Auditing is the periodic examination and review of a network to ensure that it meets security and regulatory compliance."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.025",
  "question": "When NAC is used to manage an enterprise network, what is most likely to happen to a notebook system once reconnected to the intranet after it has been out of the office for six weeks while in use by an executive on an international business trip?",
  "answer": "C",
  "explanation": "NAC often operates in a pre-admission philosophy in which a system must meet all current security requirements (such as patch application and antivirus updates) before it is allowed to communicate with the network. This often means systems that are not in compliance are quarantined or otherwise involved in a captive portal strategy in order to force compliance before network access is restored."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.117",
  "question": "What is the most common reaction to the loss of physical and infrastructure support?",
  "answer": "C",
  "explanation": "In most cases, you must simply wait until the emergency or condition expires and things return to normal. If physical and infrastructure support is lost, such as after a catastrophe, regular activity (including deploying updates, performing scans, or tightening controls) is not possible."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.014",
  "question": "What form of interference is generated by a power differential between hot and ground wires?",
  "answer": "A",
  "explanation": "Common mode noise is generated by the difference in power between the hot and ground wires of a power source or operating electrical equipment."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.124",
  "question": "Who is responsible for the day-to-day maintenance of objects?",
  "answer": "C",
  "explanation": "A custodian is someone who has been assigned to or delegated the day-to-day responsibility of proper storage and protection of objects. A user is any subject who accesses objects on a system to perform some action or accomplish a work task. An owner is the person who has final corporate responsibility for the protection and storage of data. When discussing access to objects, three subject labels are used: user, owner, and custodian. Therefore, administrator is not an appropriate choice."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.111",
  "question": "The Goguen-Meseguer model is an ",
  "answer": "A",
  "explanation": "The Goguen-Meseguer model is an integrity model based on predetermining the set or domain—a list of objects that a subject can access."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.060",
  "question": "What programming environment offered by Microsoft includes the Common Language Interface?",
  "answer": "C",
  "explanation": "The .NET Framework includes the Common Language Interface to support multiple programming languages."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.108",
  "question": "???? is a form of programming attack that is used to either falsify information being sent to a visitor or cause their system to give up information without authorization.",
  "answer": "D",
  "explanation": "XML exploitation is a form of programming attack that is used to either falsify information being sent to a visitor or cause their system to give up information without authorization."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.147",
  "question": "Third-party governance cannot be mandated for whom?",
  "answer": "D",
  "explanation": "Commercial competitors or any other entity that is not directly connected or related to the primary organization cannot have that organization’s third-party governance mandated or forced on them."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.128",
  "question": "What type of information is not normally included in the risk acceptance/mitigation portion of BCP (business continuity plan) documentation?",
  "answer": "C",
  "explanation": "Insurance policies are an example of risk assignment/transference and would not be described in the risk acceptance/mitigation section of the documentation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.093",
  "question": "Which of the following is not a goal of the change control process of configuration management?",
  "answer": "B",
  "explanation": "While most business decisions need to include cost analysis, the change control process of configuration management is not directly concerned with cost effectiveness."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.014",
  "question": "What security services are provided by Kerberos for authentication traffic?",
  "answer": "C",
  "explanation": "Kerberos provides confidentiality and integrity protection security services for authentication traffic using symmetric cryptography to encrypt tickets sent over the network to prove identification and provide authentication. The security services provide by Kerberos are not directly related to availability or nonrepudiation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.055",
  "question": "A team that knows substantial information about its target, including on-site hardware/software inventory and configuration details, is best described as what?",
  "answer": "D",
  "explanation": "Partial-knowledge teams possess a detailed account of organizational assets, including hardware and software inventory, prior to a penetration test."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.047",
  "question": "What is the foundation of user and personnel security?",
  "answer": "B",
  "explanation": "Job descriptions are essential to user and personnel security. Only when it’s based on a job description does a background check have true meaning. Without a job description, auditing and monitoring cannot determine when a user performs tasks outside of their assigned work. Without a job description, administrators do not know what level of access to assign via DAC."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.121",
  "question": "What type of intellectual property protection is best suited for computer software?",
  "answer": "D",
  "explanation": "Trade secrets are one of the best legal protections for computer software."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.012",
  "question": "Of the following choices, what best describes the purpose of a honeypot or a honeynet?",
  "answer": "",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.104",
  "question": "RSA encryption relies on the use of two ????.",
  "answer": "B",
  "explanation": "The strength of RSA encryption relies on the difficulty of factoring the two large prime numbers used to generate the encryption key."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.027",
  "question": "Which of the following passwords uses a challenge-response mechanism to create a one-time password?",
  "answer": "B",
  "explanation": "An asynchronous token generates and displays one-time passwords using a challenge-response process to generate the password. A synchronous token is synchronized with an authentication server and generates synchronous one-time passwords. Static passwords are not one-time passwords but instead stay the same for a period of time. A passphrase is a static password created from an easy-to-remember phrase."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.104",
  "question": "What security model is based on dynamic changes of user privileges and access based on user activity?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.055",
  "question": "What form of security planning is designed to focus on timeframes of approximately one year and may include scheduling of tasks, assignment of responsibilities, hiring plans, maintenance plans, and even acquisition plans?",
  "answer": "D",
  "explanation": "Tactical planning is designed to focus on timeframes of approximately one year and may include scheduling of tasks, assignment of responsibilities, hiring plans, maintenance plans, and even acquisition plans"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.061",
  "question": "What method is not integral to assuring effective and reliable security staffing?",
  "answer": "D",
  "explanation": "Screening, bonding, and training are all vital procedures for ensuring effective and reliable security staffing because they verify the integrity and validate the suitability of said staffers."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.119",
  "question": "What part of the Common Criteria specifies the claims of security from the vendor that are built into a target of evaluation?",
  "answer": "D",
  "explanation": "Security targets (STs) specify the claims of security from the vendor that are built into a TOE."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.108",
  "question": "Who is responsible for authoring the principle that can be summed up as “the enemy knows the system”?",
  "answer": "C",
  "explanation": "Kerckchoffs’s principle states that a cryptographic system should remain secure even when all details of the system, except the key, are public knowledge."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.060",
  "question": "????? is any hardware, software, or administrative policy or procedure that defines and enforces access and restriction rights on an organizational level.",
  "answer": "C",
  "explanation": "Access control is any hardware, software, or organizational administrative policy or procedure that grants or restricts access, monitors and records attempts to access, identifies users attempting to access, and determines whether access is authorized."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.129",
  "question": "A ???? contains levels with various compartments that are isolated from the rest of the security domain.",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.083",
  "question": "What type of system is authorized to process data at different classification levels only when all users have authorized access to those classification levels?",
  "answer": "B",
  "explanation": "Systems running in system-high mode are authorized to process data at different classification levels only if all system users have access to the highest level of classification processed."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.088",
  "question": "What regulation formalizes the prudent man rule, requiring that senior executives of an organization take personal responsibility for ensuring due care?",
  "answer": "D",
  "explanation": "The Federal Sentencing Guidelines formalized the prudent man rule and applied it to information security."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.011",
  "question": "Which of the following actions should never occur during the detection phase of incident response?",
  "answer": "A",
  "explanation": "You should never power down a compromised system during the early stages of incident response because this may destroy valuable evidence stored in volatile memory. The other answers include steps that will isolate a system without destroying evidence in typical scenarios."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.109",
  "question": "What law requires that communications carriers cooperate with federal agencies conducting a wiretap?",
  "answer": "B",
  "explanation": "The Communications Assistance to Law Enforcement Act (CALEA) requires all communications carriers to make wiretaps possible for law enforcement with an appropriate court order."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.022",
  "question": "Which one of the following is not a basic requirement for the admissibility of evidence?",
  "answer": "A",
  "explanation": "To be admissible, evidence must be relevant, material, and competent."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.029",
  "question": "A biometric system is matching subjects to a database using a one-to-many search. What is this providing?",
  "answer": "D",
  "explanation": "Biometric systems using a one-to-many search provide identification by searching a database for a match. Biometric systems using a one-to-one search provide authentication. Biometric systems do not provide authorization or accountability."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.043",
  "question": "Which one of the following files might be modified or created by a companion virus?",
  "answer": "A",
  "explanation": "Companion viruses are self-contained executable files with filenames similar to those of existing system/program files but with a modified extension. The virus file is executed when an unsuspecting user types the filename without the extension at the command prompt."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.107",
  "question": "Which of the following access control techniques uses labels to identify subjects and objects?",
  "answer": "D",
  "explanation": "Mandatory access control uses labels to identify subjects and objects and grants access based on matching labels. Discretionary access control requires all objects to have an owner. Nondiscretionary access control provides centralized access controlled by an administrator. Role-based access control provides access based on membership within a role."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.113",
  "question": "Of the following choices, what is the most appropriate action to take during the mitigation phase of incident response?",
  "answer": "A",
  "explanation": "During the mitigation phase of an incident, you would take steps to isolate and contain the incident. You would gather evidence during the response phase. Personnel notify appropriate people during the reporting phase. Servers are restored during the recovery phase."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.066",
  "question": "The purchasing of insurance is a form of ????.",
  "answer": "B",
  "explanation": "Insurance is a form of risk assignment or transference."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.041",
  "question": "What technology may database developers use to restrict users’ access to a limited subset of database attributes or records?",
  "answer": "D",
  "explanation": "Database views use SQL statements to limit the amount of information that users can view from a table."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.020",
  "question": "All of the following are implications of multilayer protocols except which one?",
  "answer": "D",
  "explanation": "Static IP addressing is not an implication of multilayer protocols; it is the identification feature of the IP protocol.\n VLAN hopping is because it relies upon encapsulating a packet with one VLAN with another package for another VLAN."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.138",
  "question": "QOn what principle does a SYN flood attack operate?",
  "answer": "D",
  "explanation": "SYN flood attacks are targeted at the standard three-way handshake process used by TCP/IP to initiate communication sessions."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.008",
  "question": "The term personal area network is most closely associated with what wireless technology?",
  "answer": "A",
  "explanation": "802.15 (aka Bluetooth) creates personal area networks (PANs)."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.127",
  "question": "Which backup facility is large enough to support current operational capacity and load, including the supportive infrastructure?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.073",
  "question": "What is the primary purpose of most malware today?",
  "answer": "B",
  "explanation": "Most malware is designed to add systems to botnets, where they are later used for other nefarious purposes, such as sending spam or participating in distributed denial-of-service attacks."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.053",
  "question": "Practicing the activities that maintain continued application of security protocol, policy, and procedure is also called what?",
  "answer": "B",
  "explanation": "Due diligence is the action taken to apply, enforce, and perform responsibilities or rules as governed by organizational policy."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.079",
  "question": "Among the following choices, what kind of IDS is considered an expert system?",
  "answer": "A",
  "explanation": "A behavior-based intrusion detection system (IDS) can be labeled an expert system or a pseudo-artificial intelligence system because it can learn and make assumptions about events. In other words, the IDS can act like a human expert by evaluating current events against known events."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.109",
  "question": "During the deencapsulation procedure, the ",
  "answer": "B",
  "explanation": "During the deencapsulation procedure, the Data Link layer strips its information and sends the message up to the Network layer.\n I think this is trying to say the the deencapsulation process starts at the data link layer.\n Its not a good question."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.088",
  "question": "Which of the following network devices is used to connect networks that are using different network protocols?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.106",
  "question": "Which of the following forms of authentication provides the strongest security?",
  "answer": "C",
  "explanation": "C is 2 factor while the others are not.\nAmong these options, passphrase and a smart card provide the strongest authentication security because they deliver two-factor authentication. A password and a PIN are both in the same factor. Despite the security offered by one-time passwords, a two-factor authenticator is stronger."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.076",
  "question": "What is the maximum key length supported by the Advanced Encryption Standard’s Rijndael encryption algorithm?",
  "answer": "C",
  "explanation": "The AES/Rijndael algorithm is capable of operating with 128-, 192-, or 256-bit keys. The algorithm uses a block size equal to the length of the key."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.066",
  "question": "When possible, operations controls should be ????.",
  "answer": "D",
  "explanation": "When possible, operations controls should be invisible, or transparent, to users. This keeps users from feeling hampered by security and reduces their knowledge of the overall security scheme, thus further restricting the likelihood that users will violate system security deliberately."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.128",
  "question": "In what security mode must each user have access approval and valid need to know for all information processed by the system?",
  "answer": "A",
  "explanation": "The scenario presented in the question describes the three characteristics of dedicated mode."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.131",
  "question": "Which security mode provides the most granular control over resources and users?",
  "answer": "B",
  "explanation": "System high mode provides the most granular control over resources and users because it enforces clearances, requires need to know, and allows the processing of only single sensitivity levels. All the other levels either do not have unique need to know between users (dedicated), allow multiple levels of data processing (compartmented), or allow a wide number of users with varying clearance (multilevel)."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.027",
  "question": "Which of the following is the weakest method of authentication?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.007",
  "question": "Which one of the following is not a core principle of the Agile Manifesto?",
  "answer": "B",
  "explanation": "The Agile Manifesto says that you should build projects around motivated individuals and give them the support they need."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.148",
  "question": "Of the following choices, what is a primary benefit when images are used to deploy new systems?",
  "answer": "A",
  "explanation": "When images are used to deploy systems, the systems start with a common baseline, which is important for configuration management. Images don’t necessarily improve the evaluation, approval, deployment, and audits of patches to systems within the network. While images can include current patches to reduce their vulnerabilities, this is because the image provides a baseline. Change management provides documentation for changes."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.099",
  "question": "Which of the following is not a benefit of tunneling?",
  "answer": "D",
  "explanation": "Tunneling is generally an inefficient means of communicating because all protocols include their own error detection, error handling, acknowledgment, and session management features, and using more than one protocol at a time just compounds the overhead required to communicate a single message."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.003",
  "question": "Which of the following comes first?",
  "answer": "C",
  "explanation": "Trust comes first. Trust is built into a system by crafting the components of security. Then assurance (in other words, reliability) is evaluated using certification and/or accreditation processes."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.059",
  "question": "What form of interference is generated by a difference in power between hot and neutral wires of a power source?",
  "answer": "C",
  "explanation": "Traverse mode noise is generated by the difference in power between the hot and neutral wires of a power source or operating electrical equipment."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.137",
  "question": "The act of performing a(n) ???  in order to drive the security policy is the clearest and most direct example of management of the security function.",
  "answer": "C",
  "explanation": "The act of performing a risk assessment in order to drive the security policy is the clearest and most direct example of management of the security function."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.072",
  "question": "Which type of control provides extended options to existing controls and aids or supports administrative security policy?",
  "answer": "D",
  "explanation": "Compensation access control is deployed to provide various options to existing controls to help enforce and support a security policy."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.060",
  "question": "Generally, a privacy policy is designed to protect what?",
  "answer": "D",
  "explanation": "The purpose of a privacy policy is to inform users where they do and do not have privacy for the primary benefit of the protection of the company’s right to audit and monitor user activity."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.091",
  "question": "What amendment to the US Constitution protects individuals against wiretapping and invasions of privacy?",
  "answer": "B",
  "explanation": "The Fourth Amendment, as interpreted by the courts, includes protections against wiretapping and other invasions of privacy."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.092",
  "question": "Which backup facility is large enough to support current operational capacity and load but lacks the supportive infrastructure?",
  "answer": "D",
  "explanation": "A cold site is any facility that provides only the physical space for recovery operations while the organization using the space provides its own hardware and software systems."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.057",
  "question": "Alan conducted a vulnerability scan of a system and discovered that it is susceptible to a SQL injection attack. Which one of the following ports would an attacker most likely use to carry out this attack?",
  "answer": "A",
  "explanation": "While SQL injection attacks do target databases, they do so by using web servers as intermediaries. Therefore, SQL injection attacks take place over web ports, such as 80 and 443, and not database ports, such as 1433 and 1521."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.026",
  "question": "When conducting an internal investigation, what is the most common source of evidence?",
  "answer": "D",
  "explanation": "Internal investigations usually operate under the authority of senior managers, who grant access (i.e., voluntary surrender) to all information and resources necessary to conduct the investigation."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.073",
  "question": "During what phase of incident response do you collect evidence such as firewall logs?",
  "answer": "B",
  "explanation": "Evidence collection takes place during the response phase of the incident. Incidents are identified and verified during the detection phase. Compliance with laws might occur during the reporting phase, depending on the incident. Personnel typically perform a root-cause analysis during the remediation phase."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.006",
  "question": "What aspect of an organizational security policy, procedure, or process affects all other aspects?",
  "answer": "D",
  "explanation": "Physical security is the most prominent aspect of an organizational security policy because it directly and indirectly influences all other forms. Without physical security, no other processes and procedures are reliable."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.089",
  "question": "Which one of the following types of attack is most difficult to defend against?",
  "answer": "D",
  "explanation": "It is difficult to defend against distributed denial-of-service attacks because of their sophistication and complexity."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.098",
  "question": "When you are configuring a wireless extension to an intranet, once you’ve configured WPA-2 with 802.1x authentication, what additional security step could you implement in order to offer additional reliable security?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.127",
  "question": "Which one of the following methods is not a valid method of destroying data on a hard drive?",
  "answer": "B",
  "explanation": "Copying data over existing data is not reliable because data may remain on the drive as data remanence. Some software programs can overwrite the data with patterns of 1s and 0s to destroy the data. Shredding or disintegrating the platters will destroy the data."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.026",
  "question": "What is the most important aspect of a biometric device?",
  "answer": "A",
  "explanation": "The most important aspect of a biometric factor is its accuracy. If a biometric factor is not accurate, it may allow unauthorized users into a system. Acceptability by users, the amount of time it takes to enroll, and the invasiveness of the biometric device are additional considerations but not as important as its accuracy."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.115",
  "question": "What is confidentiality dependent on?",
  "answer": "A",
  "explanation": "Without object integrity, confidentiality cannot be maintained. In fact, integrity and confidentiality depend on one another."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.059",
  "question": "What phase of the Electronic Discovery Reference Model performs a rough cut of irrelevant information?",
  "answer": "B",
  "explanation": "Processing screens the collected information to perform a “rough cut” of irrelevant information, reducing the amount of information requiring detailed screening."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.057",
  "question": "A(n) ??? system is one in which all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment.",
  "answer": "D",
  "explanation": "A trusted system is one in which all protection mechanisms work together to process sensitive data for many types of users while maintaining a stable and secure computing environment."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.003",
  "question": "How might you map out the organizational needs for transfer to or establishment in a new facility?",
  "answer": "D",
  "explanation": "Critical path analysis can be defined as the logical sequencing of a series of events such that planners and integrators possess considerable information for the decision-making process."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.036",
  "question": "The Clark-Wilson access model is also called a(n) ",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.098",
  "question": "What character, if eliminated from all web form input, would prevent the execution of many cross-site scripting attacks?",
  "answer": "<",
  "explanation": "This is required for the script tag"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.116",
  "question": "What type of attack uses malicious email and targets a group of employees within a single company?",
  "answer": "B",
  "explanation": "Spear phishing targets a specific group of people such as a group of employees within a single company. Phishing goes to anyone without any specific target. Whaling is a form of phishing that targets high-level executives. Vishing is a form of phishing that commonly uses Voice over IP (VoIP)."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.116",
  "question": "A ",
  "answer": "A",
  "explanation": "Hybrid environments combine both hierarchical and compartmentalized environments so that security levels have subcompartments."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.011",
  "question": "What is the frequency of an IT infrastructure security audit or security review based on?",
  "answer": "C",
  "explanation": "The frequency of an IT infrastructure security audit or security review is based on risk. You must establish the existence of sufficient risk to warrant the expense of, and interruption caused by, a security audit on a more or less frequent basis. Asset value and threats are part of risk but are not the whole picture, and assessments are not performed based only on either of these. A high-value asset with a low level of threats doesn’t present a high risk. Similarly, a low-value asset with a high level of threats doesn’t present a high risk. The decision to perform an audit isn’t usually relegated to an administrator."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.097",
  "question": "In what type of attack does the intruder initiate connections to both a client and a server?",
  "answer": "C",
  "explanation": "In the man-in-the-middle attack, a malicious individual sits between two communicating parties and intercepts all communications (including the setup of the cryptographic session).\n Meet-in-the middle is about having the some plain text and some output ciphertext and iterating through all k1 and k2 keys to find the combination that produces what is known."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.047",
  "question": "Which of the following provides the best protection against mishandling media that contains sensitive information?",
  "answer": "A",
  "explanation": "Marking (or labeling) media is the best choice of the available answers to protect against mishandling media. When properly marked, personnel are more likely to handle media properly. Purging and sanitizing methods remove sensitive information but do not protect against mishandling. Data retention refers to how long an organization keeps the data, not how it handles the data."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.004",
  "question": "What type of virus modifies its own code as it travels from system to system in an attempt to evade signature detection?",
  "answer": "A",
  "explanation": "Polymorphic viruses actually modify their own code as they travel from system to system. The virus’s propagation and destruction techniques remain the same, but the signature of the virus is somewhat different each time it infects a new system."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.039",
  "question": "Which of the following is not true?",
  "answer": "A",
  "explanation": "Avoid combining policies, standards, baselines, guidelines, and procedures in a single document. Each of these structures must exist as a separate entity because each performs a different specialized function."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.086",
  "question": "Which is the most common form of perimeter security device or mechanism for any given business?",
  "answer": "D",
  "explanation": "Lighting is by far the most pervasive and basic element of security because it illuminates areas and makes signs of hidden danger visible to all."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.119",
  "question": "What security principle states that a thorough understanding of a system’s operational details is not necessary for most routine activities?",
  "answer": "B",
  "explanation": "Abstraction states that a detailed understanding of lower system levels is not a necessary requirement for working at higher levels."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.079",
  "question": "The University of Outer Mongolia runs a web application that processes student tuition payments via credit card and is subject to PCI DSS. The university does not wish to perform web vulnerability scans on a regular basis because they consider them too time-consuming. What technology may they put in place that eliminates the PCI DSS requirement for recurring web vulnerability scans?",
  "answer": "A",
  "explanation": "PCI DSS allows organizations to choose between performing annual web vulnerability assessment tests or installing a web application firewall."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.005",
  "question": "What form of mobile device management provides users with a list of approved devices from which to select the device to implement?",
  "answer": "B",
  "explanation": "The concept of CYOD (choose your own device) provides users with a list of approved devices from which to select the device to implement. BYOD (bring your own device) is a policy that allows employees to bring their own personal mobile devices into work and use those devices to connect to (or through) the company network to business resources and/or the Internet. The concept of COPE (company-owned, personally enabled) is for the organization to purchase devices and provide them to employees. OCSP (Online Certificate Status Protocol) is used to check the revocation status of certificates."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.046",
  "question": "What is the countermeasure cost/benefit equation?",
  "answer": "C",
  "explanation": "To make the determination of whether the safeguard is financially equitable, use the following countermeasure cost/benefit equation: (ALE before countermeasure – ALE after implementing the countermeasure) – annual cost of countermeasure = value of the countermeasure to the company."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.078",
  "question": "What is the purpose of a security impact analysis in the context of change management?",
  "answer": "D",
  "explanation": "A security impact analysis reviews change requests and evaluates them for potential negative impacts. All changes aren’t necessarily approved or rejected. The analysis doesn’t attempt to identify changes."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.065",
  "question": "Which of the following describes putting similar elements into groups, classes, or roles that are assigned security controls, restrictions, or permissions as a collective?",
  "answer": "B",
  "explanation": "Abstraction describes putting similar elements into groups, classes, or roles that are assigned security controls, restrictions, or permissions as a collective."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.074",
  "question": "Which of the following is not a valid issue to consider when evaluating a safeguard?",
  "answer": "B",
  "explanation": "New safeguards establish new baselines; thus, compliance with existing baselines is not a valid consideration point."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be2.114",
  "question": "Which of the following is not a valid security measure to protect against brute-force and dictionary attacks?",
  "answer": "C",
  "explanation": "Requiring users to log in remotely does not protect against password attacks such as brute-force or dictionary attacks. Strong password policies, physical access control, and two-factor authentication all improve the protection against brute-force and dictionary password attacks."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.004",
  "question": "Which backup format stores only those files that have been set with the archive bit and have been modified since the last complete backup?",
  "answer": "A",
  "explanation": "Differential backups store all files that have been modified since the time of the most recent full backup; they affect only those files that have the archive bit turned on, enabled, or set to 1."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be3.018",
  "question": "An organization wants to implement a cloud-based service using a combination of two separate clouds. Which deployment model should they choose?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.031",
  "question": "Which of the following is not an element defined under the Clark-Wilson model?",
  "answer": "C",
  "explanation": "A constrained data item (CDI) is any data item whose integrity is protected by the security model. An unconstrained data item (UDI) is any data item that is not controlled by the security model. Any data that is to be input and hasn’t been validated, or any output, would be considered an unconstrained data item. An integrity verification procedure (IVP) is a procedure that scans data items and confirms their integrity. Transformation procedures (TPs) are the only procedures that are allowed to modify a CDI. The limited access to CDIs through TPs forms the backbone of the Clark-Wilson integrity model."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.052",
  "question": "A person who illicitly gains the trust or credentials from a trusted party has committed what criminal act?",
  "answer": "D",
  "explanation": "Social engineering is an attempt to deceive an insider into performing questionable actions on behalf of some unauthorized outsider."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.079",
  "question": "What procedure returns business facilities and environments to a working state?",
  "answer": "B",
  "explanation": "Disaster restoration involves restoring a business facility and environment to a workable state."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.022",
  "question": "What is an access control list (ACL) based on?",
  "answer": "A",
  "explanation": "An ACL is based on an object and includes a list of subjects that are granted access. A capability table is focused on a subject and includes a list of objects the subject can access. Roles and accounts are examples of subjects and may be included in an ACL, but they aren’t the focus."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.045",
  "question": "Any process, mechanism, or tool that guides an organizational security implementation is what type of control?",
  "answer": "Directive controls guide an organizational security implementation and as such are control statements.",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.064",
  "question": "Which one of the following is not a part of documenting the business continuity plan?",
  "answer": "C",
  "explanation": "BCP documentation can be an arduous task, but it should not require the creation of a new position."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.063",
  "question": "The Twofish algorithm uses an encryption technique not found in other algorithms that XORs the plain text with a separate subkey before the first round of encryption. What is this called?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.022",
  "question": "What type of processing makes use of a multithreading technique at the operating system level?",
  "answer": "",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.082",
  "question": "What attack involves an interruptive malicious user positioned between a client and server attempting to take over?",
  "answer": "C",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be4.036",
  "question": "Tom built a database table consisting of the names, telephone numbers, and customer IDs for his business. The table contains information on 30 customers. What is the cardinality of this table?",
  "answer": "C",
  "explanation": "The cardinality of a table refers to the number of rows in the table, whereas the degree of a table is the number of columns."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.105",
  "question": "Which of the following requirements does not come from the Children’s Online Privacy Protection Act?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be5.045",
  "question": "What is a hardware-imposed network segmentation that requires a routing function to support intersegment communications otherwise known as?",
  "answer": "C",
  "explanation": "A VLAN (virtual LAN) is a hardware-imposed network segmentation created by switches that requires a routing function to support communication between different segments."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c01.13",
  "question": "Which of the following is not considered an example of data hiding?",
  "answer": "A",
  "explanation": "Preventing an authorized reader of an object from deleting that object is just an example of access control, not data hiding. If you can read an object, it is not hidden from you."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c02.17",
  "question": "What process or event is typically hosted by an organization and is targeted to groups of employees with similar job functions?",
  "answer": "C",
  "explanation": "Training is teaching employees to perform their work tasks and to comply with the security policy. Training is typically hosted by an organization and is targeted to groups of employees with similar job functions."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c02.10",
  "question": "QWhich of the following represents accidental or intentional exploitations of vulnerabilities?",
  "answer": "C",
  "explanation": "Threat events are accidental or intentional exploitations of vulnerabilities.\nBreaches are intentional bypassing of controls"
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.c02.20",
  "question": "You’ve performed a basic quantitative risk analysis on a specific threat/vulnerability/risk relation. You select a possible countermeasure. When performing the calculations again, which of the following factors will change?",
  "answer": "D",
  "explanation": "A countermeasure directly affects the annualized rate of occurrence, primarily because the counter-measure is designed to prevent the occurrence of the risk, thus reducing its frequency per year.\n What is changing here is the introduction of the countermeasure."
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be6.137",
  "question": "Which of the following attacks is the best example of a financial attack?",
  "answer": "Phone phreaking attacks are designed to obtain service while avoiding financial costs.",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "tb475934.CISSPSG8E.be1.065",
  "question": "Once a system is compromised, ",
  "answer": "D",
  "explanation": "Corrective access control is deployed to restore systems to normal after an unwanted or unauthorized activity has occurred."
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d-A",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d",
  "choice": "A",
  "answer": "IPSEC"
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d-B",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d",
  "choice": "B",
  "answer": "VPN"
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d-C",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d",
  "choice": "C",
  "answer": "SLA"
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d-D",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d",
  "choice": "D",
  "answer": "BCP"
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294-A",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294",
  "choice": "A",
  "answer": "Full Interruption Test"
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294-B",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294",
  "choice": "B",
  "answer": "Parallel Test"
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294-C",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294",
  "choice": "C",
  "answer": "Simulation Test"
 },
 {
  "type": "ANSWER",
  "id": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294-D",
  "QuestionId": "034e318f-259a-4342-a5cc-f00081ae227d024c2bbe-104a-40bc-8200-588107d4d294",
  "choice": "D",
  "answer": "Structure Walk Through"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.015-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.015",
  "choice": "A",
  "answer": "Federal database for CIA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.015-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.015",
  "choice": "B",
  "answer": "Kerberos"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.015-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.015",
  "choice": "C",
  "answer": "Diameter"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.015-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.015",
  "choice": "D",
  "answer": "SSO"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.107-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.107",
  "choice": "A",
  "answer": "Passive Device"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.107-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.107",
  "choice": "B",
  "answer": "Self Powered Device"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.107-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.107",
  "choice": "C",
  "answer": "Field Powered Device"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.107-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.107",
  "choice": "D",
  "answer": "Transponder"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.038-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.038",
  "choice": "A",
  "answer": "Sarbanes-Oxley Act 2002"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.038-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.038",
  "choice": "B",
  "answer": "COBIT"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.038-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.038",
  "choice": "C",
  "answer": "Accreditation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.038-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.038",
  "choice": "D",
  "answer": "Top-down approach"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.007-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.007",
  "choice": "A",
  "answer": "Incremental backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.007-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.007",
  "choice": "B",
  "answer": "Differential backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.007-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.007",
  "choice": "C",
  "answer": "Full backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.007-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.007",
  "choice": "D",
  "answer": "Electronic vaulting"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.046-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.046",
  "choice": "A",
  "answer": "Traffic isolation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.046-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.046",
  "choice": "B",
  "answer": "Data/traffic encryption"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.046-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.046",
  "choice": "C",
  "answer": "Traffic management"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.046-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.046",
  "choice": "D",
  "answer": "Reduced vulnerability to sniffers"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.063-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.063",
  "choice": "A",
  "answer": "Host based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.063-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.063",
  "choice": "B",
  "answer": "Network based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.063-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.063",
  "choice": "C",
  "answer": "Knowledge based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.063-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.063",
  "choice": "D",
  "answer": "Behavior based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.038-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.038",
  "choice": "A",
  "answer": "CIA Triad"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.038-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.038",
  "choice": "B",
  "answer": "CIA Triplex"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.038-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.038",
  "choice": "C",
  "answer": "Operations Security Tetrahedron"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.038-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.038",
  "choice": "D",
  "answer": "Operations security triple"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.084-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.084",
  "choice": "A",
  "answer": "Patent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.084-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.084",
  "choice": "B",
  "answer": "Copyright"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.084-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.084",
  "choice": "C",
  "answer": "Trademark"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.084-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.084",
  "choice": "D",
  "answer": "Trade secret"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.150-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.150",
  "choice": "A",
  "answer": "SaaS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.150-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.150",
  "choice": "B",
  "answer": "PaaS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.150-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.150",
  "choice": "C",
  "answer": "IaaS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.150-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.150",
  "choice": "D",
  "answer": "SECaaS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.055-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.055",
  "choice": "A",
  "answer": "An open system does not allow anyone to view its programming code."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.055-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.055",
  "choice": "B",
  "answer": "A closed system does not define whether or not its programming code can be viewed."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.055-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.055",
  "choice": "C",
  "answer": "An open source program can only be distributed for free."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.055-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.055",
  "choice": "D",
  "answer": "A closed source program cannot be reverse engineered or decompiled."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.084-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.084",
  "choice": "A",
  "answer": "Split-knowledge proof"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.084-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.084",
  "choice": "B",
  "answer": "Work function"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.084-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.084",
  "choice": "C",
  "answer": "Digital signature"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.084-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.084",
  "choice": "D",
  "answer": "Zero-knowledge proof"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be6.104-A",
  "QuestionId": "b475934.CISSPSG8E.be6.104",
  "choice": "A",
  "answer": "Nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be6.104-B",
  "QuestionId": "b475934.CISSPSG8E.be6.104",
  "choice": "B",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be6.104-C",
  "QuestionId": "b475934.CISSPSG8E.be6.104",
  "choice": "C",
  "answer": "Availability"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be6.104-D",
  "QuestionId": "b475934.CISSPSG8E.be6.104",
  "choice": "D",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.026-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.026",
  "choice": "A",
  "answer": "Recording of event data"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.026-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.026",
  "choice": "B",
  "answer": "Data reduction"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.026-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.026",
  "choice": "C",
  "answer": "Log analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.026-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.026",
  "choice": "D",
  "answer": "Deployment of countermeasures"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.096-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.096",
  "choice": "A",
  "answer": "Rivest"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.096-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.096",
  "choice": "B",
  "answer": "AES"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.096-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.096",
  "choice": "C",
  "answer": "10 system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.096-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.096",
  "choice": "D",
  "answer": "Skipjack"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be5.059-A",
  "QuestionId": "b475934.CISSPSG8E.be5.059",
  "choice": "A",
  "answer": "$25,000,000"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be5.059-B",
  "QuestionId": "b475934.CISSPSG8E.be5.059",
  "choice": "B",
  "answer": "$12,500,000"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be5.059-C",
  "QuestionId": "b475934.CISSPSG8E.be5.059",
  "choice": "C",
  "answer": "$250,000"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be5.059-D",
  "QuestionId": "b475934.CISSPSG8E.be5.059",
  "choice": "D",
  "answer": "$125,000"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.061-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.061",
  "choice": "A",
  "answer": "Continuity strategy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.061-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.061",
  "choice": "B",
  "answer": "Quantitative analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.061-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.061",
  "choice": "C",
  "answer": "Likelihood assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.061-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.061",
  "choice": "D",
  "answer": "Qualitative analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.039-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.039",
  "choice": "A",
  "answer": "CIA Triad"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.039-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.039",
  "choice": "B",
  "answer": "Concentric circle"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.039-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.039",
  "choice": "C",
  "answer": "Operations security triple"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.039-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.039",
  "choice": "D",
  "answer": "Separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.114-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.114",
  "choice": "A",
  "answer": "Take-Grant model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.114-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.114",
  "choice": "B",
  "answer": "State machine model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.114-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.114",
  "choice": "C",
  "answer": "Brewer and Nash model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.114-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.114",
  "choice": "D",
  "answer": "Clark Wilson model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.047-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.047",
  "choice": "A",
  "answer": "An electromagnet"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.047-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.047",
  "choice": "B",
  "answer": "A credential reader"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.047-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.047",
  "choice": "C",
  "answer": "A door sensor"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.047-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.047",
  "choice": "D",
  "answer": "A biometric scanner"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.030-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.030",
  "choice": "A",
  "answer": "VLAN hopping"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.030-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.030",
  "choice": "B",
  "answer": "Caller ID falsification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.030-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.030",
  "choice": "C",
  "answer": "Vishing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.030-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.030",
  "choice": "D",
  "answer": "SPIT"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.10-A",
  "QuestionId": "tb475934.CISSPSG8E.c08.10",
  "choice": "A",
  "answer": "The boundary of the physically secure area surrounding your system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.10-B",
  "QuestionId": "tb475934.CISSPSG8E.c08.10",
  "choice": "B",
  "answer": "The imaginary boundary that separates the TCB from the rest of the system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.10-C",
  "QuestionId": "tb475934.CISSPSG8E.c08.10",
  "choice": "C",
  "answer": "The network where your firewall resides"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.10-D",
  "QuestionId": "tb475934.CISSPSG8E.c08.10",
  "choice": "D",
  "answer": "Any connections to your computer system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.029-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.029",
  "choice": "A",
  "answer": "Tailgating"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.029-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.029",
  "choice": "B",
  "answer": "Masquerading"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.029-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.029",
  "choice": "C",
  "answer": "Impersonation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.029-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.029",
  "choice": "D",
  "answer": "Piggybacking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.at.27-A",
  "QuestionId": "tb475934.CISSPSG8E.at.27",
  "choice": "A",
  "answer": "Logging usage data"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.at.27-B",
  "QuestionId": "tb475934.CISSPSG8E.at.27",
  "choice": "B",
  "answer": "War dialing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.at.27-C",
  "QuestionId": "tb475934.CISSPSG8E.at.27",
  "choice": "C",
  "answer": "Penetration testing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.at.27-D",
  "QuestionId": "tb475934.CISSPSG8E.at.27",
  "choice": "D",
  "answer": "Deploying secured desktop workstations"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.11-A",
  "QuestionId": "tb475934.CISSPSG8E.c09.11",
  "choice": "A",
  "answer": "Hard disk"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.11-B",
  "QuestionId": "tb475934.CISSPSG8E.c09.11",
  "choice": "B",
  "answer": "Backup tape"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.11-C",
  "QuestionId": "tb475934.CISSPSG8E.c09.11",
  "choice": "C",
  "answer": "Removable drives"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.11-D",
  "QuestionId": "tb475934.CISSPSG8E.c09.11",
  "choice": "D",
  "answer": "RAM"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.049-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.049",
  "choice": "A",
  "answer": "Block inbound packets whose source address is an internal address"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.049-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.049",
  "choice": "B",
  "answer": "Block outbound packets whose source address is an external address"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.049-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.049",
  "choice": "C",
  "answer": "Block outbound packets whose source address is an unassigned internal address"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.049-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.049",
  "choice": "D",
  "answer": "Block inbound packets whose source address is on a block/black list"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.077-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.077",
  "choice": "A",
  "answer": "Hot site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.077-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.077",
  "choice": "B",
  "answer": "Warm site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.077-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.077",
  "choice": "C",
  "answer": "Cold site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.077-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.077",
  "choice": "D",
  "answer": "MAA site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.05-A",
  "QuestionId": "tb475934.CISSPSG8E.c09.05",
  "choice": "A",
  "answer": "Software flaws"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.05-B",
  "QuestionId": "tb475934.CISSPSG8E.c09.05",
  "choice": "B",
  "answer": "Access to the internet"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.05-C",
  "QuestionId": "tb475934.CISSPSG8E.c09.05",
  "choice": "C",
  "answer": "Control of a mechanism in the physical world"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.05-D",
  "QuestionId": "tb475934.CISSPSG8E.c09.05",
  "choice": "D",
  "answer": "Power loss"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.063-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.063",
  "choice": "A",
  "answer": "Risk identification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.063-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.063",
  "choice": "B",
  "answer": "Resource prioritization"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.063-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.063",
  "choice": "C",
  "answer": "Impact assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.063-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.063",
  "choice": "D",
  "answer": "Likelihood assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.124-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.124",
  "choice": "A",
  "answer": "The digital certificate of the CA is authentic."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.124-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.124",
  "choice": "B",
  "answer": "You trust the sender of the certificate."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.124-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.124",
  "choice": "C",
  "answer": "The certificate is not listed on a CRL."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.124-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.124",
  "choice": "D",
  "answer": "The certificate actually contains the public key."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.040-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.040",
  "choice": "A",
  "answer": "Data restoration"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.040-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.040",
  "choice": "B",
  "answer": "Personal safety"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.040-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.040",
  "choice": "C",
  "answer": "Containing damage"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.040-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.040",
  "choice": "D",
  "answer": "Activating an alternate site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.005-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.005",
  "choice": "A",
  "answer": "25"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.005-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.005",
  "choice": "B",
  "answer": "110"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.005-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.005",
  "choice": "C",
  "answer": "68"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.005-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.005",
  "choice": "D",
  "answer": "443"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.117-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.117",
  "choice": "A",
  "answer": "Unauthorized subjects should be prevented from making modifications."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.117-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.117",
  "choice": "B",
  "answer": "Authorized subjects should be prevented from making unauthorized modifications."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.117-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.117",
  "choice": "C",
  "answer": "Objects should be available for access at all times without interruption to authorized individuals."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.117-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.117",
  "choice": "D",
  "answer": "Objects should be internally and externally consistent so that their data is a correct and true reflection of the real world and any relationship with any child, peer, or parent object is valid, consistent, and verifiable."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.135-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.135",
  "choice": "A",
  "answer": "People"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.135-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.135",
  "choice": "B",
  "answer": "Documentation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.135-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.135",
  "choice": "C",
  "answer": "Infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.135-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.135",
  "choice": "D",
  "answer": "Buildings/facilities"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.010-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.010",
  "choice": "A",
  "answer": "Software Capability Maturity model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.010-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.010",
  "choice": "B",
  "answer": "Waterfall model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.010-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.010",
  "choice": "C",
  "answer": "Development cycle"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.010-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.010",
  "choice": "D",
  "answer": "Spiral model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.134-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.134",
  "choice": "A",
  "answer": "Feedback"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.134-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.134",
  "choice": "B",
  "answer": "Hookup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.134-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.134",
  "choice": "C",
  "answer": "Cascading"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.134-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.134",
  "choice": "D",
  "answer": "Waterfall"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.017-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.017",
  "choice": "A",
  "answer": "Protocol data unit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.017-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.017",
  "choice": "B",
  "answer": "Segment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.017-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.017",
  "choice": "C",
  "answer": "Datagram"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.017-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.017",
  "choice": "D",
  "answer": "Frame"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c11.13-A",
  "QuestionId": "tb475934.CISSPSG8E.c11.13",
  "choice": "A",
  "answer": "They are able to log traffic information."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c11.13-B",
  "QuestionId": "tb475934.CISSPSG8E.c11.13",
  "choice": "B",
  "answer": "They are able to block viruses."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c11.13-C",
  "QuestionId": "tb475934.CISSPSG8E.c11.13",
  "choice": "C",
  "answer": "They are able to issue alarms based on suspected attacks."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c11.13-D",
  "QuestionId": "tb475934.CISSPSG8E.c11.13",
  "choice": "D",
  "answer": "They are unable to prevent internal attacks."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.032-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.032",
  "choice": "A",
  "answer": "Known plain-text attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.032-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.032",
  "choice": "B",
  "answer": "Frequency analysis attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.032-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.032",
  "choice": "C",
  "answer": "Chosen cipher-text attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.032-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.032",
  "choice": "D",
  "answer": "Meet-in-the-middle attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.147-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.147",
  "choice": "A",
  "answer": "Accountability logging"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.147-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.147",
  "choice": "B",
  "answer": "ACL verification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.147-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.147",
  "choice": "C",
  "answer": "Single sign-on"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.147-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.147",
  "choice": "D",
  "answer": "Trusted OS hardening"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.05-A",
  "QuestionId": "tb475934.CISSPSG8E.c08.05",
  "choice": "A",
  "answer": "A resource a user or process wants to access"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.05-B",
  "QuestionId": "tb475934.CISSPSG8E.c08.05",
  "choice": "B",
  "answer": "A user or process that wants to access a resource"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.05-C",
  "QuestionId": "tb475934.CISSPSG8E.c08.05",
  "choice": "C",
  "answer": "A list of valid access rules"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.05-D",
  "QuestionId": "tb475934.CISSPSG8E.c08.05",
  "choice": "D",
  "answer": "The sequence of valid access types"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.130-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.130",
  "choice": "A",
  "answer": "Automated"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.130-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.130",
  "choice": "B",
  "answer": "Manual"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.130-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.130",
  "choice": "C",
  "answer": "Function"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.130-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.130",
  "choice": "D",
  "answer": "Controlled"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.003-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.003",
  "choice": "A",
  "answer": "VAST"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.003-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.003",
  "choice": "B",
  "answer": "Trike"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.003-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.003",
  "choice": "C",
  "answer": "STRIDE"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.003-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.003",
  "choice": "D",
  "answer": "DREAD"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.05-A",
  "QuestionId": "tb475934.CISSPSG8E.c10.05",
  "choice": "A",
  "answer": "It must be human compatible."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.05-B",
  "QuestionId": "tb475934.CISSPSG8E.c10.05",
  "choice": "B",
  "answer": "It must include the use of nonwater fire suppressants."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.05-C",
  "QuestionId": "tb475934.CISSPSG8E.c10.05",
  "choice": "C",
  "answer": "The humidity must be kept between 40 and 60 percent."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.05-D",
  "QuestionId": "tb475934.CISSPSG8E.c10.05",
  "choice": "D",
  "answer": "The temperature must be kept between 60 and 75 degrees Fahrenheit."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c14.14-A",
  "QuestionId": "tb475934.CISSPSG8E.c14.14",
  "choice": "A",
  "answer": "Discretionary Access Control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c14.14-B",
  "QuestionId": "tb475934.CISSPSG8E.c14.14",
  "choice": "B",
  "answer": "Nondiscretionary Access Control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c14.14-C",
  "QuestionId": "tb475934.CISSPSG8E.c14.14",
  "choice": "C",
  "answer": "Mandatory Access Control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c14.14-D",
  "QuestionId": "tb475934.CISSPSG8E.c14.14",
  "choice": "D",
  "answer": "Compliance-Based Access Control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.051-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.051",
  "choice": "A",
  "answer": "Restrictive control over physical access"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.051-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.051",
  "choice": "B",
  "answer": "Policy-driven strong password enforcement"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.051-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.051",
  "choice": "C",
  "answer": "Two-factor authentication deployment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.051-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.051",
  "choice": "D",
  "answer": "Requiring authentication timeouts"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.101-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.101",
  "choice": "A",
  "answer": "Candidate key"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.101-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.101",
  "choice": "B",
  "answer": "Primary key"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.101-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.101",
  "choice": "C",
  "answer": "Unique key"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.101-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.101",
  "choice": "D",
  "answer": "Foreign key"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.056-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.056",
  "choice": "A",
  "answer": "Sniffing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.056-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.056",
  "choice": "B",
  "answer": "Eavesdropping"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.056-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.056",
  "choice": "C",
  "answer": "Snooping"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.056-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.056",
  "choice": "D",
  "answer": "War dialing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.049-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.049",
  "choice": "A",
  "answer": "Semantic integrity mechanism"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.049-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.049",
  "choice": "B",
  "answer": "Concurrency"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.049-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.049",
  "choice": "C",
  "answer": "Polyinstantiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.049-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.049",
  "choice": "D",
  "answer": "Database partitioning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.02-A",
  "QuestionId": "tb475934.CISSPSG8E.c08.02",
  "choice": "A",
  "answer": "Formal acceptance of a stated system configuration"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.02-B",
  "QuestionId": "tb475934.CISSPSG8E.c08.02",
  "choice": "B",
  "answer": "A functional evaluation of the manufacturer’s goals for each hardware and software component to meet integration standards"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.02-C",
  "QuestionId": "tb475934.CISSPSG8E.c08.02",
  "choice": "C",
  "answer": "Acceptance of test results that prove the computer system enforces the security policy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.02-D",
  "QuestionId": "tb475934.CISSPSG8E.c08.02",
  "choice": "D",
  "answer": "The process to specify secure communication between machines"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.148-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.148",
  "choice": "A",
  "answer": "Banner grabbing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.148-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.148",
  "choice": "B",
  "answer": "Port scanning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.148-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.148",
  "choice": "C",
  "answer": "Open source intelligence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.148-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.148",
  "choice": "D",
  "answer": "Enumeration"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.009-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.009",
  "choice": "A",
  "answer": "5 GHz"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.009-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.009",
  "choice": "B",
  "answer": "900 MHz"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.009-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.009",
  "choice": "C",
  "answer": "7 GHz"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.009-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.009",
  "choice": "D",
  "answer": "2.4 GHz"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.012-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.012",
  "choice": "A",
  "answer": "Error checking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.012-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.012",
  "choice": "B",
  "answer": "Redundancy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.012-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.012",
  "choice": "C",
  "answer": "Flow control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.012-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.012",
  "choice": "D",
  "answer": "Bandwidth on demand"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.062-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.062",
  "choice": "A",
  "answer": "Mission-critical data center"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.062-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.062",
  "choice": "B",
  "answer": "Main workspaces"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.062-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.062",
  "choice": "C",
  "answer": "Server vault"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.062-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.062",
  "choice": "D",
  "answer": "Wiring closet"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.022-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.022",
  "choice": "A",
  "answer": "Send large amounts of data to a victim"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.022-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.022",
  "choice": "B",
  "answer": "Cause a buffer overflow"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.022-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.022",
  "choice": "C",
  "answer": "Hide the identity of an attacker through misdirection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.022-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.022",
  "choice": "D",
  "answer": "Steal user account names and passwords"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.052-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.052",
  "choice": "A",
  "answer": "Detective access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.052-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.052",
  "choice": "B",
  "answer": "Preventive access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.052-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.052",
  "choice": "C",
  "answer": "Corrective access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.052-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.052",
  "choice": "D",
  "answer": "Deterrent access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.137-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.137",
  "choice": "A",
  "answer": "External connection attempts"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.137-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.137",
  "choice": "B",
  "answer": "Execution of malicious code"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.137-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.137",
  "choice": "C",
  "answer": "Access to controlled objects"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.137-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.137",
  "choice": "D",
  "answer": "None of the above"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.024-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.024",
  "choice": "A",
  "answer": "Large number of blocked connection attempts on port 22"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.024-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.024",
  "choice": "B",
  "answer": "Large number of successful connection attempts on port 80"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.024-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.024",
  "choice": "C",
  "answer": "Large number of successful connection attempts on port 443"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.024-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.024",
  "choice": "D",
  "answer": "Large number of disk failure events"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be4.052-A",
  "QuestionId": "b475934.CISSPSG8E.be4.052",
  "choice": "A",
  "answer": "Man-in-the-middle"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be4.052-B",
  "QuestionId": "b475934.CISSPSG8E.be4.052",
  "choice": "B",
  "answer": "Spoofing"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be4.052-C",
  "QuestionId": "b475934.CISSPSG8E.be4.052",
  "choice": "C",
  "answer": "Hijack"
 },
 {
  "type": "ANSWER",
  "id": "b475934.CISSPSG8E.be4.052-D",
  "QuestionId": "b475934.CISSPSG8E.be4.052",
  "choice": "D",
  "answer": "Cracking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.120-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.120",
  "choice": "A",
  "answer": "192 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.120-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.120",
  "choice": "B",
  "answer": "256 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.120-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.120",
  "choice": "C",
  "answer": "512 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.120-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.120",
  "choice": "D",
  "answer": "1024 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.081-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.081",
  "choice": "A",
  "answer": "Zero"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.081-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.081",
  "choice": "B",
  "answer": "One"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.081-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.081",
  "choice": "C",
  "answer": "Two"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.081-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.081",
  "choice": "D",
  "answer": "No limit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.094-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.094",
  "choice": "A",
  "answer": "HIPAA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.094-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.094",
  "choice": "B",
  "answer": "SOX"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.094-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.094",
  "choice": "C",
  "answer": "GLBA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.094-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.094",
  "choice": "D",
  "answer": "FERPA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.123-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.123",
  "choice": "A",
  "answer": "Real evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.123-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.123",
  "choice": "B",
  "answer": "Best evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.123-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.123",
  "choice": "C",
  "answer": "Parol evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.123-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.123",
  "choice": "D",
  "answer": "Chain of evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.078-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.078",
  "choice": "A",
  "answer": "Transaction logging"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.078-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.078",
  "choice": "B",
  "answer": "Electronic vaulting"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.078-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.078",
  "choice": "C",
  "answer": "Remote journaling"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.078-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.078",
  "choice": "D",
  "answer": "Remote mirroring"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.001-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.001",
  "choice": "A",
  "answer": "Functions of an object"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.001-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.001",
  "choice": "B",
  "answer": "Information flow from objects to subjects"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.001-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.001",
  "choice": "C",
  "answer": "Unrestricted admittance of subjects on a system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.001-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.001",
  "choice": "D",
  "answer": "Administration of ACLs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.035-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.035",
  "choice": "A",
  "answer": "Nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.035-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.035",
  "choice": "B",
  "answer": "Privacy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.035-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.035",
  "choice": "C",
  "answer": "Abstraction"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.035-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.035",
  "choice": "D",
  "answer": "Redundancy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.123-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.123",
  "choice": "A",
  "answer": "Mitigation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.123-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.123",
  "choice": "B",
  "answer": "Assignment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.123-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.123",
  "choice": "C",
  "answer": "Tolerance"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.123-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.123",
  "choice": "D",
  "answer": "Rejection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.103-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.103",
  "choice": "A",
  "answer": "Periodic security audits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.103-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.103",
  "choice": "B",
  "answer": "Deployment of all available controls"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.103-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.103",
  "choice": "C",
  "answer": "Performance reviews"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.103-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.103",
  "choice": "D",
  "answer": "Audit reports for shareholders"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.076-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.076",
  "choice": "A",
  "answer": "Functional"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.076-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.076",
  "choice": "B",
  "answer": "Operational"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.076-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.076",
  "choice": "C",
  "answer": "Strategic"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.076-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.076",
  "choice": "D",
  "answer": "Tactical"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.137-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.137",
  "choice": "A",
  "answer": "Bandwidth on-demand connection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.137-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.137",
  "choice": "B",
  "answer": "Switched virtual circuit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.137-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.137",
  "choice": "C",
  "answer": "Permanent virtual circuit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.137-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.137",
  "choice": "D",
  "answer": "CSU/DSU connection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.132-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.132",
  "choice": "A",
  "answer": "Military attacks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.132-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.132",
  "choice": "B",
  "answer": "Thrill attacks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.132-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.132",
  "choice": "C",
  "answer": "Grudge attacks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.132-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.132",
  "choice": "D",
  "answer": "Insider attacks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.037-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.037",
  "choice": "A",
  "answer": "5 percent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.037-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.037",
  "choice": "B",
  "answer": "10 percent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.037-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.037",
  "choice": "C",
  "answer": "50 percent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.037-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.037",
  "choice": "D",
  "answer": "100 percent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.008-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.008",
  "choice": "A",
  "answer": "Change logs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.008-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.008",
  "choice": "B",
  "answer": "Security logs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.008-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.008",
  "choice": "C",
  "answer": "System logs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.008-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.008",
  "choice": "D",
  "answer": "Audit trail"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.047-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.047",
  "choice": "A",
  "answer": "Degaussing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.047-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.047",
  "choice": "B",
  "answer": "Destruction"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.047-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.047",
  "choice": "C",
  "answer": "Declassification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.047-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.047",
  "choice": "D",
  "answer": "Defenestration"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.058-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.058",
  "choice": "A",
  "answer": "IT"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.058-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.058",
  "choice": "B",
  "answer": "Facility"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.058-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.058",
  "choice": "C",
  "answer": "Corporate"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.058-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.058",
  "choice": "D",
  "answer": "Security"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.122-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.122",
  "choice": "A",
  "answer": "Type 1"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.122-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.122",
  "choice": "B",
  "answer": "Type 2"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.122-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.122",
  "choice": "C",
  "answer": "Type 3"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.122-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.122",
  "choice": "D",
  "answer": "Type 4"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.072-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.072",
  "choice": "A",
  "answer": "Hierarchical environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.072-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.072",
  "choice": "B",
  "answer": "Hybrid environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.072-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.072",
  "choice": "C",
  "answer": "Compartmentalized environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.072-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.072",
  "choice": "D",
  "answer": "Organizational environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.144-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.144",
  "choice": "A",
  "answer": "Reliable log storage system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.144-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.144",
  "choice": "B",
  "answer": "Proper warning banner notification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.144-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.144",
  "choice": "C",
  "answer": "Legal defense/support of authentication"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.144-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.144",
  "choice": "D",
  "answer": "Use of discretionary access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.104-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.104",
  "choice": "A",
  "answer": "Asset valuation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.104-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.104",
  "choice": "B",
  "answer": "Threat modeling"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.104-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.104",
  "choice": "C",
  "answer": "Vulnerability analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.104-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.104",
  "choice": "D",
  "answer": "User entitlement audit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.143-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.143",
  "choice": "A",
  "answer": "$300,000"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.143-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.143",
  "choice": "B",
  "answer": "$1,050,000"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.143-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.143",
  "choice": "C",
  "answer": "$1,200,000"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.143-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.143",
  "choice": "D",
  "answer": "$1,500,000"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.028-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.028",
  "choice": "A",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.028-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.028",
  "choice": "B",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.028-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.028",
  "choice": "C",
  "answer": "Accessibility"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.028-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.028",
  "choice": "D",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.136-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.136",
  "choice": "A",
  "answer": "Main entrance to a secure area"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.136-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.136",
  "choice": "B",
  "answer": "Primary entrance for the public to enter a retail space"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.136-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.136",
  "choice": "C",
  "answer": "On secondary or side exits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.136-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.136",
  "choice": "D",
  "answer": "On internal office intersections"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.059-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.059",
  "choice": "A",
  "answer": "Risk acceptance details"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.059-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.059",
  "choice": "B",
  "answer": "Emergency response guidelines"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.059-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.059",
  "choice": "C",
  "answer": "Risk assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.059-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.059",
  "choice": "D",
  "answer": "Mobile site plan"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.073-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.073",
  "choice": "A",
  "answer": "Access controls"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.073-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.073",
  "choice": "B",
  "answer": "Monitoring"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.073-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.073",
  "choice": "C",
  "answer": "Account policies"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.073-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.073",
  "choice": "D",
  "answer": "Performance review"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.059-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.059",
  "choice": "A",
  "answer": "External connection attempts"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.059-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.059",
  "choice": "B",
  "answer": "Execution of malicious code"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.059-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.059",
  "choice": "C",
  "answer": "Access to controlled objects"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.059-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.059",
  "choice": "D",
  "answer": "None of the above"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.028-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.028",
  "choice": "A",
  "answer": "A flooding attack is occurring."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.028-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.028",
  "choice": "B",
  "answer": "A new session is being initiated."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.028-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.028",
  "choice": "C",
  "answer": "A reset has been transmitted."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.028-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.028",
  "choice": "D",
  "answer": "A custom-crafted IP packet is being broadcast."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.067-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.067",
  "choice": "A",
  "answer": "Change management program"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.067-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.067",
  "choice": "B",
  "answer": "Configuration management program"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.067-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.067",
  "choice": "C",
  "answer": "Port scanners"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.067-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.067",
  "choice": "D",
  "answer": "Vulnerability scanners"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.088-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.088",
  "choice": "A",
  "answer": "Documentary evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.088-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.088",
  "choice": "B",
  "answer": "Testimonial evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.088-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.088",
  "choice": "C",
  "answer": "Real evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.088-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.088",
  "choice": "D",
  "answer": "Hearsay evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.113-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.113",
  "choice": "A",
  "answer": "MBR"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.113-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.113",
  "choice": "B",
  "answer": "Companion"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.113-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.113",
  "choice": "C",
  "answer": "Stealth"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.113-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.113",
  "choice": "D",
  "answer": "Multipartite"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.131-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.131",
  "choice": "A",
  "answer": "Quantitative decision-making"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.131-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.131",
  "choice": "B",
  "answer": "Continuity decision-making"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.131-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.131",
  "choice": "C",
  "answer": "Qualitative decision-making"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.131-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.131",
  "choice": "D",
  "answer": "Impact decision-making"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.087-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.087",
  "choice": "A",
  "answer": "RFC 1492"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.087-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.087",
  "choice": "B",
  "answer": "RFC 1661"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.087-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.087",
  "choice": "C",
  "answer": "RFC 1918"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.087-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.087",
  "choice": "D",
  "answer": "RFC 3947"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.049-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.049",
  "choice": "A",
  "answer": "Content-dependent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.049-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.049",
  "choice": "B",
  "answer": "Context-dependent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.049-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.049",
  "choice": "C",
  "answer": "Semantic integrity mechanisms"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.049-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.049",
  "choice": "D",
  "answer": "Perturbation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.053-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.053",
  "choice": "A",
  "answer": "Asset or employee reduction"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.053-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.053",
  "choice": "B",
  "answer": "A distribution of profits to shareholders"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.053-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.053",
  "choice": "C",
  "answer": "A release of documentation to the public"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.053-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.053",
  "choice": "D",
  "answer": "A transmission of data to law enforcement during an investigation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.041-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.041",
  "choice": "A",
  "answer": "Security token"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.041-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.041",
  "choice": "B",
  "answer": "Capabilities list"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.041-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.041",
  "choice": "C",
  "answer": "Security label"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.041-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.041",
  "choice": "D",
  "answer": "Access matrix"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.135-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.135",
  "choice": "A",
  "answer": "Bursty traffic focused"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.135-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.135",
  "choice": "B",
  "answer": "Fixed known delays"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.135-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.135",
  "choice": "C",
  "answer": "Sensitive to data loss"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.135-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.135",
  "choice": "D",
  "answer": "Supports any type of traffic"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.023-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.023",
  "choice": "A",
  "answer": "7 years"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.023-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.023",
  "choice": "B",
  "answer": "14 years"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.023-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.023",
  "choice": "C",
  "answer": "20 years"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.023-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.023",
  "choice": "D",
  "answer": "35 years"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.058-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.058",
  "choice": "A",
  "answer": "Risk assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.058-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.058",
  "choice": "B",
  "answer": "Likelihood assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.058-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.058",
  "choice": "C",
  "answer": "Impact assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.058-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.058",
  "choice": "D",
  "answer": "Resource prioritization"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.099-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.099",
  "choice": "A",
  "answer": "Brewer and Nash model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.099-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.099",
  "choice": "B",
  "answer": "Biba model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.099-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.099",
  "choice": "C",
  "answer": "Bell-LaPadula model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.099-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.099",
  "choice": "D",
  "answer": "Clark-Wilson model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.083-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.083",
  "choice": "A",
  "answer": "Star axiom"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.083-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.083",
  "choice": "B",
  "answer": "Simple property"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.083-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.083",
  "choice": "C",
  "answer": "No read up"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.083-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.083",
  "choice": "D",
  "answer": "No write down"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.132-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.132",
  "choice": "A",
  "answer": "You can’t filter on encrypted traffic."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.132-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.132",
  "choice": "B",
  "answer": "VPNs cannot cross firewalls."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.132-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.132",
  "choice": "C",
  "answer": "Firewalls block all outbound VPN connections."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.132-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.132",
  "choice": "D",
  "answer": "Firewalls greatly reduce the throughput of VPNs."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.033-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.033",
  "choice": "A",
  "answer": "Transformation procedures (TPs)"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.033-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.033",
  "choice": "B",
  "answer": "Integrity verification procedure (IVP)"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.033-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.033",
  "choice": "C",
  "answer": "Independent modification algorithm (IMA)"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.033-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.033",
  "choice": "D",
  "answer": "Dependent modification algorithm (DMA)"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.085-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.085",
  "choice": "A",
  "answer": "Developing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.085-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.085",
  "choice": "B",
  "answer": "Diagnosing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.085-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.085",
  "choice": "C",
  "answer": "Determining"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.085-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.085",
  "choice": "D",
  "answer": "Designing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.091-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.091",
  "choice": "A",
  "answer": "Legal representatives"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.091-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.091",
  "choice": "B",
  "answer": "Information security representative"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.091-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.091",
  "choice": "C",
  "answer": "Technical and functional experts"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.091-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.091",
  "choice": "D",
  "answer": "Chief executive officer"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.056-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.056",
  "choice": "A",
  "answer": "Governance"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.056-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.056",
  "choice": "B",
  "answer": "Ownership"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.056-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.056",
  "choice": "C",
  "answer": "Take grant"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.056-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.056",
  "choice": "D",
  "answer": "Separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.054-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.054",
  "choice": "A",
  "answer": "Subject only"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.054-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.054",
  "choice": "B",
  "answer": "Object only"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.054-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.054",
  "choice": "C",
  "answer": "Subject or object"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.054-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.054",
  "choice": "D",
  "answer": "Neither a subject nor an object"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.087-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.087",
  "choice": "A",
  "answer": "HMAC"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.087-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.087",
  "choice": "B",
  "answer": "DSA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.087-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.087",
  "choice": "C",
  "answer": "MD5"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.087-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.087",
  "choice": "D",
  "answer": "SHA-1"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.024-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.024",
  "choice": "A",
  "answer": "Grouping subjects"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.024-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.024",
  "choice": "B",
  "answer": "Grouping privileges"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.024-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.024",
  "choice": "C",
  "answer": "Grouping programs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.024-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.024",
  "choice": "D",
  "answer": "Grouping objects"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.026-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.026",
  "choice": "A",
  "answer": "Logic bombs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.026-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.026",
  "choice": "B",
  "answer": "Worms"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.026-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.026",
  "choice": "C",
  "answer": "Trojan horses"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.026-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.026",
  "choice": "D",
  "answer": "Spyware"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.088-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.088",
  "choice": "A",
  "answer": "Duplicate security is imposed on other systems."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.088-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.088",
  "choice": "B",
  "answer": "Unauthorized changes to the system are prevented."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.088-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.088",
  "choice": "C",
  "answer": "Vulnerable resources are locked down when threatened"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.088-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.088",
  "choice": "D",
  "answer": "Changes can be rolled back to a previous state."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.133-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.133",
  "choice": "A",
  "answer": "Rainbow tables"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.133-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.133",
  "choice": "B",
  "answer": "Dictionary word list"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.133-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.133",
  "choice": "C",
  "answer": "Brute force"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.133-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.133",
  "choice": "D",
  "answer": "Educated guess"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.044-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.044",
  "choice": "A",
  "answer": "ISO/OSI reference model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.044-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.044",
  "choice": "B",
  "answer": "Concentric circle"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.044-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.044",
  "choice": "C",
  "answer": "Operations security triple"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.044-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.044",
  "choice": "D",
  "answer": "CIA Triad"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.105-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.105",
  "choice": "A",
  "answer": "Black box"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.105-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.105",
  "choice": "B",
  "answer": "DTMF"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.105-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.105",
  "choice": "C",
  "answer": "Vishing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.105-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.105",
  "choice": "D",
  "answer": "Remote dialing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.140-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.140",
  "choice": "A",
  "answer": "Simple integrity property"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.140-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.140",
  "choice": "B",
  "answer": "Time of use"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.140-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.140",
  "choice": "C",
  "answer": "Need to know"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.140-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.140",
  "choice": "D",
  "answer": "Separation of duties"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.045-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.045",
  "choice": "A",
  "answer": "CFAA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.045-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.045",
  "choice": "B",
  "answer": "Federal Sentencing Guidelines"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.045-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.045",
  "choice": "C",
  "answer": "GLBA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.045-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.045",
  "choice": "D",
  "answer": "Sarbanes–Oxley"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.106-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.106",
  "choice": "A",
  "answer": "Confusion"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.106-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.106",
  "choice": "B",
  "answer": "Transposition"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.106-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.106",
  "choice": "C",
  "answer": "Polymorphism"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.106-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.106",
  "choice": "D",
  "answer": "Diffusion"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.061-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.061",
  "choice": "A",
  "answer": "Tester or test-taker"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.061-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.061",
  "choice": "B",
  "answer": "Client or representative"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.061-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.061",
  "choice": "C",
  "answer": "Outsider or administrator"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.061-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.061",
  "choice": "D",
  "answer": "Observer or auditor"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.090-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.090",
  "choice": "A",
  "answer": "ISAKMP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.090-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.090",
  "choice": "B",
  "answer": "SKIP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.090-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.090",
  "choice": "C",
  "answer": "IPComp"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.090-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.090",
  "choice": "D",
  "answer": "SSL"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.126-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.126",
  "choice": "A",
  "answer": "Biba"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.126-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.126",
  "choice": "B",
  "answer": "Take grant"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.126-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.126",
  "choice": "C",
  "answer": "Goguen−Meseguer"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.126-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.126",
  "choice": "D",
  "answer": "Sutherland"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.120-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.120",
  "choice": "A",
  "answer": "Mandatory access control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.120-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.120",
  "choice": "B",
  "answer": "Discretionary access control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.120-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.120",
  "choice": "C",
  "answer": "Role-based access control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.120-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.120",
  "choice": "D",
  "answer": "Rule-based access control model"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.017-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.017",
  "choice": "A",
  "answer": "L2F"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.017-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.017",
  "choice": "B",
  "answer": "PPTP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.017-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.017",
  "choice": "C",
  "answer": "IPsec"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.017-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.017",
  "choice": "D",
  "answer": "SSH"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.080-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.080",
  "choice": "A",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.080-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.080",
  "choice": "B",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.080-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.080",
  "choice": "C",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.080-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.080",
  "choice": "D",
  "answer": "Nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.011-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.011",
  "choice": "A",
  "answer": "A product that inspects incoming and outgoing traffic across a network boundary to deny transit to unwanted, unauthorized, or suspect packets"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.011-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.011",
  "choice": "B",
  "answer": "A device that provides secure termination or aggregation for IP phones, VoIP handsets, and softphones"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.011-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.011",
  "choice": "C",
  "answer": "A device that, using complex content categorization criteria and content inspection, prevents potentially dangerous content from entering a network"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.011-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.011",
  "choice": "D",
  "answer": "A product that automates the inspection of audit logs and real-time event information to detect intrusion attempts and possibly also system failures"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.095-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.095",
  "choice": "A",
  "answer": "AH"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.095-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.095",
  "choice": "B",
  "answer": "ISAKMP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.095-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.095",
  "choice": "C",
  "answer": "DH"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.095-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.095",
  "choice": "D",
  "answer": "ESP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.048-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.048",
  "choice": "A",
  "answer": "Stand-alone"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.048-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.048",
  "choice": "B",
  "answer": "Wired extension"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.048-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.048",
  "choice": "C",
  "answer": "Enterprise extended"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.048-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.048",
  "choice": "D",
  "answer": "Bridge"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.067-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.067",
  "choice": "A",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.067-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.067",
  "choice": "B",
  "answer": "Nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.067-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.067",
  "choice": "C",
  "answer": "Sensitivity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.067-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.067",
  "choice": "D",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.089-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.089",
  "choice": "A",
  "answer": "Level 0"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.089-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.089",
  "choice": "B",
  "answer": "Level 1"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.089-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.089",
  "choice": "C",
  "answer": "Level 3"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.089-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.089",
  "choice": "D",
  "answer": "Level 4"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.023-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.023",
  "choice": "A",
  "answer": "Denial of service"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.023-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.023",
  "choice": "B",
  "answer": "Distributed denial of service"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.023-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.023",
  "choice": "C",
  "answer": "Distributed reflective denial of service"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.023-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.023",
  "choice": "D",
  "answer": "Differential denial of service"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.058-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.058",
  "choice": "A",
  "answer": "Beyond a reasonable doubt"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.058-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.058",
  "choice": "B",
  "answer": "Beyond the shadow of a doubt"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.058-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.058",
  "choice": "C",
  "answer": "Preponderance of the evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.058-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.058",
  "choice": "D",
  "answer": "Clear and convincing evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.080-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.080",
  "choice": "A",
  "answer": "Register the song with the U.S. Copyright Office."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.080-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.080",
  "choice": "B",
  "answer": "Mark the song with the © symbol."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.080-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.080",
  "choice": "C",
  "answer": "Mail himself a copy of the song in a sealed envelope."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.080-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.080",
  "choice": "D",
  "answer": "Nothing."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.002-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.002",
  "choice": "A",
  "answer": "Cross-talk noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.002-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.002",
  "choice": "B",
  "answer": "Radio frequency interference"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.002-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.002",
  "choice": "C",
  "answer": "Traverse mode noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.002-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.002",
  "choice": "D",
  "answer": "Common mode noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.029-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.029",
  "choice": "A",
  "answer": "Forged spam email appearing to come from your organization"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.029-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.029",
  "choice": "B",
  "answer": "Unauthorized use of an account by the legitimate user’s relative"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.029-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.029",
  "choice": "C",
  "answer": "Probing a network searching for vulnerable services"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.029-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.029",
  "choice": "D",
  "answer": "Infection of a system by a virus"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.032-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.032",
  "choice": "A",
  "answer": "User mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.032-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.032",
  "choice": "B",
  "answer": "Supervisory mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.032-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.032",
  "choice": "C",
  "answer": "Kernel mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.032-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.032",
  "choice": "D",
  "answer": "Privileged mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.146-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.146",
  "choice": "A",
  "answer": "Dual stack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.146-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.146",
  "choice": "B",
  "answer": "Tunneling"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.146-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.146",
  "choice": "C",
  "answer": "IPsec v6"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.146-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.146",
  "choice": "D",
  "answer": "NAT-PT"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.12-A",
  "QuestionId": "tb475934.CISSPSG8E.c08.12",
  "choice": "A",
  "answer": "A security model states policies an organization must follow."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.12-B",
  "QuestionId": "tb475934.CISSPSG8E.c08.12",
  "choice": "B",
  "answer": "A security model provides a framework to implement a security policy."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.12-C",
  "QuestionId": "tb475934.CISSPSG8E.c08.12",
  "choice": "C",
  "answer": "A security model is a technical evaluation of each part of a computer system to assess its concordance with security standards."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c08.12-D",
  "QuestionId": "tb475934.CISSPSG8E.c08.12",
  "choice": "D",
  "answer": "A security model is the process of formal acceptance of a certified configuration."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.075-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.075",
  "choice": "A",
  "answer": "Host-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.075-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.075",
  "choice": "B",
  "answer": "Network-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.075-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.075",
  "choice": "C",
  "answer": "Knowledge-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.075-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.075",
  "choice": "D",
  "answer": "Behavior-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.12-A",
  "QuestionId": "tb475934.CISSPSG8E.c01.12",
  "choice": "A",
  "answer": "Multiple"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.12-B",
  "QuestionId": "tb475934.CISSPSG8E.c01.12",
  "choice": "B",
  "answer": "Series"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.12-C",
  "QuestionId": "tb475934.CISSPSG8E.c01.12",
  "choice": "C",
  "answer": "Parallel"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.12-D",
  "QuestionId": "tb475934.CISSPSG8E.c01.12",
  "choice": "D",
  "answer": "Filter"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.060-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.060",
  "choice": "A",
  "answer": "Production"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.060-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.060",
  "choice": "B",
  "answer": "Processing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.060-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.060",
  "choice": "C",
  "answer": "Review"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.060-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.060",
  "choice": "D",
  "answer": "Presentation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.078-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.078",
  "choice": "A",
  "answer": "1"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.078-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.078",
  "choice": "B",
  "answer": "6"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.078-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.078",
  "choice": "C",
  "answer": "15"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.078-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.078",
  "choice": "D",
  "answer": "30"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.038-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.038",
  "choice": "A",
  "answer": "Any potential occurrence that can cause an undesirable or unwanted outcome"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.038-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.038",
  "choice": "B",
  "answer": "The actual occurrence of an event that results in loss"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.038-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.038",
  "choice": "C",
  "answer": "The likelihood that any specific threat will exploit a specific vulnerability to cause harm to an asset"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.038-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.038",
  "choice": "D",
  "answer": "An instance of being exposed to asset loss due to a threat"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c20.12-A",
  "QuestionId": "tb475934.CISSPSG8E.c20.12",
  "choice": "A",
  "answer": "Initial"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c20.12-B",
  "QuestionId": "tb475934.CISSPSG8E.c20.12",
  "choice": "B",
  "answer": "Repeatable"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c20.12-C",
  "QuestionId": "tb475934.CISSPSG8E.c20.12",
  "choice": "C",
  "answer": "Defined"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c20.12-D",
  "QuestionId": "tb475934.CISSPSG8E.c20.12",
  "choice": "D",
  "answer": "Managed"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.06-A",
  "QuestionId": "tb475934.CISSPSG8E.c09.06",
  "choice": "A",
  "answer": "A cloud environment maintained, used, and paid for by a group of users or organizations for their shared benefit, such as collaboration and data exchange"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.06-B",
  "QuestionId": "tb475934.CISSPSG8E.c09.06",
  "choice": "B",
  "answer": "A cloud service within a corporate network and isolated from the internet"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.06-C",
  "QuestionId": "tb475934.CISSPSG8E.c09.06",
  "choice": "C",
  "answer": "A cloud service that is accessible to the general public typically over an internet connection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c09.06-D",
  "QuestionId": "tb475934.CISSPSG8E.c09.06",
  "choice": "D",
  "answer": "A cloud service that is partially hosted within an organization for private use and that uses external services to offer resources to outsiders"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.061-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.061",
  "choice": "A",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.061-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.061",
  "choice": "B",
  "answer": "Scalability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.061-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.061",
  "choice": "C",
  "answer": "Accountability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.061-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.061",
  "choice": "D",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.145-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.145",
  "choice": "A",
  "answer": "Log files can be used to reconstruct events leading up to an incident."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.145-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.145",
  "choice": "B",
  "answer": "Attackers may try to erase their activity during or after an attack."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.145-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.145",
  "choice": "C",
  "answer": "Unprotected log files cannot be used as evidence."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.145-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.145",
  "choice": "D",
  "answer": "Log files include information on why an attack occurred."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.122-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.122",
  "choice": "A",
  "answer": "OLAP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.122-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.122",
  "choice": "B",
  "answer": "LDAP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.122-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.122",
  "choice": "C",
  "answer": "OSCP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.122-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.122",
  "choice": "D",
  "answer": "BGP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c04.06-A",
  "QuestionId": "tb475934.CISSPSG8E.c04.06",
  "choice": "A",
  "answer": "Privacy Act"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c04.06-B",
  "QuestionId": "tb475934.CISSPSG8E.c04.06",
  "choice": "B",
  "answer": "Fourth Amendment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c04.06-C",
  "QuestionId": "tb475934.CISSPSG8E.c04.06",
  "choice": "C",
  "answer": "Second Amendment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c04.06-D",
  "QuestionId": "tb475934.CISSPSG8E.c04.06",
  "choice": "D",
  "answer": "Gramm-Leach-Bliley Act"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.087-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.087",
  "choice": "A",
  "answer": "0 to 1,023"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.087-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.087",
  "choice": "B",
  "answer": "80, 135, 110, 25"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.087-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.087",
  "choice": "C",
  "answer": "0 to 65, 536"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.087-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.087",
  "choice": "D",
  "answer": "32,000 to 65,536"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.021-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.021",
  "choice": "A",
  "answer": "Internal audit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.021-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.021",
  "choice": "B",
  "answer": "External audit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.021-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.021",
  "choice": "C",
  "answer": "Criminal investigation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.021-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.021",
  "choice": "D",
  "answer": "Assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.095-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.095",
  "choice": "A",
  "answer": "Companion naming"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.095-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.095",
  "choice": "B",
  "answer": "Encryption"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.095-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.095",
  "choice": "C",
  "answer": "AV tampering"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.095-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.095",
  "choice": "D",
  "answer": "Exploitation of administrative privileges"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.035-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.035",
  "choice": "A",
  "answer": "No read up"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.035-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.035",
  "choice": "B",
  "answer": "No read down"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.035-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.035",
  "choice": "C",
  "answer": "No write up"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.035-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.035",
  "choice": "D",
  "answer": "No write down"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.100-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.100",
  "choice": "A",
  "answer": "Clark-Wilson"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.100-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.100",
  "choice": "B",
  "answer": "Goguen-Meseguer"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.100-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.100",
  "choice": "C",
  "answer": "Graham-Denning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.100-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.100",
  "choice": "D",
  "answer": "Bell-LaPadula"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.033-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.033",
  "choice": "A",
  "answer": "Humans are perfect."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.033-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.033",
  "choice": "B",
  "answer": "Software is not trusted."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.033-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.033",
  "choice": "C",
  "answer": "Technology is always improving."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.033-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.033",
  "choice": "D",
  "answer": "Hardware is faulty."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.062-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.062",
  "choice": "A",
  "answer": "Transport"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.062-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.062",
  "choice": "B",
  "answer": "Tunnel"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.062-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.062",
  "choice": "C",
  "answer": "Vector"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.062-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.062",
  "choice": "D",
  "answer": "Transparent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.013-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.013",
  "choice": "A",
  "answer": "Confidentiality, integrity, and availability (the CIA Triad)"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.013-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.013",
  "choice": "B",
  "answer": "Authentication, authorization, and accounting (AAA)"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.013-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.013",
  "choice": "C",
  "answer": "The relationship between assets, vulnerabilities, and threats"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.013-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.013",
  "choice": "D",
  "answer": "Due care, due diligence, and operations controls"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.096-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.096",
  "choice": "A",
  "answer": "Multipart viruses"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.096-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.096",
  "choice": "B",
  "answer": "Stealth viruses"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.096-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.096",
  "choice": "C",
  "answer": "Encrypted viruses"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.096-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.096",
  "choice": "D",
  "answer": "Polymorphic viruses"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.125-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.125",
  "choice": "A",
  "answer": "ABAC"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.125-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.125",
  "choice": "B",
  "answer": "DAC"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.125-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.125",
  "choice": "C",
  "answer": "MAC"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.125-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.125",
  "choice": "D",
  "answer": "RBAC"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.150-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.150",
  "choice": "A",
  "answer": "PBKDF2"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.150-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.150",
  "choice": "B",
  "answer": "ECDHE"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.150-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.150",
  "choice": "C",
  "answer": "HMAC"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.150-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.150",
  "choice": "D",
  "answer": "OCSP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.096-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.096",
  "choice": "A",
  "answer": "WPS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.096-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.096",
  "choice": "B",
  "answer": "WEP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.096-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.096",
  "choice": "C",
  "answer": "WPA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.096-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.096",
  "choice": "D",
  "answer": "WPA2"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.098-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.098",
  "choice": "A",
  "answer": "Ring"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.098-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.098",
  "choice": "B",
  "answer": "Star"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.098-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.098",
  "choice": "C",
  "answer": "Bus"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.098-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.098",
  "choice": "D",
  "answer": "Mesh"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.081-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.081",
  "choice": "A",
  "answer": "Clustering"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.081-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.081",
  "choice": "B",
  "answer": "Differential backups"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.081-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.081",
  "choice": "C",
  "answer": "Remote journaling"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.081-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.081",
  "choice": "D",
  "answer": "Tape rotation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.088-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.088",
  "choice": "A",
  "answer": "Auxiliary system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.088-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.088",
  "choice": "B",
  "answer": "Centralized system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.088-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.088",
  "choice": "C",
  "answer": "Localized system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.088-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.088",
  "choice": "D",
  "answer": "Proprietary system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.068-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.068",
  "choice": "A",
  "answer": "Cleaning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.068-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.068",
  "choice": "B",
  "answer": "Removal"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.068-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.068",
  "choice": "C",
  "answer": "Stealth"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.068-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.068",
  "choice": "D",
  "answer": "Polymorphism"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.099-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.099",
  "choice": "A",
  "answer": "Trojan horse"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.099-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.099",
  "choice": "B",
  "answer": "Direct access to media"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.099-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.099",
  "choice": "C",
  "answer": "Permission creep"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.099-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.099",
  "choice": "D",
  "answer": "Covert channel"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.121-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.121",
  "choice": "A",
  "answer": "Accountability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.121-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.121",
  "choice": "B",
  "answer": "Authorization"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.121-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.121",
  "choice": "C",
  "answer": "Auditing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.121-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.121",
  "choice": "D",
  "answer": "Nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.067-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.067",
  "choice": "A",
  "answer": "Theft"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.067-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.067",
  "choice": "B",
  "answer": "Fraud"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.067-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.067",
  "choice": "C",
  "answer": "Omission"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.067-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.067",
  "choice": "D",
  "answer": "Collusion"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.082-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.082",
  "choice": "A",
  "answer": "Trademark"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.082-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.082",
  "choice": "B",
  "answer": "Trade secret"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.082-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.082",
  "choice": "C",
  "answer": "Copyright"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.082-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.082",
  "choice": "D",
  "answer": "Patent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.079-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.079",
  "choice": "A",
  "answer": "Certification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.079-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.079",
  "choice": "B",
  "answer": "Evaluation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.079-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.079",
  "choice": "C",
  "answer": "Accreditation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.079-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.079",
  "choice": "D",
  "answer": "Formal analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.043-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.043",
  "choice": "A",
  "answer": "Trusted"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.043-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.043",
  "choice": "B",
  "answer": "Authorized"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.043-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.043",
  "choice": "C",
  "answer": "Available"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.043-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.043",
  "choice": "D",
  "answer": "Baseline"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.070-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.070",
  "choice": "A",
  "answer": "Biba"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.070-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.070",
  "choice": "B",
  "answer": "Sutherland"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.070-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.070",
  "choice": "C",
  "answer": "Clark-Wilson"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.070-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.070",
  "choice": "D",
  "answer": "Gramm-Leach-Bliley"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.101-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.101",
  "choice": "A",
  "answer": "Full backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.101-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.101",
  "choice": "B",
  "answer": "Remote journaling"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.101-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.101",
  "choice": "C",
  "answer": "Incremental backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.101-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.101",
  "choice": "D",
  "answer": "Differential backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.150-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.150",
  "choice": "A",
  "answer": "Technical"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.150-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.150",
  "choice": "B",
  "answer": "Physical"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.150-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.150",
  "choice": "C",
  "answer": "Administrative"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.150-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.150",
  "choice": "D",
  "answer": "Logical"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.128-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.128",
  "choice": "A",
  "answer": "Routing traffic"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.128-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.128",
  "choice": "B",
  "answer": "Protocol translator"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.128-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.128",
  "choice": "C",
  "answer": "Attenuation protection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.128-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.128",
  "choice": "D",
  "answer": "Creating virtual LANs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.126-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.126",
  "choice": "A",
  "answer": "Policy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.126-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.126",
  "choice": "B",
  "answer": "Standard"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.126-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.126",
  "choice": "C",
  "answer": "Procedure"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.126-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.126",
  "choice": "D",
  "answer": "Guideline"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.108-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.108",
  "choice": "A",
  "answer": "General description of the evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.108-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.108",
  "choice": "B",
  "answer": "Name of the person collecting the evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.108-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.108",
  "choice": "C",
  "answer": "Relationship of the evidence to the crime"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.108-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.108",
  "choice": "D",
  "answer": "Time and date the evidence was collected"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.011-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.011",
  "choice": "A",
  "answer": "Hosts and servers"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.011-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.011",
  "choice": "B",
  "answer": "Clients and terminals"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.011-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.011",
  "choice": "C",
  "answer": "Hosts and networks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.011-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.011",
  "choice": "D",
  "answer": "Servers and domain controllers"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.120-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.120",
  "choice": "A",
  "answer": "Graham-Denning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.120-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.120",
  "choice": "B",
  "answer": "Bell-LaPadula"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.120-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.120",
  "choice": "C",
  "answer": "Take-Grant"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.120-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.120",
  "choice": "D",
  "answer": "Sutherland"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.028-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.028",
  "choice": "A",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.028-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.028",
  "choice": "B",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.028-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.028",
  "choice": "C",
  "answer": "Availability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.028-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.028",
  "choice": "D",
  "answer": "Accountability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.140-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.140",
  "choice": "A",
  "answer": "Recovery access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.140-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.140",
  "choice": "B",
  "answer": "Corrective access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.140-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.140",
  "choice": "C",
  "answer": "Detective access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.140-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.140",
  "choice": "D",
  "answer": "Compensation access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.040-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.040",
  "choice": "A",
  "answer": "To provide sufficient security on each individual host."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.040-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.040",
  "choice": "B",
  "answer": "Centralized security mechanisms are too expensive."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.040-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.040",
  "choice": "C",
  "answer": "Network security safeguards do not provide any protection for hosts."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.040-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.040",
  "choice": "D",
  "answer": "Hardware security options are ineffective against software exploits."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.036-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.036",
  "choice": "A",
  "answer": "DREAD"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.036-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.036",
  "choice": "B",
  "answer": "Probability * Damage Potential"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.036-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.036",
  "choice": "C",
  "answer": "Qualitative analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.036-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.036",
  "choice": "D",
  "answer": "High/medium/low"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.099-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.099",
  "choice": "A",
  "answer": "FEMA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.099-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.099",
  "choice": "B",
  "answer": "NIFC"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.099-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.099",
  "choice": "C",
  "answer": "USGS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.099-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.099",
  "choice": "D",
  "answer": "USFWS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.031-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.031",
  "choice": "A",
  "answer": "TrueCrypt"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.031-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.031",
  "choice": "B",
  "answer": "PGP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.031-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.031",
  "choice": "C",
  "answer": "EFS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.031-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.031",
  "choice": "D",
  "answer": "BitLocker"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.092-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.092",
  "choice": "A",
  "answer": "Bridges and switches"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.092-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.092",
  "choice": "B",
  "answer": "Firewalls"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.092-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.092",
  "choice": "C",
  "answer": "Hubs and repeaters"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.092-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.092",
  "choice": "D",
  "answer": "Routers"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.130-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.130",
  "choice": "A",
  "answer": "Ready"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.130-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.130",
  "choice": "B",
  "answer": "Waiting"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.130-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.130",
  "choice": "C",
  "answer": "Running"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.130-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.130",
  "choice": "D",
  "answer": "Supervisory"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.114-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.114",
  "choice": "A",
  "answer": "Structured walk-through"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.114-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.114",
  "choice": "B",
  "answer": "Read-through"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.114-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.114",
  "choice": "C",
  "answer": "Simulation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.114-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.114",
  "choice": "D",
  "answer": "Parallel"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.052-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.052",
  "choice": "A",
  "answer": "Audit purpose"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.052-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.052",
  "choice": "B",
  "answer": "Audit scope"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.052-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.052",
  "choice": "C",
  "answer": "Audit results"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.052-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.052",
  "choice": "D",
  "answer": "Audit overview"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.003-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.003",
  "choice": "A",
  "answer": "Iris scan"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.003-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.003",
  "choice": "B",
  "answer": "Retina scan"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.003-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.003",
  "choice": "C",
  "answer": "Fingerprint"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.003-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.003",
  "choice": "D",
  "answer": "Facial geometry"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.033-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.033",
  "choice": "A",
  "answer": "ISO 17799"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.033-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.033",
  "choice": "B",
  "answer": "COBIT"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.033-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.033",
  "choice": "C",
  "answer": "OSSTMM"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.033-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.033",
  "choice": "D",
  "answer": "Common Criteria (IS 15408)"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.068-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.068",
  "choice": "A",
  "answer": "Parallel test"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.068-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.068",
  "choice": "B",
  "answer": "Full-interruption test"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.068-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.068",
  "choice": "C",
  "answer": "Structured walk-through"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.068-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.068",
  "choice": "D",
  "answer": "Simulation test"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.141-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.141",
  "choice": "A",
  "answer": "Measuring and evaluating security metrics"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.141-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.141",
  "choice": "B",
  "answer": "Monitoring performance"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.141-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.141",
  "choice": "C",
  "answer": "Vulnerability analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.141-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.141",
  "choice": "D",
  "answer": "Crafting a baseline"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.121-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.121",
  "choice": "A",
  "answer": "By creating a data warehouse"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.121-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.121",
  "choice": "B",
  "answer": "By performing data mining"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.121-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.121",
  "choice": "C",
  "answer": "By hosting a data mart"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.121-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.121",
  "choice": "D",
  "answer": "By authoring a data dictionary"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.129-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.129",
  "choice": "A",
  "answer": "Password guessing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.129-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.129",
  "choice": "B",
  "answer": "Encryption cracking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.129-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.129",
  "choice": "C",
  "answer": "IV interception"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.129-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.129",
  "choice": "D",
  "answer": "Packet replay attacks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.19-A",
  "QuestionId": "tb475934.CISSPSG8E.c10.19",
  "choice": "A",
  "answer": "Wet pipe system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.19-B",
  "QuestionId": "tb475934.CISSPSG8E.c10.19",
  "choice": "B",
  "answer": "Dry pipe system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.19-C",
  "QuestionId": "tb475934.CISSPSG8E.c10.19",
  "choice": "C",
  "answer": "Preaction system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c10.19-D",
  "QuestionId": "tb475934.CISSPSG8E.c10.19",
  "choice": "D",
  "answer": "Deluge system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.084-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.084",
  "choice": "A",
  "answer": "Detection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.084-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.084",
  "choice": "B",
  "answer": "Response"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.084-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.084",
  "choice": "C",
  "answer": "Mitigation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.084-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.084",
  "choice": "D",
  "answer": "Remediation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.105-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.105",
  "choice": "A",
  "answer": "Isolation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.105-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.105",
  "choice": "B",
  "answer": "Bounds"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.105-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.105",
  "choice": "C",
  "answer": "Confinement"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.105-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.105",
  "choice": "D",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.035-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.035",
  "choice": "A",
  "answer": "Penetration testing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.035-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.035",
  "choice": "B",
  "answer": "Compliance checking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.035-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.035",
  "choice": "C",
  "answer": "Auditing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.035-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.035",
  "choice": "D",
  "answer": "Ethical hacking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.025-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.025",
  "choice": "A",
  "answer": "Reimaged"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.025-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.025",
  "choice": "B",
  "answer": "Updated at next refresh cycle"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.025-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.025",
  "choice": "C",
  "answer": "Quarantine"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.025-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.025",
  "choice": "D",
  "answer": "User must reset their password"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.117-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.117",
  "choice": "A",
  "answer": "Deploying OS updates"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.117-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.117",
  "choice": "B",
  "answer": "Vulnerability scanning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.117-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.117",
  "choice": "C",
  "answer": "Waiting for the event to expire"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.117-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.117",
  "choice": "D",
  "answer": "Tightening of access controls"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.014-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.014",
  "choice": "A",
  "answer": "Common mode noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.014-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.014",
  "choice": "B",
  "answer": "Traverse mode noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.014-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.014",
  "choice": "C",
  "answer": "Cross-talk noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.014-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.014",
  "choice": "D",
  "answer": "Radio frequency interference"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.124-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.124",
  "choice": "A",
  "answer": "The user"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.124-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.124",
  "choice": "B",
  "answer": "The owner"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.124-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.124",
  "choice": "C",
  "answer": "The custodian"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.124-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.124",
  "choice": "D",
  "answer": "The administrator"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.111-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.111",
  "choice": "A",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.111-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.111",
  "choice": "B",
  "answer": "Confidentiality"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.111-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.111",
  "choice": "C",
  "answer": "Non-interference"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.111-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.111",
  "choice": "D",
  "answer": "Availability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.060-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.060",
  "choice": "A",
  "answer": "COM"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.060-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.060",
  "choice": "B",
  "answer": "DCOM"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.060-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.060",
  "choice": "C",
  "answer": ".NET Framework"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.060-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.060",
  "choice": "D",
  "answer": "ActiveX"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.108-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.108",
  "choice": "A",
  "answer": "SQL injection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.108-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.108",
  "choice": "B",
  "answer": "Buffer overflow"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.108-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.108",
  "choice": "C",
  "answer": "DDoS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.108-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.108",
  "choice": "D",
  "answer": "XML exploitation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.147-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.147",
  "choice": "A",
  "answer": "Internal entities"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.147-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.147",
  "choice": "B",
  "answer": "External consultants and suppliers"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.147-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.147",
  "choice": "C",
  "answer": "Subsidiaries"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.147-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.147",
  "choice": "D",
  "answer": "Commercial competitors"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.128-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.128",
  "choice": "A",
  "answer": "Reasons for accepting risks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.128-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.128",
  "choice": "B",
  "answer": "Potential future events that might warrant reconsideration of the decision"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.128-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.128",
  "choice": "C",
  "answer": "Identification of insurance policies that apply to a given risk"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.128-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.128",
  "choice": "D",
  "answer": "Risk mitigation provisions and processes"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.093-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.093",
  "choice": "A",
  "answer": "Implement changes in a monitored and orderly manner."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.093-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.093",
  "choice": "B",
  "answer": "Changes are cost effective."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.093-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.093",
  "choice": "C",
  "answer": "A formalized testing process is included to verify that a change produces expected results."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.093-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.093",
  "choice": "D",
  "answer": "All changes can be reversed."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.014-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.014",
  "choice": "A",
  "answer": "Availability and nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.014-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.014",
  "choice": "B",
  "answer": "Confidentiality and nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.014-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.014",
  "choice": "C",
  "answer": "Confidentiality and integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.014-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.014",
  "choice": "D",
  "answer": "Availability and authorization"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.055-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.055",
  "choice": "A",
  "answer": "Zero knowledge"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.055-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.055",
  "choice": "B",
  "answer": "Infinite knowledge"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.055-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.055",
  "choice": "C",
  "answer": "Absolute knowledge"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.055-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.055",
  "choice": "D",
  "answer": "Partial knowledge"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.047-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.047",
  "choice": "A",
  "answer": "Background checks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.047-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.047",
  "choice": "B",
  "answer": "Job descriptions"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.047-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.047",
  "choice": "C",
  "answer": "Auditing and monitoring"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.047-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.047",
  "choice": "D",
  "answer": "Discretionary access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.121-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.121",
  "choice": "A",
  "answer": "Copyright"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.121-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.121",
  "choice": "B",
  "answer": "Trademark"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.121-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.121",
  "choice": "C",
  "answer": "Patent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.121-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.121",
  "choice": "D",
  "answer": "Trade secret"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.012-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.012",
  "choice": "A",
  "answer": "To keep attackers away from real systems or networks they might otherwise attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.012-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.012",
  "choice": "B",
  "answer": "To provide a lure for attackers by advertising the presence of a weak or insecure system or network"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.012-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.012",
  "choice": "C",
  "answer": "To provide an isolated network for testing software"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.012-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.012",
  "choice": "D",
  "answer": "To lure attackers into a bogus system or network environment and present sufficient material of apparent worth or interest to keep the attacker around long enough to track them down"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.104-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.104",
  "choice": "A",
  "answer": "One-way functions"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.104-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.104",
  "choice": "B",
  "answer": "Prime numbers"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.104-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.104",
  "choice": "C",
  "answer": "Hash functions"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.104-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.104",
  "choice": "D",
  "answer": "Elliptic curves"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.027-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.027",
  "choice": "A",
  "answer": "Synchronous one-time passwords"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.027-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.027",
  "choice": "B",
  "answer": "Asynchronous one-time passwords"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.027-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.027",
  "choice": "C",
  "answer": "Strong static passwords"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.027-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.027",
  "choice": "D",
  "answer": "Passphrases"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.104-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.104",
  "choice": "A",
  "answer": "Sutherland"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.104-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.104",
  "choice": "B",
  "answer": "Brewer–Nash"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.104-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.104",
  "choice": "C",
  "answer": "Biba"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.104-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.104",
  "choice": "D",
  "answer": "Graham–Denning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.055-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.055",
  "choice": "A",
  "answer": "Strategic"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.055-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.055",
  "choice": "B",
  "answer": "Operational"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.055-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.055",
  "choice": "C",
  "answer": "Administrative"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.055-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.055",
  "choice": "D",
  "answer": "Tactical"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.061-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.061",
  "choice": "A",
  "answer": "Screening"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.061-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.061",
  "choice": "B",
  "answer": "Bonding"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.061-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.061",
  "choice": "C",
  "answer": "Training"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.061-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.061",
  "choice": "D",
  "answer": "Conditioning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.119-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.119",
  "choice": "A",
  "answer": "Protection profiles"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.119-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.119",
  "choice": "B",
  "answer": "Evaluation assurance level"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.119-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.119",
  "choice": "C",
  "answer": "Certificate authority"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.119-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.119",
  "choice": "D",
  "answer": "Security target"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.108-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.108",
  "choice": "A",
  "answer": "Rivest"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.108-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.108",
  "choice": "B",
  "answer": "Schneier"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.108-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.108",
  "choice": "C",
  "answer": "Kerckchoffs"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.108-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.108",
  "choice": "D",
  "answer": "Shamir"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.060-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.060",
  "choice": "A",
  "answer": "Logical control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.060-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.060",
  "choice": "B",
  "answer": "Technical control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.060-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.060",
  "choice": "C",
  "answer": "Access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.060-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.060",
  "choice": "D",
  "answer": "Administrative control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.129-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.129",
  "choice": "A",
  "answer": "Hybrid environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.129-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.129",
  "choice": "B",
  "answer": "Compartmentalized environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.129-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.129",
  "choice": "C",
  "answer": "Hierarchical environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.129-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.129",
  "choice": "D",
  "answer": "Security environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.083-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.083",
  "choice": "A",
  "answer": "Compartmented mode system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.083-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.083",
  "choice": "B",
  "answer": "System-high mode system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.083-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.083",
  "choice": "C",
  "answer": "Multilevel mode system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.083-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.083",
  "choice": "D",
  "answer": "Dedicated mode system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.088-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.088",
  "choice": "A",
  "answer": "National Information Infrastructure Protection Act"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.088-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.088",
  "choice": "B",
  "answer": "Federal Information Security Management Act"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.088-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.088",
  "choice": "C",
  "answer": "Information Security Reform Act"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.088-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.088",
  "choice": "D",
  "answer": "Federal Sentencing Guidelines"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.011-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.011",
  "choice": "A",
  "answer": "Powering down a compromised system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.011-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.011",
  "choice": "B",
  "answer": "Disconnecting a compromised system from the network"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.011-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.011",
  "choice": "C",
  "answer": "Isolating a compromised system from other systems on the network"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.011-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.011",
  "choice": "D",
  "answer": "Preserving the system in a running state"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.109-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.109",
  "choice": "A",
  "answer": "CFAA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.109-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.109",
  "choice": "B",
  "answer": "CALEA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.109-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.109",
  "choice": "C",
  "answer": "EPPIA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.109-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.109",
  "choice": "D",
  "answer": "ECPA"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.022-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.022",
  "choice": "A",
  "answer": "Timely"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.022-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.022",
  "choice": "B",
  "answer": "Relevant"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.022-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.022",
  "choice": "C",
  "answer": "Material"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.022-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.022",
  "choice": "D",
  "answer": "Competent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.029-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.029",
  "choice": "A",
  "answer": "Authentication"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.029-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.029",
  "choice": "B",
  "answer": "Authorization"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.029-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.029",
  "choice": "C",
  "answer": "Accountability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.029-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.029",
  "choice": "D",
  "answer": "Identification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.043-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.043",
  "choice": "A",
  "answer": "COMMAND.EXE"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.043-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.043",
  "choice": "B",
  "answer": "CONFIG.SYS"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.043-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.043",
  "choice": "C",
  "answer": "AUTOEXEC.BAT"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.043-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.043",
  "choice": "D",
  "answer": "WIN32.DLL"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.107-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.107",
  "choice": "A",
  "answer": "Discretionary access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.107-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.107",
  "choice": "B",
  "answer": "Nondiscretionary access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.107-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.107",
  "choice": "C",
  "answer": "Role-based access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.107-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.107",
  "choice": "D",
  "answer": "Mandatory access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.113-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.113",
  "choice": "A",
  "answer": "Isolate and contain"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.113-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.113",
  "choice": "B",
  "answer": "Gather evidence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.113-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.113",
  "choice": "C",
  "answer": "Report"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.113-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.113",
  "choice": "D",
  "answer": "Restore service"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.066-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.066",
  "choice": "A",
  "answer": "Risk mitigation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.066-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.066",
  "choice": "B",
  "answer": "Risk assignment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.066-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.066",
  "choice": "C",
  "answer": "Risk acceptance"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.066-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.066",
  "choice": "D",
  "answer": "Risk rejection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.041-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.041",
  "choice": "A",
  "answer": "Polyinstantiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.041-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.041",
  "choice": "B",
  "answer": "Cell suppression"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.041-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.041",
  "choice": "C",
  "answer": "Aggregation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.041-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.041",
  "choice": "D",
  "answer": "Views"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.020-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.020",
  "choice": "A",
  "answer": "VLAN hopping"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.020-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.020",
  "choice": "B",
  "answer": "Multiple encapsulation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.020-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.020",
  "choice": "C",
  "answer": "Filter evasion using tunneling"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.020-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.020",
  "choice": "D",
  "answer": "Static IP addressing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.138-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.138",
  "choice": "A",
  "answer": "Sending overly large SYN packets"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.138-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.138",
  "choice": "B",
  "answer": "Exploiting a platform flaw in Windows"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.138-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.138",
  "choice": "C",
  "answer": "Using an amplification network to flood a victim with packets"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.138-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.138",
  "choice": "D",
  "answer": "Exploiting the TCP/IP three-way handshake"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.008-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.008",
  "choice": "A",
  "answer": "802.15"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.008-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.008",
  "choice": "B",
  "answer": "802.11"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.008-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.008",
  "choice": "C",
  "answer": "802.16"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.008-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.008",
  "choice": "D",
  "answer": "802.3"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.127-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.127",
  "choice": "A",
  "answer": "Mobile site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.127-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.127",
  "choice": "B",
  "answer": "Cloud provider"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.127-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.127",
  "choice": "C",
  "answer": "Hot site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.127-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.127",
  "choice": "D",
  "answer": "Cold site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.073-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.073",
  "choice": "A",
  "answer": "Infecting word processor documents"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.073-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.073",
  "choice": "B",
  "answer": "Creating botnets"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.073-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.073",
  "choice": "C",
  "answer": "Destroying data"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.073-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.073",
  "choice": "D",
  "answer": "Sending spam"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.053-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.053",
  "choice": "A",
  "answer": "Due notice"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.053-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.053",
  "choice": "B",
  "answer": "Due diligence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.053-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.053",
  "choice": "C",
  "answer": "Due care"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.053-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.053",
  "choice": "D",
  "answer": "Due date"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.079-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.079",
  "choice": "A",
  "answer": "Behavior-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.079-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.079",
  "choice": "B",
  "answer": "Network-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.079-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.079",
  "choice": "C",
  "answer": "Knowledge-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.079-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.079",
  "choice": "D",
  "answer": "Host-based"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.109-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.109",
  "choice": "A",
  "answer": "Transport"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.109-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.109",
  "choice": "B",
  "answer": "Data Link"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.109-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.109",
  "choice": "C",
  "answer": "Presentation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.109-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.109",
  "choice": "D",
  "answer": "Ethernet"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.088-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.088",
  "choice": "A",
  "answer": "Bridge"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.088-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.088",
  "choice": "B",
  "answer": "Router"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.088-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.088",
  "choice": "C",
  "answer": "Switch"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.088-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.088",
  "choice": "D",
  "answer": "Gateway"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.106-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.106",
  "choice": "A",
  "answer": "Password and a PIN"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.106-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.106",
  "choice": "B",
  "answer": "One-time password"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.106-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.106",
  "choice": "C",
  "answer": "Passphrase and a smart card"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.106-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.106",
  "choice": "D",
  "answer": "Fingerprint"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.076-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.076",
  "choice": "A",
  "answer": "128 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.076-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.076",
  "choice": "B",
  "answer": "192 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.076-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.076",
  "choice": "C",
  "answer": "256 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.076-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.076",
  "choice": "D",
  "answer": "512 bits"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.066-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.066",
  "choice": "A",
  "answer": "Simple"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.066-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.066",
  "choice": "B",
  "answer": "Administrative"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.066-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.066",
  "choice": "C",
  "answer": "Preventive"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.066-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.066",
  "choice": "D",
  "answer": "Transparent"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.128-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.128",
  "choice": "A",
  "answer": "Dedicated mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.128-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.128",
  "choice": "B",
  "answer": "System high mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.128-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.128",
  "choice": "C",
  "answer": "Compartmented mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.128-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.128",
  "choice": "D",
  "answer": "Multilevel mode"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.131-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.131",
  "choice": "A",
  "answer": "Dedicated"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.131-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.131",
  "choice": "B",
  "answer": "System high"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.131-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.131",
  "choice": "C",
  "answer": "Compartmented"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.131-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.131",
  "choice": "D",
  "answer": "Multilevel"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.027-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.027",
  "choice": "A",
  "answer": "Synchronous one-time passwords"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.027-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.027",
  "choice": "B",
  "answer": "Asynchronous one-time passwords"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.027-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.027",
  "choice": "C",
  "answer": "Strong static passwords"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.027-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.027",
  "choice": "D",
  "answer": "Retina scans"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.007-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.007",
  "choice": "A",
  "answer": "Simplicity is essential."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.007-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.007",
  "choice": "B",
  "answer": "Build projects around all team members equally."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.007-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.007",
  "choice": "C",
  "answer": "Working software is the primary measure of progress."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.007-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.007",
  "choice": "D",
  "answer": "The best designs emerge from self-organizing teams."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.148-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.148",
  "choice": "A",
  "answer": "Provides baseline for configuration management"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.148-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.148",
  "choice": "B",
  "answer": "Improves patch management response times"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.148-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.148",
  "choice": "C",
  "answer": "Reduces vulnerabilities from unpatched systems"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.148-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.148",
  "choice": "D",
  "answer": "Provides documentation for changes"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.099-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.099",
  "choice": "A",
  "answer": "Tunneling provides a connection method across untrusted systems."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.099-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.099",
  "choice": "B",
  "answer": "Traffic within a tunnel is isolated from inspection devices."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.099-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.099",
  "choice": "C",
  "answer": "Tunneling allows nonroutable traffic to be routed across other networks."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.099-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.099",
  "choice": "D",
  "answer": "Each encapsulated protocol includes its own error detection, error handling, acknowledgment, and session management features."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.003-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.003",
  "choice": "A",
  "answer": "Accreditation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.003-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.003",
  "choice": "B",
  "answer": "Assurance"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.003-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.003",
  "choice": "C",
  "answer": "Trust"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.003-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.003",
  "choice": "D",
  "answer": "Certification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.059-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.059",
  "choice": "A",
  "answer": "Radio frequency interference"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.059-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.059",
  "choice": "B",
  "answer": "Cross-talk noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.059-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.059",
  "choice": "C",
  "answer": "Traverse mode noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.059-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.059",
  "choice": "D",
  "answer": "Common mode noise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.137-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.137",
  "choice": "A",
  "answer": "Ethical hacking exercise"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.137-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.137",
  "choice": "B",
  "answer": "Full interruption test"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.137-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.137",
  "choice": "C",
  "answer": "Risk assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.137-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.137",
  "choice": "D",
  "answer": "Public review of policy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.072-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.072",
  "choice": "A",
  "answer": "Recovery access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.072-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.072",
  "choice": "B",
  "answer": "Corrective access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.072-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.072",
  "choice": "C",
  "answer": "Restorative access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.072-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.072",
  "choice": "D",
  "answer": "Compensation access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.060-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.060",
  "choice": "A",
  "answer": "A user’s privacy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.060-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.060",
  "choice": "B",
  "answer": "The public’s freedom"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.060-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.060",
  "choice": "C",
  "answer": "Intellectual property"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.060-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.060",
  "choice": "D",
  "answer": "A company’s right to audit"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.091-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.091",
  "choice": "A",
  "answer": "First"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.091-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.091",
  "choice": "B",
  "answer": "Fourth"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.091-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.091",
  "choice": "C",
  "answer": "Fifth"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.091-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.091",
  "choice": "D",
  "answer": "Tenth"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.092-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.092",
  "choice": "A",
  "answer": "Mobile site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.092-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.092",
  "choice": "B",
  "answer": "Service bureau"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.092-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.092",
  "choice": "C",
  "answer": "Hot site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.092-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.092",
  "choice": "D",
  "answer": "Cold site"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.057-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.057",
  "choice": "A",
  "answer": "443"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.057-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.057",
  "choice": "B",
  "answer": "565"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.057-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.057",
  "choice": "C",
  "answer": "1433"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.057-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.057",
  "choice": "D",
  "answer": "1521"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.026-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.026",
  "choice": "A",
  "answer": "Historical data"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.026-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.026",
  "choice": "B",
  "answer": "Search warrant"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.026-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.026",
  "choice": "C",
  "answer": "Subpoena"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.026-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.026",
  "choice": "D",
  "answer": "Voluntary surrender"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.073-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.073",
  "choice": "A",
  "answer": "Detection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.073-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.073",
  "choice": "B",
  "answer": "Response"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.073-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.073",
  "choice": "C",
  "answer": "Compliance"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.073-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.073",
  "choice": "D",
  "answer": "Remediation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.006-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.006",
  "choice": "A",
  "answer": "Awareness training"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.006-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.006",
  "choice": "B",
  "answer": "Logical security"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.006-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.006",
  "choice": "C",
  "answer": "Disaster recovery"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.006-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.006",
  "choice": "D",
  "answer": "Physical security"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.089-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.089",
  "choice": "A",
  "answer": "Scanning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.089-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.089",
  "choice": "B",
  "answer": "Malicious code"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.089-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.089",
  "choice": "C",
  "answer": "Grudge"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.089-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.089",
  "choice": "D",
  "answer": "Distributed denial of service"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.098-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.098",
  "choice": "A",
  "answer": "Require a VPN"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.098-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.098",
  "choice": "B",
  "answer": "Disable SSID broadcast."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.098-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.098",
  "choice": "C",
  "answer": "Issue static IP addresses."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.098-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.098",
  "choice": "D",
  "answer": "Use MAC filtering."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.127-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.127",
  "choice": "A",
  "answer": "Purging the drive with a software program"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.127-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.127",
  "choice": "B",
  "answer": "Copying data over the existing data"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.127-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.127",
  "choice": "C",
  "answer": "Removing the platters and shredding them"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.127-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.127",
  "choice": "D",
  "answer": "Removing the platters and disintegrating them"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.026-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.026",
  "choice": "A",
  "answer": "Accuracy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.026-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.026",
  "choice": "B",
  "answer": "Acceptability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.026-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.026",
  "choice": "C",
  "answer": "Enrollment time"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.026-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.026",
  "choice": "D",
  "answer": "Invasiveness"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.115-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.115",
  "choice": "A",
  "answer": "Integrity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.115-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.115",
  "choice": "B",
  "answer": "Availability"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.115-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.115",
  "choice": "C",
  "answer": "Nonrepudiation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.115-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.115",
  "choice": "D",
  "answer": "Auditing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.059-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.059",
  "choice": "A",
  "answer": "Collection"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.059-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.059",
  "choice": "B",
  "answer": "Processing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.059-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.059",
  "choice": "C",
  "answer": "Review"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.059-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.059",
  "choice": "D",
  "answer": "Analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.057-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.057",
  "choice": "A",
  "answer": "Assured"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.057-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.057",
  "choice": "B",
  "answer": "Updated"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.057-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.057",
  "choice": "C",
  "answer": "Protected"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.057-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.057",
  "choice": "D",
  "answer": "Trusted"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.003-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.003",
  "choice": "A",
  "answer": "Inventory assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.003-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.003",
  "choice": "B",
  "answer": "Threat assessment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.003-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.003",
  "choice": "C",
  "answer": "Risk analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.003-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.003",
  "choice": "D",
  "answer": "Critical path analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.036-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.036",
  "choice": "A",
  "answer": "Encrypted"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.036-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.036",
  "choice": "B",
  "answer": "Triple"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.036-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.036",
  "choice": "C",
  "answer": "Restricted"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.036-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.036",
  "choice": "D",
  "answer": "Unrestricted"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.098-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.098",
  "choice": "A",
  "answer": "$"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.098-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.098",
  "choice": "B",
  "answer": "&"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.098-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.098",
  "choice": "C",
  "answer": "˂"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.116-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.116",
  "choice": "A",
  "answer": "Phishing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.116-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.116",
  "choice": "B",
  "answer": "Spear phishing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.116-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.116",
  "choice": "C",
  "answer": "Whaling"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.116-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.116",
  "choice": "D",
  "answer": "Vishing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.116-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.116",
  "choice": "A",
  "answer": "Hybrid environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.116-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.116",
  "choice": "B",
  "answer": "Compartmentalized environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.116-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.116",
  "choice": "C",
  "answer": "Hierarchical environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.116-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.116",
  "choice": "D",
  "answer": "Security environment"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.011-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.011",
  "choice": "A",
  "answer": "Asset value"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.011-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.011",
  "choice": "B",
  "answer": "Administrator discretion"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.011-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.011",
  "choice": "C",
  "answer": "Risk"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.011-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.011",
  "choice": "D",
  "answer": "Level of realized threats"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.097-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.097",
  "choice": "A",
  "answer": "Chosen plain-text attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.097-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.097",
  "choice": "B",
  "answer": "Meet-in-the-middle attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.097-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.097",
  "choice": "C",
  "answer": "Man-in-the-middle attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.097-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.097",
  "choice": "D",
  "answer": "Replay attack"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.047-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.047",
  "choice": "A",
  "answer": "Marking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.047-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.047",
  "choice": "B",
  "answer": "Purging"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.047-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.047",
  "choice": "C",
  "answer": "Sanitizing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.047-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.047",
  "choice": "D",
  "answer": "Retaining"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.004-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.004",
  "choice": "A",
  "answer": "Polymorphic"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.004-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.004",
  "choice": "B",
  "answer": "Encrypted"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.004-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.004",
  "choice": "C",
  "answer": "Multipartite"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.004-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.004",
  "choice": "D",
  "answer": "Stealth"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.039-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.039",
  "choice": "A",
  "answer": "Policies, standards, baselines, guidelines, and procedures can be combined in a single document."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.039-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.039",
  "choice": "B",
  "answer": "Not all users need to know the security standards, baselines, guidelines, and procedures for all security classification levels."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.039-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.039",
  "choice": "C",
  "answer": "When changes occur, it is easier to update and redistribute only the affected material rather than update a monolithic policy and redistribute it."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.039-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.039",
  "choice": "D",
  "answer": "Higher up the formalization structure (that is, security policies), there are fewer documents because they are general broad discussions of overview and goals. Further down the formalization structure (that is, guidelines and procedures), there are many documents because they contain details specific to a limited number of systems, networks, divisions, areas, and so on."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.086-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.086",
  "choice": "A",
  "answer": "Security guards"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.086-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.086",
  "choice": "B",
  "answer": "Fences"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.086-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.086",
  "choice": "C",
  "answer": "Badges"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.086-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.086",
  "choice": "D",
  "answer": "Lighting"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.119-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.119",
  "choice": "A",
  "answer": "Process isolation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.119-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.119",
  "choice": "B",
  "answer": "Abstraction"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.119-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.119",
  "choice": "C",
  "answer": "Monitoring"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.119-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.119",
  "choice": "D",
  "answer": "Hardware segmentation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.079-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.079",
  "choice": "A",
  "answer": "Web application firewall"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.079-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.079",
  "choice": "B",
  "answer": "Intrusion prevention system"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.079-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.079",
  "choice": "C",
  "answer": "Network vulnerability scanner"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.079-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.079",
  "choice": "D",
  "answer": "None. There is no exception to the recurring web vulnerability scan requirement."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.005-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.005",
  "choice": "A",
  "answer": "BYOD"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.005-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.005",
  "choice": "B",
  "answer": "CYOD"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.005-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.005",
  "choice": "C",
  "answer": "COPE"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.005-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.005",
  "choice": "D",
  "answer": "OCSP"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.046-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.046",
  "choice": "A",
  "answer": "SLE * ARO"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.046-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.046",
  "choice": "B",
  "answer": "EF AV ARO"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.046-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.046",
  "choice": "C",
  "answer": "(ALE1 – ALE2) – CM cost"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.046-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.046",
  "choice": "D",
  "answer": "Total risk + controls gap"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.078-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.078",
  "choice": "A",
  "answer": "To approve changes"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.078-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.078",
  "choice": "B",
  "answer": "To reject changes"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.078-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.078",
  "choice": "C",
  "answer": "To identify changes"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.078-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.078",
  "choice": "D",
  "answer": "To review changes"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.065-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.065",
  "choice": "A",
  "answer": "Data classification"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.065-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.065",
  "choice": "B",
  "answer": "Abstraction"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.065-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.065",
  "choice": "C",
  "answer": "Superzapping"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.065-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.065",
  "choice": "D",
  "answer": "Using covert channels"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.074-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.074",
  "choice": "A",
  "answer": "Cost/benefit analysis"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.074-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.074",
  "choice": "B",
  "answer": "Compliance with existing baselines"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.074-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.074",
  "choice": "C",
  "answer": "Legal liability and prudent due care"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.074-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.074",
  "choice": "D",
  "answer": "Compatibility with IT infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.114-A",
  "QuestionId": "tb475934.CISSPSG8E.be2.114",
  "choice": "A",
  "answer": "Enforce strong passwords through a security policy."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.114-B",
  "QuestionId": "tb475934.CISSPSG8E.be2.114",
  "choice": "B",
  "answer": "Maintain strict control over physical access."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.114-C",
  "QuestionId": "tb475934.CISSPSG8E.be2.114",
  "choice": "C",
  "answer": "Require all users to log in remotely."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be2.114-D",
  "QuestionId": "tb475934.CISSPSG8E.be2.114",
  "choice": "D",
  "answer": "Use two-factor authentication."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.004-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.004",
  "choice": "A",
  "answer": "Differential backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.004-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.004",
  "choice": "B",
  "answer": "Partial backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.004-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.004",
  "choice": "C",
  "answer": "Incremental backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.004-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.004",
  "choice": "D",
  "answer": "Full backup"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.018-A",
  "QuestionId": "tb475934.CISSPSG8E.be3.018",
  "choice": "A",
  "answer": "Community"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.018-B",
  "QuestionId": "tb475934.CISSPSG8E.be3.018",
  "choice": "B",
  "answer": "Hybrid"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.018-C",
  "QuestionId": "tb475934.CISSPSG8E.be3.018",
  "choice": "C",
  "answer": "Private"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be3.018-D",
  "QuestionId": "tb475934.CISSPSG8E.be3.018",
  "choice": "D",
  "answer": "Public"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.031-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.031",
  "choice": "A",
  "answer": "Constrained data item"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.031-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.031",
  "choice": "B",
  "answer": "Transformation procedures"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.031-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.031",
  "choice": "C",
  "answer": "Redundant commit statement"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.031-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.031",
  "choice": "D",
  "answer": "Integrity verification procedure"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.052-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.052",
  "choice": "A",
  "answer": "Espionage"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.052-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.052",
  "choice": "B",
  "answer": "Sabotage"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.052-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.052",
  "choice": "C",
  "answer": "Reverse engineering"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.052-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.052",
  "choice": "D",
  "answer": "Social engineering"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.079-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.079",
  "choice": "A",
  "answer": "Reparation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.079-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.079",
  "choice": "B",
  "answer": "Restoration"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.079-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.079",
  "choice": "C",
  "answer": "Respiration"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.079-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.079",
  "choice": "D",
  "answer": "Recovery"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.022-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.022",
  "choice": "A",
  "answer": "An object"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.022-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.022",
  "choice": "B",
  "answer": "A subject"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.022-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.022",
  "choice": "C",
  "answer": "A role"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.022-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.022",
  "choice": "D",
  "answer": "An account"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.045-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.045",
  "choice": "A",
  "answer": "Administrative control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.045-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.045",
  "choice": "B",
  "answer": "Corrective control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.045-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.045",
  "choice": "C",
  "answer": "Directive control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.045-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.045",
  "choice": "D",
  "answer": "Detective control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.064-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.064",
  "choice": "A",
  "answer": "Documentation of policies for continuity"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.064-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.064",
  "choice": "B",
  "answer": "Historical record"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.064-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.064",
  "choice": "C",
  "answer": "Job creation"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.064-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.064",
  "choice": "D",
  "answer": "Identification of flaws"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.063-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.063",
  "choice": "A",
  "answer": "Preencrypting"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.063-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.063",
  "choice": "B",
  "answer": "Prewhitening"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.063-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.063",
  "choice": "C",
  "answer": "Precleaning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.063-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.063",
  "choice": "D",
  "answer": "Prepending"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.022-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.022",
  "choice": "A",
  "answer": "Symmetric multiprocessing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.022-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.022",
  "choice": "B",
  "answer": "Multitasking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.022-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.022",
  "choice": "C",
  "answer": "Multiprogramming"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.022-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.022",
  "choice": "D",
  "answer": "Massively parallel processing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.082-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.082",
  "choice": "A",
  "answer": "Man-in-the-middle"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.082-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.082",
  "choice": "B",
  "answer": "Spoofing"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.082-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.082",
  "choice": "C",
  "answer": "Hijacking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.082-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.082",
  "choice": "D",
  "answer": "Cracking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.036-A",
  "QuestionId": "tb475934.CISSPSG8E.be4.036",
  "choice": "A",
  "answer": "Two"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.036-B",
  "QuestionId": "tb475934.CISSPSG8E.be4.036",
  "choice": "B",
  "answer": "Three"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.036-C",
  "QuestionId": "tb475934.CISSPSG8E.be4.036",
  "choice": "C",
  "answer": "Thirty"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be4.036-D",
  "QuestionId": "tb475934.CISSPSG8E.be4.036",
  "choice": "D",
  "answer": "Undefined"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.105-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.105",
  "choice": "A",
  "answer": "Children must be kept anonymous when participating in online forums."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.105-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.105",
  "choice": "B",
  "answer": "Parents must be provided with the opportunity to review information collected from their children."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.105-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.105",
  "choice": "C",
  "answer": "Parents must give verifiable consent to the collection of information about children."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.105-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.105",
  "choice": "D",
  "answer": "Websites must have a privacy notice."
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.045-A",
  "QuestionId": "tb475934.CISSPSG8E.be5.045",
  "choice": "A",
  "answer": "Subnet"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.045-B",
  "QuestionId": "tb475934.CISSPSG8E.be5.045",
  "choice": "B",
  "answer": "DMZ"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.045-C",
  "QuestionId": "tb475934.CISSPSG8E.be5.045",
  "choice": "C",
  "answer": "VLAN"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be5.045-D",
  "QuestionId": "tb475934.CISSPSG8E.be5.045",
  "choice": "D",
  "answer": "Extranet"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.13-A",
  "QuestionId": "tb475934.CISSPSG8E.c01.13",
  "choice": "A",
  "answer": "Preventing an authorized reader of an object from deleting that object"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.13-B",
  "QuestionId": "tb475934.CISSPSG8E.c01.13",
  "choice": "B",
  "answer": "Keeping a database from being accessed by unauthorized visitors"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.13-C",
  "QuestionId": "tb475934.CISSPSG8E.c01.13",
  "choice": "C",
  "answer": "Restricting a subject at a lower classification level from accessing data at a higher classification level"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c01.13-D",
  "QuestionId": "tb475934.CISSPSG8E.c01.13",
  "choice": "D",
  "answer": "Preventing an application from accessing hardware directly"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.17-A",
  "QuestionId": "tb475934.CISSPSG8E.c02.17",
  "choice": "A",
  "answer": "Education"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.17-B",
  "QuestionId": "tb475934.CISSPSG8E.c02.17",
  "choice": "B",
  "answer": "Awareness"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.17-C",
  "QuestionId": "tb475934.CISSPSG8E.c02.17",
  "choice": "C",
  "answer": "Training"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.17-D",
  "QuestionId": "tb475934.CISSPSG8E.c02.17",
  "choice": "D",
  "answer": "Termination"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.10-A",
  "QuestionId": "tb475934.CISSPSG8E.c02.10",
  "choice": "A",
  "answer": "Threat events"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.10-B",
  "QuestionId": "tb475934.CISSPSG8E.c02.10",
  "choice": "B",
  "answer": "Risks"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.10-C",
  "QuestionId": "tb475934.CISSPSG8E.c02.10",
  "choice": "C",
  "answer": "Threat agents"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.10-D",
  "QuestionId": "tb475934.CISSPSG8E.c02.10",
  "choice": "D",
  "answer": "Breaches"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.20-A",
  "QuestionId": "tb475934.CISSPSG8E.c02.20",
  "choice": "A",
  "answer": "Exposure factor"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.20-B",
  "QuestionId": "tb475934.CISSPSG8E.c02.20",
  "choice": "B",
  "answer": "Single loss expectancy"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.20-C",
  "QuestionId": "tb475934.CISSPSG8E.c02.20",
  "choice": "C",
  "answer": "Asset value"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.c02.20-D",
  "QuestionId": "tb475934.CISSPSG8E.c02.20",
  "choice": "D",
  "answer": "Annualized rate of occurrence"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.137-A",
  "QuestionId": "tb475934.CISSPSG8E.be6.137",
  "choice": "A",
  "answer": "Denial of service"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.137-B",
  "QuestionId": "tb475934.CISSPSG8E.be6.137",
  "choice": "B",
  "answer": "Website defacement"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.137-C",
  "QuestionId": "tb475934.CISSPSG8E.be6.137",
  "choice": "C",
  "answer": "Port scanning"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be6.137-D",
  "QuestionId": "tb475934.CISSPSG8E.be6.137",
  "choice": "D",
  "answer": "Phone phreaking"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.065-A",
  "QuestionId": "tb475934.CISSPSG8E.be1.065",
  "choice": "A",
  "answer": "Compensation access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.065-B",
  "QuestionId": "tb475934.CISSPSG8E.be1.065",
  "choice": "B",
  "answer": "Recovery access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.065-C",
  "QuestionId": "tb475934.CISSPSG8E.be1.065",
  "choice": "C",
  "answer": "Restorative access control"
 },
 {
  "type": "ANSWER",
  "id": "tb475934.CISSPSG8E.be1.065-D",
  "QuestionId": "tb475934.CISSPSG8E.be1.065",
  "choice": "D",
  "answer": "Corrective access control"
 }
]