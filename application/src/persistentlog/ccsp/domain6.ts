export const Log = [
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.10",
  "question": "When does the EU Data Protection Directive (Directive 95/46/EC) apply to data processed?",
  "answer": "A",
  "explanation": "Directive 95/46/EC of the European Parliament and of the Council of October 24, 1995, on the protection of individuals with regard to the processing of personal data and on the free movement of such data, regulates the processing of personal data within the European Union. It is designed to protect the privacy and protection of all personal data collected for or about citizens of the European Union, especially as it relates to the processing, using, or exchanging of such data. The data protection directive encompasses the key elements from article 8 of the European Convention on Human Rights, which states its intention to respect the rights of privacy in personal and family life, as well as in the home and in personal correspondence. This directive applies to data processed by automated means and data contained in paper files. It does not apply to the processing of data in these instances: By a natural person in the course of purely personal or household activities In the course of an activity that falls outside the scope of community law, such as operations concerning public safety, defense, or state security The directive aims to protect the rights and freedoms of persons with respect to the processing of personal data by laying down guidelines determining when this processing is lawful."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.11",
  "question": "Which of the following are contractual components that the CCSP should review and understand fully when contracting with a CSP? (Choose two.)",
  "answer": "B,D",
  "explanation": [
   {
    "value": "Scope of processing: Clear understanding of the permissible types of data processing should be provided. The specifications should also list the purpose for which the data can be processed or utilized."
   },
   {
    "value": "Use of subcontractors: Understanding where any processing, transmission, storage, or use of information will occur. A complete list should be drawn up including the entity, location, rationale, form of data use (processing, transmission, and storage), and any limitations or nonpermitted uses. Contractually, the requirement for the procuring organization to be informed as to where data has been provided or will be utilized by a subcontractor is essential."
   },
   {
    "value": "Deletion of data: Where the business operations no longer require information to be retained for a specific purpose (that is, not retaining for convenience or potential future uses), the deletion of information should occur in line with the organizations data retention policies and standards. Data deletion is also of critical importance when contractors and subcontractors no longer provide services or when a contract is terminated."
   },
   {
    "value": "Appropriate or required data security controls: Where processing, transmission, or storage of data and resources is outsourced, the same level of security controls should be required for any entry’s contracting or subcontracting services. Ideally, security controls should be of a higher level (which is the case for a large number of cloud computing services) than the existing levels of controls; however, this is never to be taken as a given in the absence of confirmation or verification. Additionally, technical security controls should be unequivocally called out and stipulated in the contract; they are applicable to any subcontractors as well."
   },
   {
    "value": "Locations of data: To ensure compliance with regulatory and legal requirements, the CCSP needs to understand the location of contractors and subcontractors. She must pay particular attention to where the organization is located and where operations, data centers, and headquarters are located. The CCSP needs to know where information is being stored, processed, and transmitted. Finally, any contingency or continuity requirements may require failover to different geographic locations, which can affect or violate regulatory or contractual requirements. The CCSP should fully understand these and accept them prior to engagement of services with any contractor, subcontractor, or CSP."
   },
   {
    "value": "Return or restitution of data: For both contractors and subcontractors where a contract is terminated, the timely and orderly return of data has to be required both contractually and within the SLA. Format and structure of data should be clearly documented, with an emphasis on structured and agreed-upon formats being clearly understood by all parties. Data retention periods should be explicitly understood, with the return of data to the organization that owns the data, resulting in the removal or secure deletion on any contractors’ or subcontractors’ systems or storage."
   },
   {
    "value": "Right to audit subcontractors: Right to audit clauses should allow for the organization owning the data (not possessing) to audit or engage the services of an independent party to ensure that contractual and regulatory requirements are being satisfied by either the contractor or the subcontractor.\n Gordon, Adam. The Official (ISC)2 Guide to the CCSP CBK (pp. 483-484). Wiley. Kindle Edition."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.13",
  "question": "What does an audit scope statement provide to a cloud service customer or organization?",
  "answer": "B",
  "explanation": [
   {
    "value": "General statement of focus and objectives Scope of audit (including exclusions)"
   },
   {
    "value": "Type of audit (certification, attestation, and so on)"
   },
   {
    "value": "Security assessment requirements Assessment criteria (including ratings)"
   },
   {
    "value": "Acceptance criteria"
   },
   {
    "value": "Deliverables"
   },
   {
    "value": "Classification (confidential, highly confidential, secret, top secret, public, and so on) The audit scope statement can also catalog the circulation list, along with key individuals associated with the audit."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.14",
  "question": "Which of the following should be carried out first when seeking to perform a gap analysis?",
  "answer": "C",
  "explanation": [
   {
    "value": "Obtain management support from the right managers."
   },
   {
    "value": "Define the scope and objectives."
   },
   {
    "value": "Plan an assessment schedule."
   },
   {
    "value": "Agree on a plan."
   },
   {
    "value": "Conduct information gathering exercises Interview key personnel."
   },
   {
    "value": "Review supporting documentation."
   },
   {
    "value": "Verify the information obtained."
   },
   {
    "value": "Identify any potential risks."
   },
   {
    "value": "Document the findings."
   },
   {
    "value": "Develop a report and recommendations."
   },
   {
    "value": "Present the report."
   },
   {
    "value": "Sign off and accept the report. \nThe objective of a gap analysis is to identify and report on any gaps or risks that may affect the AIC of key information assets. The value of such an assessment is often determined based on what you did not know or for an independent resource to communicate to relevant management or senior personnel such risks, as opposed to internal resources saying what you need or should be doing."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.15",
  "question": "What is the first international set of privacy controls in the cloud?",
  "answer": "Consent: CSPs must not use the personal data they receive for advertising and marketing unless expressly instructed to do so by the customers. In addition, a customer should be able to employ the service without having to consent to the use of her personal data for advertising or marketing.,Control: Customers have explicit control over how CSPs are to use their information.,Transparency: CSPs must inform customers about items such as where their data resides. CSPs also need to disclose to customers the use of any subcontractors who will be used to process PII.,Communication: CSPs should keep clear records about any incident and their response to it, and they should notify customers.,Independent and yearly audit: To remain compliant, the CSP must subject itself to yearly third-party reviews. This allows the customer to rely upon the findings to support her own regulatory obligations.\n Trust is key for consumers leveraging the cloud; therefore, vendors of cloud services are working toward adopting the stringent privacy principles outlined in ISO 27018.",
  "explanation": ""
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.10-A",
  "QuestionId": "CDS.2021-10-19.10",
  "choice": "A",
  "answer": "The directive applies to data processed by automated means and data contained in paper files."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.10-B",
  "QuestionId": "CDS.2021-10-19.10",
  "choice": "B",
  "answer": "The directive applies to data processed by a natural person in the course of purely personal activities."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.10-C",
  "QuestionId": "CDS.2021-10-19.10",
  "choice": "C",
  "answer": "The directive applies to data processed in the course of an activity that falls outside the scope of community law, such as public safety."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.10-D",
  "QuestionId": "CDS.2021-10-19.10",
  "choice": "D",
  "answer": "The directive applies to data processed by automated means in the course of purely personal activities."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.11-A",
  "QuestionId": "CDS.2021-10-19.11",
  "choice": "A",
  "answer": "Concurrently maintainable site infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.11-B",
  "QuestionId": "CDS.2021-10-19.11",
  "choice": "B",
  "answer": "Use of subcontractors"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.11-C",
  "QuestionId": "CDS.2021-10-19.11",
  "choice": "C",
  "answer": "Redundant site infrastructure capacity components"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.11-D",
  "QuestionId": "CDS.2021-10-19.11",
  "choice": "D",
  "answer": "Scope of processing"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.13-A",
  "QuestionId": "CDS.2021-10-19.13",
  "choice": "A",
  "answer": "The credentials of the auditors, as well as the projected cost of the audit"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.13-B",
  "QuestionId": "CDS.2021-10-19.13",
  "choice": "B",
  "answer": "The required level of information for the client or organization subject to the audit to fully understand (and agree) with the scope, focus, and type of assessment being performed"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.13-C",
  "QuestionId": "CDS.2021-10-19.13",
  "choice": "C",
  "answer": "A list of all the security controls to be audited"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.13-D",
  "QuestionId": "CDS.2021-10-19.13",
  "choice": "D",
  "answer": "The outcome of the audit, as well as any findings that need to be addressed"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.14-A",
  "QuestionId": "CDS.2021-10-19.14",
  "choice": "A",
  "answer": "Define scope and objectives."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.14-B",
  "QuestionId": "CDS.2021-10-19.14",
  "choice": "B",
  "answer": "Identify potential risks."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.14-C",
  "QuestionId": "CDS.2021-10-19.14",
  "choice": "C",
  "answer": "Obtain management support."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.14-D",
  "QuestionId": "CDS.2021-10-19.14",
  "choice": "D",
  "answer": "Conduct information gathering"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.15-A",
  "QuestionId": "CDS.2021-10-19.15",
  "choice": "A",
  "answer": "ISO/IEC 27032"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.15-B",
  "QuestionId": "CDS.2021-10-19.15",
  "choice": "B",
  "answer": "ISO/IEC 27005"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.15-C",
  "QuestionId": "CDS.2021-10-19.15",
  "choice": "C",
  "answer": "ISO/IEC 27002"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.15-D",
  "QuestionId": "CDS.2021-10-19.15",
  "choice": "D",
  "answer": "ISO/IEC 27018"
 }
]