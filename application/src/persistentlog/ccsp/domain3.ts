export const Log = [
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-01.1",
  "question": "What is a cloud carrier?",
  "answer": "B",
  "explanation": [
   {
    "value": "Cloud service consumer: Person or organization that maintains a business relationship with, and uses service from, CSPs."
   },
   {
    "value": "Cloud service provider: Person, organization, or entity responsible for making a service available to service consumers."
   },
   {
    "value": "Cloud carrier: The intermediary that provides connectivity and transport of cloud services between CSPs and cloud consumers. \nIn the NIST Cloud Computing reference model, the network and communication function is provided as part of the cloud carrier role. In practice, this is an IP service, increasingly delivered through IPv4 and IPv6. This IP network might not be part of the public Internet."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-01.2",
  "question": "Which of the following statements about SDN is correct (Choose 2)?",
  "answer": "A, D",
  "explanation": [
   {
    "value": "Directly programmable: Network control is directly programmable because it is decoupled from forwarding functions."
   },
   {
    "value": "Agile: Abstracting control from forwarding lets administrators dynamically adjust network-wide traffic flow to meet changing needs."
   },
   {
    "value": "Centrally managed: Network intelligence is (logically) centralized in software-based SDN controllers that maintain a global view of the network, which appears to applications and policy engines as a single, logical switch."
   },
   {
    "value": "Programmatically configured: SDN lets network managers configure, manage, secure, and optimize network resources quickly via dynamic, automated SDN programs, which they can write themselves because the programs do not depend on proprietary software."
   },
   {
    "value": "Open standards based and vendor neutral: When implemented through open standards, SDN simplifies network design and operation because instructions are provided by SDN controllers instead of multiple, vendor-specific devices and protocols."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-01.3",
  "question": "With regards to management of the compute resources of a host in a cloud environment, what does a reservation provide?",
  "answer": "B",
  "explanation": "The use of reservations, limits, and shares provides the contextual ability for an administrator to allocate the compute resources of a host. A reservation creates a guaranteed minimum resource allocation that must be met by the host with physical compute resources to allow a guest to power on and operate. This reservation is traditionally available for either CPU or RAM, or both, as needed. A limit creates a maximum ceiling for a resource allocation. This ceiling may be fixed, or it may be expandable, allowing for the acquisition of more compute resources through\na borrowing scheme from the root resource provider (the host). Shares are used to arbitrate the issues associated with compute resource contention situations. Resource contention implies the existence of too many requests for resources based on the actual available resources currently in the system. If resource contention takes place, share values are used to prioritize compute resource access for all guests assigned a certain number of shares. The shares are weighed and used as a percentage against all outstanding shares assigned and in use by all powered-on guests to calculate the amount of resources each guest is given access to. The higher the share value assigned to the guest, the larger the percentage of the remaining resources they are given access to during the contention period."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-01.4",
  "question": "What is the key issue associated with the object storage type that the CCSP has to be aware of?",
  "answer": "A",
  "explanation": "The features you get in an object storage system are typically minimal. You can store, retrieve, copy, and delete files, as well as control which users can undertake these actions. If you want the ability to search or to have a central repository of object metadata that other applications can draw on, you generally have to implement them yourself. Amazon S3 and other object storage systems provide REST APIs that allow programmers to work with the containers and objects. The key issue that the CCSP has to be aware of with object storage systems is that data consistency is achieved only eventually. Whenever you update a file, you may have to wait until the change is propagated to all the replicas before requests return the latest version. This makes object storage unsuitable for data that changes frequently. However, it provides a good solution for data that does not change much, such as backups, archives, video and audio files, and VM images."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-01.5",
  "question": "What types of risks are typically associated with virtualization?",
  "answer": "B",
  "explanation": [
   {
    "value": "Guest breakout: This occurs when there is a breakout of a guest OS so that it can access the hypervisor or other guests. This is presumably facilitated by a hypervisor flaw."
   },
   {
    "value": "Snapshot and image security: The portability of images and snapshots makes people forget that images and snapshots can contain sensitive information and need protecting."
   },
   {
    "value": "Sprawl: This occurs when you lose control of the amount of content on your image store."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-04.1",
  "question": "When using a SaaS solution, who is responsible for application security?",
  "answer": "D",
  "explanation": "Implementation of controls requires cooperation and a clear demarcation of responsibility between the CSP and the cloud consumer. Without that, there is a real risk for certain important controls to be absent. For example, IaaS providers typically do not consider guest OS hardening their responsibility.\n Notice there there is an band of shared responsibility"
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-04.2",
  "question": "Which of the following are examples of trust zones? (Choose two.)",
  "answer": "B,C",
  "explanation": "A trust zone can be defined as a network segment within which data flows relatively freely, whereas data flowing in and out of the trust zone is subject to stronger restrictions. Some examples of trust zones include demilitarized zones (DMZs); site-specific zones, such as segmentation according to department or function; and application-defined zones, such as the three tiers of a web application."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-04.3",
  "question": "What are the relevant cloud infrastructure characteristics that can be considered distinct advantages in realizing a BCDR plan objective with regards to cloud computing environments?",
  "answer": "B",
  "explanation": [
   {
    "value": "Rapid elasticity and on-demand self-service lead to a flexible infrastructure that can be quickly deployed to execute an actual DR without hitting unexpected ceilings."
   },
   {
    "value": "Broad network connectivity reduces operational risk."
   },
   {
    "value": "Cloud infrastructure providers have resilient infrastructure, and an external BCDR provider has the potential for being experienced and capable because their technical and people resources are being shared across a number of tenants."
   },
   {
    "value": "Pay-per-use can mean that the total BCDR strategy can be a lot cheaper than alternative solutions. During normal operation, the BCDR solution is likely to have a low cost. Of course, as part of due diligence in your BCDR plan, you should validate all assumptions with the candidate service provider and ensure that they are documented in your SLAs."
   }
  ]
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.1-A",
  "QuestionId": "CDS.2021-10-01.1",
  "choice": "A",
  "answer": "A person, organization, or entity responsible for making a service available to service consumers"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.1-B",
  "QuestionId": "CDS.2021-10-01.1",
  "choice": "B",
  "answer": "The intermediary that provides connectivity and transport of cloud services between CSPs and cloud service consumers"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.1-C",
  "QuestionId": "CDS.2021-10-01.1",
  "choice": "C",
  "answer": "A person or organization that maintains a business relationship with, and uses service from, CSPs"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.1-D",
  "QuestionId": "CDS.2021-10-01.1",
  "choice": "D",
  "answer": "The intermediary that provides business continuity of cloud services between cloud service consumers"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.2-A",
  "QuestionId": "CDS.2021-10-01.2",
  "choice": "A",
  "answer": "SDN enables you to execute the control plane software on general-purpose hardware, allowing for the decoupling from specific network hardware configurations and allowing for the use of commodity servers. Further, the use of software-based controllers permits a view of the network that presents a logical switch to the applications running above, allowing for access via APIs that can be used to configure, manage, and secure network resources."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.2-B",
  "QuestionId": "CDS.2021-10-01.2",
  "choice": "B",
  "answer": "SDN’s objective is to provide a clearly defined network control plane to manage network traffic that is not separated from the forwarding plane. This approach allows for network control to become directly programmable and for dynamic adjustment of traffic flows to address changing patterns of consumption."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.2-C",
  "QuestionId": "CDS.2021-10-01.2",
  "choice": "C",
  "answer": "SDN enables you to execute the control plane software on specific hardware, allowing for the binding of specific network hardware configurations. Further, the use of software-based controllers permits a view of the network that presents a logical switch to the applications running above, allowing for access via APIs that can be used to configure, manage, and secure network resources."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.2-D",
  "QuestionId": "CDS.2021-10-01.2",
  "choice": "D",
  "answer": "SDN’s objective is to offer a clearly defined and separate network control plane to manage network traffic that is separated from the forwarding plane. This approach permits network control to become directly programmable and distinct from forwarding, allowing for dynamic adjustment of traffic flows to address changing patterns of consumption."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.3-A",
  "QuestionId": "CDS.2021-10-01.3",
  "choice": "A",
  "answer": "The ability to arbitrate the issues associated with compute resource contention situations. Resource contention implies that there are too many requests for resources based on the actual available resources currently in the system."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.3-B",
  "QuestionId": "CDS.2021-10-01.3",
  "choice": "B",
  "answer": "A guaranteed minimum resource allocation that must be met by the host with physical compute resources to allow a guest to power on and operate."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.3-C",
  "QuestionId": "CDS.2021-10-01.3",
  "choice": "C",
  "answer": "A maximum ceiling for a resource allocation. This ceiling may be fixed, or it may be expandable, allowing for the acquisition of more compute resources through a borrowing scheme from the root resource provider (the host)."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.3-D",
  "QuestionId": "CDS.2021-10-01.3",
  "choice": "D",
  "answer": "A guaranteed maximum resource allocation that must be met by the host with physical compute resources to allow a guest to power on and operate."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.4-A",
  "QuestionId": "CDS.2021-10-01.4",
  "choice": "A",
  "answer": "Data consistency, which is achieved only after change propagation to all replica instances has taken place"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.4-B",
  "QuestionId": "CDS.2021-10-01.4",
  "choice": "B",
  "answer": "Access control"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.4-C",
  "QuestionId": "CDS.2021-10-01.4",
  "choice": "C",
  "answer": "Data consistency, which is achieved only after change propagation to a specified percentage of replica instances has taken place"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.4-D",
  "QuestionId": "CDS.2021-10-01.4",
  "choice": "D",
  "answer": "Continuous monitoring"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.5-A",
  "QuestionId": "CDS.2021-10-01.5",
  "choice": "A",
  "answer": "Loss of governance, snapshot and image security, and sprawl"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.5-B",
  "QuestionId": "CDS.2021-10-01.5",
  "choice": "B",
  "answer": "Guest breakout, snapshot and image availability, and compliance"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.5-C",
  "QuestionId": "CDS.2021-10-01.5",
  "choice": "C",
  "answer": "Guest breakout, snapshot and image security, and sprawl"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-01.5-D",
  "QuestionId": "CDS.2021-10-01.5",
  "choice": "D",
  "answer": "Guest breakout, knowledge level required to manage, and sprawl"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.1-A",
  "QuestionId": "CDS.2021-10-04.1",
  "choice": "A",
  "answer": "Both the cloud service consumer and the enterprise"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.1-B",
  "QuestionId": "CDS.2021-10-04.1",
  "choice": "B",
  "answer": "The enterprise only"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.1-C",
  "QuestionId": "CDS.2021-10-04.1",
  "choice": "C",
  "answer": "The CSP only"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.1-D",
  "QuestionId": "CDS.2021-10-04.1",
  "choice": "D",
  "answer": "Both CSP and the enterprise"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.2-A",
  "QuestionId": "CDS.2021-10-04.2",
  "choice": "A",
  "answer": "A specific application being used to carry out a general function such as printing"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.2-B",
  "QuestionId": "CDS.2021-10-04.2",
  "choice": "B",
  "answer": "Segmentation according to department"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.2-C",
  "QuestionId": "CDS.2021-10-04.2",
  "choice": "C",
  "answer": "A web application with a two-tiered architecture"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.2-D",
  "QuestionId": "CDS.2021-10-04.2",
  "choice": "D",
  "answer": "Storage of a baseline configuration on a workstation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.3-A",
  "QuestionId": "CDS.2021-10-04.3",
  "choice": "A",
  "answer": "Rapid elasticity, provider-specific network connectivity, and a pay-per-use model"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.3-B",
  "QuestionId": "CDS.2021-10-04.3",
  "choice": "B",
  "answer": "Rapid elasticity, broad network connectivity, and a multitenancy model"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.3-C",
  "QuestionId": "CDS.2021-10-04.3",
  "choice": "C",
  "answer": "Rapid elasticity, broad network connectivity, and a pay-per-use model"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-04.3-D",
  "QuestionId": "CDS.2021-10-04.3",
  "choice": "D",
  "answer": "Continuous monitoring, broad network connectivity, and a pay-per-use model"
 }
]