export const Log = [
 {
  "type": "QUESTION",
  "id": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07",
  "question": "What are the 3 things that you must understand before you can determine the necessary controls t deploy for data protetion in a cloud environment?",
  "answer": "B",
  "explanation": "Actors are those wanting to use data\nLocations are where the data is stored\nFunctions are the operations to perform on the data...e.g. access,process,store\n Once we have these 3 we can design appropriate controls."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-20.1",
  "question": "What are the three things that you must understand before you can determine the necessary controls to deploy for data protection in a cloud environment?",
  "answer": "B",
  "explanation": [
   {
    "value": "Functions of the data"
   },
   {
    "value": "Locations of the data"
   },
   {
    "value": "Actors upon the data \nOnce you understand and document these three items, you can design the appropriate controls and apply them to the system to safeguard data and control access to it. These controls can be of a preventive, detective (monitoring), or corrective nature."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-21.1",
  "question": "Which of the following are storage types used with an IaaS solution?",
  "answer": "D",
  "explanation": [
   {
    "value": "Volume storage: A virtual hard drive that can be attached to a VM instance and be used to host data within a file system. Volumes attached to IaaS instances behave just like a physical drive or an array does. Examples include VMware VMFS, Amazon EBS, Rackspace RAID, and OpenStack Cinder."
   },
   {
    "value": "Object storage: Object storage is like a file share accessed via APIs or a web interface. Examples include Amazon S3 and Rackspace cloud files."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-21.2",
  "question": "Which of the following are data storage types used with a PaaS solution?",
  "answer": "B",
  "explanation": [
   {
    "value": "Unstructured: Information that does not reside in a traditional row-column database. Unstructured data files often include text and multimedia content. Examples include email messages, word processing documents, videos, photos, audio files, presentations, web pages, and many other kinds of business documents. Note that although these sorts of files may have an internal structure, they are still considered unstructured because the data they contain does not fit neatly in a database."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-21.3",
  "question": "Which of the following can be deployed to help ensure the confidentiality of data in the cloud? (Choose two.)",
  "answer": "A,C",
  "explanation": [
   {
    "value": "Encryption: For preventing unauthorized data viewing"
   },
   {
    "value": "DLP: For auditing and preventing unauthorized data exfiltration"
   },
   {
    "value": "File and database access monitor: For detecting unauthorized access to data stored in files and databases"
   },
   {
    "value": "Obfuscation, anonymization, tokenization, and masking: Different alternatives for the protection of data without encryption"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-21.4",
  "question": "Where would the monitoring engine be deployed when using a network-based DLP system?",
  "answer": "C",
  "explanation": [
   {
    "value": "DIM: Sometimes referred to as network-based or gateway DLP. In this topology, the monitoring engine is deployed near the organizational gateway to monitor outgoing protocols such as HTTP, HTTPS, SMTP, and FTP. The topology can be a mixture of proxy based, bridge, network tapping, or SMTP relays. To scan encrypted HTTPS traffic, appropriate mechanisms to enable SSL interception and broker are required to be integrated into the system architecture."
   },
   {
    "value": "DAR: Sometimes referred to as storage based. In this topology, the DLP engine is installed where the data is at rest, usually one or more storage subsystems and file and application servers. This topology is effective for data discovery and tracking usage but may require integration with network or endpoint-based DLP for policy enforcement."
   },
   {
    "value": "DIU: Sometimes referred to as client or endpoint based, the DLP application is installed on a user’s workstations and endpoint devices. This topology offers insights into how users use the data, with the ability to add protection that network DLP may not be able to provide. The challenge with client-based DLP is the complexity, time, and resources to implement across all endpoint devices, often across multiple locations and significant numbers of users."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-21.5",
  "question": "When using transparent encryption of a database, where does the encryption engine reside?",
  "answer": "D",
  "explanation": [
   {
    "value": "File-level encryption: Database servers typically reside on volume storage. For this deployment, you are encrypting the volume or folder of the database, with the encryption engine and keys residing on the instances attached to the volume. External file system encryption protects from media theft, lost backups, and external attack but does not protect against attacks with access to the application layer, the instance’s OS, or the database itself."
   },
   {
    "value": "Transparent encryption: Many database-management systems have the ability to encrypt the entire database or specific portions, such as tables. The encryption engine resides within the database, and it is transparent to the application. Keys usually reside within the instance, although processing and management of them may also be offloaded to an external KMS. This encryption can provide effective protection from media theft, backup system intrusions, and certain database and application-level attacks."
   },
   {
    "value": "Application-level encryption: In application-level encryption, the encryption engine resides at the application that is utilizing the database. Application encryption can act as a robust mechanism to protect against a range of threats, such as compromised administrative accounts and other database and application-level attacks. Because the data is encrypted before reaching the database, it is challenging to perform indexing, searches, and metadata collection. Encrypting at the application layer can be challenging, based on the expertise requirements for cryptographic development and integration."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-21.6",
  "question": "What are three analysis methods used with data discovery techniques?",
  "answer": "A",
  "explanation": [
   {
    "value": "Metadata: Data that describes data; all relational databases store metadata that describes tables and column attributes."
   },
   {
    "value": "Labels: Where data elements are grouped with a tag that describes the data. This can be done at the time the data is created, or tags can be added over time to provide additional information and references to describe the data. In many ways, labels are just like metadata but slightly less formal. Some relational database platforms provide mechanisms to create data labels, but this method is more commonly used with flat files, becoming increasingly useful as more firms move to ISAM or quasi-relational data storage, such as Amazon’s SimpleDB, to handle fast-growing data sets. This form of discovery is similar to a Google search, with the greater the number of similar labels, the greater likelihood of a match. Effectiveness is dependent on the use of labels."
   },
   {
    "value": "Content analysis: In this form of analysis, you investigate the data itself by employing pattern matching, hashing, statistical, lexical, or other forms of probability analysis."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-23.1",
  "question": "In the context of privacy and data protection, what is a controller?",
  "answer": "C",
  "explanation": "Where the purposes and means of processing are determined by national or community laws or regulations, the controller or the specific criteria for his nomination may be designated by national or community law. The customer determines the ultimate purpose of the processing and decides on the outsourcing or the delegation of all or part of the concerned activities to external organizations. Therefore, the customer acts as a controller. In this role, the customer is responsible and subject to all the legal duties that are addressed in the privacy and data protection (P&DP) laws applicable to the controller’s role. The customer may task the service provider with choosing the methods and the technical or organizational measures to be used to achieve the purposes of the controller."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-23.2",
  "question": "What is the CSA CCM?",
  "answer": "B",
  "explanation": "The CSA CCM is an essential and up-to-date security controls framework that is addressed to the cloud community and stakeholders. A fundamental richness of the CCM is its ability to provide mapping and cross relationships with the main industry-accepted security standards, regulations, and controls frameworks such as the ISO 27001/27002, ISACA’s COBIT, and PCI DSS."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-26.1",
  "question": "Which of the following are common capabilities of IRM solutions?",
  "answer": "A",
  "explanation": [
   {
    "value": "Persistent protection: Ensures that documents, messages, and attachments are protected at rest, in transit, and even after they’re distributed to recipients"
   },
   {
    "value": "Dynamic policy control: Allows content owners to define and change user permissions (view, forward, copy, or print) and recall or expire content even after distribution"
   },
   {
    "value": "Automatic expiration: Provides the ability to automatically revoke access to documents, emails, and attachments at any point, thus allowing information security policies to be enforced wherever content is distributed or stored"
   },
   {
    "value": "Continuous audit trail: Provides confirmation that content was delivered and viewed and offers proof of compliance with your organization’s information security policies"
   },
   {
    "value": "Support for existing authentication security infrastructure: Reduces administrator involvement and speeds deployment by leveraging user and group information that exists in directories and authentication systems"
   },
   {
    "value": "Mapping for repository: ACLs Automatically maps the ACL-based permissions into policies that control the content outside the repository"
   },
   {
    "value": "Integration with all third-party email filtering engines: Allows organizations to automatically secure outgoing email messages in compliance with corporate information security policies and federal regulatory requirements"
   },
   {
    "value": "Additional security and protection capabilities: Allows users additional capabilities such as these: Determine who can access a document Prohibit printing of an entire document or selected portions Disable copy and paste and screen capture capabilities Watermark pages if printing privileges are granted Expire or revoke document access at any time Track all document activity through a complete audit trail"
   },
   {
    "value": "Support for email applications: Provides interface and support for email programs such as Microsoft Outlook and IBM Lotus Notes\nSupport for other document types: Other document types, besides Microsoft Office and PDF, can be supported as well"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-26.2",
  "question": "What are the four elements that a data retention policy should define?",
  "answer": "D",
  "explanation": "A data retention policy is an organization’s established protocol for retaining information for operational or regulatory compliance needs. The objectives of a data retention policy are to keep important information for future use or reference, to organize information so it can be searched and accessed at a later date, and to dispose of information that is no longer needed. The policy balances the legal, regulation, and business data archival requirements against data storage costs, complexity, and other data considerations.="
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-26.3",
  "question": "Which of the following methods for the safe disposal of electronic records can always be used within a cloud environment?",
  "answer": "B",
  "explanation": [
   {
    "value": "Physical destruction: Physically destroying the media by incineration, shredding, or other means."
   },
   {
    "value": "Degaussing: Using strong magnets for scrambling data on magnetic media such as hard drives and tapes."
   },
   {
    "value": "Overwriting: Writing random data over the actual data. The more times the overwriting process occurs, the more thorough the destruction of the data is considered to be."
   },
   {
    "value": "Encryption: Using an encryption method to rewrite the data in an encrypted format to make it unreadable without the encryption key."
   },
   {
    "value": "Crypto-shredding: Because the first three options are not fully applicable to cloud computing, the only reasonable method remaining is encrypting the data. The process of encrypting the data to dispose of it is called digital shredding or crypto-shredding. Crypto-shredding is the process of deliberately destroying the encryption keys that were used to encrypt the data originally. Because the data is encrypted with the keys, the data is rendered unreadable (at least until the encryption protocol used can be broken or is capable of being brute-forced by an attacker). To perform proper crypto-shredding, consider the following: The data should be encrypted completely without leaving clear text remaining. The technique must make sure that the encryption keys are totally unrecoverable. This can be hard to accomplish if an external CSP or other third party manages the keys."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-09-26.4",
  "question": "To support continuous operations, which of the following principles should be adopted as part of the security operations policies?",
  "answer": "C",
  "explanation": [
   {
    "value": [
     {
      "value": "Detecting new events: The goal of auditing is to detect information security events. Policies should be created that define what a security event is and how to address it."
     },
     {
      "value": "Adding new rules: Rules are built to detect new events. Rules allow for mapping of expected values to log files and detect events. In continuous operation mode, rules have to be updated to address new risks."
     },
     {
      "value": "Reducing false positives: The quality of the continuous operations audit logging depends on the ability to gradually reduce the number of false positives to maintain operational efficiency. This requires constant improvement of the rule set in use."
     },
     {
      "value": "Contract and authority maintenance: Points of contact for applicable regulatory authorities, national and local law enforcement, and other legal jurisdictional authorities should be maintained and regularly updated as per the business need (that is, a change in impacted scope or a change in any compliance obligation). This ensures direct compliance liaisons have been established and will prepare for a forensic investigation requiring rapid engagement with law enforcement."
     },
     {
      "value": "Secure disposal: Policies and procedures shall be established with supporting business processes and technical measures implemented for the secure disposal and complete removal of data from all storage media. This is to ensure data is not recoverable by any computer forensic means."
     }
    ]
   }
  ]
 },
 {
  "type": "ANSWER",
  "id": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07-A",
  "QuestionId": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07",
  "choice": "A",
  "answer": "Management, Provisioning and Location"
 },
 {
  "type": "ANSWER",
  "id": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07-B",
  "QuestionId": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07",
  "choice": "B",
  "answer": "Function, Location , Actors"
 },
 {
  "type": "ANSWER",
  "id": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07-C",
  "QuestionId": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07",
  "choice": "C",
  "answer": "Actors, policies and procedures"
 },
 {
  "type": "ANSWER",
  "id": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07-D",
  "QuestionId": "CDS.ec48077d-5026-498f-b98f-be069c6a5d07",
  "choice": "D",
  "answer": "Lifecycle, Functions and Cost"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-20.1-A",
  "QuestionId": "CDS.2021-09-20.1",
  "choice": "A",
  "answer": "Management, provisioning, and location"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-20.1-B",
  "QuestionId": "CDS.2021-09-20.1",
  "choice": "B",
  "answer": "Function, location, and actors"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-20.1-C",
  "QuestionId": "CDS.2021-09-20.1",
  "choice": "C",
  "answer": "Actors, policies, and procedures"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-20.1-D",
  "QuestionId": "CDS.2021-09-20.1",
  "choice": "D",
  "answer": "Lifecycle, function, and cost"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.1-A",
  "QuestionId": "CDS.2021-09-21.1",
  "choice": "A",
  "answer": "Volume and block"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.1-B",
  "QuestionId": "CDS.2021-09-21.1",
  "choice": "B",
  "answer": "Structured and object"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.1-C",
  "QuestionId": "CDS.2021-09-21.1",
  "choice": "C",
  "answer": "Unstructured and ephemeral"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.1-D",
  "QuestionId": "CDS.2021-09-21.1",
  "choice": "D",
  "answer": "Volume and object"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.2-A",
  "QuestionId": "CDS.2021-09-21.2",
  "choice": "A",
  "answer": "Raw and block"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.2-B",
  "QuestionId": "CDS.2021-09-21.2",
  "choice": "B",
  "answer": "Structured and unstructured"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.2-C",
  "QuestionId": "CDS.2021-09-21.2",
  "choice": "C",
  "answer": "Unstructured and ephemeral"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.2-D",
  "QuestionId": "CDS.2021-09-21.2",
  "choice": "D",
  "answer": "Tabular and object"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.3-A",
  "QuestionId": "CDS.2021-09-21.3",
  "choice": "A",
  "answer": "Encryption"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.3-B",
  "QuestionId": "CDS.2021-09-21.3",
  "choice": "B",
  "answer": "SLAs"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.3-C",
  "QuestionId": "CDS.2021-09-21.3",
  "choice": "C",
  "answer": "Masking"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.3-D",
  "QuestionId": "CDS.2021-09-21.3",
  "choice": "D",
  "answer": "Continuous monitoring"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.4-A",
  "QuestionId": "CDS.2021-09-21.4",
  "choice": "A",
  "answer": "On a user’s workstation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.4-B",
  "QuestionId": "CDS.2021-09-21.4",
  "choice": "B",
  "answer": "In the storage system"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.4-C",
  "QuestionId": "CDS.2021-09-21.4",
  "choice": "C",
  "answer": "Near the organizational gateway"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.4-D",
  "QuestionId": "CDS.2021-09-21.4",
  "choice": "D",
  "answer": "On a VLAN"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.5-A",
  "QuestionId": "CDS.2021-09-21.5",
  "choice": "A",
  "answer": "At the application using the database"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.5-B",
  "QuestionId": "CDS.2021-09-21.5",
  "choice": "B",
  "answer": "On the instances attached to the volume"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.5-C",
  "QuestionId": "CDS.2021-09-21.5",
  "choice": "C",
  "answer": "In a key management system"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.5-D",
  "QuestionId": "CDS.2021-09-21.5",
  "choice": "D",
  "answer": "Within the database"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.6-A",
  "QuestionId": "CDS.2021-09-21.6",
  "choice": "A",
  "answer": "Metadata, labels, and content analysis"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.6-B",
  "QuestionId": "CDS.2021-09-21.6",
  "choice": "B",
  "answer": "Metadata, structural analysis, and labels"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.6-C",
  "QuestionId": "CDS.2021-09-21.6",
  "choice": "C",
  "answer": "Statistical analysis, labels, and content analysis"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-21.6-D",
  "QuestionId": "CDS.2021-09-21.6",
  "choice": "D",
  "answer": "Bit splitting, labels, and content analysis"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.1-A",
  "QuestionId": "CDS.2021-09-23.1",
  "choice": "A",
  "answer": "One who cannot be identified, directly or indirectly, in particular by reference to an identification number or to one or more factors specific to his physical, physiological, mental, economic, cultural, or social identity"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.1-B",
  "QuestionId": "CDS.2021-09-23.1",
  "choice": "B",
  "answer": "One who can be identified, directly or indirectly, in particular by reference to an identification number or to one or more factors specific to his physical, physiological, mental, economic, cultural, or social identity"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.1-C",
  "QuestionId": "CDS.2021-09-23.1",
  "choice": "C",
  "answer": "The natural or legal person, public authority, agency, or any other body that alone or jointly with others determines the purposes and means of the processing of personal data"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.1-D",
  "QuestionId": "CDS.2021-09-23.1",
  "choice": "D",
  "answer": "A natural or legal person, public authority, agency, or any other body that processes personal data on behalf of the customer"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.2-A",
  "QuestionId": "CDS.2021-09-23.2",
  "choice": "A",
  "answer": "A set of regulatory requirements for CSPs"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.2-B",
  "QuestionId": "CDS.2021-09-23.2",
  "choice": "B",
  "answer": "An inventory of cloud service security controls that are arranged into separate security domains"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.2-C",
  "QuestionId": "CDS.2021-09-23.2",
  "choice": "C",
  "answer": "A set of software development lifecycle requirements for CSPs"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-23.2-D",
  "QuestionId": "CDS.2021-09-23.2",
  "choice": "D",
  "answer": "An inventory of cloud service security controls that are arranged into a hierarchy of security domains"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.1-A",
  "QuestionId": "CDS.2021-09-26.1",
  "choice": "A",
  "answer": "Persistent protection, dynamic policy control, automatic expiration, continuous audit trail, and support for existing authentication infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.1-B",
  "QuestionId": "CDS.2021-09-26.1",
  "choice": "B",
  "answer": "Persistent protection, static policy control, automatic expiration, continuous audit trail, and support for existing authentication infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.1-C",
  "QuestionId": "CDS.2021-09-26.1",
  "choice": "C",
  "answer": "Persistent protection, dynamic policy control, manual expiration, continuous audit trail, and support for existing authentication infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.1-D",
  "QuestionId": "CDS.2021-09-26.1",
  "choice": "D",
  "answer": "Persistent protection, dynamic policy control, automatic expiration, intermittent audit trail, and support for existing authentication infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.2-A",
  "QuestionId": "CDS.2021-09-26.2",
  "choice": "A",
  "answer": "Retention periods, data access methods, data security, and data retrieval procedures"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.2-B",
  "QuestionId": "CDS.2021-09-26.2",
  "choice": "B",
  "answer": "Retention periods, data formats, data security, and data destruction procedures"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.2-C",
  "QuestionId": "CDS.2021-09-26.2",
  "choice": "C",
  "answer": "Retention periods, data formats, data security, and data communication procedures"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.2-D",
  "QuestionId": "CDS.2021-09-26.2",
  "choice": "D",
  "answer": "Retention periods, data formats, data security, and data retrieval procedures"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.3-A",
  "QuestionId": "CDS.2021-09-26.3",
  "choice": "A",
  "answer": "Physical destruction"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.3-B",
  "QuestionId": "CDS.2021-09-26.3",
  "choice": "B",
  "answer": "Encryption"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.3-C",
  "QuestionId": "CDS.2021-09-26.3",
  "choice": "C",
  "answer": "Overwriting"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.3-D",
  "QuestionId": "CDS.2021-09-26.3",
  "choice": "D",
  "answer": "Degaussing"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.4-A",
  "QuestionId": "CDS.2021-09-26.4",
  "choice": "A",
  "answer": "Application logging, contract and authority maintenance, secure disposal, and business continuity preparation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.4-B",
  "QuestionId": "CDS.2021-09-26.4",
  "choice": "B",
  "answer": "Audit logging, contract and authority maintenance, secure usage, and incident response legal preparation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.4-C",
  "QuestionId": "CDS.2021-09-26.4",
  "choice": "C",
  "answer": "Audit logging, contract and authority maintenance, secure disposal, and incident response legal preparation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-09-26.4-D",
  "QuestionId": "CDS.2021-09-26.4",
  "choice": "D",
  "answer": "Transaction logging, contract and authority maintenance, secure disposal, and DR preparation"
 }
]