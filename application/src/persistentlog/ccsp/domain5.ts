export const Log = [
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-06.10",
  "question": "At which of the following levels should logical design for data separation be incorporated?",
  "answer": "B",
  "explanation": [
   {
    "value": "Compute nodes"
   },
   {
    "value": "Management plane"
   },
   {
    "value": "Storage nodes"
   },
   {
    "value": "Control plane"
   },
   {
    "value": "Network\n All other answers deal with OSI layers rather than these architectural layers"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-06.11",
  "question": "Which of the following is the correct name for Tier II of the Uptime Institute Data Center Site Infrastructure Tier Standard Topology?",
  "answer": "D",
  "explanation": "The Uptime Institute is a leader in data center design and management. Their “Data Center Site Infrastructure Tier Standard: Topology” document provides the baseline that many enterprises use to rate their data center designs. The document describes a four-tiered architecture for data center design, with each tier being progressively more secure, reliable, and redundant in its design and operational elements. The document also addresses the supporting infrastructure systems that these designs will rely on, such as power generation systems, ambient temperature control, and makeup (backup) water systems. The four tiers are listed in order from left to right (Figure A.4).\n Tier 1: Basic Site Infrastructure\nTier 2: Redundant Site Infrastructure\nTier 3: Concurrently maintainable Site Infrastructure\nTier 4: Fault Tolerant Site infrastructure"
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.1",
  "question": "Which of the following is the recommended operating range for temperature and humidity in a data center?",
  "answer": "B",
  "explanation": "The American Society of Heating, Refrigeration, and Air Conditioning Engineers (ASHRAE) Technical Committee 9.9 created a widely accepted set of guidelines for optimal temperature and humidity set points in the data center. The guidelines are available as the 2008 ASHRAE Environmental Guidelines for Datacom Equipment. These guidelines specify a required and allowable range of temperature and humidity, as follows: Metric Range Low-end temperature 64.4° F (18° C) High-end temperature 80.6° F (27° C) Low-end moisture 40% relative humidity and 41.9° F (5.5° C) dew point High-end moisture 60% relative humidity and 59° F (15° C) dew point"
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.2",
  "question": "Which of the following are supported authentication methods for iSCSI? (Choose two.)",
  "answer": "A,C",
  "explanation": [
   {
    "value": "Kerberos: A network authentication protocol designed to provide strong authentication for client/server applications by using secret key cryptography. The Kerberos protocol uses strong cryptography so that a client can prove its identity to a server (and vice versa) across an insecure network connection. After a client and server use Kerberos to prove their identity, they can encrypt all their communications to ensure privacy and data integrity as they go about their business."
   },
   {
    "value": "SRP: A secure password-based authentication and key-exchange protocol that exchanges a cryptographically strong secret as a by-product of successful authentication. This enables the two parties to communicate securely."
   },
   {
    "value": "SPKM1/2: Provides authentication, key establishment, data integrity, and data confidentiality in an online distributed application environment using a public-key infrastructure. The use of a public-key infrastructure allows digital signatures supporting nonrepudiation to be employed for message exchanges."
   },
   {
    "value": "CHAP: Used to periodically verify the identity of the peer using a three-way handshake. This is done upon initial link establishment and may be repeated anytime after the link has been established."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.3",
  "question": "What are the two biggest challenges associated with the use of IPSec in cloud computing environments?",
  "answer": "C",
  "explanation": [
   {
    "value": "Configuration management: The use of IPSec is optional, and as such, many endpoint devices connecting to cloud infrastructure do not have IPSec support enabled and configured. If IPSec is not enabled on the endpoint, then depending on the configuration choices made on the server side of the IPSec solution, the endpoint may not be able to connect and complete a transaction if it does not support IPSec. CSPs may not have the proper visibility on the customer endpoints or the server infrastructure to understand IPSec configurations. As a result, the ability to ensure the use of IPSec to secure network traffic may be limited."
   },
   {
    "value": "Performance: The use of IPSec imposes a performance penalty on the systems deploying the technology. Although the impact to the performance of an average system is small, it is the cumulative effect of IPSec across an enterprise architecture, end to end, that must be evaluated prior to implementation."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.4",
  "question": "When setting up resource sharing within a host cluster, which option would you choose to mediate resource contention?",
  "answer": "A",
  "explanation": [
   {
    "value": "Reservations guarantee that a certain amount of the cluster’s pooled resources will be made available to a specified VM. - Limits guarantee a certain maximum amount of the cluster’s pooled resources will be made available to a specified VM."
   },
   {
    "value": "Shares provision the remaining resources left in a cluster when there is resource contention. Specifically, shares allow the cluster’s reservations to be allocated and then to address any remaining resources that may be available for use by members of the cluster through a prioritized percentage-based allocation mechanism."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.5",
  "question": "When using maintenance mode, which two items are disabled and which item remains enabled?",
  "answer": "A",
  "explanation": "Maintenance mode is utilized when updating or configuring different components of the cloud environment. While in maintenance mode, customer access is blocked, and alerts are disabled. (Logging is still enabled.)"
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.6",
  "question": "What are the three generally accepted service models of cloud computing?",
  "answer": "C",
  "explanation": [
   {
    "value": "SaaS: Customers can use the provider’s applications running on a cloud infrastructure. The applications are accessible from various client devices through a thin client interface such as a web browser (for example, web-based email) or a program interface. The consumer does not manage or control the underlying cloud infrastructure, including network, servers, OSs, storage, or even individual application capabilities, with the possible exception of limited user-specific application configuration settings."
   },
   {
    "value": "PaaS: Consumers can deploy onto the cloud infrastructure consumer-created or acquired applications created using programming languages and tools that the provider supports. The consumer does not manage or control the underlying cloud infrastructure, including network, servers, OSs, or storage, but has control over the deployed applications and possibly application hosting environment configurations."
   },
   {
    "value": "IaaS: The capability provided to the consumer is to provision processing, storage, networks, and other fundamental computing resources where the consumer can deploy and run arbitrary software, which can include OSs and applications. The consumer does not manage or control the underlying cloud infrastructure but has control over OSs, storage, and deployed applications and possibly limited control of select networking components, such as host firewalls."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.7",
  "question": "What is a key characteristic of a honeypot?",
  "answer": "B",
  "explanation": "A honeypot is used to detect, deflect, or in some manner counteract attempts at unauthorized use of information systems. Generally, a honeypot consists of a computer, data, or a network site that appears to be part of a network but is actually isolated and monitored and that seems to contain information or a resource of value to attackers."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.8",
  "question": "What does the concept of nondestructive testing mean in the context of a vulnerability assessment?",
  "answer": "A",
  "explanation": "During a vulnerability assessment, the cloud environment is tested for known vulnerabilities. Detected vulnerabilities are not exploited during a vulnerability assessment (nondestructive testing) and may require further validation to detect false positives."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.9",
  "question": "Seeking to follow good design practices and principles, the CCSP should create the physical network design based on which of the following?",
  "answer": "D",
  "explanation": [
   {
    "value": "It is created from a logical network design"
   },
   {
    "value": "It often expands elements found in a logical design For instance, a WAN connection on a logical design diagram can be shown as a line between two buildings. When transformed into a physical design, that single line can expand into the connection, routers, and other equipment at each end of the connection. \nThe actual connection media might be shown on a physical design as well as manufacturers and other qualities of the network implementation.\n Gordon, Adam. The Official (ISC)2 Guide to the CCSP CBK (p. 475). Wiley. Kindle Edition."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-12.10",
  "question": "What should configuration management always be tied to?",
  "answer": "B",
  "explanation": "The need to tie configuration management to change management is because change management has to approve any changes to all production systems prior to them taking place. In other words, there should never be a change that is allowed to take place to a Configuration Item (CI) in a production system unless change management has approved the change first."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-14.1",
  "question": "What are the objectives of change management? (Choose all that apply.)",
  "answer": "A,B",
  "explanation": [
   {
    "value": "Respond to a customer’s changing business requirements while maximizing value and reducing incidents, disruption, and rework."
   },
   {
    "value": "Respond to business and IT requests for change that aligns services with business needs."
   },
   {
    "value": "Ensure that changes are recorded and evaluated."
   },
   {
    "value": "Ensure that authorized changes are prioritized, planned, tested, implemented, documented, and reviewed in a controlled manner."
   },
   {
    "value": "Ensure that all changes to CIs are recorded in the configuration management system."
   },
   {
    "value": "Optimize overall business risk; it is often correct to minimize business risk, but sometimes it is appropriate to knowingly accept a risk because of the potential \nbenefit."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-14.2",
  "question": "What is the definition of an incident according to the ITIL framework?",
  "answer": "A",
  "explanation": "According to the ITIL framework, an incident is defined as an unplanned interruption to an IT service or a reduction in the quality of an IT service."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-14.3",
  "question": "What is the difference between BC and BCM?",
  "answer": "D",
  "explanation": [
   {
    "value": "BC: The capability of the organization to continue delivery of products or services at acceptable predefined levels following a disruptive incident (Source: ISO 22301:2012)."
   },
   {
    "value": "BCM: A holistic management process that identifies potential threats to an organization and the impacts to business operations that those threats, if realized, might cause. It provides a framework for building organizational resilience with the capability of an effective response that safeguards the interests of its key stakeholders, reputation, brand, and value-creating activities (Source: ISO 22301:2012)."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-14.4",
  "question": "What are the four steps in the risk-management process?",
  "answer": "B",
  "explanation": "Risk-management processes include framing risk, assessing risk, responding to risk, and monitoring risk. Note the four steps in the risk-management process, which includes the risk assessment step and the information and communications flows necessary to make the process work effectively"
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-14.5",
  "question": "An organization will conduct a risk assessment to evaluate which of the following?",
  "answer": "C",
  "explanation": [
   {
    "value": "Threats to its assets"
   },
   {
    "value": "Vulnerabilities present in the environment"
   },
   {
    "value": "The likelihood that a threat will be realized by taking advantage of an exposure (or probability and frequency when dealing with quantitative assessment) The impact that the exposure being realized will have on the organization Countermeasures available that can reduce the threat’s ability to exploit the exposure or that can lessen the impact to the organization when a threat is able to exploit a vulnerability"
   },
   {
    "value": "The residual risk, or the amount of risk that is left over when appropriate controls are properly applied to lessen or remove the vulnerability"
   },
   {
    "value": "An organization may also document evidence of the countermeasure in a deliverable called an exhibit or evidence. An exhibit can provide an audit trail for the organization and, likewise, evidence for any internal or external auditors that may have questions about the organization’s current state of risk."
   },
   {
    "value": "Why undertake such an endeavor? Without knowing which assets are critical and which would be most at risk within an organization, it is not possible to appropriately protect those assets."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-14.6",
  "question": "What is the minimum and customary practice of responsible protection of assets that affects a community or societal norm?",
  "answer": "D",
  "explanation": "Due diligence is the act of investigating and understanding the risks the company faces. A company practices due care by developing security policies, procedures, and standards. Due care shows that a company has taken responsibility for the activities that take place within the corporation and has taken the necessary steps to help protect the company, its resources, and employees from possible risks. So due diligence is understanding the current threats and risks and due care is implementing countermeasures to provide protection from those threats. If a company does not practice due care and due diligence pertaining to the security of its assets, it can be legally charged with negligence and held accountable for any ramifications of that negligence."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.1",
  "question": "Within the realm of IT security, which of the following combinations best defines risk?",
  "answer": "B",
  "explanation": "A vulnerability is a lack of a countermeasure or a weakness in a countermeasure that is in place. A threat is any potential danger that is associated with the exploitation of a vulnerability. The threat is that someone, or something, will identify a specific vulnerability and use it against the company or individual. A risk is the likelihood of a threat agent exploiting a vulnerability and the corresponding business impact."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.2",
  "question": "Qualitative risk assessment is earmarked by which of the following?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.3",
  "question": "SLE is calculated by using which of the following?",
  "answer": "C",
  "explanation": "SLE must be calculated to provide an estimate of loss. SLE is defined as the difference between the original value and the remaining value of an asset after a single exploit. The formula for calculating SLE is as follows: SLE = asset value (in $) × exposure factor (loss due to successful threat exploit, as a %) Losses can include lack of availability of data assets due to data loss, theft, alteration, or DoS (perhaps due to business continuity or security issues)."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-19.4",
  "question": "What is the process flow of digital forensics?",
  "answer": "C",
  "explanation": [
   {
    "value": "Collection: Identifying, labeling, recording, and acquiring data from the possible sources of relevant data, while following procedures that preserve the integrity of the data."
   },
   {
    "value": "Examination: Forensically processing collected data using a combination of automated and manual methods, and assessing and extracting data of particular interest, while preserving the integrity of the data."
   },
   {
    "value": "Analysis: Analyzing the results of the examination, using legally justifiable methods and techniques, to derive useful information that addresses the questions that were the impetus for performing the collection and examination. \n-Reporting: Reporting the results of the analysis, which may include describing the actions used, explaining how tools and procedures were"
   }
  ]
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.10-A",
  "QuestionId": "CDS.2021-10-06.10",
  "choice": "A",
  "answer": "Compute nodes and network"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.10-B",
  "QuestionId": "CDS.2021-10-06.10",
  "choice": "B",
  "answer": "Storage nodes and application"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.10-C",
  "QuestionId": "CDS.2021-10-06.10",
  "choice": "C",
  "answer": "Control plane and session"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.10-D",
  "QuestionId": "CDS.2021-10-06.10",
  "choice": "D",
  "answer": "Management plane and presentation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.11-A",
  "QuestionId": "CDS.2021-10-06.11",
  "choice": "A",
  "answer": "Concurrently Maintainable Site Infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.11-B",
  "QuestionId": "CDS.2021-10-06.11",
  "choice": "B",
  "answer": "Fault-Tolerant Site Infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.11-C",
  "QuestionId": "CDS.2021-10-06.11",
  "choice": "C",
  "answer": "Basic Site Infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.11-D",
  "QuestionId": "CDS.2021-10-06.11",
  "choice": "D",
  "answer": "Redundant Site Infrastructure Capacity Components"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.1-A",
  "QuestionId": "CDS.2021-10-12.1",
  "choice": "A",
  "answer": "Between 62° F and 81° F and 40 percent and 65 percent relative humidity"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.1-B",
  "QuestionId": "CDS.2021-10-12.1",
  "choice": "B",
  "answer": "Between 64° F and 81° F and 40 percent and 60 percent relative humidity"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.1-C",
  "QuestionId": "CDS.2021-10-12.1",
  "choice": "C",
  "answer": "Between 64° F and 84° F and 30 percent and 60 percent relative humidity"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.1-D",
  "QuestionId": "CDS.2021-10-12.1",
  "choice": "D",
  "answer": "Between 60° F and 85° F and 40 percent and 60 percent relative humidity"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.2-A",
  "QuestionId": "CDS.2021-10-12.2",
  "choice": "A",
  "answer": "Kerberos"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.2-B",
  "QuestionId": "CDS.2021-10-12.2",
  "choice": "B",
  "answer": "TLS"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.2-C",
  "QuestionId": "CDS.2021-10-12.2",
  "choice": "C",
  "answer": "SRP"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.2-D",
  "QuestionId": "CDS.2021-10-12.2",
  "choice": "D",
  "answer": "L2TP"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.3-A",
  "QuestionId": "CDS.2021-10-12.3",
  "choice": "A",
  "answer": "Access control and patch management"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.3-B",
  "QuestionId": "CDS.2021-10-12.3",
  "choice": "B",
  "answer": "Auditability and governance"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.3-C",
  "QuestionId": "CDS.2021-10-12.3",
  "choice": "C",
  "answer": "Configuration management and performance"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.3-D",
  "QuestionId": "CDS.2021-10-12.3",
  "choice": "D",
  "answer": "Training customers on how to use IPSec and documentation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.4-A",
  "QuestionId": "CDS.2021-10-12.4",
  "choice": "A",
  "answer": "Reservations"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.4-B",
  "QuestionId": "CDS.2021-10-12.4",
  "choice": "B",
  "answer": "Limits"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.4-C",
  "QuestionId": "CDS.2021-10-12.4",
  "choice": "C",
  "answer": "Clusters"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.4-D",
  "QuestionId": "CDS.2021-10-12.4",
  "choice": "D",
  "answer": "Shares"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.5-A",
  "QuestionId": "CDS.2021-10-12.5",
  "choice": "A",
  "answer": "Customer access and alerts are disabled while logging remains enabled."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.5-B",
  "QuestionId": "CDS.2021-10-12.5",
  "choice": "B",
  "answer": "Customer access and logging are disabled while alerts remain enabled."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.5-C",
  "QuestionId": "CDS.2021-10-12.5",
  "choice": "C",
  "answer": "Logging and alerts are disabled while the ability to deploy new VMs remains enabled."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.5-D",
  "QuestionId": "CDS.2021-10-12.5",
  "choice": "D",
  "answer": "Customer access and alerts are disabled while the ability to power on VMs remains enabled."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.6-A",
  "QuestionId": "CDS.2021-10-12.6",
  "choice": "A",
  "answer": "IaaS, DRaaS, and PaaS"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.6-B",
  "QuestionId": "CDS.2021-10-12.6",
  "choice": "B",
  "answer": "PaaS, SECaaS, and IaaS"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.6-C",
  "QuestionId": "CDS.2021-10-12.6",
  "choice": "C",
  "answer": "SaaS, PaaS, and IaaS"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.6-D",
  "QuestionId": "CDS.2021-10-12.6",
  "choice": "D",
  "answer": "Desktop as a service, PaaS, and IaaS"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.7-A",
  "QuestionId": "CDS.2021-10-12.7",
  "choice": "A",
  "answer": "Isolated, nonmonitored environment"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.7-B",
  "QuestionId": "CDS.2021-10-12.7",
  "choice": "B",
  "answer": "Isolated, monitored environment"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.7-C",
  "QuestionId": "CDS.2021-10-12.7",
  "choice": "C",
  "answer": "Composed of virtualized infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.7-D",
  "QuestionId": "CDS.2021-10-12.7",
  "choice": "D",
  "answer": "Composed of physical infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.8-A",
  "QuestionId": "CDS.2021-10-12.8",
  "choice": "A",
  "answer": "Detected vulnerabilities are not exploited during the vulnerability assessment."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.8-B",
  "QuestionId": "CDS.2021-10-12.8",
  "choice": "B",
  "answer": "Known vulnerabilities are not exploited during the vulnerability assessment."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.8-C",
  "QuestionId": "CDS.2021-10-12.8",
  "choice": "C",
  "answer": "Detected vulnerabilities are not exploited after the vulnerability assessment."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.8-D",
  "QuestionId": "CDS.2021-10-12.8",
  "choice": "D",
  "answer": "Known vulnerabilities are not exploited before the vulnerability assessment."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.9-A",
  "QuestionId": "CDS.2021-10-12.9",
  "choice": "A",
  "answer": "A statement of work"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.9-B",
  "QuestionId": "CDS.2021-10-12.9",
  "choice": "B",
  "answer": "A series of interviews with stakeholders"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.9-C",
  "QuestionId": "CDS.2021-10-12.9",
  "choice": "C",
  "answer": "A design policy statement"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.9-D",
  "QuestionId": "CDS.2021-10-12.9",
  "choice": "D",
  "answer": "A logical network design"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.10-A",
  "QuestionId": "CDS.2021-10-12.10",
  "choice": "A",
  "answer": "Financial management"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.10-B",
  "QuestionId": "CDS.2021-10-12.10",
  "choice": "B",
  "answer": "Change management"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.10-C",
  "QuestionId": "CDS.2021-10-12.10",
  "choice": "C",
  "answer": "IT service management"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-12.10-D",
  "QuestionId": "CDS.2021-10-12.10",
  "choice": "D",
  "answer": "Business relationship management"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.1-A",
  "QuestionId": "CDS.2021-10-14.1",
  "choice": "A",
  "answer": "Respond to a customer’s changing business requirements while maximizing value and reducing incidents, disruption, and rework."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.1-B",
  "QuestionId": "CDS.2021-10-14.1",
  "choice": "B",
  "answer": "Ensure that changes are recorded and evaluated."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.1-C",
  "QuestionId": "CDS.2021-10-14.1",
  "choice": "C",
  "answer": "Respond to business and IT requests for change that will disassociate services with business needs."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.1-D",
  "QuestionId": "CDS.2021-10-14.1",
  "choice": "D",
  "answer": "Ensure that all changes are prioritized, planned, tested, implemented, documented, and reviewed in a controlled manner."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.2-A",
  "QuestionId": "CDS.2021-10-14.2",
  "choice": "A",
  "answer": "An unplanned interruption to an IT service or a reduction in the quality of an IT service"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.2-B",
  "QuestionId": "CDS.2021-10-14.2",
  "choice": "B",
  "answer": "A planned interruption to an IT service or a reduction in the quality of an IT service T"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.2-C",
  "QuestionId": "CDS.2021-10-14.2",
  "choice": "C",
  "answer": "The unknown cause of one or more problems"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.2-D",
  "QuestionId": "CDS.2021-10-14.2",
  "choice": "D",
  "answer": "The identified root cause of a problem"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.3-A",
  "QuestionId": "CDS.2021-10-14.3",
  "choice": "A",
  "answer": "BC is defined as the capability of the organization to continue delivery of products or services at acceptable predefined levels following a disruptive incident. BCM is defined as a holistic management process that identifies actual threats to an organization and the impacts to business operations that those threats, if realized, will cause. BCM provides a framework for building organizational resilience with the capability of an effective response that safeguards its key processes, reputation, brand, and value-creating activities."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.3-B",
  "QuestionId": "CDS.2021-10-14.3",
  "choice": "B",
  "answer": "BC is defined as a holistic process that identifies potential threats to an organization and the impacts to business operations that those threats, if realized, might cause. BC provides a framework for building organizational resilience with the capability of an effective response that safeguards the interests of its key stakeholders, reputation, brand, and value-creating activities. BCM is defined as the capability of the organization to continue delivery of products or services at acceptable predefined levels following a disruptive incident."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.3-C",
  "QuestionId": "CDS.2021-10-14.3",
  "choice": "C",
  "answer": "BC is defined as the capability of the first responder to continue delivery of products or services at acceptable predefined levels following a disruptive incident. BCM is defined as a holistic management process that identifies potential threats to an organization and the impacts to business operations that those threats, if realized, will cause. BCM provides a framework for building organizational resilience with the capability of an effective response that safeguards the interests of its key stakeholders, reputation, brand, and value-creating activities."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.3-D",
  "QuestionId": "CDS.2021-10-14.3",
  "choice": "D",
  "answer": "BC is defined as the capability of the organization to continue delivery of products or services at acceptable predefined levels following a disruptive incident. BCM is defined as a holistic management process that identifies potential threats to an organization and the impacts to business operations that those threats, if realized, might cause. BCM provides a framework for building organizational resilience with the capability of an effective response that safeguards the interests of its key stakeholders, reputation, brand, and value-creating activities."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.4-A",
  "QuestionId": "CDS.2021-10-14.4",
  "choice": "A",
  "answer": "Assessing, monitoring, transferring, and responding"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.4-B",
  "QuestionId": "CDS.2021-10-14.4",
  "choice": "B",
  "answer": "Framing, assessing, monitoring, and responding"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.4-C",
  "QuestionId": "CDS.2021-10-14.4",
  "choice": "C",
  "answer": "Framing, monitoring, documenting, and responding"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.4-D",
  "QuestionId": "CDS.2021-10-14.4",
  "choice": "D",
  "answer": "Monitoring, assessing, optimizing, and responding"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.5-A",
  "QuestionId": "CDS.2021-10-14.5",
  "choice": "A",
  "answer": "Threats to its assets, vulnerabilities not present in the environment, the likelihood that a threat will be realized by taking advantage of an exposure, the impact that the exposure being realized will have on the organization, and the residual risk"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.5-B",
  "QuestionId": "CDS.2021-10-14.5",
  "choice": "B",
  "answer": "Threats to its assets, vulnerabilities present in the environment, the likelihood that a threat will be realized by taking advantage of an exposure, the impact that the exposure being realized will have on another organization, and the residual risk"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.5-C",
  "QuestionId": "CDS.2021-10-14.5",
  "choice": "C",
  "answer": "Threats to its assets, vulnerabilities present in the environment, the likelihood that a threat will be realized by taking advantage of an exposure, the impact that the exposure being realized will have on the organization, and the residual risk"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.5-D",
  "QuestionId": "CDS.2021-10-14.5",
  "choice": "D",
  "answer": "Threats to its assets, vulnerabilities present in the environment, the likelihood that a threat will be realized by taking advantage of an exposure, the impact that the exposure being realized will have on the organization, and the total risk"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.6-A",
  "QuestionId": "CDS.2021-10-14.6",
  "choice": "A",
  "answer": "Due diligence"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.6-B",
  "QuestionId": "CDS.2021-10-14.6",
  "choice": "B",
  "answer": "Risk mitigation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.6-C",
  "QuestionId": "CDS.2021-10-14.6",
  "choice": "C",
  "answer": "Asset protection"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-14.6-D",
  "QuestionId": "CDS.2021-10-14.6",
  "choice": "D",
  "answer": "Due care"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.1-A",
  "QuestionId": "CDS.2021-10-19.1",
  "choice": "A",
  "answer": "Threat coupled with a breach"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.1-B",
  "QuestionId": "CDS.2021-10-19.1",
  "choice": "B",
  "answer": "Threat coupled with a vulnerability"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.1-C",
  "QuestionId": "CDS.2021-10-19.1",
  "choice": "C",
  "answer": "Vulnerability coupled with an attack"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.1-D",
  "QuestionId": "CDS.2021-10-19.1",
  "choice": "D",
  "answer": "Threat coupled with a breach of security"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.2-A",
  "QuestionId": "CDS.2021-10-19.2",
  "choice": "A",
  "answer": "Ease of implementation; it can be completed by personnel with a limited understanding of the risk assessment process"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.2-B",
  "QuestionId": "CDS.2021-10-19.2",
  "choice": "B",
  "answer": "Can be completed by personnel with a limited understanding of the risk assessment process and uses detailed metrics used for calculating risk"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.2-C",
  "QuestionId": "CDS.2021-10-19.2",
  "choice": "C",
  "answer": "Detailed metrics used for calculating risk and ease of implementation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.2-D",
  "QuestionId": "CDS.2021-10-19.2",
  "choice": "D",
  "answer": "Can be completed by personnel with a limited understanding of the risk-assessment process and detailed metrics used for calculating risk"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.3-A",
  "QuestionId": "CDS.2021-10-19.3",
  "choice": "A",
  "answer": "Asset value and ARO"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.3-B",
  "QuestionId": "CDS.2021-10-19.3",
  "choice": "B",
  "answer": "Asset value, LAFE, and SAFE"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.3-C",
  "QuestionId": "CDS.2021-10-19.3",
  "choice": "C",
  "answer": "Asset value and exposure factor"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.3-D",
  "QuestionId": "CDS.2021-10-19.3",
  "choice": "D",
  "answer": "LAFE and ARO"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.4-A",
  "QuestionId": "CDS.2021-10-19.4",
  "choice": "A",
  "answer": "Identification of incident and evidence, analysis, collection, examination, and presentation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.4-B",
  "QuestionId": "CDS.2021-10-19.4",
  "choice": "B",
  "answer": "Identification of incident and evidence, examination, collection, analysis, and presentation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.4-C",
  "QuestionId": "CDS.2021-10-19.4",
  "choice": "C",
  "answer": "Identification of incident and evidence, collection, examination, analysis, and presentation"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-19.4-D",
  "QuestionId": "CDS.2021-10-19.4",
  "choice": "D",
  "answer": "Identification of incident and evidence, collection, analysis, examination, and presentation"
 }
]