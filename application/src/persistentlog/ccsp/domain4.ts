export const Log = [
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-05.1",
  "question": "What is REST?",
  "answer": "B",
  "explanation": [
   {
    "value": "REST: A software architecture style consisting of guidelines and best practices for creating scalable web services6"
   },
   {
    "value": "SOAP: A protocol specification for exchanging structured information in the implementation of web services in computer networks7"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-05.2",
  "question": "What are the phases of a software development lifecycle process model?",
  "answer": "A",
  "explanation": [
   {
    "value": "Planning and requirements analysis: Business and security requirements and standards are being determined. This phase is the main focus of the project managers and stakeholders. Meetings with managers, stakeholders, and users are held to determine requirements. The software development lifecycle calls for all business requirements (functional and nonfunctional) to be defined even before initial design begins. Planning for the quality assurance requirements and identification of the risks associated with the project is also done in the planning stage. The requirements are then analyzed for their validity and the possibility of incorporating them into the system to be developed."
   },
   {
    "value": "Defining: This phase is meant to clearly define and document the product requirements to place them in front of the customer and get them approved. This is done through a requirement specification document, which consists of all the product requirements to be designed and developed during the project lifecycle."
   },
   {
    "value": "Designing: This phase helps in specifying hardware and system requirements and overall system architecture. The system design specifications serve as input for the next phase of the model. Threat modeling and secure design elements should be discussed here."
   },
   {
    "value": "Developing: Upon receiving the system design documents, work is divided in modules or units and actual coding is started. This is typically the longest phase of the software development lifecycle. Activities include code review, unit testing, and static analysis."
   },
   {
    "value": "Testing: After the code is developed, it is tested against the requirements to make sure that the product is actually solving the needs gathered during the requirements phase. During this phase, unit testing, integration testing, system testing, and acceptance testing are accomplished."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-06.1",
  "question": "When does an XSS flaw occur?",
  "answer": "B\nB",
  "explanation": "XSS flaws occur whenever an application takes untrusted data and sends it to a web browser without proper validation or escaping. XSS allows attackers to execute scripts in the victim’s browser, which can hijack user sessions, deface websites, or redirect the user to malicious sites."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-06.2",
  "question": "What are the six components that make up the STRIDE threat model?",
  "answer": "A",
  "explanation": [
   {
    "value": "Spoofing: Attacker assumes identity of subject"
   },
   {
    "value": "Tampering: Attacker alters data or messages"
   },
   {
    "value": "Repudiation: Illegitimate denial of an event"
   },
   {
    "value": "Information disclosure: Information is obtained without authorization"
   },
   {
    "value": "Denial of service: Attacker overloads system to deny legitimate access"
   },
   {
    "value": "Elevation of privilege: Attacker gains a privilege level above what is permitted"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-06.3",
  "question": "In a federated environment, who is the relying party, and what does it do?",
  "answer": "C",
  "explanation": "In a federated environment, there is an identity provider and a relying party. The identity provider holds all the identities and generates a token for known users. The relying party is the service provider and consumes these tokens."
 },
 {
  "type": "QUESTION",
  "id": "CDS.2021-10-06.1",
  "question": "What are the five steps used to create an ASMP?",
  "answer": "D",
  "explanation": "ISO/IEC 27034-1 defines an ASMP to manage and maintain each ANF. The ASMP is created in five steps: Specifying the application requirements and environment Assessing application security risks Creating and maintaining the ANF Provisioning and operating the application Auditing the security of the application"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.1-A",
  "QuestionId": "CDS.2021-10-05.1",
  "choice": "A",
  "answer": "A protocol specification for exchanging structured information in the implementation of web services in computer networks"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.1-B",
  "QuestionId": "CDS.2021-10-05.1",
  "choice": "B",
  "answer": "A software architecture style consisting of guidelines and best practices for creating scalable web services"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.1-C",
  "QuestionId": "CDS.2021-10-05.1",
  "choice": "C",
  "answer": "The name of the process that an organization or person who moves data between CSPs uses to document what he is doing"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.1-D",
  "QuestionId": "CDS.2021-10-05.1",
  "choice": "D",
  "answer": "The intermediary process that provides business continuity of cloud services between cloud consumers and CSPs"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.2-A",
  "QuestionId": "CDS.2021-10-05.2",
  "choice": "A",
  "answer": "Planning and requirements analysis, defining, designing, developing, testing, and maintenance"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.2-B",
  "QuestionId": "CDS.2021-10-05.2",
  "choice": "B",
  "answer": "Defining, planning and requirements analysis, designing, developing, testing, and maintenance"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.2-C",
  "QuestionId": "CDS.2021-10-05.2",
  "choice": "C",
  "answer": "Planning and requirements analysis, defining, designing, testing, developing, and maintenance"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-05.2-D",
  "QuestionId": "CDS.2021-10-05.2",
  "choice": "D",
  "answer": "Planning and requirements analysis, designing, defining, developing, testing, and maintenance"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-A",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "A",
  "answer": "Whenever an application takes trusted data and sends it to a web browser without proper validation or escaping"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-B",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "B",
  "answer": "Whenever an application takes untrusted data and sends it to a web browser without proper validation or escaping"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-C",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "C",
  "answer": "Whenever an application takes trusted data and sends it to a web browser with proper validation or escaping"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-D",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "D",
  "answer": "Whenever an application takes untrusted data and sends it to a web browser with proper validation or escaping"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.2-A",
  "QuestionId": "CDS.2021-10-06.2",
  "choice": "A",
  "answer": "Spoofing, tampering, repudiation, information disclosure, DoS, and elevation of privilege"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.2-B",
  "QuestionId": "CDS.2021-10-06.2",
  "choice": "B",
  "answer": "Spoofing, tampering, nonrepudiation, information disclosure, DoS, and elevation of privilege"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.2-C",
  "QuestionId": "CDS.2021-10-06.2",
  "choice": "C",
  "answer": "Spoofing, tampering, repudiation, information disclosure, DDoS, and elevation of privilege"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.2-D",
  "QuestionId": "CDS.2021-10-06.2",
  "choice": "D",
  "answer": "Spoofing, tampering, repudiation, information disclosure, DoS, and social engineering"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.3-A",
  "QuestionId": "CDS.2021-10-06.3",
  "choice": "A",
  "answer": "The relying party is the identity provider; it consumes the tokens that the service provider generates."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.3-B",
  "QuestionId": "CDS.2021-10-06.3",
  "choice": "B",
  "answer": "The relying party is the service provider; it consumes the tokens that the customer generates."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.3-C",
  "QuestionId": "CDS.2021-10-06.3",
  "choice": "C",
  "answer": "The relying party is the service provider; it consumes the tokens that the identity provider generates."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.3-D",
  "QuestionId": "CDS.2021-10-06.3",
  "choice": "D",
  "answer": "The relying party is the customer; he consumes the tokens that the identity provider generates."
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-A",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "A",
  "answer": "Specifying the application requirements and environment, creating and maintaining the ANF, assessing application security risks, provisioning and operating the application, and auditing the security of the application"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-B",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "B",
  "answer": "Assessing application security risks, specifying the application requirements and environment, creating and maintaining the ANF, provisioning and operating the application, and auditing the security of the application"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-C",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "C",
  "answer": "Specifying the application requirements and environment, assessing application security risks, provisioning and operating the application, auditing the security of the application, and creating and maintaining the ANF"
 },
 {
  "type": "ANSWER",
  "id": "CDS.2021-10-06.1-D",
  "QuestionId": "CDS.2021-10-06.1",
  "choice": "D",
  "answer": "Specifying the application requirements and environment, assessing application security risks, creating and maintaining the ANF, provisioning and operating the application, and auditing the security of the application"
 }
]