export const Log = [
 {
  "type": "QUESTION",
  "id": "CDA.071bbe65-f413-44fd-b22f-823731870c9b",
  "question": "Which of the following are attributes of cloud computing?",
  "answer": "A",
  "explanation": [
   {
    "value": "NIST definition of Cloud Computing"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321",
  "question": "Which of the following are distinguishing characteristics of a managed service provider?",
  "answer": "D",
  "explanation": "Explanation: According to the MSP Alliance, typically MSPs have the following distinguishing characteristics: Have some form of NOC service Have some form of help desk service Can remotely monitor and manage all or a majority of the objects for the customer Can proactively maintain the objects under management for the customer Can deliver these solutions with some form of predictable billing model, where the customer knows with great accuracy what her regular IT management expense will be"
 },
 {
  "type": "QUESTION",
  "id": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907",
  "question": "Which of the following are cloud computing roles",
  "answer": "B",
  "explanation": "Other answer involve a non cloud computing role entity. The following groups form the key roles and functions associated with cloud computing. They do not constitute an exhaustive list but highlight the main roles and functions within cloud computing: Cloud customer: An individual or entity that utilizes or subscribes to cloud-based services or resources. CSP: A company that provides cloud-based platform, infrastructure, application, or storage services to other organizations or individuals, usually for a fee; otherwise known to clients “as a service.” Cloud backup service provider: A third-party entity that manages and holds operational responsibilities for cloud-based data backup services and solutions to customers from a central data center. CSB: Typically a third-party entity or company that looks to extend or enhance value to multiple customers of cloud-based services through relationships with multiple CSPs. It acts as a liaison between cloud services customers and CSPs, selecting the best provider for each customer and monitoring the services. The CSB can be utilized as a “middleman” to broker the best deal and customize services to the customer’s requirements. May also resell cloud services. Cloud service auditor: Third-party organization that verifies attainment of SLAs."
 },
 {
  "type": "QUESTION",
  "id": "CDA.68305716-19f8-420b-96de-45849d0b45b5",
  "question": "Which of the following are essential characteristics of cloud computing? (Choose two.)",
  "answer": "A,D",
  "explanation": "Explanation: According to “The NIST Definition of Cloud Computing,” the essential characteristics of cloud computing are as follows: On-demand self-service: A consumer can unilaterally provision computing capabilities, such as server time and network storage, as needed automatically without requiring human interaction with each service provider. Broad network access: Capabilities are available over the network and accessed through standard mechanisms that promote use by heterogeneous thin or thick client platforms (such as mobile phones, tablets, laptops, and workstations). Resource pooling: The provider’s computing resources are pooled to serve multiple consumers using a multitenant model, with different physical and virtual resources dynamically assigned and reassigned according to consumer demand. There is a sense of location independence in that the customer generally has no control or knowledge over the exact location of the provided resources but may be able to specify location at a higher level of abstraction (such as country, state, or data center). Examples of resources include storage, processing, memory, and network bandwidth. Rapid elasticity: Capabilities can be elastically provisioned and released, in some cases automatically, to scale rapidly outward and inward commensurate with demand. To the consumer, the capabilities available for provisioning often appear to be unlimited and can be appropriated in any quantity at any time. Measured service: Cloud systems automatically control and optimize resource use by leveraging a metering capability at some level of abstraction appropriate to the type of service (such as storage, processing, bandwidth, and active user accounts). Resource usage can be monitored, controlled, and reported, providing transparency for both the provider and the consumer of the utilized service."
 },
 {
  "type": "QUESTION",
  "id": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d",
  "question": "Which of the following are considered to be the building blocks of cloud computing?",
  "answer": "A",
  "explanation": "The building blocks of cloud computing are composed of RAM, CPU, storage, and networking."
 },
 {
  "type": "QUESTION",
  "id": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8",
  "question": "When using an IaaS solution, what is the capability provided to the customer?",
  "answer": "D",
  "explanation": "Explanation: According to “The NIST Definition of Cloud Computing,” in IaaS, “the capability provided to the consumer is to provision processing, storage, networks, and other fundamental computing resources where the consumer is able to deploy and run arbitrary software, which can include operating systems and applications. The consumer does not manage or control the underlying cloud infrastructure but has control over operating systems, storage, and deployed applications; and possibly limited control of select networking components (e.g., host firewalls).”2"
 },
 {
  "type": "QUESTION",
  "id": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc",
  "question": "When using an IaaS solution, what is a key benefit provided to the customer?",
  "answer": "A",
  "explanation": [
   {
    "value": "Usage is metered and priced on the basis of units (or instances) consumed. This can also be billed back to specific departments or functions."
   },
   {
    "value": "It has an ability to scale up and down infrastructure services based on actual usage. This is particularly useful and beneficial where there are significant spikes and dips within the usage curve for infrastructure. It has a reduced cost of ownership."
   },
   {
    "value": "There is no need to buy assets for everyday use, no loss of asset value over time, and reduced costs of maintenance and support. It has a reduced energy and cooling costs along with “green IT” environment effect with optimum use of IT resources and systems."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c",
  "question": "When using a PaaS solution, what is the capability provided to the customer?",
  "answer": "C",
  "explanation": "According to “The NIST Definition of Cloud Computing,” in PaaS, “the capability provided to the consumer is to deploy onto the cloud infrastructure consumer-created or acquired applications created using programming languages, libraries, services, and tools supported by the provider. The consumer does not manage or control the underlying cloud infrastructure including network, servers, operating systems, or storage, but has control over the deployed applications and possibly configuration settings for the application-hosting environment.”3"
 },
 {
  "type": "QUESTION",
  "id": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2",
  "question": "What is a key capability or characteristic of PaaS?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5",
  "question": "When using a SaaS solution, what is the capability provided to the customer?",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107",
  "question": "What are the four cloud deployment models?",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287",
  "question": "What are the six stages of the cloud secure data lifecycle?",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.2021-09-16-1",
  "question": "What are SOC 1/SOC 2/SOC 3?",
  "answer": "C",
  "explanation": "An SOC 1 is a report on controls at a service organization that may be relevant to a user entity’s internal control over financial reporting. An SOC 2 report is based on the existing SysTrust and WebTrust principles. The purpose of an SOC 2 report is to evaluate an organization’s information systems relevant to security, availability, processing integrity, confidentiality, or privacy. An SOC 3 report is also based on the existing SysTrust and WebTrust principles, like a SOC 2 report. The difference is that the SOC 3 report does not detail the testing performed."
 },
 {
  "type": "QUESTION",
  "id": "CDA.2021-09-16-2",
  "question": "What are the five Trust Services principles?",
  "answer": "A",
  "explanation": [
   {
    "value": "Security: The system is protected against unauthorized access, both physical and logical."
   },
   {
    "value": "Availability: The system is available for operation and use as committed or agreed."
   },
   {
    "value": "Processing Integrity: System processing is complete, accurate, timely, and authorized."
   },
   {
    "value": "Confidentiality: Information designated as confidential is protected as committed or agreed."
   },
   {
    "value": "Privacy: Personal information is collected, used, retained, disclosed, and disposed of in conformity with the provider’s privacy policy."
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDA.2021-09-16-3",
  "question": "What is a security concern for a PaaS Solution?",
  "answer": "D",
  "explanation": [
   {
    "value": "System and Resource Isolation"
   },
   {
    "value": "User Level Permissions"
   },
   {
    "value": "User Access Management"
   },
   {
    "value": "Protection Again Malware"
   }
  ]
 },
 {
  "type": "QUESTION",
  "id": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac",
  "question": "There are 4 identified cloud deployment models: private, public, hybrid and ...",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b",
  "question": "Which cloud deployment model makes sense when the cloud infrastructure is provisioned for exclusive use by a specific community of consumers from organizations that have shared concerns (e.g., mission, security requirements, policy, and compliance considerations).",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e",
  "question": "What is it called when a cloud broker has the option to combine services which are not fixed.",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.6eeb9b6a-4fd7-4a63-b3cf-7bd8ec7b44a2",
  "question": "What is the data portability facet for transferring data from a source system to a target system using formats that can be decoded on the target system",
  "answer": "B",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.d66b95ea-8057-41c6-995d-95fd09976c5b",
  "question": "What is the name of the data portability facet for transferring data from a source system to a target system so that the data model is understood within the context of the subject area by the target  ",
  "answer": "A",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98",
  "question": "What is the cloud interoperablity data facet : The commonality of the communication between cloud consumer and provider and other providers (e.g., HTTP/S and various message queuing standards) ",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6",
  "question": "What is the cloud interoperablity data facet : Situation where the results of the use of the exchanged information match the expected outcome ",
  "answer": "D",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "question": "An organization is consuming cloud services and data from the same logical demarcation as another company that is its competitor and a peer in the same market. Which model describes this situation? (L2.3)",
  "answer": "A",
  "explanation": "The community cloud infrastructure is provisioned for exclusive use by a specific community of consumers from organizations that have shared concerns (e.g., mission, security requirements, policy, and compliance considerations)."
 },
 {
  "type": "QUESTION",
  "id": "CDA.2021-09-27.1",
  "question": "It is always good to develop technical solutions right after understanding business requirements. ",
  "answer": "False",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "CDA.30c9ac39-77f7-49d7-ab8a-fa4246bcd2cf",
  "question": "A mutable system management scheme would mean patching a virtual machine rather than launching a new replacement. (L2.2)",
  "answer": "True",
  "explanation": "An immutable system would mean launching a new virtual machine image rather than patching an existing one."
 },
 {
  "type": "ANSWER",
  "id": "CDA.071bbe65-f413-44fd-b22f-823731870c9b-A",
  "QuestionId": "CDA.071bbe65-f413-44fd-b22f-823731870c9b",
  "choice": "A",
  "answer": "Minimal management effort and shared resources"
 },
 {
  "type": "ANSWER",
  "id": "CDA.071bbe65-f413-44fd-b22f-823731870c9b-B",
  "QuestionId": "CDA.071bbe65-f413-44fd-b22f-823731870c9b",
  "choice": "B",
  "answer": "High cost and unique resources"
 },
 {
  "type": "ANSWER",
  "id": "CDA.071bbe65-f413-44fd-b22f-823731870c9b-C",
  "QuestionId": "CDA.071bbe65-f413-44fd-b22f-823731870c9b",
  "choice": "C",
  "answer": "Rapid provisioning and slow release of resources"
 },
 {
  "type": "ANSWER",
  "id": "CDA.071bbe65-f413-44fd-b22f-823731870c9b-D",
  "QuestionId": "CDA.071bbe65-f413-44fd-b22f-823731870c9b",
  "choice": "D",
  "answer": "Limited access and service provider interation"
 },
 {
  "type": "ANSWER",
  "id": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321-A",
  "QuestionId": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321",
  "choice": "A",
  "answer": "Have some form of NOC but and a help desk"
 },
 {
  "type": "ANSWER",
  "id": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321-B",
  "QuestionId": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321",
  "choice": "B",
  "answer": "Be able to remotely monitor and manage objects for the customer and but hand off to other provider to maintain these objects under management."
 },
 {
  "type": "ANSWER",
  "id": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321-C",
  "QuestionId": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321",
  "choice": "C",
  "answer": "Have some form of a help desk but no NOC."
 },
 {
  "type": "ANSWER",
  "id": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321-D",
  "QuestionId": "CDA.8459fc27-bd37-42e1-ace0-f8657a16c321",
  "choice": "D",
  "answer": "Be able to remotely monitor and manage objects for the customer and proactively maintain these objects under management."
 },
 {
  "type": "ANSWER",
  "id": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907-A",
  "QuestionId": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907",
  "choice": "A",
  "answer": "Cloud customer and financial auditor"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907-B",
  "QuestionId": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907",
  "choice": "B",
  "answer": "CSP and backup service provider"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907-C",
  "QuestionId": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907",
  "choice": "C",
  "answer": "Cloud service broker and user"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907-D",
  "QuestionId": "CDA.dd5f82e7-b3d4-438d-9719-38ee63796907",
  "choice": "D",
  "answer": "Cloud service auditor and object"
 },
 {
  "type": "ANSWER",
  "id": "CDA.68305716-19f8-420b-96de-45849d0b45b5-A",
  "QuestionId": "CDA.68305716-19f8-420b-96de-45849d0b45b5",
  "choice": "A",
  "answer": "on demand self service"
 },
 {
  "type": "ANSWER",
  "id": "CDA.68305716-19f8-420b-96de-45849d0b45b5-B",
  "QuestionId": "CDA.68305716-19f8-420b-96de-45849d0b45b5",
  "choice": "B",
  "answer": "Unmeasured service"
 },
 {
  "type": "ANSWER",
  "id": "CDA.68305716-19f8-420b-96de-45849d0b45b5-C",
  "QuestionId": "CDA.68305716-19f8-420b-96de-45849d0b45b5",
  "choice": "C",
  "answer": "resource isolation"
 },
 {
  "type": "ANSWER",
  "id": "CDA.68305716-19f8-420b-96de-45849d0b45b5-D",
  "QuestionId": "CDA.68305716-19f8-420b-96de-45849d0b45b5",
  "choice": "D",
  "answer": "broad network access"
 },
 {
  "type": "ANSWER",
  "id": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d-A",
  "QuestionId": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d",
  "choice": "A",
  "answer": "Data, access control, virtualization, and services"
 },
 {
  "type": "ANSWER",
  "id": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d-B",
  "QuestionId": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d",
  "choice": "B",
  "answer": "Storage, networking, printing, and virtualization"
 },
 {
  "type": "ANSWER",
  "id": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d-C",
  "QuestionId": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d",
  "choice": "C",
  "answer": "CPU, RAM, storage, and networking"
 },
 {
  "type": "ANSWER",
  "id": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d-D",
  "QuestionId": "CDA.49e27678-2a2a-46b8-9a17-86a75001349d",
  "choice": "D",
  "answer": "Data, CPU, RAM, and access control"
 },
 {
  "type": "ANSWER",
  "id": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8-A",
  "QuestionId": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8",
  "choice": "A",
  "answer": "To provision processing, storage, networks, and other fundamental computing resources where the consumer is not able to deploy and run arbitrary software, which can include OSs and applications"
 },
 {
  "type": "ANSWER",
  "id": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8-B",
  "QuestionId": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8",
  "choice": "B",
  "answer": "To provision processing, storage, networks, and other fundamental computing resources where the provider is able to deploy and run arbitrary software, which can include OSs and applications"
 },
 {
  "type": "ANSWER",
  "id": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8-C",
  "QuestionId": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8",
  "choice": "C",
  "answer": "To provision processing, storage, networks, and other fundamental computing resources where the auditor is able to deploy and run arbitrary software, which can include OSs and applications"
 },
 {
  "type": "ANSWER",
  "id": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8-D",
  "QuestionId": "CDA.e79475eb-cc21-43c7-8dc6-9c8cc8435aa8",
  "choice": "D",
  "answer": "To provision processing, storage, networks, and other fundamental computing resources where the consumer is able to deploy and run arbitrary software, which can include OSs and applications"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc-A",
  "QuestionId": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc",
  "choice": "A",
  "answer": "Metered and priced usage on the basis of units consumed"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc-B",
  "QuestionId": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc",
  "choice": "B",
  "answer": "The ability to scale up infrastructure services based on projected usage"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc-C",
  "QuestionId": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc",
  "choice": "C",
  "answer": "Increased energy and cooling system efficiencies"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc-D",
  "QuestionId": "CDA.c9f4cd9d-8afe-411f-86b1-127edfb6c2fc",
  "choice": "D",
  "answer": "Transferred cost of ownership"
 },
 {
  "type": "ANSWER",
  "id": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c-A",
  "QuestionId": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c",
  "choice": "A",
  "answer": "To deploy onto the cloud infrastructure provider-created or acquired applications created using programming languages, libraries, services, and tools that the provider supports. The consumer does manage or control the underlying cloud infrastructure, including network, servers, operating systems, or storage, but has control over the deployed applications and possibly configuration settings for the application-hosting environment."
 },
 {
  "type": "ANSWER",
  "id": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c-B",
  "QuestionId": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c",
  "choice": "B",
  "answer": "To deploy onto the cloud infrastructure consumer-created or acquired applications created using programming languages, libraries, services, and tools that the provider supports. The provider does not manage or control the underlying cloud infrastructure, including network, servers, operating systems, or storage, but has control over the deployed applications and possibly configuration settings for the application-hosting environment."
 },
 {
  "type": "ANSWER",
  "id": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c-C",
  "QuestionId": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c",
  "choice": "C",
  "answer": "To deploy onto the cloud infrastructure consumer-created or acquired applications created using programming languages, libraries, services, and tools that the provider supports. The consumer does not manage or control the underlying cloud infrastructure, including network, servers, operating systems, or storage, but has control over the deployed applications and possibly configuration settings for the application-hosting environment."
 },
 {
  "type": "ANSWER",
  "id": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c-D",
  "QuestionId": "CDA.3d961cf4-fd93-42cb-85c1-4cf2e4d2959c",
  "choice": "D",
  "answer": "To deploy onto the cloud infrastructure consumer-created or acquired applications created using programming languages, libraries, services, and tools that the consumer supports. The consumer does manages or controls the underlying cloud infrastructure, including network, servers, operating systems, or storage."
 },
 {
  "type": "ANSWER",
  "id": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2-A",
  "QuestionId": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2",
  "choice": "A",
  "answer": "Support for a homogenous hosting environment"
 },
 {
  "type": "ANSWER",
  "id": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2-B",
  "QuestionId": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2",
  "choice": "B",
  "answer": "Ability to reduce lock-in"
 },
 {
  "type": "ANSWER",
  "id": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2-C",
  "QuestionId": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2",
  "choice": "C",
  "answer": "Support for a single programming language"
 },
 {
  "type": "ANSWER",
  "id": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2-D",
  "QuestionId": "CDA.15f31a58-b313-42f7-b528-ab6dd01127f2",
  "choice": "D",
  "answer": "Ability to manually scale"
 },
 {
  "type": "ANSWER",
  "id": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5-A",
  "QuestionId": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5",
  "choice": "A",
  "answer": "To use the provider’s applications running on a cloud infrastructure. The applications are accessible from various client devices through either a thin client interface, such as a web browser (for example, web-based email), or a program interface. The consumer does not manage or control the underlying cloud infrastructure, including network, servers, operating systems, storage, or even individual application capabilities, with the possible exception of limited user-specific application configuration settings."
 },
 {
  "type": "ANSWER",
  "id": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5-B",
  "QuestionId": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5",
  "choice": "B",
  "answer": "To use the provider’s applications running on a cloud infrastructure. The applications are accessible from various client devices through either a thin client interface, such as a web browser (for example, web-based email), or a program interface. The consumer does manage or control the underlying cloud infrastructure, including network, servers, operating systems, storage, or even individual application capabilities, with the possible exception of limited user-specific application configuration settings."
 },
 {
  "type": "ANSWER",
  "id": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5-C",
  "QuestionId": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5",
  "choice": "C",
  "answer": "To use the consumer’s applications running on a cloud infrastructure. The applications are accessible from various client devices through either a thin client interface, such as a web browser (for example, web-based email), or a program interface. The consumer does not manage or control the underlying cloud infrastructure, including network, servers, operating systems, storage, or even individual application capabilities, with the possible exception of limited user-specific application configuration settings."
 },
 {
  "type": "ANSWER",
  "id": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5-D",
  "QuestionId": "CDA.4566530d-3b0d-463b-bcac-51bce69345f5",
  "choice": "D",
  "answer": "To use the consumer’s applications running on a cloud infrastructure. The applications are accessible from various client devices through either a thin client interface, such as a web browser (for example, web-based email), or a program interface. The consumer does manage or control the underlying cloud infrastructure, including network, servers, operating systems, storage, or even individual application capabilities, with the possible exception of limited user-specific application configuration settings."
 },
 {
  "type": "ANSWER",
  "id": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107-A",
  "QuestionId": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107",
  "choice": "A",
  "answer": "Public, internal, hybrid, and community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107-B",
  "QuestionId": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107",
  "choice": "B",
  "answer": "External, private, hybrid, and community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107-C",
  "QuestionId": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107",
  "choice": "C",
  "answer": "Public, private, joint, and community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107-D",
  "QuestionId": "CDA.8a9c3e62-93f7-4738-a699-9e0fc59d3107",
  "choice": "D",
  "answer": "Public, private, hybrid, and community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287-A",
  "QuestionId": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287",
  "choice": "A",
  "answer": "Create, use, store, share, archive, and destroy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287-B",
  "QuestionId": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287",
  "choice": "B",
  "answer": "Create, store, use, share, archive, and destroy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287-C",
  "QuestionId": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287",
  "choice": "C",
  "answer": "Create, share, store, archive, use, and destroy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287-D",
  "QuestionId": "CDA.bb7d1269-f073-49b8-bbbf-4007d3a5b287",
  "choice": "D",
  "answer": "Create, archive, use, share, store, and destroy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-1-A",
  "QuestionId": "CDA.2021-09-16-1",
  "choice": "A",
  "answer": "Risk Management Frameworks"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-1-B",
  "QuestionId": "CDA.2021-09-16-1",
  "choice": "B",
  "answer": "Access Control"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-1-C",
  "QuestionId": "CDA.2021-09-16-1",
  "choice": "C",
  "answer": "Audit"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-2-A",
  "QuestionId": "CDA.2021-09-16-2",
  "choice": "A",
  "answer": "Security, Availability, Processing Integrity, Confidentiality and Privacy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-2-B",
  "QuestionId": "CDA.2021-09-16-2",
  "choice": "B",
  "answer": "Security, Auditability, Processing Integrity, Confidentiality and Privacy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-2-C",
  "QuestionId": "CDA.2021-09-16-2",
  "choice": "C",
  "answer": "Security, Auditability, Customer Integrity, Confidentiality and Privacy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-2-D",
  "QuestionId": "CDA.2021-09-16-2",
  "choice": "D",
  "answer": "Security, Availability, Processing Integrity, Confidentiality and NonRepudiation"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-3-A",
  "QuestionId": "CDA.2021-09-16-3",
  "choice": "A",
  "answer": "Virtual Machine Attacks"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-3-B",
  "QuestionId": "CDA.2021-09-16-3",
  "choice": "B",
  "answer": "Web Application Security"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-3-C",
  "QuestionId": "CDA.2021-09-16-3",
  "choice": "C",
  "answer": "Data Access and Policies"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-16-3-D",
  "QuestionId": "CDA.2021-09-16-3",
  "choice": "D",
  "answer": "System and Resouce Isolation"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac-A",
  "QuestionId": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac",
  "choice": "A",
  "answer": "Community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac-B",
  "QuestionId": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac",
  "choice": "B",
  "answer": "Infrastructure"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac-C",
  "QuestionId": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac",
  "choice": "C",
  "answer": "Platform"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac-D",
  "QuestionId": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac",
  "choice": "D",
  "answer": "On-Demand"
 },
 {
  "type": "ANSWER",
  "id": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac-E",
  "QuestionId": "CDA.c1935777-520a-4be7-b3cc-f87914d3a6ac",
  "choice": "E",
  "answer": ""
 },
 {
  "type": "ANSWER",
  "id": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b-A",
  "QuestionId": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b",
  "choice": "A",
  "answer": "Hybrid"
 },
 {
  "type": "ANSWER",
  "id": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b-B",
  "QuestionId": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b",
  "choice": "B",
  "answer": "Community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b-C",
  "QuestionId": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b",
  "choice": "C",
  "answer": "Private"
 },
 {
  "type": "ANSWER",
  "id": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b-D",
  "QuestionId": "CDA.1fe717b5-d92f-4ec2-97d1-4445cd8bba8b",
  "choice": "D",
  "answer": "Public"
 },
 {
  "type": "ANSWER",
  "id": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e-A",
  "QuestionId": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e",
  "choice": "A",
  "answer": "Portability"
 },
 {
  "type": "ANSWER",
  "id": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e-B",
  "QuestionId": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e",
  "choice": "B",
  "answer": "Interoperability"
 },
 {
  "type": "ANSWER",
  "id": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e-C",
  "QuestionId": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e",
  "choice": "C",
  "answer": "Service Aggregation"
 },
 {
  "type": "ANSWER",
  "id": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e-D",
  "QuestionId": "CDA.42ad4b59-b87f-483d-adab-1240ced0bc2e",
  "choice": "D",
  "answer": "Arbitrage"
 },
 {
  "type": "ANSWER",
  "id": "CDA.6eeb9b6a-4fd7-4a63-b3cf-7bd8ec7b44a2-A",
  "QuestionId": "CDA.6eeb9b6a-4fd7-4a63-b3cf-7bd8ec7b44a2",
  "choice": "A",
  "answer": "Semantic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.6eeb9b6a-4fd7-4a63-b3cf-7bd8ec7b44a2-B",
  "QuestionId": "CDA.6eeb9b6a-4fd7-4a63-b3cf-7bd8ec7b44a2",
  "choice": "B",
  "answer": "Syntactic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.6eeb9b6a-4fd7-4a63-b3cf-7bd8ec7b44a2-C",
  "QuestionId": "CDA.6eeb9b6a-4fd7-4a63-b3cf-7bd8ec7b44a2",
  "choice": "C",
  "answer": "Policy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d66b95ea-8057-41c6-995d-95fd09976c5b-A",
  "QuestionId": "CDA.d66b95ea-8057-41c6-995d-95fd09976c5b",
  "choice": "A",
  "answer": "Semantic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d66b95ea-8057-41c6-995d-95fd09976c5b-B",
  "QuestionId": "CDA.d66b95ea-8057-41c6-995d-95fd09976c5b",
  "choice": "B",
  "answer": "Syntactic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d66b95ea-8057-41c6-995d-95fd09976c5b-C",
  "QuestionId": "CDA.d66b95ea-8057-41c6-995d-95fd09976c5b",
  "choice": "C",
  "answer": "Policy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98-A",
  "QuestionId": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98",
  "choice": "A",
  "answer": "Policy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98-B",
  "QuestionId": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98",
  "choice": "B",
  "answer": "Syntactic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98-C",
  "QuestionId": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98",
  "choice": "C",
  "answer": "Semantic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98-D",
  "QuestionId": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98",
  "choice": "D",
  "answer": "Transport"
 },
 {
  "type": "ANSWER",
  "id": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98-E",
  "QuestionId": "CDA.d4d06baf-039d-42d2-bbaf-e9a057ef0a98",
  "choice": "E",
  "answer": "Behavioral"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6-A",
  "QuestionId": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6",
  "choice": "A",
  "answer": "Policy"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6-B",
  "QuestionId": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6",
  "choice": "B",
  "answer": "Syntactic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6-C",
  "QuestionId": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6",
  "choice": "C",
  "answer": "Semantic"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6-D",
  "QuestionId": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6",
  "choice": "D",
  "answer": "Transport"
 },
 {
  "type": "ANSWER",
  "id": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6-E",
  "QuestionId": "CDA.dc5623c6-5cc9-446c-a204-aa64ea0115b6",
  "choice": "E",
  "answer": "Behavioral"
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-A",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "A",
  "answer": "Community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-B",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "B",
  "answer": "IaaS"
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-C",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "C",
  "answer": "Public"
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-D",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "D",
  "answer": "PaaS"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-27.1-A",
  "QuestionId": "CDA.2021-09-27.1",
  "choice": "A",
  "answer": "True"
 },
 {
  "type": "ANSWER",
  "id": "CDA.2021-09-27.1-B",
  "QuestionId": "CDA.2021-09-27.1",
  "choice": "B",
  "answer": "False"
 },
 {
  "type": "ANSWER",
  "id": "CDA.30c9ac39-77f7-49d7-ab8a-fa4246bcd2cf-A",
  "QuestionId": "CDA.30c9ac39-77f7-49d7-ab8a-fa4246bcd2cf",
  "choice": "A",
  "answer": "True"
 },
 {
  "type": "ANSWER",
  "id": "CDA.30c9ac39-77f7-49d7-ab8a-fa4246bcd2cf-B",
  "QuestionId": "CDA.30c9ac39-77f7-49d7-ab8a-fa4246bcd2cf",
  "choice": "B",
  "answer": "False"
 }
]