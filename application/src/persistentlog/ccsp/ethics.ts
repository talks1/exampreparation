export const Log = [
 {
  "type": "QUESTION",
  "id": "ETH.14c04a21-b094-4309-af29-248671ec2756",
  "question": "Which of the of the following are not one of the ICS2 code of ethics",
  "answer": "B,E",
  "explanation": ""
 },
 {
  "type": "QUESTION",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "question": "Which of the following statements best aligns with ISC2 code of ethics (Choose Four)",
  "answer": "A,C,G,H",
  "explanation": ""
 },
 {
  "type": "ANSWER",
  "id": "ETH.14c04a21-b094-4309-af29-248671ec2756-A",
  "QuestionId": "ETH.14c04a21-b094-4309-af29-248671ec2756",
  "choice": "A",
  "answer": "Protect Society"
 },
 {
  "type": "ANSWER",
  "id": "ETH.14c04a21-b094-4309-af29-248671ec2756-B",
  "QuestionId": "ETH.14c04a21-b094-4309-af29-248671ec2756",
  "choice": "B",
  "answer": "Use methods, tools, responsible"
 },
 {
  "type": "ANSWER",
  "id": "ETH.14c04a21-b094-4309-af29-248671ec2756-C",
  "QuestionId": "ETH.14c04a21-b094-4309-af29-248671ec2756",
  "choice": "C",
  "answer": "Act honorable, honestly,justly, responsible and legally"
 },
 {
  "type": "ANSWER",
  "id": "ETH.14c04a21-b094-4309-af29-248671ec2756-D",
  "QuestionId": "ETH.14c04a21-b094-4309-af29-248671ec2756",
  "choice": "D",
  "answer": "Provide diligent and competent services to principals"
 },
 {
  "type": "ANSWER",
  "id": "ETH.14c04a21-b094-4309-af29-248671ec2756-E",
  "QuestionId": "ETH.14c04a21-b094-4309-af29-248671ec2756",
  "choice": "E",
  "answer": "Avoid competing professional and personal interests"
 },
 {
  "type": "ANSWER",
  "id": "ETH.14c04a21-b094-4309-af29-248671ec2756-F",
  "QuestionId": "ETH.14c04a21-b094-4309-af29-248671ec2756",
  "choice": "F",
  "answer": "Advance and protect the profession"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-A",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "A",
  "answer": "Work to protect society, public trust and infraststructure"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-B",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "B",
  "answer": "Avoid using the internet as a test network, consider the potential outcomes of your actions"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-C",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "C",
  "answer": "be honest, act responsibly and within the confines of the law"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-D",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "D",
  "answer": "Deliver your product ontime , as defined and within the allowed budget"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-E",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "E",
  "answer": "Be decisive, confident and articulate when dealing with principles"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-F",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "F",
  "answer": "Safeguard your system using a complement of administrative technical and physical controls"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-G",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "G",
  "answer": "be competent in what you do and be diligent in the maintenance of that competenence"
 },
 {
  "type": "ANSWER",
  "id": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b-H",
  "QuestionId": "EHT.45f534d7-1276-456a-ac81-fe0f4033bb6b",
  "choice": "H",
  "answer": "Seek, through your actions, to improve the profession"
 }
]