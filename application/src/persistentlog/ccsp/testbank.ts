export const Log = [
 {
  "type": "QUESTION",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "question": "QUESTION",
  "answer": "A",
  "explanation": "The community cloud infrastructure is provisioned for exclusive use by a specific community of consumers from organizations that have shared concerns (e.g., mission, security requirements, policy, and compliance considerations)."
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-A",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "A",
  "answer": "Community"
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-B",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "B",
  "answer": "IaaS"
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-C",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "C",
  "answer": "Public"
 },
 {
  "type": "ANSWER",
  "id": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159-D",
  "QuestionId": "CDA.76ec1bf2-90ec-49c0-a19b-cbf8eee2f159",
  "choice": "D",
  "answer": "PaaS"
 }
]