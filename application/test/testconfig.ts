const inmemoryConfig = {
    database: {    
      DBDIALECT: 'sqlite',
      force: true,
      RESYNC: 'true',
      logging: true,
    },
    seed: false
  }
  
  const localExamConfig = {
    database: {    
      USER: "gpc",
      PASSWORD: 'gpc',
      DBHOST: 'localhost',
      DB: 'gpc',
      DBDIALECT: 'mssql',
      schema: 'exam',
      force: true,
      RESYNC: 'true',
      logging: true,
    },
    seed: false
  }
  
  export const examConfig = inmemoryConfig

