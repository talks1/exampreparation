
import { describe} from 'riteway';
import {ExamService} from '../src/service'
import {Log} from '../src/persistentlog/ccsp'
import Debug from 'debug'
import {examConfig as config} from './testconfig'
import {getModel} from '../src/model/exam'

const debug = Debug('test')

describe.only('Exam', async assert => {
  try {
    let model = await getModel(config.database)
    const modelService = await ExamService(model)

    const log = Log
    debug(log)
    await modelService.seed('ccsp',log)

    const spec = await modelService.queryQuestions('Terminal','CCSP')

    debug(JSON.stringify(spec))

    model.close()
  }catch(ex){
    console.log(ex)
  }

  assert({
    given: 'no arguments',
    should: 'return 0',
    actual: 0,
    expected: 0
  });

})